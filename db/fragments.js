import gql from 'graphql-tag';

const SIMPLE_TERRITORIO_FRAGMENT = gql`
    fragment SimpleTerritorioFragment on TerritorioType {
        id
        nome
        municipioReferenciaId
    }
`;

export const USUARIA_FRAGMENT = gql`
    ${SIMPLE_TERRITORIO_FRAGMENT}
    fragment UsuariaFragment on UsuariaType {
        id
        username
        fullName
        codigoUsoCelular
        cpf
        email
        territorios {
            ...SimpleTerritorioFragment
        }
    }
`;

export const CONFLITO_FRAGMENT = gql`
    fragment ConflitoFragment on ConflitoType {
        id
        nome
        posicao
        descricao
        tipoConflitoId
    }
`;

export const AREA_DE_USO_FRAGMENT = gql`
    fragment AreaDeUsoFragment on AreaDeUsoType {
        id
        nome
        posicao
        descricao
        area
        tipoAreaDeUsoId
    }
`;

export const MENSAGEM_FRAGMENT = gql`
    fragment MensagemFragment on MensagemType {
        id
        texto
        extra
        dataEnvio
        dataRecebimento
        originIsDevice
    }
`;

export const ANEXO_FRAGMENT = gql`
    fragment AnexoFragment on AnexoType {
        id
        nome
        descricao
        mimetype
        fileSize
        arquivo
        tipoAnexo
        criacao
        ultimaAlteracao
        publico
        thumb
        localFileUri @client
        localThumbUri @client
    }
`;

export const TERRITORIO_FRAGMENT = gql`
    ${AREA_DE_USO_FRAGMENT}
    ${CONFLITO_FRAGMENT}
    ${ANEXO_FRAGMENT}
    ${MENSAGEM_FRAGMENT}
    fragment TerritorioFragment on TerritorioType {
        id
        nome
        anoFundacao
        qtdeFamilias
        statusId
        tiposComunidade {
            id
        }
        municipioReferenciaId
        poligono
        publico
        enviadoPtt
        cep
        descricaoAcesso
        descricaoAcessoPrivacidade
        zonaLocalizacao
        outraZonaLocalizacao
        autoIdentificacao
        segmentoId
        historiaDescricao
        ecNomeContato
        ecLogradouro
        ecNumero
        ecComplemento
        ecBairro
        ecCep
        ecCaixaPostal
        ecMunicipioId
        ecEmail
        ecTelefone
        areasDeUso {
            ...AreaDeUsoFragment
        }
        conflitos {
            ...ConflitoFragment
        }
        anexos {
            ...AnexoFragment
        }
        mensagens {
            ...MensagemFragment
        }
    }
`;
