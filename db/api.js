import { InMemoryCache, makeVar } from '@apollo/client/cache';
import { onError } from '@apollo/client/link/error';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ReactNativeFile } from 'apollo-upload-client';
import * as FileSystem from 'expo-file-system';
import * as Sharing from 'expo-sharing';
import Constants from 'expo-constants';
import moment from 'moment';
import uuid from 'react-native-uuid';

import CURRENT_USER from '../bases/current_user';
import {
    GET_ANEXOS,
    GET_AREAS_DE_USO,
    GET_CONFLITOS,
    GET_MENSAGENS,
    GET_SETTINGS,
} from './queries';
import SETTINGS from '../bases/settings';
import { delay, ehTmpId, log, logError, omitDeep } from '../utils/utils';
import { DIR_RELATORIOS, fazAnexoLocalUri, fazAnexoRemoteUri, limpaTodosAnexos } from './arquivos';
import { updateHandlerByName } from '../utils/graphqlUpdateHandlers';
import slugify from 'slugify';

/** @type {String} */
export const DASHBOARD_ENDPOINT = Constants.manifest.extra.dashboardEndpoint;
/** @type {String} */
export const TOKEN_AUTH_ENDPOINT = `${DASHBOARD_ENDPOINT}/api-token-auth/`;
const PEDIR_CODIGO_SEGURANCA_ENDPOINT = `${DASHBOARD_ENDPOINT}/codigo-seguranca/`;
const RELATORIO_ENDPOINT = `${DASHBOARD_ENDPOINT}/territorio/relatorio/`;

/** @type {Object} */
export const LOGIN_ERROR = {
    USER_NOT_FOUND: 'USER_NOT_FOUND',
    NO_USER_AND_NO_PASSWORD: 'NO_USER_AND_NO_PASSWORD',
    NO_PASSWORD: 'NO_PASSWORD',
    MISFORMATTED_USER: 'MISFORMATTED_USER',
    NETWORK: 'NETWORK',
};

/** @type {String} */
let cachedToken;

/**
 * Resgata o token da usuária
 * @method
 * @return {Promise} Token (string)
 */
export const getToken = async () => {
    if (cachedToken) {
        /*DEBUG*/ //console.log("has cachedToken", cachedToken);
        return cachedToken;
    }
    cachedToken = await AsyncStorage.getItem('token');
    return cachedToken;
};

/**
 * Invalida o token, no caso de erro do servidor
 */
export const resetTokenOnError = onError(({ networkError }) => {
    // @ts-ignore
    if (networkError && networkError.name === 'ServerError' && networkError.statusCode === 401) {
        // remove cached token on 401 from the server
        console.log('Token foi invalidado');
        cachedToken = null;
    }
});

export const anexosLocaisVar = makeVar({});

/** @type {InMemoryCache} */
export const tonomapaCache = new InMemoryCache();

/**
 * Desconecta do sistema, apaga arquivos e o cache
 * @method
 */
export const sair = async (client) => {
    deleteCachedToken();
    await AsyncStorage.clear();
    await limpaTodosAnexos();
    await client.clearStore();
};

/**
 * Communicate with the server
 * @method
 * @param username String
 * @param password String
 * @param onSuccess Function Função onSuccess(token)
 * @param onError Function Função onError(errorCode, message)
 */
export const serverLogin = (username, password, onSuccess, onError) => {
    const data = new FormData();
    data.append('username', username);
    data.append('password', password);

    fetch(TOKEN_AUTH_ENDPOINT, {
        method: 'POST',
        body: data,
    })
        .then((res) => {
            res.json().then((res) => {
                /*DEBUG*/ //console.log("login ok!");
                /*DEBUG*/ //console.log(res);
                if (res.token) {
                    onSuccess(res.token);
                } else {
                    let errorCode;
                    if (username === '') {
                        errorCode = LOGIN_ERROR.NO_USER_AND_NO_PASSWORD;
                    } else if (username.length !== 11) {
                        errorCode = LOGIN_ERROR.MISFORMATTED_USER;
                    } else if (password === '') {
                        errorCode = LOGIN_ERROR.NO_PASSWORD;
                    } else {
                        errorCode = LOGIN_ERROR.USER_NOT_FOUND;
                    }
                    console.log(res, errorCode);
                    onError(errorCode);
                }
            });
        })
        .catch((err) => {
            console.log(`login error ${LOGIN_ERROR.NETWORK}: ${err.message}`);
            log(`login error ${LOGIN_ERROR.NETWORK}`);
            logError(err);
            onError(
                LOGIN_ERROR.NETWORK,
                'Não foi possível conectar ao servidor. Favor verificar sua conexão com a internet e tentar novamente.'
            );
        });
};

export const authStateReducer = (prevState, action) => {
    switch (action.type) {
        case 'RESTORE_TOKEN':
            return {
                ...prevState,
                userToken: action.token,
                isLoading: false,
            };
        case 'SIGN_IN':
            return {
                ...prevState,
                isSignout: false,
                userToken: action.token,
                isLoading: false,
            };
        case 'IS_SIGNING_IN':
            return {
                ...prevState,
                isSignout: false,
                userToken: null,
                isLoading: true,
            };
        case 'SIGN_OUT':
            return {
                ...prevState,
                isSignout: true,
                userToken: null,
            };
        case 'SIGN_IN_ERROR':
            return {
                ...prevState,
                userToken: null,
                isLoading: false,
            };
    }
};

export const enviaPedidoCodigoSeguranca = async ({ usuaria, nome, cpf, fotoDocumento }) => {
    const formData = new FormData();
    formData.append('username', usuaria.replace(/[\W_]/g, ''));
    formData.append('cpf', cpf);
    // formData.append('name', nome);
    formData.append('foto_documento', {
        ...fotoDocumento,
    });

    let firstRes;

    try {
        firstRes = await fetch(PEDIR_CODIGO_SEGURANCA_ENDPOINT, {
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'multipart/form-data',
            },
            body: formData,
        });
    } catch (e) {
        console.log('resultError', e);
        const errorMsg = `Falhou a internet!.`;
        log(errorMsg);
        logError(e);
        return { ok: false, msg: errorMsg };
    }

    if (!firstRes.ok) {
        const errorMsg = 'Não foi possível enviar os dados!';
        console.log(errorMsg);
        return { ok: false, msg: errorMsg };
    }

    // Enviado com sucesso
    const response = await firstRes.json();
    return { ok: true, response };
};

/**
 * Define o cache do token
 * @method
 * @param  {String} token
 */
export const setCachedToken = (token) => {
    cachedToken = token;
};

/**
 * Apaga o token do cache
 * @method
 */
export const deleteCachedToken = () => {
    cachedToken = null;
};

export const carregaSettings = (client) => {
    const result = client.readQuery({
        query: GET_SETTINGS,
    });
    if (result) {
        const resultSettings = result.settings;
        const newSettings = {
            ...SETTINGS,
            ...resultSettings,
        };
        return newSettings;
    }
    return { ...SETTINGS };
};

/**
 * Atualiza as configurações locais da usuária no aparelho
 * @method
 * @param  {Object}  newSettings Pode ser o objeto completo SETTINGS ou apenas um dos seus atributos
 * @return {Promise}             Objeto SETTINGS com as configurações atualizadas
 */
export const persisteSettings = async (newSettings, client) => {
    let gravaSettings;
    if (newSettings) {
        if (!('settings' in newSettings)) {
            const settings = carregaSettings(client);
            gravaSettings = {
                ...settings,
                ...newSettings,
            };
        } else {
            gravaSettings = newSettings.settings;
        }
    }
    client.writeQuery({
        data: { settings: gravaSettings },
        query: GET_SETTINGS,
    });
    return gravaSettings;
};

export const populateCachedData = (data, forcePopulate = false) => {
    // console.log(`entered populatedCachedData, forcePopulate = ${forcePopulate}`);
    // Populate related anexos, areasDeUso and conflitos to save http requests:
    if (data && data.currentUser) {
        const variables = {
            territorioId: data.currentUser.currentTerritorio.id,
        };
        let shouldPopulate = forcePopulate;

        if (!forcePopulate) {
            const anexosData = tonomapaCache.readQuery({
                query: GET_ANEXOS,
                variables,
            });
            shouldPopulate = anexosData === null;
        }
        if (shouldPopulate) {
            const anexos = data.currentUser.currentTerritorio.anexos.map((anexo) => {
                return {
                    ...anexo,
                    localFileUri: '',
                    localThumbUri: '',
                };
            });
            tonomapaCache.writeQuery({
                query: GET_ANEXOS,
                data: {
                    anexos,
                },
                variables,
            });
        }

        if (!forcePopulate) {
            const areasDeUsoData = tonomapaCache.readQuery({
                query: GET_AREAS_DE_USO,
                variables,
            });
            shouldPopulate = areasDeUsoData === null;
        }
        if (shouldPopulate) {
            tonomapaCache.writeQuery({
                query: GET_AREAS_DE_USO,
                data: {
                    areasDeUso: data.currentUser.currentTerritorio.areasDeUso,
                },
                variables,
            });
        }

        if (!forcePopulate) {
            const conflitosData = tonomapaCache.readQuery({
                query: GET_CONFLITOS,
                variables,
            });
            shouldPopulate = conflitosData === null;
        }
        if (shouldPopulate) {
            tonomapaCache.writeQuery({
                query: GET_CONFLITOS,
                data: {
                    conflitos: data.currentUser.currentTerritorio.conflitos,
                },
                variables,
            });
        }

        if (!forcePopulate) {
            const mensagensData = tonomapaCache.readQuery({
                query: GET_MENSAGENS,
                variables,
            });
            shouldPopulate = mensagensData === null;
        }
        if (shouldPopulate) {
            tonomapaCache.writeQuery({
                query: GET_MENSAGENS,
                data: {
                    mensagens: data.currentUser.currentTerritorio.mensagens,
                },
                variables,
            });
        }
    }
};

/**
 * Pega o índice da mensagem atual
 * @method
 * @param  {object} currentUser A usuária
 * @return number             Índice da mensagem atual
 */
export const getCurrentMensagemIndex = (currentUser) => {
    if (
        !currentUser ||
        !currentUser.currentTerritorio ||
        !currentUser.currentTerritorio.mensagens
    ) {
        return -1;
    }
    let i = currentUser.currentTerritorio.mensagens.length - 1;
    while (
        i >= 0 &&
        (!currentUser.currentTerritorio.mensagens[i].originIsDevice ||
            currentUser.currentTerritorio.mensagens[i].id)
    ) {
        i--;
    }
    return i;
};

/*
    Atenção: esta função deve ser alterada conforme muda o objeto currentUser. Ela retorna um clone não referenciado do objeto completo (deepshallowcopy)
*/
export const cloneCurrentUser = (currentUser) => {
    const coordinates =
        currentUser.currentTerritorio.poligono && currentUser.currentTerritorio.poligono.coordinates
            ? currentUser.currentTerritorio.poligono.coordinates.map((obj) => [...obj])
            : CURRENT_USER.poligono;
    return {
        ...currentUser,
        currentTerritorio: {
            ...currentUser.currentTerritorio,
            poligono: {
                ...currentUser.currentTerritorio.poligono,
                coordinates,
            },
            // areasDeUso: currentUser.currentTerritorio.areasDeUso.map((obj) => ({ ...obj })),
            // conflitos: currentUser.currentTerritorio.conflitos.map((obj) => ({ ...obj })),
            // anexos: currentUser.currentTerritorio.anexos.map((obj) => ({ ...obj })),
            mensagens: currentUser.currentTerritorio.mensagens.map((obj) => ({ ...obj })),
            tiposComunidade: currentUser.currentTerritorio.tiposComunidade.map((obj) => ({
                ...obj,
            })),
        },
    };
};

export const preparaEnviaTerritorioParaAnalise = ({ territorioId }) => {
    const variables = { territorioId };
    const context = {
        serializationKey: 'USUARIA_MUTATION',
    };
    return {
        variables,
        context,
    };
};

export const preparaEnviaTerritorioParaPtt = ({ territorioId }) => {
    const variables = { territorioId };
    const context = {
        serializationKey: 'USUARIA_MUTATION',
    };
    return {
        variables,
        context,
    };
};

export const preparaUpdateAppData = async (currentUser) => {
    const newCurrentUser = cloneCurrentUser(omitDeep(currentUser, '__typename'));
    const optimisticCurrentUser = cloneCurrentUser(currentUser);

    // Adjust newCurrentUser:
    newCurrentUser.pushToken = await AsyncStorage.getItem('pushToken');
    delete newCurrentUser.codigoUsoCelular;
    delete newCurrentUser.currentTerritorio.areasDeUso;
    delete newCurrentUser.currentTerritorio.conflitos;
    delete newCurrentUser.currentTerritorio.anexos;
    delete newCurrentUser.currentTerritorio.mensagens;
    delete newCurrentUser.currentTerritorio.territorios;

    // Adjust optimisticCurrentUser:
    optimisticCurrentUser.pushToken = newCurrentUser.pushToken;
    const optimisticResponse = {
        updateAppData: {
            currentUser: optimisticCurrentUser,
            errors: [],
            success: true,
            token: null,
        },
    };

    const context = {
        serializationKey: 'USUARIA_MUTATION',
        shouldTrack: true,
        localId: newCurrentUser.currentTerritorio.id,
    };

    if (ehTmpId(newCurrentUser.currentTerritorio.id)) {
        delete newCurrentUser.currentTerritorio.id;
    }

    const variables = {
        currentUser: newCurrentUser,
    };

    return {
        variables,
        optimisticResponse,
        context,
    };
};

export const preparaDeleteUsoOuConflito = ({ localId, type }) => {
    const variables = { id: localId };
    const optimisticResponse = {
        [`delete${type}`]: {
            success: true,
            errors: [],
        },
    };
    const context = {
        serializationKey: 'USOS_OU_CONFLITOS_MUTATION',
        shouldTrack: true,
        localId,
    };
    return {
        variables,
        optimisticResponse,
        context,
    };
};

export const preparaDeleteUsuaria = ({ localId }) => {
    const variables = { id: localId };
    const context = {
        serializationKey: 'USUARIA_MUTATION',
        shouldTrack: false,
        localId,
    };
    return {
        variables,
        context,
    };
};

export const preparaCreateOrUpdateUsoOuConflito = ({
    usoOuConflitoEditado,
    currentTerritorioId,
    type,
}) => {
    const newUsoOuConflito = {
        ...usoOuConflitoEditado,
    };
    const localId = usoOuConflitoEditado.id;
    switch (type) {
        case 'AreaDeUso':
            delete newUsoOuConflito.tipoConflitoId;
            break;
        case 'Conflito':
            delete newUsoOuConflito.tipoAreaDeUsoId;
            break;
    }
    const variables = {
        ...newUsoOuConflito,
        territorioId: currentTerritorioId,
    };
    const optimisticResponse = {
        [`update${type}`]: {
            data: {
                ...variables,
                // ultimaAlteracao: new Date().toISOString(),
            },
            success: true,
            errors: [],
        },
    };
    const context = {
        serializationKey: 'USOS_OU_CONFLITOS_MUTATION',
        shouldTrack: true,
        localId,
    };
    if (ehTmpId(localId)) {
        delete variables.id;
    }
    return {
        variables,
        optimisticResponse,
        context,
    };
};

export const preparaDeleteAllUsosEConflitos = (territorioId) => {
    const variables = { territorioId };
    const optimisticResponse = {
        deleteAllUsosEConflitos: {
            success: true,
            errors: [],
        },
    };
    const context = {
        serializationKey: 'USOS_OU_CONFLITOS_MUTATION',
        shouldTrack: true,
        localId: null,
    };
    return {
        variables,
        optimisticResponse,
        context,
    };
};

export const preparaDeleteAnexo = ({ localId }) => {
    const variables = { id: localId };
    const optimisticResponse = {
        deleteAnexo: {
            success: true,
            errors: [],
        },
    };
    const context = {
        serializationKey: 'ANEXO_MUTATION',
        shouldTrack: true,
        localId,
    };
    return {
        variables,
        optimisticResponse,
        context,
    };
};

export const preparaCreateOrUpdateMensagem = ({ newMensagem, currentTerritorioId }) => {
    const variables = {
        ...newMensagem,
        territorioId: currentTerritorioId,
    };
    const optimisticResponse = {
        updateMensagem: {
            data: {
                ...variables,
            },
            success: true,
            errors: [],
        },
    };
    const context = {
        serializationKey: 'MENSAGEM_MUTATION',
        shouldTrack: true,
        localId: newMensagem.id,
    };
    if (ehTmpId(newMensagem.id)) {
        delete variables.id;
    }
    // delete variables.extra;
    return {
        variables,
        optimisticResponse,
        context,
    };
};

export const preparaCreateOrUpdateAnexo = ({ newAnexo, currentTerritorioId }) => {
    const variables = {
        ...newAnexo,
        territorioId: currentTerritorioId,
    };
    const optimisticResponse = {
        updateAnexo: {
            data: {
                ...variables,
                ultimaAlteracao: new Date().toISOString(),
            },
            success: true,
            errors: [],
        },
    };
    const context = {
        serializationKey: 'ANEXO_MUTATION',
        shouldTrack: true,
        localId: newAnexo.id,
    };
    if (ehTmpId(newAnexo.id)) {
        delete variables.id;
    }
    delete variables.localFileUri;
    delete variables.localThumbUri;
    delete variables.criacao;
    delete variables.fileSize;
    delete variables.mimetype;
    delete variables.thumb;
    return {
        variables,
        optimisticResponse,
        context,
    };
};

export const downloadAnexo = async (anexo, anexos, currentTerritorioId) => {
    const sourceUri = fazAnexoRemoteUri(anexo.arquivo);
    let downloadResult;
    const destinationUri = fazAnexoLocalUri(anexo.arquivo);
    try {
        downloadResult = await FileSystem.downloadAsync(sourceUri, destinationUri, {});
    } catch (err) {
        console.log('Não foi possível baixar o arquivo!', err);
        log(`onVisualizarItem: Não foi possível baixar o arquivo ${anexo.arquivo}`);
        logError(err);
        return {
            errors: [
                `Houve um erro ao tentar baixar o arquivo ${anexo.nome} (id ${
                    anexo.id
                }): ${JSON.stringify(err)}`,
            ],
        };
    }

    if (Math.floor(downloadResult.status / 100) !== 2) {
        console.log('Não foi possível baixar este arquivo.', downloadResult);
        return {
            errors: [
                `Houve um erro ao tentar baixar o arquivo ${anexo.nome} (id ${anexo.id}): Status=${downloadResult.status}`,
            ],
        };
    }

    const newAnexos = anexos.map((newAnexo) => {
        if (anexo.id === newAnexo.id) {
            return {
                ...newAnexo,
                localFileUri: downloadResult.uri,
            };
        }
        return newAnexo;
    });
    tonomapaCache.writeQuery({
        query: GET_ANEXOS,
        variables: {
            territorioId: currentTerritorioId,
        },
        data: {
            anexos: newAnexos,
        },
    });
    return { errors: null, uri: downloadResult.uri };
};

export const downloadThumbs = async (anexos, currentTerritorioId) => {
    const missingAnexos = anexos.filter((anexo) => {
        return !anexo.localThumbUri && !anexo.localFileUri && anexo.thumb;
    });
    const downloadPromises = [];
    const destinationUris = [];
    for (const anexo of missingAnexos) {
        const sourceUri = `${Constants.manifest.extra.dashboardEndpoint}${anexo.thumb}`;
        const destinationUri = fazAnexoLocalUri(anexo.thumb, 'thumb');
        downloadPromises.push(FileSystem.downloadAsync(sourceUri, destinationUri));
        destinationUris[destinationUri] = anexo.id;
    }
    const results = await Promise.all(downloadPromises);
    let newAnexos = [...anexos];
    for (const result of results) {
        if (Math.floor(result.status / 100) !== 2) {
            //Download failed
            console.log(`Failed download thumb ${result.uri}`);
            continue;
        }
        newAnexos = newAnexos.map((newAnexo) => {
            if (destinationUris[result.uri] === newAnexo.id) {
                return {
                    ...newAnexo,
                    localThumbUri: result.uri,
                };
            }
            return newAnexo;
        });
    }
    tonomapaCache.writeQuery({
        query: GET_ANEXOS,
        variables: {
            territorioId: currentTerritorioId,
        },
        data: {
            anexos: newAnexos,
        },
    });
};

export const executeTrackedMutations = async (client, isAppLaunch = false) => {
    const runAsync = async () => {
        const keys = await AsyncStorage.getAllKeys();
        const promises = [];
        for (const key of keys) {
            if (key.includes('trackedMutations-')) {
                const trackedMutationJSON = await AsyncStorage.getItem(key);
                const { operationName, context, query, variables } =
                    JSON.parse(trackedMutationJSON);
                if (variables.arquivo && typeof variables.arquivo === 'object') {
                    variables.arquivo = new ReactNativeFile(variables.arquivo);
                }
                const mutateParams = {
                    context,
                    mutation: query,
                    variables,
                };

                mutateParams.update = (cache, { data }) => {
                    updateHandlerByName({
                        operationName,
                        cache,
                        data,
                        localId: context.localId,
                    });
                };
                promises.push(client.mutate(mutateParams));
                AsyncStorage.removeItem(key);
                // console.log(`Executed and Removed ${key}.`);
            }
        }
        try {
            await Promise.all(promises);
            return true;
        } catch (e) {
            log('Erro ao executar a fila do executeTrackedMutations');
            logError(e);
            return false;
        }
    };
    await delay(2000);
    await runAsync();
};

export const addTrackedMutation = ({ operationId, operationName, query, variables, context }) => {
    const item = JSON.stringify({ operationName, query, variables, context });
    AsyncStorage.setItem(operationId, item);
    // console.log('added trackedMutation ' + operationId);
};

export const getTerritorioReport = async (currentUser) => {
    const territorioId = currentUser.currentTerritorio.id;
    const territorioNome = currentUser.currentTerritorio.nome;
    const date = moment().format('DD-MM-YYYY');
    const url = `${RELATORIO_ENDPOINT}${territorioId}/`;
    const filename = slugify(`tonomapa_${territorioNome}_${date}.pdf`, {
        replacement: '-', // replace spaces with replacement character, defaults to `-`
        remove: undefined, // remove characters that match regex, defaults to `undefined`
        lower: true, // convert to lower case, defaults to `false`
        strict: false, // strip special characters except replacement, defaults to `false`
        locale: 'pt', // language code of the locale to use
        trim: true, // trim leading and trailing replacement chars, defaults to `true`
    });
    const destinationUri = DIR_RELATORIOS + filename;

    // const { exists } = await FileSystem.getInfoAsync(destinationUri);
    // if (exists) {
    //     try {
    //         await Sharing.shareAsync(destinationUri, { UTI: '.pdf', mimeType: 'application/pdf' });
    //     } catch (error) {
    //         return false;
    //     }
    //     return true;
    // }

    const token = await getToken();
    let uri;

    try {
        const ret = await FileSystem.downloadAsync(url, destinationUri, {
            headers: { authorization: `JWT ${token}` },
        });
        uri = ret.uri;
    } catch (error) {
        console.log('Erro ao tentar baixar o relatório', error);
        return false;
    }
    try {
        await Sharing.shareAsync(uri, { UTI: '.pdf', mimeType: 'application/pdf' });
    } catch (error) {
        return false;
    }
    return true;
};
