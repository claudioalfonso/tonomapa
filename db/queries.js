import gql from 'graphql-tag';
import {
    TERRITORIO_FRAGMENT,
    USUARIA_FRAGMENT,
    ANEXO_FRAGMENT,
    AREA_DE_USO_FRAGMENT,
    CONFLITO_FRAGMENT,
    MENSAGEM_FRAGMENT,
} from './fragments';

export const GET_TERRITORIO_STATUS_ID = gql`
    query territorioStatusId($territorioId: ID) {
        territorioStatusId(territorioId: $territorioId)
    }
`;

export const ENVIA_TERRITORIO_PARA_ANALISE = gql`
    ${TERRITORIO_FRAGMENT}
    mutation EnviaTerritorioParaAnalise($territorioId: ID) {
        enviaTerritorioParaAnalise(territorioId: $territorioId) {
            data {
                ...TerritorioFragment
            }
            success
            errors
        }
    }
`;

export const ENVIA_TERRITORIO_PARA_PTT = gql`
    ${TERRITORIO_FRAGMENT}
    mutation EnviaTerritorioParaPtt($territorioId: ID) {
        enviaTerritorioParaPtt(territorioId: $territorioId) {
            data {
                ...TerritorioFragment
            }
            success
            errors
        }
    }
`;

//
// CurrentUser
//

export const GET_USER_DATA_WITH_TERRITORIO_ID = gql`
    ${USUARIA_FRAGMENT}
    ${TERRITORIO_FRAGMENT}
    query currentUser($territorioId: ID) {
        currentUser {
            ...UsuariaFragment
            currentTerritorio(id: $territorioId) {
                ...TerritorioFragment
            }
        }
    }
`;

export const GET_USER_DATA = gql`
    ${USUARIA_FRAGMENT}
    ${TERRITORIO_FRAGMENT}
    query GetUserData {
        currentUser {
            ...UsuariaFragment
            currentTerritorio {
                ...TerritorioFragment
            }
        }
    }
`;

export const SEND_USER_DATA = gql`
    ${USUARIA_FRAGMENT}
    ${TERRITORIO_FRAGMENT}
    mutation updateAppData($currentUser: UsuariaInput) {
        updateAppData(input: { currentUser: $currentUser }) {
            currentUser {
                ...UsuariaFragment
                currentTerritorio {
                    ...TerritorioFragment
                }
            }
            token
            errors
            success
        }
    }
`;

export const GET_USUARIA = gql`
    ${USUARIA_FRAGMENT}
    query GetUsuaria {
        currentUser {
            ...UsuariaFragment
        }
    }
`;

export const GET_TERRITORIO = gql`
    query GetTerritorio($id: ID) {
        territorio(id: $id) {
            id
            nome
            qtdeFamilias
        }
    }
`;

export const DELETE_TERRITORIO = gql`
    mutation DeleteTerritorio($id: ID!) {
        deleteTerritorio(id: $id) {
            success
            errors
        }
    }
`;

export const DELETE_USUARIA = gql`
    mutation DeleteUsuaria($id: ID!) {
        deleteUsuaria(id: $id) {
            success
            errors
        }
    }
`;

//
// Settings
//

export const GET_SETTINGS = gql`
    {
        settings {
            appIntro
            useGPS
            useMapLegacy
            mapType
            editaPoligonos
            editing
            unidadeArea
            showAtaBalloon
            showGPSHelpBalloon
        }
    }
`;

//
// Anexos
//

export const GET_ANEXOS = gql`
    ${ANEXO_FRAGMENT}
    query GetAnexos($territorioId: ID) {
        anexos(territorioId: $territorioId) {
            ...AnexoFragment
        }
    }
`;

export const CREATE_OR_UPDATE_ANEXO = gql`
    ${ANEXO_FRAGMENT}
    mutation CreateOrUpdateAnexo(
        $id: ID
        $territorioId: ID
        $nome: String!
        $descricao: String
        $arquivo: Upload!
        $tipoAnexo: String
        $publico: Boolean
    ) {
        updateAnexo(
            id: $id
            territorioId: $territorioId
            nome: $nome
            descricao: $descricao
            arquivo: $arquivo
            tipoAnexo: $tipoAnexo
            publico: $publico
        ) {
            data {
                ...AnexoFragment
            }
            success
            errors
        }
    }
`;

export const DELETE_ANEXO = gql`
    mutation DeleteAnexo($id: ID!) {
        deleteAnexo(id: $id) {
            errors
            success
        }
    }
`;

//
// Áreas de Uso
//

export const GET_AREAS_DE_USO = gql`
    ${AREA_DE_USO_FRAGMENT}
    query GetAreasDeUso($territorioId: ID) {
        areasDeUso(territorioId: $territorioId) {
            ...AreaDeUsoFragment
        }
    }
`;

export const CREATE_OR_UPDATE_AREA_DE_USO = gql`
    ${AREA_DE_USO_FRAGMENT}
    mutation CreateOrUpdateAreaDeUso(
        $id: ID
        $territorioId: ID
        $nome: String
        $posicao: String
        $descricao: String
        $area: Decimal
        $tipoAreaDeUsoId: String
    ) {
        updateAreaDeUso(
            id: $id
            territorioId: $territorioId
            nome: $nome
            posicao: $posicao
            descricao: $descricao
            area: $area
            tipoAreaDeUsoId: $tipoAreaDeUsoId
        ) {
            data {
                ...AreaDeUsoFragment
            }
            success
            errors
        }
    }
`;

export const DELETE_AREA_DE_USO = gql`
    mutation DeleteAreaDeUso($id: ID) {
        deleteAreaDeUso(id: $id) {
            success
        }
    }
`;

//
// Conflitos
//

export const GET_CONFLITOS = gql`
    ${CONFLITO_FRAGMENT}
    query GetConflitos($territorioId: ID) {
        conflitos(territorioId: $territorioId) {
            ...ConflitoFragment
        }
    }
`;

export const CREATE_OR_UPDATE_CONFLITO = gql`
    ${CONFLITO_FRAGMENT}
    mutation CreateOrUpdateConflito(
        $id: ID
        $territorioId: ID
        $nome: String
        $posicao: String
        $descricao: String
        $tipoConflitoId: String
    ) {
        updateConflito(
            id: $id
            territorioId: $territorioId
            nome: $nome
            posicao: $posicao
            descricao: $descricao
            tipoConflitoId: $tipoConflitoId
        ) {
            data {
                ...ConflitoFragment
            }
            success
            errors
        }
    }
`;

export const DELETE_CONFLITO = gql`
    mutation DeleteConflito($id: ID) {
        deleteConflito(id: $id) {
            success
        }
    }
`;

//
// Áreas de uso ou Conflito:
//

export const DELETE_ALL_USOS_E_CONFLITOS = gql`
    mutation DeleteAllUsosEConflitos($territorioId: ID!) {
        deleteAllUsosEConflitos(territorioId: $territorioId) {
            success
            errors
        }
    }
`;

//
// Mensagens
//

export const GET_MENSAGENS = gql`
    ${MENSAGEM_FRAGMENT}
    query GetMensagens($territorioId: ID!, $minId: ID, $senderIsDashboard: Boolean) {
        mensagens(
            territorioId: $territorioId
            senderIsDashboard: $senderIsDashboard
            minId: $minId
            jsonDumpExtra: true
        ) {
            ...MensagemFragment
        }
    }
`;

export const CREATE_OR_UPDATE_MENSAGEM = gql`
    ${MENSAGEM_FRAGMENT}
    mutation CreateOrUpdateMensagem(
        $id: ID
        $territorioId: ID
        $remetenteId: String
        $texto: String
        $extra: JSONString
        $dataEnvio: DateTime
        $dataRecebimento: DateTime
        $originIsDevice: Boolean
    ) {
        updateMensagem(
            id: $id
            territorioId: $territorioId
            remetenteId: $remetenteId
            texto: $texto
            extra: $extra
            dataEnvio: $dataEnvio
            dataRecebimento: $dataRecebimento
            originIsDevice: $originIsDevice
        ) {
            data {
                ...MensagemFragment
            }
            success
            errors
        }
    }
`;

export const DELETE_MENSAGEM = gql`
    mutation DeleteMensagem($id: ID!) {
        deleteMensagem(id: $id) {
            success
        }
    }
`;
