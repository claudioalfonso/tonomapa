import { Dimensions, PixelRatio, Platform } from 'react-native';
import metrics from '../utils/metrics';

const { width, height } = Dimensions.get('window');

/**
 * @type {Number}
 */
export const ASPECT_RATIO = width / height;

/**
 * @type {Number}
 */
export const LATITUDE_DELTA = 0.0922;

/**
 * @type {Number}
 */
export const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

/**
 * @type object
 */
export const ICONE_TIPO = {
    locationMark: require('../assets/images/gps/escuro/noun_Gps_Add.png'),
    locationMarkClaro: require('../assets/images/gps//claro/noun_Gps_Add.png'),
    verticeMovel: require('../assets/images/vertices/escuro/30/vertice.png'),
    verticeMovelClaro: require('../assets/images/vertices/claro/30/vertice.png'),
    vertice: require('../assets/images/vertices/escuro/30/vertice.png'),
    verticeClaro: require('../assets/images/vertices/claro/30/vertice.png'),
};

/**
 * @type object
 */
export const LOGO_IMAGE = {
    colorida: require('../assets/images/logo/colorida/192/logo.png'),
    branca: require('../assets/images/logo/branca/192/logo.png'),
};

/**
 * @type object
 */
export const initialState = {
    region: null,
    polygons: [],
    markers: [],
    markersTipoUsoConflito: [],
    modo: null,
    locationMarkerCoordinate: null,
    currentUser: null,
    enviarDadosDialogo: {
        botaoEnviar: true,
        botaoCancelar: true,
        botaoOk: null,
        etapa: 'situacao',
        situacao: null,
        erro: null,
        apiErrors: {
            graphQLErrors: [],
            networkError: '',
        },
    },
    carregando: null,
    preparandoShot: null,
    statusMessage: null,
    mapType: null,
    settings: {},
};

/**
 * @method reducer
 * @param  {Object} state
 * @param  {Object} newState
 * @return {Object}
 */
export const reducer = (state, newState) => {
    return { ...state, ...newState };
};

/**
 * @type object
 */
const edgePadding = {
    top: metrics.tenWidth * 8,
    bottom: metrics.tenWidth * 8,
    left: 0,
    right: 0,
};

const edgePaddingAndroid = {
    top: PixelRatio.getPixelSizeForLayoutSize(edgePadding.top),
    bottom: PixelRatio.getPixelSizeForLayoutSize(edgePadding.bottom),
    left: PixelRatio.getPixelSizeForLayoutSize(edgePadding.left),
    right: PixelRatio.getPixelSizeForLayoutSize(edgePadding.right),
};

/**
 * @type object
 */
export const edgePaddingAdjusted = Platform.OS === 'android' ? edgePaddingAndroid : edgePadding;
