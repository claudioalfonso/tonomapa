const foundation = {
    Basic: {
        White: '#FFFFFF',
        Gray: '#595959',
        Disabled: '#ADADAD',
    },
    Feedback: {
        Error: '#D74F5A',
        Success: '#348B57',
        Info: '#666C70',
    },
    TextColor: {
        White: '#FFFFFF',
        Gray: '#3A3A3A',
    },
    CorDeTexto: {
        CorDeTexto: '#FFFFFF',
        CorDeTexto2: '#3A3A3A',
    },
    Brown: {
        B300: '#DB6616',
        B400: '#BF5A15',
        B75: '#F0C09F',
    },
    Green: {
        G300: '#A1C736',
        G400: '#718B26',
        G75: '#D8E8AD',
        G55: '#EEFFC1',
    },
};

const colors = {
    divider: '#00000029',
    warning: '#D87F3E',
    polygonBackgroundHybrid: `${foundation.Basic.White}55`,
    polygonBackgroundStandard: `${foundation.Green.G300}55`,
    appOverlay: `${foundation.Basic.White}66`,
    Foundation: {
        ...foundation,
    },
};

export default colors;
