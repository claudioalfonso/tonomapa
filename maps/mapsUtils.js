import * as Location from 'expo-location';

// @ts-ignore
import tipos from '../bases/tipos';
import { formataNumeroBR, resgataEstadoMunicipioDoId } from '../utils/utils';
import { LATITUDE_DELTA, LONGITUDE_DELTA } from '../assets/constants';
import { geraIconeTipoUsoOuConflito } from '../bases/icones';

export const UNIDADES_AREA = {
    METRO_QUADRADO: {
        sigla: 'm²',
        siglaMilhar: 'km²',
        nome: 'Metro quadrado',
        razao: 1,
    },
    HECTARE: {
        sigla: 'h',
        siglaMilhar: '',
        nome: 'Hectare',
        razao: 1 / 10000,
    },
};

export const GEOLOCATION_OPTIONS = {
    accuracy: Location.Accuracy.Highest,
    distanceInterval: 5,
    timeInterval: 1000,
};

const calculateAreaInSquareMeters = (x1, x2, y1, y2) => {
    let area = (y1 * x2 - x1 * y2) / 2;
    return area;
};

const calculateYSegment = (latitudeRef, latitude, circumference) => {
    return ((latitude - latitudeRef) * circumference) / 360.0;
};

const calculateXSegment = (longitudeRef, longitude, latitude, circumference) => {
    return (
        ((longitude - longitudeRef) * circumference * Math.cos(latitude * (Math.PI / 180))) / 360.0
    );
};

export const calculaAreaPoligono = (
    locations,
    unidadeArea = UNIDADES_AREA.HECTARE,
    formatada = false
) => {
    if (!locations.length) {
        return 0;
    }
    if (locations.length < 3) {
        return 0;
    }
    let radius = 6371000;
    const diameter = radius * 2;
    const circumference = diameter * Math.PI;
    const listY = [];
    const listX = [];
    const listArea = [];
    // calculate segment x and y in degrees for each point

    const latitudeRef = locations[0].latitude;
    const longitudeRef = locations[0].longitude;
    for (let i = 1; i < locations.length; i++) {
        let latitude = locations[i].latitude;
        let longitude = locations[i].longitude;
        listY.push(calculateYSegment(latitudeRef, latitude, circumference));
        listX.push(calculateXSegment(longitudeRef, longitude, latitude, circumference));
    }

    // calculate areas for each triangle segment
    for (let i = 1; i < listX.length; i++) {
        let x1 = listX[i - 1];
        let y1 = listY[i - 1];
        let x2 = listX[i];
        let y2 = listY[i];
        listArea.push(calculateAreaInSquareMeters(x1, x2, y1, y2));
    }

    // sum areas of all triangle segments
    let areasSum = 0;
    listArea.forEach((area) => (areasSum = areasSum + area));

    // get absolute value of area
    let areaCalc = Math.abs(areasSum) * unidadeArea.razao; // Math.sqrt(areasSum * areasSum);

    return formatada ? formataArea(areaCalc, unidadeArea) : areaCalc;
};

export const formataArea = (areaCalc, unidadeArea = UNIDADES_AREA.HECTARE) => {
    let unidade = unidadeArea.sigla;
    let decimals = 1;
    if (unidadeArea.sigla === UNIDADES_AREA.HECTARE.sigla) {
        decimals = areaCalc === Math.round(areaCalc) ? 0 : 1;
    } else {
        if (areaCalc > 1050000000) {
            areaCalc = Math.round(areaCalc / 100000000) / 10;
            unidade = 'mil km²';
            decimals = areaCalc === Math.round(areaCalc) ? 0 : 1;
        } else if (areaCalc > 950000000) {
            areaCalc = '';
            unidade = 'mil km²';
        } else if (areaCalc > 1050000) {
            areaCalc = Math.round(areaCalc / 100000) / 10;
            unidade = 'km²';
            decimals = areaCalc === Math.round(areaCalc) ? 0 : 1;
        } else if (areaCalc > 950000) {
            areaCalc = '';
            unidade = '1 km²';
        } else if (areaCalc > 1050) {
            areaCalc = Math.round(areaCalc / 100) / 10;
            unidade = 'mil m²';
            decimals = areaCalc === Math.round(areaCalc) ? 0 : 1;
        } else if (areaCalc > 950) {
            areaCalc = '';
            unidade = 'mil m²';
        } else {
            areaCalc = Math.round(areaCalc);
            unidade = 'm²';
            decimals = 0;
        }
    }
    areaCalc = areaCalc ? '~' + formataNumeroBR(areaCalc, decimals) + ' ' + unidade : '~' + unidade;

    return areaCalc;
};

export const getRegionForCoordinates = (coords) => {
    let minX, maxX, minY, maxY;

    ((coord) => {
        minX = coord.latitude;
        maxX = coord.latitude;
        minY = coord.longitude;
        maxY = coord.longitude;
    })(coords[0]);

    // calculate rect
    coords.map((coord) => {
        minX = Math.min(minX, coord.latitude);
        maxX = Math.max(maxX, coord.latitude);
        minY = Math.min(minY, coord.longitude);
        maxY = Math.max(maxY, coord.longitude);
    });

    const midX = (minX + maxX) / 2;
    const midY = (minY + maxY) / 2;
    // let deltaX = maxX - minX;
    // let deltaY = maxY - minY;

    return {
        latitude: midX,
        longitude: midY,
        latitudeDelta: 0.002,
        longitudeDelta: 0.002,
    };
};

export const qtdePoligonos = (currentTerritorio) => {
    let qtde = 0;
    if (currentTerritorio && currentTerritorio.poligono && currentTerritorio.poligono.coordinates) {
        qtde = currentTerritorio.poligono.coordinates.length;
    }
    return qtde;
};

export const areaPoligonos = (polygons) => {
    if (polygons.length) {
        return polygons.reduce((area, current) => area + current.area, 0);
    }
    return null;
};

export const getNameTipoUsoOuConflito = (marker) => {
    let nameTipoUsoOuConflito;
    if (marker.object.__typename === 'ConflitoType') {
        for (let i = 0; i < tipos.data.tiposConflito.length; i++) {
            if (marker.object.tipoConflitoId === tipos.data.tiposConflito[i].id) {
                nameTipoUsoOuConflito = tipos.data.tiposConflito[i].nome;
                break;
            }
        }
    } else {
        for (let i = 0; i < tipos.data.tiposAreaDeUso.length; i++) {
            if (marker.object.tipoAreaDeUsoId === tipos.data.tiposAreaDeUso[i].id) {
                nameTipoUsoOuConflito = tipos.data.tiposAreaDeUso[i].nome;
                break;
            }
        }
    }
    return nameTipoUsoOuConflito;
};

export /**
 * Translate polygons and markers to map
 */
const gqlTerritorioToMapa = ({ currentUser, conflitos, areasDeUso, unidadeArea }) => {
    // signOut();
    const polygons = [];
    const markers = [];
    const todasCoords = { latitude: [], longitude: [] };
    const mapMarker = [];

    // Generate map polygons:
    if (currentUser.currentTerritorio.poligono) {
        for (const poly of currentUser.currentTerritorio.poligono.coordinates) {
            const newPolygon = {
                id: polygons.length + 1,
                coordinates: [],
                holes: [],
                area: 0,
            };
            for (let i = 0; i < poly[0].length - 1; i++) {
                newPolygon.coordinates.push({
                    latitude: poly[0][i][1],
                    longitude: poly[0][i][0],
                });
                todasCoords.latitude.push(poly[0][i][1]);
                todasCoords.longitude.push(poly[0][i][0]);
            }
            newPolygon.area = calculaAreaPoligono(newPolygon.coordinates, unidadeArea);
            polygons.push(newPolygon);
        }
    }

    const markersTipoUsoConflito = [];
    // Generate map markers - conflitos
    for (const conflito of conflitos) {
        if (conflito && conflito.posicao) {
            const icone = geraIconeTipoUsoOuConflito('conflito', conflito.tipoConflitoId);
            const newConflito = {
                id: markers.length + 1,
                title: conflito.nome,
                description: conflito.descricao,
                coordinate: {
                    latitude: conflito.posicao.coordinates[1],
                    longitude: conflito.posicao.coordinates[0],
                },
                icone,
                object: conflito,
            };
            todasCoords.latitude.push(conflito.posicao.coordinates[1]);
            todasCoords.longitude.push(conflito.posicao.coordinates[0]);
            markers.push(newConflito);
            mapMarker.push(null);
            markersTipoUsoConflito.push(getNameTipoUsoOuConflito(newConflito));
        }
    }

    // Generate map markers - locais de uso
    for (const areaDeUso of areasDeUso) {
        if (areaDeUso?.posicao) {
            const icone = geraIconeTipoUsoOuConflito('areaDeUso', areaDeUso.tipoAreaDeUsoId);
            const newAreaDeUso = {
                id: markers.length + 1,
                title: areaDeUso.nome,
                description: areaDeUso.descricao,
                coordinate: {
                    latitude: areaDeUso.posicao.coordinates[1],
                    longitude: areaDeUso.posicao.coordinates[0],
                },
                icone,
                object: areaDeUso,
            };
            todasCoords.latitude.push(areaDeUso.posicao.coordinates[1]);
            todasCoords.longitude.push(areaDeUso.posicao.coordinates[0]);
            markers.push(newAreaDeUso);
            mapMarker.push(null);
            markersTipoUsoConflito.push(getNameTipoUsoOuConflito(newAreaDeUso));
        }
    }

    let region;
    const { municipio } = resgataEstadoMunicipioDoId(
        currentUser.currentTerritorio.municipioReferenciaId
    );
    if (polygons.length > 0 || markers.length > 0) {
        const diffCoords = {
            latitude: Math.max(...todasCoords.latitude) - Math.min(...todasCoords.latitude),
            longitude: Math.max(...todasCoords.longitude) - Math.min(...todasCoords.longitude),
        };
        region = {
            latitude: Math.min(...todasCoords.latitude) + diffCoords.latitude / 2,
            longitude: Math.min(...todasCoords.longitude) + diffCoords.longitude / 2,
            latitudeDelta: diffCoords.latitude + LATITUDE_DELTA / 4,
            longitudeDelta: diffCoords.longitude + LATITUDE_DELTA / 4,
        };
    } else {
        region = {
            latitude: municipio.latitude,
            longitude: municipio.longitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
        };
    }

    return {
        region,
        polygons,
        markers,
        markersTipoUsoConflito,
        mapMarker,
    };
};

export const getAllMapElementsCoords = ({ polygons, markers, editing = null }) => {
    let coords = [];
    for (let i = 0; i < polygons.length; i++) {
        coords = coords.concat(polygons[i].coordinates);
    }
    for (let i = 0; i < markers.length; i++) {
        coords.push(markers[i].coordinate);
    }
    if (editing && editing.coordinates.length >= 3) {
        coords = coords.concat(editing.coordinates);
    }
    return coords;
};
