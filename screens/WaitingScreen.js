import React from 'react';

import ToNoMapaLoading from '../components/ToNoMapaLoading';

export default function WaitingScreen() {
    return <ToNoMapaLoading />;
}
