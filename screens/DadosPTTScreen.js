import React, { useEffect, useReducer, useRef, useState } from 'react';
import { View, Alert } from 'react-native';
import { Button, Text, TextInput, Switch, HelperText } from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { TextInputMask } from 'react-native-masked-text';
import { useQuery } from '@apollo/client';

import { Inputs, Layout } from '../styles';
import colors from '../assets/colors';
import { PTT_TERRITORIO_VALIDADOR, DADOSPTT_SCREEN_VALIDADOR } from '../utils/schemas';
import TELAS from '../bases/telas';
import { adjustTerritorioFieldTypes } from '../bases/dadosbasicos';
// @ts-ignore
import tipos from '../bases/tipos';
import { GET_USER_DATA } from '../db/queries';
import { deepEqual } from '../utils/utils';
import metrics from '../utils/metrics';

import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import ToNoMapaLoading from '../components/ToNoMapaLoading';
import {
    MunicipioSelectH,
    GenericSelect,
    TextInputDefault,
    TextInputCEP,
    TextInputTelefone,
} from '../components/inputs';
import { ButtonContainedPrimary, ButtonTextPrimary } from '../components/buttons';
import PTTBottomBar from '../components/PTTBottomBar';

const zonasLocalizacao = [
    { label: 'RURAL', value: 'RURAL' },
    { label: 'URBANA', value: 'URBANA' },
    { label: 'MISTA', value: 'MISTA' },
    { label: 'OUTRO', value: 'OUTRO' },
];

const getSegmentos = () => {
    return tipos.data.segmentos.map((segmento) => ({
        label: segmento.nome,
        value: segmento.id,
    }));
};

export default function DadosPTTScreen({ navigation, route }) {
    const reducer = (state, newState) => {
        return { ...state, ...newState };
    };
    const initialState = {
        ecEstadoId: null,
        newCurrentUser: null,
    };

    /**
     * @type object
     */
    const [state, setState] = useReducer(reducer, initialState);

    const [errors, setErrors] = useState([]);

    const { data: currentUserData } = useQuery(GET_USER_DATA);
    const currentUser = currentUserData ? currentUserData.currentUser : null;

    const scrollRef = useRef(null);

    useEffect(() => {
        if (scrollRef.current) {
            scrollRef.current.scrollToPosition(0, 0, true);
        }
    }, []);

    useEffect(() => {
        setState({
            newCurrentUser: currentUser,
            ecEstadoId: currentUser.currentTerritorio.ecMunicipioId
                ? parseInt(currentUser.currentTerritorio.ecMunicipioId.toString().substring(0, 2))
                : null,
        });
    }, [currentUser]);

    const updateTerritorio = (data) => {
        setState({
            newCurrentUser: {
                ...state.newCurrentUser,
                currentTerritorio: {
                    ...state.newCurrentUser.currentTerritorio,
                    ...data,
                },
            },
        });
    };
    const updateUser = (data) => {
        setState({
            newCurrentUser: {
                ...state.newCurrentUser,
                ...data,
            },
        });
    };

    const validarTerritorio = async (schema, mode) => {
        let errors = [];
        await schema.validate(state.newCurrentUser, { abortEarly: false }).catch((err) => {
            errors = err.errors.map((e) => {
                const error = e.split('|');
                return error;
            });
        });

        setErrors(errors);
        if (errors.length > 0) {
            // let message = 'Os seguintes dados estão incorretos ou não foram fornecidos:\n\n';
            // for (let erro of errors) {
            //     message += erro + '\n';
            // }
            // mode === 'save'
            //     ? Alert.alert(
            //           'Dados incompletos',
            //           message,
            //           [
            //               {
            //                   text: 'OK',
            //                   onPress: async () => {},
            //               },
            //           ],
            //           { cancelable: false }
            //       )
            //     : Alert.alert(
            //           'Dados incompletos',
            //           message,
            //           [
            //               {
            //                   text: 'FECHAR',
            //                   onPress: async () => {},
            //               },
            //               {
            //                   text: 'SALVAR MESMO ASSIM',
            //                   onPress: async () => {
            //                       salvarFn();
            //                   },
            //               },
            //           ],
            //           { cancelable: false }
            //       );
            return false;
        }
        return true;
    };

    const salvarFn = async () => {
        if (await validarTerritorio(DADOSPTT_SCREEN_VALIDADOR, 'save')) {
            const newCurrentUser = {
                ...state.newCurrentUser,
                currentTerritorio: adjustTerritorioFieldTypes(
                    state.newCurrentUser.currentTerritorio
                ),
            };
            const params = { tela: TELAS.MAPA };
            if (!deepEqual(newCurrentUser, currentUser)) {
                params.newCurrentUser = newCurrentUser;
                params.snackBarMessage = 'As informações PTT foram atualizadas!';
            }
            navigation.navigate('Home', params);
        }
    };

    const validate = async () => {
        if (await validarTerritorio(PTT_TERRITORIO_VALIDADOR)) {
            Alert.alert(
                'Dados corretos',
                'Todos os campos estão corretos!',
                [
                    {
                        text: 'FECHAR',
                        onPress: async () => {},
                    },
                    {
                        text: 'SALVAR',
                        onPress: async () => {
                            salvarFn();
                        },
                    },
                ],
                { cancelable: false }
            );
        }
    };

    if (!state.newCurrentUser) {
        return <ToNoMapaLoading />;
    }

    const getErrorMsg = (key) => {
        const error = errors.find((e) => {
            return e[0] === key;
        });
        if (!error) {
            return null;
        }
        return error[1];
    };

    return (
        <View style={Layout.containerStretched}>
            <ToNoMapaAppbar navigation={navigation} title="Dados do PTT" />
            <View style={Layout.body}>
                <KeyboardAwareScrollView
                    ref={scrollRef}
                    keyboardShouldPersistTaps="handled"
                    enableOnAndroid={true}
                    extraScrollHeight={200}>
                    {state.newCurrentUser && (
                        <View style={{ ...Layout.innerBody, paddingBottom: 200 }}>
                            <TextInputDefault
                                editable={false}
                                label="Nome da comunidade"
                                value={state.newCurrentUser.currentTerritorio.nome}
                            />

                            <MunicipioSelectH
                                disabled={true}
                                estadoId={
                                    state.newCurrentUser.currentTerritorio.municipioReferenciaId
                                        ? parseInt(
                                              state.newCurrentUser.currentTerritorio.municipioReferenciaId
                                                  .toString()
                                                  .substring(0, 2)
                                          )
                                        : null
                                }
                                municipioId={
                                    state.newCurrentUser.currentTerritorio.municipioReferenciaId
                                        ? parseInt(
                                              state.newCurrentUser.currentTerritorio
                                                  .municipioReferenciaId
                                          )
                                        : null
                                }
                                onValueChange={() => {}}
                            />

                            <TextInputCEP
                                label="CEP da comunidade"
                                value={state.newCurrentUser.currentTerritorio.cep}
                                onChangeText={(text) => {
                                    updateTerritorio({
                                        cep: text,
                                    });
                                }}
                                omitDot={true}
                                error={getErrorMsg('cep')}
                            />

                            <TextInputDefault
                                label="Seu email"
                                required={true}
                                keyboardType="email-address"
                                value={state.newCurrentUser.email}
                                onChangeText={(value) => updateUser({ email: value })}
                                error={getErrorMsg('email')}
                                info="Você receberá emails da Plataforma PTT para seguir  cadastrando por lá!"
                            />

                            <TextInputDefault
                                label="Acesso: Como faz para chegar na comunidade"
                                multiline
                                numberOfLines={3}
                                value={state.newCurrentUser.currentTerritorio.descricaoAcesso}
                                onChangeText={(value) =>
                                    updateTerritorio({ descricaoAcesso: value })
                                }
                                error={getErrorMsg('descricaoAcesso')}
                            />

                            <View style={[Layout.rowAnexoForm, { marginHorizontal: 10 }]}>
                                <Text style={Layout.textLeftFromSwitch}>
                                    A descrição de acesso acima pode ser disponibilizada
                                    publicamente?
                                </Text>
                                <Switch
                                    style={Layout.switchRight}
                                    color={colors.Foundation.Green.G300}
                                    value={
                                        state.newCurrentUser.currentTerritorio
                                            .descricaoAcessoPrivacidade
                                    }
                                    onValueChange={() => {
                                        updateTerritorio({
                                            descricaoAcessoPrivacidade:
                                                !state.newCurrentUser.currentTerritorio
                                                    .descricaoAcessoPrivacidade,
                                        });
                                    }}
                                />
                            </View>

                            <GenericSelect
                                label="Localização"
                                items={zonasLocalizacao}
                                selectedValue={
                                    state.newCurrentUser.currentTerritorio.zonaLocalizacao
                                }
                                onValueChange={(value) =>
                                    updateTerritorio({ zonaLocalizacao: value })
                                }
                                error={getErrorMsg('zonaLocalizacao')}
                            />

                            {state.newCurrentUser.currentTerritorio.zonaLocalizacao === 'OUTRO' && (
                                <TextInputDefault
                                    label="Outra localização"
                                    required={true}
                                    value={
                                        state.newCurrentUser.currentTerritorio.outraZonaLocalizacao
                                    }
                                    onChangeText={(value) =>
                                        updateTerritorio({ outraZonaLocalizacao: value })
                                    }
                                    error={getErrorMsg('outraZonaLocalizacao')}
                                />
                            )}

                            <TextInputDefault
                                label="Auto identificação: Como a comunidade se auto nomeia"
                                value={state.newCurrentUser.currentTerritorio.autoIdentificacao}
                                onChangeText={(value) =>
                                    updateTerritorio({ autoIdentificacao: value })
                                }
                                error={getErrorMsg('autoIdentificacao')}
                            />

                            <GenericSelect
                                label="Segmento da comunidade"
                                items={getSegmentos()}
                                selectedValue={state.newCurrentUser.currentTerritorio.segmentoId}
                                onValueChange={(value) => updateTerritorio({ segmentoId: value })}
                                error={getErrorMsg('segmentoId')}
                            />

                            <TextInputDefault
                                label="Descrição da história"
                                multiline
                                numberOfLines={5}
                                value={state.newCurrentUser.currentTerritorio.historiaDescricao}
                                onChangeText={(value) =>
                                    updateTerritorio({ historiaDescricao: value })
                                }
                                error={getErrorMsg('historiaDescricao')}
                            />

                            <TextInputDefault
                                label="Nome do contato"
                                value={state.newCurrentUser.currentTerritorio.ecNomeContato}
                                onChangeText={(value) => updateTerritorio({ ecNomeContato: value })}
                                error={getErrorMsg('ecNomeContato')}
                            />

                            <TextInputDefault
                                label="Endereço"
                                value={state.newCurrentUser.currentTerritorio.ecLogradouro}
                                onChangeText={(value) => updateTerritorio({ ecLogradouro: value })}
                                error={getErrorMsg('ecLogradouro')}
                            />

                            <TextInputDefault
                                label="Número"
                                keyboardType="numeric"
                                value={state.newCurrentUser.currentTerritorio.ecNumero}
                                onChangeText={(value) => updateTerritorio({ ecNumero: value })}
                                error={getErrorMsg('ecNumero')}
                            />

                            <TextInputDefault
                                label="Complemento"
                                value={state.newCurrentUser.currentTerritorio.ecComplemento}
                                onChangeText={(value) => updateTerritorio({ ecComplemento: value })}
                                error={getErrorMsg('ecComplemento')}
                            />

                            <TextInputDefault
                                label="Bairro"
                                value={state.newCurrentUser.currentTerritorio.ecBairro}
                                onChangeText={(value) => updateTerritorio({ ecBairro: value })}
                                error={getErrorMsg('ecBairro')}
                            />

                            <TextInputCEP
                                label="CEP do contato"
                                value={state.newCurrentUser.currentTerritorio.ecCep}
                                onChangeText={(text) => {
                                    updateTerritorio({
                                        ecCep: text,
                                    });
                                }}
                                omitDot={true}
                                error={getErrorMsg('ecCep')}
                            />

                            <TextInputDefault
                                label="Caixa postal"
                                keyboardType="number-pad"
                                value={state.newCurrentUser.currentTerritorio.ecCaixaPostal}
                                onChangeText={(value) => updateTerritorio({ ecCaixaPostal: value })}
                                error={getErrorMsg('ecCaixaPostal')}
                            />

                            <MunicipioSelectH
                                labelMunicipio="Município do contato"
                                labelEstado="Estado do contato"
                                required={true}
                                estadoId={state.ecEstadoId}
                                municipioId={
                                    state.newCurrentUser.currentTerritorio.ecMunicipioId
                                        ? parseInt(
                                              state.newCurrentUser.currentTerritorio.ecMunicipioId
                                          )
                                        : null
                                }
                                onValueChange={(value) => {
                                    if (value.hasOwnProperty('estadoId')) {
                                        setState({
                                            ecEstadoId: value.estadoId,
                                            newCurrentUser: {
                                                ...state.newCurrentUser,
                                                currentTerritorio: {
                                                    ...state.newCurrentUser.currentTerritorio,
                                                    ecMunicipioId: value.municipioId,
                                                },
                                            },
                                        });
                                        return;
                                    }
                                    updateTerritorio({
                                        ecMunicipioId: value.municipioId,
                                    });
                                }}
                                error={getErrorMsg('ecMunicipioId')}
                            />

                            <TextInputDefault
                                label="Email do contato"
                                keyboardType="email-address"
                                value={state.newCurrentUser.currentTerritorio.ecEmail}
                                onChangeText={(value) => updateTerritorio({ ecEmail: value })}
                                error={getErrorMsg('ecEmail')}
                            />

                            <TextInputTelefone
                                label="Telefone do contato"
                                value={state.newCurrentUser.currentTerritorio.ecTelefone}
                                onChangeText={(text) => {
                                    updateTerritorio({
                                        ecTelefone: text,
                                    });
                                }}
                                error={getErrorMsg('ecTelefone')}
                            />

                            <View
                                style={{ ...Layout.rowWrapper, marginTop: metrics.tenHeight * 4 }}>
                                <ButtonTextPrimary onPress={validate}>
                                    Validar formulário
                                </ButtonTextPrimary>

                                <ButtonContainedPrimary onPress={salvarFn}>
                                    Salvar
                                </ButtonContainedPrimary>
                            </View>
                        </View>
                    )}
                </KeyboardAwareScrollView>
            </View>
            <PTTBottomBar
                currentTerritorio={currentUser.currentTerritorio}
                navigation={navigation}
                tela={TELAS.DADOS_PTT}
            />
        </View>
    );
}
