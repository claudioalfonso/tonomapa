import React, { useCallback, useReducer } from 'react';
import { View, Alert, Text, BackHandler } from 'react-native';
import { useMutation, useQuery } from '@apollo/client';
import { useFocusEffect } from '@react-navigation/native';
import { HelperText } from 'react-native-paper';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import { Layout } from '../styles';
import { getSyncStatus } from '../bases/status';
import { geraTmpId } from '../utils/utils';
import metrics from '../utils/metrics';
import { GET_USER_DATA } from '../db/queries';
import MENSAGEM from '../bases/mensagem';

import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import { ButtonContainedPrimary } from '../components/buttons';
import { TextInputDefault } from '../components/inputs';

export default function FaleConoscoScreen({ navigation }) {
    const { online } = useToNoMapa();
    const reducer = (state, newState) => {
        return { ...state, ...newState };
    };
    const initialState = {
        textoMensagem: null,
    };

    const { data: currentUserData } = useQuery(GET_USER_DATA);
    const currentUser = currentUserData ? currentUserData.currentUser : null;

    /**
     * @type object
     */
    const [state, setState] = useReducer(reducer, initialState);

    const salvarFn = async () => {
        if (!online) {
            Alert.alert(
                'Sem internet!',
                'Só é possível enviar mensagem para o Tô No Mapa quando seu celular estiver conectado na internet\n\nPor favor, tente mais tarde.',
                [{ text: 'Entendi', onPress: async () => {} }],
                { cancelable: true }
            );
            return false;
        }

        const newMensagem = {
            ...MENSAGEM,
            id: geraTmpId(),
            texto: state.textoMensagem,
            // extra: JSON.stringify(currentUser),
            extra: null,
            dataEnvio: new Date(),
            originIsDevice: true,
        };
        const params = {
            newMensagem,
            snackBarMessage: 'Sua mensagem foi enviada com sucesso à equipe Tô no Mapa!',
        };
        navigation.navigate('Home', params);
    };

    const exitScreen = () => {
        navigation.navigate('Home');
        return true;
    };

    useFocusEffect(
        useCallback(() => {
            if (currentUser) {
                setState({ textoMensagem: null });
            }
        }, [])
    );

    //Android Back button
    useFocusEffect(
        useCallback(() => {
            const onBackPress = () => {
                return exitScreen();
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [navigation])
    );

    const alertSyncStatus = () => {
        const title = online ? 'Você está online!' : 'Sem internet';
        const body = online
            ? 'No momento o seu celular está conectado e você poderá enviar mensagem para a equipe Tô no Mapa'
            : 'Neste momento, não vai ser possível você enviar mensagem para a equipe Tô no Mapa.\n\nTente mais tarde, quando estiver conectada(o).';
        Alert.alert(
            title,
            body,
            [
                {
                    text: 'Entendi',
                    onPress: () => {},
                },
            ],
            { cancelable: true }
        );
        return;
    };

    const syncStatus = getSyncStatus(online, false, alertSyncStatus);

    return (
        <View style={Layout.containerStretched}>
            <ToNoMapaAppbar
                navigation={navigation}
                title="Fale conosco"
                rightActions={[{ ...syncStatus }]}
                goBack={false}
            />
            <View style={{ ...Layout.body, marginTop: metrics.tenHeight * 2 }}>
                <View style={Layout.innerBody}>
                    <Text style={Layout.textParagraph}>
                        Explique bem a sua dúvida, falha ou problema no app Tô no Mapa.
                    </Text>
                    <Text style={Layout.textParagraph}>
                        Conte COM DETALHES o que aconteceu ou o que você não está conseguindo fazer.
                    </Text>
                    <TextInputDefault
                        label="Mensagem"
                        multiline={true}
                        numberOfLines={6}
                        value={state.textoMensagem}
                        onChangeText={(value) => setState({ textoMensagem: value })}
                    />
                    <HelperText type="info">
                        Responderemos a sua mensagem por WhatsApp em até 3 dias úteis.
                    </HelperText>

                    <View style={{ ...Layout.buttonCenter, marginTop: 30 }}>
                        <ButtonContainedPrimary onPress={salvarFn}>
                            Enviar a mensagem
                        </ButtonContainedPrimary>
                    </View>
                </View>
            </View>
        </View>
    );
}
