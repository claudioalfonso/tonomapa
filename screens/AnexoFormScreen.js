import React, { useReducer, useState, useRef, useCallback } from 'react';
import { Image, View, Alert, BackHandler } from 'react-native';
import { Text, Switch, HelperText } from 'react-native-paper';
import { useFocusEffect } from '@react-navigation/native';
import * as DocumentPicker from 'expo-document-picker';
import * as mime from 'react-native-mime-types';
import { ReactNativeFile } from 'apollo-upload-client';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { FontAwesome } from '@expo/vector-icons';
import Constants from 'expo-constants';

import { deepEqual, geraTmpId } from '../utils/utils';
import { Anexos, Layout } from '../styles';
import {
    basename,
    isImage,
    isVideo,
    geraThumbDeVideo,
    iconeDoMimeType,
    formataTamanhoArquivo,
    apagaArquivos,
    fazAnexoLocalUri,
    fazNomeAnexo,
    persisteAnexo,
} from '../db/arquivos';
import colors from '../assets/colors';
import metrics from '../utils/metrics';
import ANEXO, { isAnexoPTT, TIPOS_ANEXO, TIPOS_ANEXO_INFO } from '../bases/anexo';
import { ANEXO_VALIDADOR } from '../utils/schemas';

import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import ToNoMapaLoading from '../components/ToNoMapaLoading';
import { GenericSelect, TextInputDefault } from '../components/inputs';
import {
    ButtonCancel,
    ButtonContainedPrimary,
    ButtonContainedSecondary,
    ButtonFullwidthPrimary,
    ButtonOutlinedSecondary,
} from '../components/buttons';

const exitScreen = (navigation) => {
    navigation.navigate('AnexoList');
    return true;
};

export default function AnexoFormScreen({ route, navigation }) {
    const maxFileSize = Constants.manifest.extra.maxFileSize;

    const anexosPTTOptions = () => {
        let options = [];
        for (let value in TIPOS_ANEXO) {
            options.push({
                label: TIPOS_ANEXO[value],
                value: value,
            });
        }
        return options;
    };
    const initialLocalState = {
        newAnexo: { ...ANEXO },
        editing: false,
        currentAnexo: null,
        icone: null,
    };

    const reducer = (state, newState) => {
        return { ...state, ...newState };
    };

    /**
     * @type object
     */
    const [state, setState] = useReducer(reducer, initialLocalState);

    const [errors, setErrors] = useState([]);

    const scrollRef = useRef(null);

    /**
     * @type object
     */
    const [carregando, setCarregando] = useState(false);

    useFocusEffect(
        useCallback(() => {
            if (scrollRef.current) {
                scrollRef.current.scrollToPosition(0, 0, true);
            }
        }, [])
    );

    if (route.params?.currentAnexo) {
        const newCurrentAnexo = { ...route.params.currentAnexo };
        delete route.params.currentAnexo;
        const newState = {
            editing: true,
            currentAnexo: newCurrentAnexo,
            newAnexo: newCurrentAnexo,
            icone: iconeDoMimeType(newCurrentAnexo.mimetype, 'awesome'),
        };
        setState(newState);
    } else if (route.params?.newAnexo) {
        delete route.params.newAnexo;
        const newId = geraTmpId();
        const newState = {
            editing: false,
            newAnexo: {
                ...ANEXO,
                id: newId,
            },
        };
        setState(newState);
    }

    useFocusEffect(
        React.useCallback(() => {
            //Handle Android Back button:
            const onBackPress = () => {
                return exitScreen(navigation);
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);

            return () => {
                BackHandler.removeEventListener('hardwareBackPress', onBackPress);
            };
        }, [navigation])
    );

    const validaDados = async () => {
        let newErrors = [];
        await ANEXO_VALIDADOR.validate(state.newAnexo, { abortEarly: false }).catch((err) => {
            newErrors = err.errors.map((e) => {
                const error = e.split('|');
                return error;
            });
        });

        if (!state.newAnexo.arquivo) {
            newErrors.push(['arquivo', 'Nenhum arquivo anexo']);
        }

        if (state.newAnexo.fileSize > maxFileSize) {
            newErrors.push([
                'arquivo',
                'O arquivo enviado é maior que ' +
                    formataTamanhoArquivo(maxFileSize) +
                    '. Por favor, envie um arquivo menor.',
            ]);
        }

        setErrors(newErrors);
        if (newErrors.length > 0) {
            // let message = 'Os seguintes dados precisam ser fornecidos:\n\n';
            // for (let erro of errors) {
            //     message += erro + '\n';
            // }

            // Alert.alert(
            //     'Dados incompletos',
            //     message,
            //     [
            //         {
            //             text: 'OK',
            //             onPress: async () => {},
            //         },
            //     ],
            //     { cancelable: true }
            // );
            return false;
        }
        return true;
    };

    const getAnexo = async () => {
        const result = await DocumentPicker.getDocumentAsync({
            multiple: false,
            copyToCacheDirectory: false,
        });

        if (result.type === 'success') {
            setCarregando(true);
            if (state.currentAnexo) {
                await apagaArquivos([
                    state.currentAnexo.localFileUri,
                    state.currentAnexo.localThumbUri,
                ]);
            }
            const res = await persisteAnexo({
                from: result.uri,
                filename: result.name,
            });

            if (!res || !('destinationUri' in res)) {
                console.log('Falha ao salvar o arquivo localmente');
                setCarregando(false);

                Alert.alert(
                    'Operação falhou!',
                    'Não foi possível concluir a operação. Por favor, tente novamente, com este ou outro arquivo. Se o erro persistir, ente por favor em contato com a equipe do Tô no Mapa',
                    [
                        {
                            text: 'Ok',
                            onPress: () => {},
                        },
                    ],
                    { cancelable: true }
                );
                return;
            }

            const mimetype = mime.lookup(result.name);
            // const mimetype = result.mimeType;
            const arquivo = new ReactNativeFile({
                uri: res.destinationUri,
                name: result.name,
                type: result.mimeType,
            });
            let newState = {
                newAnexo: {
                    ...state.newAnexo,
                    mimetype,
                    fileSize: result.size,
                    arquivo,
                    thumb: null,
                    localFileUri: res.destinationUri,
                    localThumbUri: '',
                },
                icone: iconeDoMimeType(mimetype, 'awesome'),
            };

            if (isVideo(mimetype)) {
                const newThumbName = fazNomeAnexo(
                    route.params.currentTerritorioId,
                    basename({ path: result.name, ext: 'jpg' })
                );
                const newThumbUri = fazAnexoLocalUri(newThumbName, 'thumb');
                const resThumbDeVideo = await geraThumbDeVideo(res.destinationUri, newThumbUri);
                if (resThumbDeVideo.ok) {
                    newState.newAnexo.localThumbUri = newThumbUri;
                }
            }
            setState(newState);
            setCarregando(false);
            return;
        }

        Alert.alert(
            'Operação falhou!',
            'Não foi possível concluir a operação. Por favor, tente novamente, com este ou outro arquivo. Se o erro persistir, ente por favor em contato com a equipe do Tô no Mapa',
            [
                {
                    text: 'Ok',
                    onPress: () => {},
                },
            ],
            { cancelable: true }
        );
    };

    const salvarFn = async () => {
        const res = await validaDados();
        if (!res) {
            console.log('Erro ao validar os dados no salvar anexo', state);
            return;
        }
        const params = deepEqual(state.newAnexo, state.currentAnexo)
            ? {}
            : { newAnexo: state.newAnexo };
        navigation.navigate('AnexoList', params);
    };

    const apagarFn = () => {
        Alert.alert(
            'Apagar arquivo',
            'Não é possível desfazer esta ação. Deseja mesmo apagar este arquivo ou imagem?',
            [
                {
                    text: 'Não, manter',
                    onPress: () => {},
                },
                {
                    text: 'Sim, apagar',
                    onPress: () => {
                        navigation.navigate('AnexoList', { apagar: state.newAnexo.id });
                    },
                },
            ],
            { cancelable: true }
        );
    };

    let thumbUri = null;
    const ehVideo = isVideo(state.newAnexo.mimetype);
    const ehImagem = isImage(state.newAnexo.mimetype);
    if (ehImagem || ehVideo) {
        thumbUri = state.newAnexo.localThumbUri || state.newAnexo.localFileUri;
    }
    const titleObject = !route.params.isPTTMode ? 'arquivo ou imagem' : 'anexo PTT';

    if (!state.newAnexo) {
        return <ToNoMapaLoading />;
    }

    const getErrorMsg = (key) => {
        const error = errors.find((e) => {
            return e[0] === key;
        });
        if (!error) {
            return null;
        }
        return error[1];
    };

    return (
        <>
            <View style={Layout.containerStretched}>
                <ToNoMapaAppbar
                    navigation={navigation}
                    title={state.editing ? 'Editar ' + titleObject : 'Novo ' + titleObject}
                    customGoBack={() => {
                        exitScreen(navigation);
                    }}
                />
                <KeyboardAwareScrollView
                    ref={scrollRef}
                    keyboardShouldPersistTaps="handled"
                    enableOnAndroid={true}>
                    <View
                        style={{ ...Layout.innerBody, paddingHorizontal: metrics.tenWidth * 3.2 }}>
                        {state.newAnexo.arquivo ? (
                            <>
                                {thumbUri ? (
                                    <View style={Anexos.itemImageWrapper}>
                                        <Image
                                            source={{ uri: thumbUri }}
                                            style={Anexos.itemImage}
                                        />
                                        {ehVideo && (
                                            <FontAwesome
                                                name="play-circle-o"
                                                color={colors.Foundation.TextColor.White}
                                                size={40}
                                                style={Anexos.playIcon}
                                            />
                                        )}
                                    </View>
                                ) : (
                                    <View style={Anexos.formIconWrapper}>
                                        <FontAwesome
                                            name={state.icone}
                                            size={metrics.tenHeight * 8}
                                            color={colors.Foundation.CorDeTexto.CorDeTexto}
                                        />
                                    </View>
                                )}
                                <ButtonOutlinedSecondary style={{ flex: 1 }} onPress={getAnexo}>
                                    Trocar arquivo...
                                </ButtonOutlinedSecondary>
                                {!getErrorMsg('mimetype') ? (
                                    <HelperText type="info">
                                        {state.newAnexo.fileSize ? (
                                            <Text>
                                                Tamanho:{' '}
                                                {formataTamanhoArquivo(state.newAnexo.fileSize)}
                                            </Text>
                                        ) : (
                                            <Text>
                                                {basename({
                                                    path:
                                                        state.newAnexo.localFileUri ||
                                                        state.newAnexo.arquivo,
                                                })}
                                            </Text>
                                        )}
                                    </HelperText>
                                ) : (
                                    <HelperText type="error">
                                        <Text>{getErrorMsg('mimetype')}</Text>
                                    </HelperText>
                                )}
                            </>
                        ) : (
                            <ButtonContainedSecondary
                                style={{ flex: 1, marginTop: metrics.tenHeight * 2 }}
                                onPress={getAnexo}>
                                Arquivo...
                            </ButtonContainedSecondary>
                        )}

                        {!!getErrorMsg('arquivo') && (
                            <HelperText type="error">{getErrorMsg('arquivo')}</HelperText>
                        )}

                        <TextInputDefault
                            label={
                                !route.params.isPTTMode
                                    ? 'Nome do arquivo/imagem'
                                    : 'Nome do anexo PTT'
                            }
                            required={true}
                            value={state.newAnexo.nome}
                            error={getErrorMsg('nome')}
                            onChangeText={(value) => {
                                setState({
                                    newAnexo: {
                                        ...state.newAnexo,
                                        nome: value,
                                    },
                                });
                            }}
                        />

                        <TextInputDefault
                            label={
                                isAnexoPTT(state.newAnexo.tipoAnexo)
                                    ? 'Descrição'
                                    : 'Descrição (opcional)'
                            }
                            multiline
                            numberOfLines={5}
                            value={state.newAnexo.descricao}
                            onChangeText={(value) => {
                                setState({
                                    newAnexo: {
                                        ...state.newAnexo,
                                        descricao: value,
                                    },
                                });
                            }}
                        />

                        {route.params.isPTTMode ? (
                            <GenericSelect
                                label="Tipo de anexo"
                                items={anexosPTTOptions()}
                                selectedValue={state.newAnexo.tipoAnexo}
                                onValueChange={(value) =>
                                    setState({
                                        newAnexo: {
                                            ...state.newAnexo,
                                            tipoAnexo: value,
                                        },
                                    })
                                }
                                info={TIPOS_ANEXO_INFO[state.newAnexo.tipoAnexo]}
                                error={getErrorMsg('tipoAnexo')}
                            />
                        ) : (
                            <View style={Layout.rowAnexoForm}>
                                <Text style={Layout.textLeftFromSwitch}>
                                    Este documento ou imagem é ata de reunião da comunidade?
                                </Text>
                                <Switch
                                    style={Layout.switchRight}
                                    color={colors.Foundation.Green.G300}
                                    value={state.newAnexo.tipoAnexo === 'ata'}
                                    onValueChange={() => {
                                        setState({
                                            newAnexo: {
                                                ...state.newAnexo,
                                                tipoAnexo:
                                                    state.newAnexo.tipoAnexo === 'ata' ? '' : 'ata',
                                            },
                                        });
                                    }}
                                />
                            </View>
                        )}
                        <View style={Layout.rowAnexoForm}>
                            <Text style={Layout.textLeftFromSwitch}>
                                Este {!route.params.isPTTMode ? 'documento ou imagem' : 'anexo'}{' '}
                                pode ser disponibilizado publicamente?
                            </Text>
                            <Switch
                                style={Layout.switchRight}
                                color={colors.Foundation.Green.G300}
                                value={state.newAnexo.publico}
                                onValueChange={() => {
                                    setState({
                                        newAnexo: {
                                            ...state.newAnexo,
                                            publico: !state.newAnexo.publico,
                                        },
                                    });
                                }}
                            />
                        </View>
                        <View
                            style={{
                                ...Layout.buttonsBottomWrapper,
                                marginTop: metrics.tenHeight * 6,
                            }}>
                            {state.editing ? (
                                <View style={Layout.row}>
                                    <ButtonCancel
                                        style={{
                                            flex: 1,
                                            marginRight: metrics.tenWidth * 0.8,
                                        }}
                                        icon="trash-can-outline"
                                        onPress={apagarFn}>
                                        Apagar
                                    </ButtonCancel>
                                    <ButtonContainedPrimary
                                        style={{
                                            flex: 1,
                                            marginLeft: metrics.tenWidth * 0.8,
                                        }}
                                        onPress={salvarFn}>
                                        Salvar
                                    </ButtonContainedPrimary>
                                </View>
                            ) : (
                                <>
                                    {state.newAnexo.arquivo && (
                                        <ButtonFullwidthPrimary onPress={salvarFn}>
                                            Salvar
                                        </ButtonFullwidthPrimary>
                                    )}
                                </>
                            )}
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
            {carregando && <ToNoMapaLoading isOverlay={true} />}
        </>
    );
}
