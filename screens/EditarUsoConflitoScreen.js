import React, { useCallback, useEffect, useReducer, useRef, useState } from 'react';
import { Alert, View, BackHandler } from 'react-native';
import { RadioButton, Text } from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useFocusEffect } from '@react-navigation/native';

import { Inputs, Layout } from '../styles';
import TELAS from '../bases/telas';
import { LABELS_CONFLITO, LABELS_USO } from '../bases/dadosbasicos';
import { parseLocaleNumber, toFixedLocale, toCleanLocale, geraTmpId } from '../utils/utils';
import metrics from '../utils/metrics';

import { TextInputDefault, TipoAreaDeUsoSelect, TipoConflitoSelect } from '../components/inputs';
import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import {
    ButtonCancel,
    ButtonContainedPrimary,
    ButtonFullwidthPrimary,
} from '../components/buttons';

export default function EditarUsoConflitoScreen({ navigation, route }) {
    let initialObjectState = {
        id: null,
        __typename: 'AreaDeUsoType',
        nome: null,
        posicao: null,
        descricao: null,
        area: null,
        tipoConflitoId: null,
        tipoAreaDeUsoId: null,
    };
    if (route.params && 'usoOuConflito' in route.params) {
        initialObjectState = {
            ...initialObjectState,
            ...route.params.usoOuConflito,
            area: toFixedLocale(route.params.usoOuConflito.area),
        };
    }

    const reducer = (state, newState) => {
        return { ...state, ...newState };
    };

    /**
     * @type object
     */
    const [objectState, setObjectState] = useReducer(reducer, initialObjectState);

    const [errors, setErrors] = useState([]);

    const scrollRef = useRef(null);

    const emEdicao = objectState.id !== null;
    const labels = objectState.__typename === 'ConflitoType' ? LABELS_CONFLITO : LABELS_USO;
    const title = emEdicao ? labels.editTitle : labels.title;
    const msg = emEdicao ? labels.snackBarMessageEdited : labels.snackBarMessage;

    useEffect(() => {
        if (scrollRef.current) {
            scrollRef.current.scrollToPosition(0, 0, true);
        }
    }, []);

    //Android Back button
    useFocusEffect(
        useCallback(() => {
            const onBackPress = () => {
                navigation.navigate('Home', { tela: TELAS.MAPEAR_USOSECONFLITOS });
                return true;
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [navigation])
    );

    const validarUsoOuConflito = () => {
        const newErrors = [];
        if (!objectState.nome || objectState.nome.trim().length === 0) {
            newErrors.push(['nome', 'O nome é obrigatório!']);
        }

        if (objectState.nome && objectState.nome.trim().length > 45) {
            newErrors.push([
                'nome',
                `${labels.nome} está muito grande. Favor reduzir para menos de 45 letras`,
            ]);
        }

        const campo =
            objectState.__typename === 'AreaDeUsoType' ? 'tipoAreaDeUsoId' : 'tipoConflitoId';
        if (!objectState[campo]) {
            newErrors.push(['tipo', `${labels.erroTipo}`]);
        }
        setErrors(newErrors);
        if (newErrors.length > 0) {
            return false;
        }

        return true;
    };

    const apagarFn = () => {
        const usoOuConflitoEditado = {
            ...objectState,
        };
        const tipoTxt =
            usoOuConflitoEditado.__typename === 'AreaDeUsoType' ? 'local de uso' : 'conflito?';
        let titleAlert = `Tem certeza que quer apagar este ${tipoTxt}?`;
        Alert.alert(
            titleAlert,
            'Você não poderá desfazer esta ação. Tem certeza?',
            [
                {
                    text: 'Não',
                },
                {
                    text: 'Sim, quero apagar',
                    onPress: async () => {
                        const labelTipoId =
                            usoOuConflitoEditado.__typename === 'AreaDeUsoType'
                                ? 'tipoConflitoId'
                                : 'tipoAreaDeUsoId';
                        delete usoOuConflitoEditado[labelTipoId];
                        navigation.navigate('Home', {
                            tela: TELAS.MAPEAR_USOSECONFLITOS,
                            apagarUsoOuConflito: true,
                            usoOuConflitoEditado,
                            snackBarMessage: labels.snackBarMessageDelete,
                        });
                    },
                },
            ],
            { cancelable: true }
        );
    };

    const salvarFn = () => {
        if (validarUsoOuConflito()) {
            const newId = objectState.id || geraTmpId();
            const usoOuConflitoEditado = {
                ...objectState,
                id: newId,
            };

            if (usoOuConflitoEditado.__typename === 'AreaDeUsoType') {
                delete usoOuConflitoEditado.tipoConflitoId;
                if (usoOuConflitoEditado.area !== null) {
                    usoOuConflitoEditado.area = parseLocaleNumber(usoOuConflitoEditado.area);
                    if (usoOuConflitoEditado.area === 0) {
                        usoOuConflitoEditado.area = null;
                    }
                }
            } else {
                delete usoOuConflitoEditado.tipoAreaDeUsoId;
                delete usoOuConflitoEditado.area;
            }

            navigation.navigate('Home', {
                usoOuConflitoEditado,
                tela: TELAS.MAPEAR_USOSECONFLITOS,
                snackBarMessage: msg,
            });
        }
    };

    const cancelarFn = () => {
        navigation.navigate('Home');
    };

    const getErrorMsg = (key) => {
        const error = errors.find((e) => {
            return e[0] === key;
        });
        if (!error) {
            return null;
        }
        return error[1];
    };

    useEffect(() => {
        if (errors.length > 0) {
            validarUsoOuConflito();
        }
    }, [objectState]);

    return (
        <View style={Layout.containerStretched}>
            <ToNoMapaAppbar navigation={navigation} title={title} />
            <View style={Layout.bodyWithBottomBar}>
                <KeyboardAwareScrollView
                    ref={scrollRef}
                    keyboardShouldPersistTaps="handled"
                    enableOnAndroid={true}
                    extraScrollHeight={120}>
                    <View style={Layout.innerBody}>
                        <View
                            style={{
                                marginTop: metrics.tenHeight * 2,
                                marginBottom: metrics.tenHeight * 2,
                            }}>
                            <RadioButton.Group
                                onValueChange={(typename) =>
                                    setObjectState({ __typename: typename })
                                }
                                value={objectState.__typename}>
                                <View style={Inputs.checkBoxes}>
                                    <RadioButton value="AreaDeUsoType" />
                                    <Text>Local de uso</Text>
                                </View>
                                <View style={Inputs.checkBoxes}>
                                    <RadioButton value="ConflitoType" />
                                    <Text>Local de conflito</Text>
                                </View>
                            </RadioButton.Group>
                        </View>

                        {objectState.__typename === 'ConflitoType' ? (
                            <TipoConflitoSelect
                                selectedValue={objectState.tipoConflitoId}
                                onValueChange={(value) => setObjectState({ tipoConflitoId: value })}
                                error={getErrorMsg('tipo')}
                            />
                        ) : (
                            <TipoAreaDeUsoSelect
                                selectedValue={objectState.tipoAreaDeUsoId}
                                onValueChange={(value) =>
                                    setObjectState({ tipoAreaDeUsoId: value })
                                }
                                error={getErrorMsg('tipo')}
                            />
                        )}

                        <TextInputDefault
                            label={`${labels.nome} (até 45 letras)`}
                            required={true}
                            value={objectState.nome}
                            onChangeText={(novoNome) => setObjectState({ nome: novoNome })}
                            error={getErrorMsg('nome')}
                        />

                        {objectState.__typename === 'AreaDeUsoType' && (
                            <TextInputDefault
                                label={labels.area}
                                value={objectState.area}
                                onChangeText={(novaArea) =>
                                    setObjectState({
                                        area: toCleanLocale(novaArea),
                                    })
                                }
                                keyboardType="decimal-pad"
                            />
                        )}

                        <TextInputDefault
                            label={labels.descricao}
                            multiline={true}
                            numberOfLines={3}
                            value={objectState.descricao}
                            onChangeText={(novaDescricao) =>
                                setObjectState({ descricao: novaDescricao })
                            }
                        />

                        <View style={Layout.buttonsBottomWrapperScroll}>
                            {emEdicao ? (
                                <View style={Layout.row}>
                                    <ButtonCancel
                                        icon="trash-can-outline"
                                        onPress={apagarFn}
                                        style={{ flex: 1, marginRight: metrics.tenWidth * 0.8 }}>
                                        Apagar
                                    </ButtonCancel>
                                    <ButtonContainedPrimary
                                        onPress={salvarFn}
                                        style={{ flex: 1, marginLeft: metrics.tenWidth * 0.8 }}>
                                        Salvar
                                    </ButtonContainedPrimary>
                                </View>
                            ) : (
                                <View style={Layout.buttonRight}>
                                    <ButtonFullwidthPrimary onPress={salvarFn}>
                                        Salvar
                                    </ButtonFullwidthPrimary>
                                </View>
                            )}
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        </View>
    );
}
