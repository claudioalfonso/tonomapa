import React, { useState, useRef, useEffect, useCallback } from 'react';
import { TouchableHighlight, BackHandler, View, Alert } from 'react-native';
import * as Clipboard from 'expo-clipboard';
import { IconButton, Switch, Text, Portal } from 'react-native-paper';
import { useFocusEffect } from '@react-navigation/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useLazyQuery, useMutation, useQuery } from '@apollo/client';
import * as Updates from 'expo-updates';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import { Layout } from '../styles';
import colors from '../assets/colors';
import {
    carregaSettings,
    persisteSettings,
    populateCachedData,
    preparaDeleteUsuaria,
    tonomapaCache,
} from '../db/api';
import TELAS from '../bases/telas';
import metrics from '../utils/metrics';

import { MAP_TYPES } from 'react-native-maps';
import { DELETE_USUARIA, GET_USER_DATA, GET_USER_DATA_WITH_TERRITORIO_ID } from '../db/queries';
import { logError, log } from '../utils/utils';

import ToNoMapaSnackbar from '../components/ToNoMapaSnackbar';
import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import ToNoMapaLoading from '../components/ToNoMapaLoading';
import DialogTerritorioSwitcherIntro from '../components/inputs/DialogTerritorioSwitcherIntro';
import {
    DialogTerritorioSwitcher,
    DialogUnidadeSwitcher,
    DialogModoMapaSwitcher,
} from '../components/inputs';
import {
    ButtonCancel,
    ButtonFullwidthPrimary,
    ButtonOutlinedSecondary,
    ButtonTextSecondary,
} from '../components/buttons';

let snackBarMessage = '';

const SNACKBAR_MESSAGES = {
    CONFIGURACOES: 'Configurações atualizadas com sucesso',
    COMUNIDADE_ALTERADA: 'Comunidade alterada com sucesso!',
    CODIGO_SEGURANCA_COPIADO: 'Código de segurança copiado!',
    CODIGO_SEGURANCA_NAO_COPIADO:
        'Erro: não foi possível copiar o código de segurança! Anote diretamente.',
};

export default function ConfiguracoesScreen({ navigation, route }) {
    const { client, signOut, online, isSyncing, syncStatus } = useToNoMapa();

    const scrollRef = useRef(null);

    const [settings, setSettings] = useState(null);

    const [loading, setLoading] = useState(false);

    const { data: currentUserData } = useQuery(GET_USER_DATA);
    const currentUser = currentUserData ? currentUserData.currentUser : null;

    /**
     * @type object
     */
    const [snackBarVisible, setSnackBarVisible] = useState(false);

    /**
     * @type object
     */
    const [dialogListaTerritoriosVisible, setDialogListaTerritoriosVisible] = useState(false);

    /**
     * @type object
     */
    const [dialogListaUnidadesVisible, setDialogListaUnidadesVisible] = useState(false);

    /**
     * @type object
     */
    const [dialogModoMapaVisible, setDialogModoMapaVisible] = useState(false);

    /**
     * @type object
     */
    const [novoTerritorio, setNovoTerritorio] = useState(null);

    const [deleteUsuaria] = useMutation(DELETE_USUARIA);

    const persisteNewTerritorio = (data) => {
        // snackBarMessage = SNACKBAR_MESSAGES.COMUNIDADE_ALTERADA;
        setLoading(false);
        setNovoTerritorio(null);
        populateCachedData(data, true);
        tonomapaCache.writeQuery({
            query: GET_USER_DATA,
            data,
        });
        navigation.navigate('Home', {
            newTerritorioId: data.currentUser.currentTerritorio.id,
            snackBarMessage: `Você está mapeando ${data.currentUser.currentTerritorio.nome}`,
        });
    };

    const [getAnotherTerritorio] = useLazyQuery(GET_USER_DATA_WITH_TERRITORIO_ID, {
        fetchPolicy: 'network-only',
        onError: (error) => {
            console.log('erro pegando território. Internet?', error);
            log(`configuracoesScreen: Erro pegando território. Internet?`);
            logError(error);
            // TODO: tratar este erro!
            setNovoTerritorio(null);
        },
    });

    const handleTerritorioSwitch = () => {
        if (novoTerritorio.id !== 'criarTerritorio') {
            setLoading(true);
            getAnotherTerritorio({
                variables: {
                    territorioId: novoTerritorio.id,
                },
            }).then((res) => {
                updateSettings({
                    settings: { showAtaBalloon: true, useGPS: false },
                    noSnackBarMessage: true,
                });
                const newCurrentTerritorioId = res.data.currentUser.currentTerritorio.id;
                if (newCurrentTerritorioId !== currentUser.currentTerritorio.id) {
                    persisteNewTerritorio(res.data);
                }
            });
            return;
        }
        setNovoTerritorio(null);
        updateSettings({
            settings: { showAtaBalloon: true, useGPS: false },
            noSnackBarMessage: true,
        });
        navigation.navigate('CadastroTerritorio');
    };

    const exitScreen = () => {
        navigation.navigate('Home', { tela: TELAS.MAPA });
        return true;
    };

    useEffect(() => {
        const newSettings = carregaSettings(client);
        setSettings(newSettings);
    }, []);

    useEffect(() => {
        if (currentUser) {
            if (scrollRef.current) {
                scrollRef.current.scrollToPosition(0, 0, true);
            }
        }
    }, [currentUser]);

    if (route.params && 'editaComunidades' in route.params) {
        delete route.params.editaComunidades;
        setDialogListaTerritoriosVisible(true);
    }

    const updateSettings = (params) => {
        persisteSettings(params.settings, client).then((newSettings) => {
            /*DEBUG*/ //console.log(params.settings, newSettings);
            setSettings(newSettings);
            if (params.restartApp) {
                Alert.alert(
                    'O Tô no Mapa será reinicializado',
                    'Para aplicar essa configuração, o aplicativo será reiniciado.\n\nTudo bem?',
                    [
                        {
                            text: 'Cancelar',
                            onPress: () => {
                                updateSettings({
                                    settings: params.revertSettings,
                                    noSnackBarMessage: true,
                                });
                            },
                        },
                        {
                            text: 'Ok',
                            onPress: async () => {
                                await Updates.reloadAsync();
                            },
                        },
                    ],
                    { cancelable: false }
                );
                return;
            }
            if (!params.noSnackBarMessage) {
                snackBarMessage = SNACKBAR_MESSAGES.CONFIGURACOES;
                setSnackBarVisible(true);
            }
        });
    };

    //Android Back button
    useFocusEffect(
        useCallback(() => {
            const onBackPress = () => {
                return exitScreen();
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [navigation])
    );

    const alertApagarConta = () => {
        const title = online ? 'Apagar sua conta no Tô no Mapa' : 'Sem internet';

        const body = online
            ? 'Em atendimento à Lei Geral de Proteção de Dados, você pode apagar sua conta do Tô no Mapa.\n\nTodas as suas informações pessoais serão apagadas. Essa ação não tem volta. Para voltar a usar o app, você terá que criar uma nova conta.\n\nTem certeza que quer apagar sua conta no Tô no Mapa?'
            : 'Só dá pra apagar sua conta com internet.\n\nPor favor, tente mais tarde!';
        const actions = online
            ? [
                  {
                      text: 'Não',
                      onPress: async () => {},
                  },
                  {
                      text: 'Sim, apagar conta',
                      onPress: async () => {
                          const localId = currentUser.id;
                          const mutateData = preparaDeleteUsuaria({ localId });
                          await deleteUsuaria(mutateData);
                          alertByeBye();
                      },
                  },
              ]
            : [
                  {
                      text: 'Entendi',
                      onPress: async () => {},
                  },
              ];
        Alert.alert(title, body, actions, { cancelable: true });
    };

    const alertByeBye = () => {
        setTimeout(() => {
            signOut();
        }, 3000);
        Alert.alert(
            'Conta apagada com sucesso!',
            'Quando quiser voltar, você será bem vinda(o) novamente!\n\nAté breve ;-)',
            [
                {
                    text: 'Sair',
                    onPress: async () => {
                        signOut();
                    },
                },
            ],
            { cancelable: false }
        );
    };

    const alertSair = () => {
        Alert.alert(
            'Desconectar do Tô no Mapa',
            `Ao se desconectar, você poderá se conectar novamente usando o seu telefone e código de segurança.\n\nAnote o código de segurança para poder voltar a se conectar!\n\nSeu código de segurança é ${currentUser.codigoUsoCelular}\n\nTem certeza que quer se desconectar do Tô no Mapa?`,
            [
                {
                    text: 'Não',
                    onPress: async () => {},
                },
                {
                    text: 'Sim, desconectar',
                    onPress: async () => {
                        signOut();
                    },
                },
            ],
            { cancelable: true }
        );
    };

    if (loading || !settings || !currentUser) {
        return <ToNoMapaLoading />;
    }

    return (
        <View style={Layout.containerStretched}>
            <ToNoMapaAppbar
                navigation={navigation}
                title="Configurações"
                customGoBack={exitScreen}
                rightActions={[{ ...syncStatus }]}
            />
            <View>
                <KeyboardAwareScrollView
                    ref={scrollRef}
                    keyboardShouldPersistTaps="handled"
                    enableOnAndroid={true}>
                    <View style={Layout.innerBody}>
                        {currentUser.codigoUsoCelular && (
                            <View style={Layout.rowConfiguracoesComDesc}>
                                <TouchableHighlight
                                    underlayColor={colors.Foundation.Green.G300}
                                    onPress={() => {
                                        Clipboard.setStringAsync(currentUser.codigoUsoCelular).then(
                                            (wasCopied) => {
                                                snackBarMessage = wasCopied
                                                    ? SNACKBAR_MESSAGES.CODIGO_SEGURANCA_COPIADO
                                                    : SNACKBAR_MESSAGES.CODIGO_SEGURANCA_NAO_COPIADO;
                                                setSnackBarVisible(true);
                                            }
                                        );
                                    }}>
                                    <View style={Layout.row}>
                                        <Text style={Layout.text}>Código de Segurança</Text>
                                        <View
                                            style={{
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                            }}>
                                            <Text style={Layout.text}>
                                                {currentUser.codigoUsoCelular}
                                            </Text>
                                            <IconButton
                                                icon="content-copy"
                                                color={colors.Foundation.Green.G300}
                                                size={16}
                                            />
                                        </View>
                                    </View>
                                </TouchableHighlight>
                                <Text style={Layout.descConfiguracoes}>
                                    Não perca o seu código de segurança! Você precisará dele e do
                                    seu número de telefone se for reinstalar o aplicativo ou trocar
                                    de aparelho.
                                </Text>
                            </View>
                        )}

                        <View style={Layout.secaoConfiguracoes}>
                            <Text style={Layout.secaoConfiguracoesText}>
                                Comunidade: {currentUser.currentTerritorio.nome}
                            </Text>
                        </View>

                        <View style={Layout.rowConfiguracoes}>
                            <ButtonOutlinedSecondary
                                icon="swap-horizontal-bold"
                                onPress={() => {
                                    setDialogListaTerritoriosVisible(true);
                                }}>
                                Mudar ou criar comunidade
                            </ButtonOutlinedSecondary>
                        </View>

                        <View style={Layout.secaoConfiguracoes}>
                            <Text style={Layout.secaoConfiguracoesText}>Opções do mapa</Text>
                        </View>

                        <View style={Layout.rowConfiguracoes}>
                            <Text style={Layout.text}>Visão do mapa</Text>
                            <ButtonTextSecondary
                                labelStyle={{ marginRight: 0 }}
                                onPress={() => {
                                    setDialogModoMapaVisible(true);
                                }}>
                                {settings.mapType === MAP_TYPES.HYBRID ? 'Satélite' : 'Mapa'}
                            </ButtonTextSecondary>
                        </View>

                        <View style={Layout.rowConfiguracoes}>
                            <Text style={Layout.text}>Unidade de área</Text>
                            <ButtonTextSecondary
                                labelStyle={{ marginRight: 0 }}
                                onPress={() => {
                                    setDialogListaUnidadesVisible(true);
                                }}>
                                {settings.unidadeArea.nome} ({settings.unidadeArea.sigla})
                            </ButtonTextSecondary>
                        </View>

                        <View style={Layout.rowConfiguracoes}>
                            <Text style={Layout.textLeftFromSwitch}>
                                Edição das áreas (polígonos)
                            </Text>
                            <Switch
                                style={Layout.switchRight}
                                color={colors.Foundation.Green.G300}
                                value={settings.editaPoligonos}
                                onValueChange={() => {
                                    settings.editaPoligonos = !settings.editaPoligonos;
                                    updateSettings({
                                        settings: {
                                            editaPoligonos: settings.editaPoligonos,
                                        },
                                    });
                                }}
                            />
                        </View>

                        {/*<View style={Layout.rowConfiguracoes}>
                            <Text style={Layout.textLeftFromSwitch}>
                                Mapear do jeito antigo do Tô no Mapa
                            </Text>
                            <Switch
                                style={Layout.switchRight}
                                color={colors.Foundation.Green.G300}
                                value={settings.useMapLegacy}
                                onValueChange={() => {
                                    settings.useMapLegacy = !settings.useMapLegacy;
                                    updateSettings({
                                        settings: {
                                            useMapLegacy: settings.useMapLegacy,
                                        },
                                    });
                                }}
                            />
                            </View>*/}

                        <View style={Layout.buttonsBottomWrapperScroll}>
                            <ButtonFullwidthPrimary onPress={alertSair}>
                                Desconectar
                            </ButtonFullwidthPrimary>
                            <ButtonCancel
                                onPress={alertApagarConta}
                                style={{ flex: 1, marginTop: metrics.tenHeight * 4 }}>
                                Apagar conta
                            </ButtonCancel>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>

            <ToNoMapaSnackbar
                visible={snackBarMessage && snackBarVisible === true}
                setVisible={setSnackBarVisible}
                message={snackBarMessage}
            />

            <Portal>
                <DialogTerritorioSwitcherIntro
                    currentUser={currentUser}
                    navigation={navigation}
                    novoTerritorio={novoTerritorio}
                    setNovoTerritorio={setNovoTerritorio}
                    dialogListaTerritoriosVisible={dialogListaTerritoriosVisible}
                    setDialogListaTerritoriosVisible={setDialogListaTerritoriosVisible}
                />

                <DialogTerritorioSwitcher
                    novoTerritorio={novoTerritorio}
                    setNovoTerritorio={setNovoTerritorio}
                    online={online}
                    isSyncing={isSyncing}
                    handleTerritorioSwitch={handleTerritorioSwitch}
                />

                <DialogModoMapaSwitcher
                    settings={settings}
                    updateSettings={updateSettings}
                    dialogModoMapaVisible={dialogModoMapaVisible}
                    setDialogModoMapaVisible={setDialogModoMapaVisible}
                />

                <DialogUnidadeSwitcher
                    settings={settings}
                    updateSettings={updateSettings}
                    dialogListaUnidadesVisible={dialogListaUnidadesVisible}
                    setDialogListaUnidadesVisible={setDialogListaUnidadesVisible}
                />
            </Portal>
        </View>
    );
}
