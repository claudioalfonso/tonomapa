import React from 'react';
import { View } from 'react-native';
import { WebView } from 'react-native-webview';
import { Button } from 'react-native-paper';
import Constants from 'expo-constants';

import { Layout } from '../styles';

import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import ToNoMapaLoading from '../components/ToNoMapaLoading';

export default function TermosDeUsoScreen({ navigation }) {
    const ActivityIndicatorElement = () => {
        return <ToNoMapaLoading isOverlay={true} />;
    };

    return (
        <View style={Layout.containerStretched}>
            <ToNoMapaAppbar
                navigation={navigation}
                title="Termos de Uso - Tô no Mapa"
                goBack={false}
            />
            <View style={{ ...Layout.containerStretched, paddingBottom: 20 }}>
                <WebView
                    source={{ uri: Constants.manifest.extra.termosDeUsoUri }}
                    startInLoadingState={true}
                    renderLoading={ActivityIndicatorElement}
                />
                <Button
                    mode="contained"
                    dark={true}
                    style={Layout.buttonCenter}
                    onPress={() => {
                        navigation.navigate('Sobre');
                    }}>
                    Voltar
                </Button>
            </View>
        </View>
    );
}
