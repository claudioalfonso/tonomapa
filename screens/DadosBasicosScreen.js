import React, { useCallback, useReducer, useRef, useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { useQuery } from '@apollo/client';
import { View, Alert, BackHandler } from 'react-native';
import { Text, Switch, HelperText } from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as DocumentPicker from 'expo-document-picker';
import * as FileSystem from 'expo-file-system';
import XMLParser from 'react-xml-parser';

import { kml } from '../maps/togeojson';
import { pluralize, formataNumeroBR, deepEqual } from '../utils/utils';
import { Inputs, Layout } from '../styles';
import colors from '../assets/colors';
import TELAS from '../bases/telas';
import { adjustTerritorioFieldTypes } from '../bases/dadosbasicos';
import STATUS from '../bases/status';
import { GET_USER_DATA } from '../db/queries';
import metrics from '../utils/metrics';

import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import ToNoMapaLoading from '../components/ToNoMapaLoading';
import { MunicipioSelectH, TextInputDefault, TipoComunidadeSelect } from '../components/inputs';
import DefaultBottomBar from '../components/DefaultBottomBar';
import {
    ButtonFullwidthPrimary,
    ButtonFullwidthSecondary,
    ButtonOutlinedSecondary,
} from '../components/buttons';

export default function DadosBasicosScreen({ navigation, route }) {
    const initialState = {
        id: null,
        nome: null,
        anoFundacao: null,
        qtdeFamilias: null,
        estadoId: null,
        municipioReferenciaId: null,
        tiposComunidade: [],
        publico: false,
        poligono: null,
        poligonoArq: null,
        loading: true,
    };

    const reducer = (state, newState) => {
        return { ...state, ...newState };
    };

    /**
     * @type object
     */
    const [state, setState] = useReducer(reducer, initialState);

    const [errors, setErrors] = useState([]);

    const scrollRef = useRef(null);

    const { data: currentUserData, loading: currentUserLoading } = useQuery(GET_USER_DATA, {
        notifyOnNetworkStatusChange: true,
    });
    const currentUser = currentUserData ? currentUserData.currentUser : null;

    const exitScreen = () => {
        navigation.navigate('Home');
        return true;
    };

    useFocusEffect(
        useCallback(() => {
            if (currentUser) {
                let estadoId = null;
                let municipioReferenciaId = null;
                if (!!currentUser.currentTerritorio.municipioReferenciaId) {
                    estadoId = parseInt(
                        currentUser.currentTerritorio.municipioReferenciaId
                            .toString()
                            .substring(0, 2)
                    );
                    municipioReferenciaId = parseInt(
                        currentUser.currentTerritorio.municipioReferenciaId
                    );
                }
                let tiposComunidade = [];
                for (let tipoComunidade of currentUser.currentTerritorio.tiposComunidade) {
                    tiposComunidade.push(tipoComunidade.id);
                }
                setState({
                    ...currentUser.currentTerritorio,
                    estadoId,
                    municipioReferenciaId,
                    anoFundacao: currentUser.currentTerritorio.anoFundacao
                        ? currentUser.currentTerritorio.anoFundacao.toString()
                        : null,
                    qtdeFamilias: currentUser.currentTerritorio.qtdeFamilias
                        ? currentUser.currentTerritorio.qtdeFamilias.toString()
                        : null,
                    tiposComunidade,
                    poligono: null,
                    loading: false,
                });
            }
        }, [currentUser])
    );

    useFocusEffect(
        useCallback(() => {
            if (scrollRef.current) {
                scrollRef.current.scrollToPosition(0, 0, true);
            }
        }, [])
    );

    const validarTerritorio = () => {
        const newErrors = [];
        if (!state.nome || 0 === state.nome.trim().length) {
            newErrors.push(['nome', 'O nome da comunidade é obrigatório!']);
        }

        if (!state.municipioReferenciaId) {
            newErrors.push(['municipioReferenciaId', 'O município de referência é obrigatório!']);
        }
        setErrors(newErrors);
        if (newErrors.length > 0) {
            // let message = 'Os seguintes dados precisam ser fornecidos:\n\n';
            // for (let erro of errors) {
            //     message += erro[1] + '\n';
            // }

            // Alert.alert(
            //     'Dados incompletos',
            //     message,
            //     [
            //         {
            //             text: 'OK',
            //             onPress: async () => {},
            //         },
            //     ],
            //     { cancelable: false }
            // );
            return false;
        }
        return true;
    };

    const salvarFn = async () => {
        if (validarTerritorio()) {
            const newCurrentUser = {
                ...currentUser,
                currentTerritorio: {
                    ...currentUser.currentTerritorio,
                    nome: state.nome,
                    municipioReferenciaId: state.municipioReferenciaId,
                    anoFundacao: state.anoFundacao,
                    qtdeFamilias: state.qtdeFamilias,
                    tiposComunidade: state.tiposComunidade,
                    publico: state.publico,
                },
            };
            if (state.poligono) {
                newCurrentUser.currentTerritorio.poligono.coordinates = [
                    ...currentUser.currentTerritorio.poligono.coordinates,
                    ...state.poligono.coordinates,
                ];
            }
            newCurrentUser.currentTerritorio = adjustTerritorioFieldTypes(
                newCurrentUser.currentTerritorio
            );

            setState({ poligonoArq: null });

            const params = { tela: TELAS.MAPA };

            if (route.params && 'cadastrarTerritorio' in route.params) {
                newCurrentUser.currentTerritorio.statusId = STATUS.EM_PREENCHIMENTO;
            }

            if (!deepEqual(newCurrentUser, currentUser)) {
                params.newCurrentUser = newCurrentUser;
                params.snackBarMessage = 'As informações da comunidade foram atualizadas!';
            }

            navigation.navigate('Home', params);
        }
    };

    const openKmlImport = () => {
        Alert.alert(
            'Importar arquivo KML',
            'Escolha o arquivo KML com os polígonos que delimitam o território. Esta função é avançada e não é necessária: você pode registrar as áreas da sua comunidade diretamente no app! Se você não sabe o que é KML, ignore este campo.',
            [
                {
                    text: 'Cancelar',
                    onPress: () => {},
                },
                {
                    text: 'Selecionar arquivo KML',
                    onPress: async () => {
                        const result = await DocumentPicker.getDocumentAsync({
                            type: 'application/vnd.google-earth.kml+xml',
                        });
                        if (result.type === 'success') {
                            const kmlContent = await FileSystem.readAsStringAsync(result.uri, {});
                            const xmlDoc = new XMLParser().parseFromString(kmlContent);
                            const json = kml(xmlDoc);

                            let polygons = [];
                            for (let element of json.features) {
                                if (element.geometry.type === 'Polygon') {
                                    let coordinates = [];
                                    for (let polygonPart of element.geometry.coordinates) {
                                        for (let i = 0; i < polygonPart.length - 1; i++) {
                                            let coordinate = polygonPart[i];
                                            coordinates.push({
                                                longitude: coordinate[0],
                                                latitude: coordinate[1],
                                            });
                                        }
                                    }
                                    polygons.push(coordinates);
                                }
                            }

                            //Agora ao formato graphQl
                            const multiPol = {
                                type: 'MultiPolygon',
                                coordinates: [],
                            };

                            for (let polygon of polygons) {
                                let backPolygon = [[]];
                                for (let i in polygon) {
                                    backPolygon[0].push([
                                        polygon[i].longitude,
                                        polygon[i].latitude,
                                    ]);
                                }
                                backPolygon[0].push([polygon[0].longitude, polygon[0].latitude]);
                                multiPol.coordinates.push(backPolygon);
                            }

                            const tamanho = formataNumeroBR(result.size / 1024);

                            const poligonoArq = {
                                nome: result.name,
                                tamanho: tamanho,
                            };

                            setState({
                                poligono: multiPol,
                                poligonoArq: poligonoArq,
                            });
                        }
                    },
                },
            ],
            { cancelable: true }
        );
    };

    //Android Back button
    useFocusEffect(
        useCallback(() => {
            const onBackPress = () => {
                return exitScreen();
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [navigation])
    );

    if (!currentUser || currentUserLoading || state.loading) {
        return <ToNoMapaLoading />;
    }

    const getErrorMsg = (key) => {
        const error = errors.find((e) => {
            return e[0] === key;
        });
        if (!error) {
            return null;
        }
        return error[1];
    };

    // useEffect(() => {
    //     if (errors.length > 0) {
    //         validarTerritorio();
    //     }
    // }, [state.nome, state.municipioId]);

    return (
        <View style={Layout.containerStretched}>
            <ToNoMapaAppbar
                navigation={navigation}
                title="Dados da comunidade"
                customGoBack={exitScreen}
            />
            <View style={Layout.bodyWithBottomBar}>
                <KeyboardAwareScrollView
                    ref={scrollRef}
                    keyboardShouldPersistTaps="handled"
                    enableOnAndroid={true}
                    extraScrollHeight={200}>
                    <View style={Layout.innerBody}>
                        <TextInputDefault
                            label="Nome da comunidade"
                            required={true}
                            value={state.nome}
                            onChangeText={(value) => setState({ nome: value })}
                            error={getErrorMsg('nome')}
                        />

                        <TextInputDefault
                            label="Ano de criação"
                            keyboardType={'number-pad'}
                            maxLength={4}
                            value={state.anoFundacao}
                            onChangeText={(value) => setState({ anoFundacao: value })}
                        />

                        <MunicipioSelectH
                            estadoId={state.estadoId}
                            municipioId={state.municipioReferenciaId}
                            onValueChange={(value) => {
                                const newState = {
                                    municipioReferenciaId: value.municipioId,
                                };
                                if (value.hasOwnProperty('estadoId')) {
                                    newState.estadoId = value.estadoId;
                                }
                                setState(newState);
                            }}
                            required={true}
                            error={getErrorMsg('municipioReferenciaId')}
                        />

                        <TextInputDefault
                            label="Quantidade de famílias"
                            keyboardType="phone-pad"
                            value={state.qtdeFamilias}
                            onChangeText={(value) => setState({ qtdeFamilias: value })}
                        />

                        <TipoComunidadeSelect
                            selectedItems={state.tiposComunidade}
                            onValueChange={(values) => {
                                setState({ tiposComunidade: values });
                            }}
                        />

                        <View style={{ marginTop: metrics.tenHeight * 2 }}>
                            {state.poligonoArq != null ? (
                                <>
                                    <ButtonFullwidthSecondary onPress={openKmlImport}>
                                        Trocar arquivo...
                                    </ButtonFullwidthSecondary>
                                    <HelperText type="info">
                                        {state.poligonoArq.nome} ({state.poligonoArq.tamanho}Kb) -{' '}
                                        {pluralize(
                                            state.poligono.coordinates.length,
                                            '1 polígono importado',
                                            state.poligono.coordinates.length +
                                                ' polígonos importados'
                                        )}
                                    </HelperText>
                                </>
                            ) : (
                                <>
                                    <ButtonOutlinedSecondary onPress={openKmlImport}>
                                        Importar KML...
                                    </ButtonOutlinedSecondary>
                                    <HelperText type="info">
                                        Avançado: você pode importar a área da comunidade de um
                                        arquivo KML
                                    </HelperText>
                                </>
                            )}
                        </View>

                        <View style={{ ...Layout.rowAnexoForm, ...Inputs.textInput }}>
                            <Text style={Layout.textLeftFromSwitch}>
                                Dados públicos: Os dados desta comunidade podem ser disponibilizados
                                publicamente?
                            </Text>
                            <Switch
                                style={Layout.switchRight}
                                color={colors.Foundation.Green.G300}
                                value={state.publico}
                                onValueChange={() => {
                                    setState({ publico: !state.publico });
                                }}
                            />
                        </View>

                        <View style={{ marginTop: metrics.tenHeight * 6 }}>
                            <ButtonFullwidthPrimary onPress={salvarFn}>
                                Salvar
                            </ButtonFullwidthPrimary>
                        </View>
                    </View>
                </KeyboardAwareScrollView>
            </View>
            <DefaultBottomBar
                currentTerritorio={currentUser.currentTerritorio}
                navigation={navigation}
                tela={TELAS.DADOS_BASICOS}
            />
        </View>
    );
}
