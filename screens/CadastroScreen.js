import React, { useCallback, useEffect, useReducer, useRef, useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';
import { View, Alert, Image, Text } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useMutation, useQuery } from '@apollo/client';
import { SafeAreaView } from 'react-native-safe-area-context';
import { cpf } from 'cpf-cnpj-validator';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import { Layout } from '../styles';
import { preparaUpdateAppData } from '../db/api';
import STATUS, { getSyncStatus } from '../bases/status';
import CURRENT_USER from '../bases/current_user';
import { GET_USER_DATA, SEND_USER_DATA } from '../db/queries';
import metrics from '../utils/metrics';
import { geraTmpId, log, logError } from '../utils/utils';

import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import ToNoMapaLoading from '../components/ToNoMapaLoading';
import { TextInputDefault, TextInputTelefone, MunicipioSelectH } from '../components/inputs';
import {
    ButtonCancel,
    ButtonContainedPrimary,
    ButtonFullwidthPrimary,
} from '../components/buttons';

export default function CadastroScreen({ route, navigation }) {
    const { signUp, online } = useToNoMapa();
    const scrollRef = useRef(null);
    const alertOfflineTitle = 'Sem internet!';
    const alertOfflineBody =
        'Não é possível fazer o cadastro sem conexão à internet.\n\nDepois de cadastrado, o App funcionará inteiramente sem internet. É só agora no cadastro que tem que ter internet.\n\nPor favor, tente novamente mais tarde, com conexão.';
    const alertOnlineTitle = 'Seu celular está conectado!';
    const alertOnlineBody =
        'Você poderá fazer o cadastro agora.\n\nApós o cadastro, o aplicativo vai funcionar totalmente sem internet!';
    const initialState = {
        municipioReferenciaId: null,
        estadoId: null,
        telefone: null,
        titulo: '',
        labelComunidade: '',
        loading: true,
    };

    const reducer = (state, newState) => {
        return { ...state, ...newState };
    };

    /**
     * @type object
     */
    const [state, setState] = useReducer(reducer, initialState);

    const [errors, setErrors] = useState([]);

    /**
     * @type object
     */
    const [newCurrentUser, setNewCurrentUser] = useReducer(reducer, CURRENT_USER);

    const { data: currentUserData } = useQuery(GET_USER_DATA);
    const currentUser = currentUserData?.currentUser;
    const territorios = currentUser?.territorios;
    const [persisteCurrentUser] = useMutation(SEND_USER_DATA, {
        onError: (error) => {
            log(`erro ao criar nova comunidade! Usuaria id=${currentUser.id}`);
            logError(error);
            // TODO: What should we do now?
        },
    });

    useFocusEffect(
        useCallback(() => {
            const newState = {
                titulo: 'Nova comunidade',
                labelComunidade: 'Nome da comunidade',
                labelSalvar: 'Criar',
                loading: false,
            };
            let newCurrentUser;
            if (currentUser) {
                newCurrentUser = {
                    ...currentUser,
                    currentTerritorio: {
                        ...CURRENT_USER.currentTerritorio,
                        statusId: STATUS.EM_PREENCHIMENTO,
                    },
                };
            } else {
                newCurrentUser = {
                    ...CURRENT_USER,
                    currentTerritorio: {
                        ...CURRENT_USER.currentTerritorio,
                        statusId: STATUS.EM_PREENCHIMENTO,
                    },
                };
            }
            if (route.params?.username) {
                newCurrentUser.username = route.params.username;
                newState.telefone = route.params.username;
                delete route.params.username;
            }
            if (route.name === 'Cadastro') {
                newState.titulo = 'Cadastro';
                newState.labelComunidade = 'Comunidade que será mapeada';
                newState.labelSalvar = 'Enviar';
            }
            setNewCurrentUser(newCurrentUser);
            setState(newState);
        }, [currentUser, navigation])
    );

    const alertSyncStatus = () => {
        Alert.alert(
            online ? alertOnlineTitle : alertOfflineTitle,
            online ? alertOnlineBody : alertOfflineBody,
            [
                {
                    text: 'Entendi',
                    onPress: () => {},
                },
            ],
            { cancelable: true }
        );
        return;
    };

    const syncStatus = getSyncStatus(online, false, alertSyncStatus);

    function validarCadastroBasico() {
        const newErrors = [];

        if (route.name === 'Cadastro') {
            if (!newCurrentUser.username || newCurrentUser.username.length < 10) {
                newErrors.push([
                    'telefone',
                    'O telefone é obrigatório e precisa ser um número válido!',
                ]);
            }

            if (!newCurrentUser.fullName || 0 === newCurrentUser.fullName.trim().length) {
                newErrors.push(['nome', 'O nome é obrigatório!']);
            }

            if (!newCurrentUser.cpf || 0 === newCurrentUser.cpf.trim().length) {
                newErrors.push(['cpf', 'O CPF obrigatório!']);
            }

            if (!cpf.isValid(newCurrentUser.cpf)) {
                newErrors.push(['cpf', 'CPF inválido']);
            }
        }

        if (!state.municipioReferenciaId) {
            newErrors.push([
                'municipioReferenciaId',
                'O Município de Referência da comunidade é obrigatório!',
            ]);
        }

        if (
            !newCurrentUser.currentTerritorio.nome ||
            0 === newCurrentUser.currentTerritorio.nome.trim().length
        ) {
            newErrors.push(['nomeComunidade', 'O nome da comunidade é obrigatório!']);
        } else if (route.name === 'CadastroTerritorio') {
            if (territorios) {
                const territorioExistente = territorios.find((territorio) => {
                    return territorio.nome === newCurrentUser.currentTerritorio.nome;
                });
                if (territorioExistente) {
                    newErrors.push([
                        'nomeComunidade',
                        'Você já está mapeando uma comunidade com este nome! Por favor, escolha outro nome.',
                    ]);
                }
            }
        }

        setErrors(newErrors);
        return newErrors.length === 0;
    }

    const alertaErro = (titulo, erro, buttons = null) => {
        Alert.alert(
            titulo,
            erro,
            buttons || [
                {
                    text: 'OK',
                    onPress: async () => {},
                },
            ],
            { cancelable: true }
        );
    };

    const alertOffline = () => {
        Alert.alert(
            alertOfflineTitle,
            alertOfflineBody,
            [
                {
                    text: 'Entendi',
                    onPress: () => {},
                },
            ],
            { cancelable: true }
        );
    };

    const salvarFn = async () => {
        if (!online) {
            alertOffline();
            return;
        }
        if (validarCadastroBasico()) {
            setState({ loading: true });
            newCurrentUser.cpf = newCurrentUser.cpf.replace(/[\W_]/g, '');
            newCurrentUser.currentTerritorio = {
                ...newCurrentUser.currentTerritorio,
                municipioReferenciaId: state.municipioReferenciaId.toString(),
                id: geraTmpId(),
                statusId: STATUS.EM_PREENCHIMENTO,
            };

            if (route.name === 'Cadastro') {
                // Here I cannot get an optimisticResponse back.
                // It must fail, since I'm creating a new user or a new territorio.
                const { variables } = await preparaUpdateAppData(newCurrentUser);
                const result = await persisteCurrentUser({ variables });
                if (result?.data?.updateAppData?.success) {
                    const mutateResult = result.data.updateAppData;

                    let token = mutateResult.token;

                    if (token) {
                        signUp(token);
                        return;
                    }

                    setState({ loading: false });
                    alertaErro(
                        'Erro de conexão - token não recebido',
                        `Por favor, entre em contato com a equipe Tô no Mapa e envie um print desta mensagem: ` +
                            JSON.stringify(mutateResult)
                    );
                } else {
                    setState({ loading: false });
                    if (
                        result?.data?.updateAppData?.errors &&
                        result.data.updateAppData.errors[0] === 'username'
                    ) {
                        alertaErro(
                            'Erro: telefone já cadastrado',
                            `O número ${newCurrentUser.username} já está cadastrado no Tô No Mapa.\n\nVocê pode pedir o seu código de segurança para usar esse número ou cadastrar um outro telefone`,
                            [
                                {
                                    text: 'Cadastrar outro telefone',
                                    onPress: () => {
                                        if (scrollRef.current) {
                                            scrollRef.current.scrollToPosition(0, 0, true);
                                        }
                                    },
                                },
                                {
                                    text: 'Pedir o código de segurança',
                                    onPress: () => {
                                        navigation.navigate('PedirCodigoSeguranca', {
                                            usuaria: newCurrentUser.username,
                                            cpf: newCurrentUser.cpf,
                                        });
                                    },
                                },
                            ]
                        );
                        return;
                    }

                    alertaErro(
                        'Erro!',
                        `Por favor, entre em contato com a equipe Tô no Mapa e envie um print desta mensagem:\n\n${JSON.stringify(
                            result
                        )}`
                    );
                }
                return;
            }

            const mutateData = await preparaUpdateAppData(newCurrentUser);
            persisteCurrentUser(mutateData);
            setState({ loading: false });
            navigation.navigate('Home', {
                newTerritorioId: null,
                snackBarMessage: `Você agora está mapeando a comunidade ${newCurrentUser.currentTerritorio.nome}`,
            });
        }
    };

    if (!newCurrentUser) {
        return <ToNoMapaLoading />;
    }

    const getErrorMsg = (key) => {
        const error = errors.find((e) => {
            return e[0] === key;
        });
        if (!error) {
            return null;
        }
        return error[1];
    };

    useEffect(() => {
        if (errors.length > 0) {
            validarCadastroBasico();
        }
    }, [newCurrentUser, state.municipioReferenciaId]);

    return (
        <>
            <View style={Layout.containerStretched}>
                <ToNoMapaAppbar
                    navigation={navigation}
                    title={state.titulo}
                    goBack={true}
                    rightActions={[{ ...syncStatus }]}
                />
                <View style={Layout.body}>
                    <KeyboardAwareScrollView
                        ref={scrollRef}
                        enableOnAndroid={true}
                        keyboardShouldPersistTaps="handled">
                        <SafeAreaView style={Layout.innerBody}>
                            <View style={{ alignItems: 'center' }}>
                                <Image
                                    source={require('../assets/images/logo/colorida/512/logo.png')}
                                    style={{
                                        height: metrics.tenHeight * 12,
                                        width: metrics.tenHeight * 12,
                                    }}
                                />
                            </View>
                            {route.name === 'Cadastro' && (
                                <>
                                    <Text style={Layout.textParagraphTitle}>Sobre você</Text>
                                    <TextInputTelefone
                                        label="Seu telefone"
                                        required={true}
                                        value={state.telefone}
                                        onChangeText={(text) => {
                                            setNewCurrentUser({
                                                username: text.replace(/[^0-9.]/g, ''),
                                            });
                                            setState({
                                                telefone: text,
                                            });
                                        }}
                                        error={getErrorMsg('telefone')}
                                    />

                                    <TextInputDefault
                                        label="Seu nome"
                                        required={true}
                                        value={newCurrentUser.fullName}
                                        onChangeText={(value) => {
                                            setNewCurrentUser({ fullName: value });
                                        }}
                                        error={getErrorMsg('nome')}
                                    />

                                    <TextInputDefault
                                        label="Seu CPF"
                                        value={newCurrentUser.cpf}
                                        keyboardType="numeric"
                                        onChangeText={(value) => {
                                            setNewCurrentUser({ cpf: cpf.format(value) });
                                        }}
                                        error={getErrorMsg('cpf')}
                                    />

                                    <Text style={Layout.textParagraphTitle}>Comunidade</Text>
                                </>
                            )}

                            <TextInputDefault
                                label={state.labelComunidade}
                                required={true}
                                value={newCurrentUser.currentTerritorio.nome}
                                onChangeText={(value) => {
                                    setNewCurrentUser({
                                        currentTerritorio: {
                                            ...newCurrentUser.currentTerritorio,
                                            nome: value,
                                        },
                                    });
                                }}
                                error={getErrorMsg('nomeComunidade')}
                            />

                            <MunicipioSelectH
                                estadoId={state.estadoId}
                                municipioId={state.municipioReferenciaId}
                                labelMunicipio="Município de Referência"
                                required={true}
                                onValueChange={(value) => {
                                    const newState = {
                                        municipioReferenciaId: value.municipioId,
                                    };
                                    if (value.hasOwnProperty('estadoId')) {
                                        newState.estadoId = value.estadoId;
                                    }
                                    setState(newState);
                                }}
                                error={getErrorMsg('municipioReferenciaId')}
                            />

                            <View
                                style={{
                                    ...Layout.buttonsBottomWrapper,
                                    marginTop: metrics.tenHeight * 4,
                                }}>
                                {route.name === 'CadastroTerritorio' ? (
                                    <View style={Layout.row}>
                                        <ButtonCancel
                                            onPress={() => {
                                                navigation.navigate('Home');
                                            }}>
                                            Cancelar
                                        </ButtonCancel>
                                        <ButtonContainedPrimary
                                            disabled={!online}
                                            onPress={salvarFn}>
                                            {state.labelSalvar} {online ? '' : '(sem internet)'}
                                        </ButtonContainedPrimary>
                                    </View>
                                ) : (
                                    <View style={Layout.buttonRight}>
                                        <ButtonFullwidthPrimary
                                            disabled={!online}
                                            onPress={salvarFn}>
                                            {state.labelSalvar} {online ? '' : '(sem internet)'}
                                        </ButtonFullwidthPrimary>
                                    </View>
                                )}
                            </View>
                        </SafeAreaView>
                    </KeyboardAwareScrollView>
                </View>
            </View>
            {state.loading && <ToNoMapaLoading isOverlay={true} />}
        </>
    );
}
