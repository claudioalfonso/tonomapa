import React, { useEffect, useReducer, useRef, useState } from 'react';
import { View, Alert, Text, Image } from 'react-native';
import { cpf } from 'cpf-cnpj-validator';
import * as ImagePicker from 'expo-image-picker';
import * as mime from 'react-native-mime-types';
import { ReactNativeFile } from 'apollo-upload-client';
import { HelperText } from 'react-native-paper';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import { Anexos, Layout } from '../styles';
import { PEDIR_CODIGO_SEGURANCA_VALIDADOR } from '../utils/schemas';
import { enviaPedidoCodigoSeguranca } from '../db/api';
import { getSyncStatus } from '../bases/status';
import { basename } from '../db/arquivos';
import metrics from '../utils/metrics';

import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import ToNoMapaLoading from '../components/ToNoMapaLoading';
import {
    ButtonContainedPrimary,
    ButtonContainedSecondary,
    ButtonOutlinedSecondary,
} from '../components/buttons';
import { TextInputDefault, TextInputTelefone } from '../components/inputs';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default function PedirCodigoSegurancaScreen({ navigation, route }) {
    const { online } = useToNoMapa();
    const reducer = (state, newState) => {
        return { ...state, ...newState };
    };
    const initialState = {
        // nome: null,
        fotoDocumento: null,
        usuaria: null,
        cpf: null,
    };

    const [carregando, setCarregando] = useState(false);

    /**
     * @type object
     */
    const [state, setState] = useReducer(reducer, initialState);

    const [errors, setErrors] = useState([]);

    const scrollRef = useRef(null);

    const validaDadosAsync = async () => {
        const schema = PEDIR_CODIGO_SEGURANCA_VALIDADOR;
        if (state.cpf) {
            state.cpf = state.cpf.replace(/[\W_]/g, '');
        }
        let errors = [];
        await schema.validate({ ...state }, { abortEarly: false }).catch((err) => {
            errors = err.errors.map((e) => {
                const error = e.split('|');
                return error;
            });
        });

        if (!cpf.isValid(state.cpf)) {
            errors.push(['cpf', 'CPF inválido! Favor corrigir.']);
        }

        setErrors(errors);
        return errors.length === 0;
    };

    const salvarFn = async () => {
        if (!online) {
            Alert.alert(
                'Sem internet!',
                'Só é possível solicitar seu código de segurança quando seu celular estiver conectado na internet\n\nPor favor, tente mais tarde.',
                [{ text: 'Entendi', onPress: async () => {} }],
                { cancelable: true }
            );
            return false;
        }

        const result = await validaDadosAsync();
        if (result) {
            setCarregando(true);
            const ret = await enviaPedidoCodigoSeguranca({ ...state });
            setCarregando(false);
            if (ret.ok) {
                Alert.alert(
                    ret.response.title,
                    ret.response.body,
                    [
                        {
                            text: 'Ok',
                            onPress: async () => {
                                navigation.navigate('Login');
                            },
                        },
                    ],
                    {
                        cancelable: true,
                    }
                );
                return;
            }

            Alert.alert(
                ret.msg,
                'O servidor parece estar fora do ar. Favor tentar mais tarde.',
                [{ text: 'Entendi', onPress: async () => {} }],
                { cancelable: true }
            );
            navigation.navigate('Login');
        }
    };

    useEffect(() => {
        if (route.params) {
            const newState = {};
            if (route.params.usuaria) {
                newState.usuaria = route.params.usuaria;
                delete route.params.usuaria;
            }
            if (route.params.cpf) {
                newState.cpf = route.params.cpf;
                delete route.params.cpf;
            }
            if (newState.usuaria || newState.cpf) {
                setState(newState);
            }
        }
    }, [navigation, route]);

    const alertSyncStatus = () => {
        const title = online ? 'Você está online!' : 'Sem internet';
        const body = online
            ? 'No momento o seu celular está conectado e você poderá fazer o pedido do código'
            : 'Neste momento, não vai ser possível você solicitar o código de segurança.\n\nTente mais tarde, quando estiver conectada(o) ';
        Alert.alert(
            title,
            body,
            [
                {
                    text: 'Entendi',
                    onPress: () => {},
                },
            ],
            { cancelable: true }
        );
        return;
    };

    const syncStatus = getSyncStatus(online, false, alertSyncStatus);

    const getFotoDocumento = async () => {
        const { granted } = await ImagePicker.requestCameraPermissionsAsync();
        const params = {
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        };
        let result;
        if (!granted) {
            // @ts-ignore
            result = await ImagePicker.launchImageLibraryAsync(params);
        } else {
            // @ts-ignore
            result = await ImagePicker.launchCameraAsync(params);
        }

        if (!result.cancelled) {
            // @ts-ignore
            const filename = basename({ path: result.uri });
            const type = mime.lookup(filename);
            const fotoDocumento = new ReactNativeFile({
                // @ts-ignore
                uri: result.uri,
                name: filename,
                type: typeof type === 'string' ? type : '',
            });
            setState({ fotoDocumento });
            return;
        }
    };

    const getErrorMsg = (key) => {
        const error = errors.find((e) => {
            return e[0] === key;
        });
        if (!error) {
            return null;
        }
        return error[1];
    };

    useEffect(() => {
        if (errors.length > 0) {
            validaDadosAsync();
        }
    }, [state.usuaria, state.cpf]);

    return (
        <>
            <View style={Layout.containerStretched}>
                <ToNoMapaAppbar
                    navigation={navigation}
                    title="Pedir o código de segurança"
                    rightActions={[{ ...syncStatus }]}
                    goBack={true}
                />
                <View style={Layout.bodyWithBottomBar}>
                    <KeyboardAwareScrollView
                        ref={scrollRef}
                        keyboardShouldPersistTaps="handled"
                        enableOnAndroid={true}
                        extraScrollHeight={120}>
                        <View style={Layout.innerBody}>
                            <Text
                                style={{
                                    ...Layout.textParagraph,
                                    marginTop: metrics.tenHeight * 2,
                                }}>
                                Se você já tem cadastro, mas esqueceu do seu código de segurança,
                                por favor envie uma foto do seu documento e diga seu nome e CPF
                            </Text>
                            <View style={{ marginTop: metrics.tenHeight * 2 }}>
                                {state.fotoDocumento ? (
                                    <>
                                        <View style={Anexos.itemImageWrapper}>
                                            <Image
                                                source={{ uri: state.fotoDocumento.uri }}
                                                style={Anexos.itemImage}
                                            />
                                        </View>
                                        <ButtonOutlinedSecondary onPress={getFotoDocumento}>
                                            Trocar arquivo...
                                        </ButtonOutlinedSecondary>
                                        <HelperText type="info">
                                            <Text>Documento com foto</Text>
                                        </HelperText>
                                    </>
                                ) : (
                                    <ButtonContainedSecondary onPress={getFotoDocumento}>
                                        Documento com foto...
                                    </ButtonContainedSecondary>
                                )}
                            </View>

                            {!!getErrorMsg('fotoDocumento') && (
                                <HelperText type="error">{getErrorMsg('fotoDocumento')}</HelperText>
                            )}
                            <TextInputTelefone
                                label="Seu telefone"
                                required={true}
                                value={state.usuaria}
                                onChangeText={(value) => setState({ usuaria: value })}
                                error={getErrorMsg('telefone')}
                            />

                            {/*<TextInputDefault
                                label="Seu nome"
                                value={state.nome}
                                onChangeText={(value) => setState({ nome: value })}
                            />*/}

                            <TextInputDefault
                                label="CPF"
                                required={true}
                                value={state.cpf}
                                keyboardType="numeric"
                                onChangeText={(value) => {
                                    setState({ cpf: cpf.format(value) });
                                }}
                                error={getErrorMsg('cpf')}
                            />

                            <View style={{ ...Layout.buttonCenter, marginTop: 30 }}>
                                <ButtonContainedPrimary onPress={salvarFn}>
                                    Enviar o pedido
                                </ButtonContainedPrimary>
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </View>
            </View>
            {carregando && <ToNoMapaLoading isOverlay={true} />}
        </>
    );
}
