import React, { useEffect, useRef, useState } from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { useQuery } from '@apollo/client';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import TELAS from '../bases/telas';
import { GET_USER_DATA } from '../db/queries';
import SETTINGS from '../bases/settings';
import STATUS, {
    isEnviarEnabled,
    isCurrentTerritorioEditable,
    isPTTMode,
    isEnviarPTTEnabled,
} from '../bases/status';
import { log, logError } from '../utils/utils';
import { Layout } from '../styles';

import SobreScreen from './SobreScreen';
import TICCAsScreen from './TICCAsScreen';
import FaleConoscoScreen from './FaleConoscoScreen';
import TermosDeUsoScreen from './TermosDeUsoScreen';
import DadosBasicosScreen from './DadosBasicosScreen';
import DadosPTTScreen from './DadosPTTScreen';
import MensagensScreen from './MensagensScreen';
import MapaScreen from './MapaScreen';
import ConfiguracoesScreen from './ConfiguracoesScreen';
import AnexoListScreen from './AnexoListScreen';
import AnexoFormScreen from './AnexoFormScreen';
import { carregaSettings } from '../db/api';

import CustomDrawerContent from '../components/CustomDrawerContent';

const RightDrawer = createDrawerNavigator();

/**
 *
 * @param {*} param0
 * @returns
 */
export default function MainScreen({ route }) {
    /*DEBUG*/ //console.log('MainScreen', route.params);
    const [tela, setTela] = useState(TELAS.MAPA);

    const { client } = useToNoMapa();

    const [settings, setSettings] = useState(SETTINGS);

    const { data: currentUserData, error: currentUserError } = useQuery(GET_USER_DATA);
    const currentUser = currentUserData ? currentUserData.currentUser : null;
    if (currentUserError) {
        console.log('erro ao carregar currentUser', currentUserError);
        log('MainScreen: erro ao carregar currentUser');
        logError(currentUserError);
    }

    const isOKTonomapa = () => {
        return (
            currentUser &&
            currentUser.currentTerritorio &&
            currentUser.currentTerritorio.statusId === STATUS.OK_TONOMAPA
        );
    };

    const [isPTTModeVar, setIsPTTModeVar] = useState(false);
    const [isEnviarEnabledVar, setIsEnviarEnabledVar] = useState(true);
    const [isEnviarPTTEnabledVar, setIsEnviarPTTEnabledVar] = useState(false);
    const [isCurrentTerritorioEditableVar, setIsCurrentTerritorioEditableVar] = useState(true);

    useEffect(() => {
        const newTela = route.params?.tela || TELAS.MAPA;
        if (newTela !== tela) {
            setTela(newTela);
        }
    }, [route.params]);

    useEffect(() => {
        const newSettings = carregaSettings(client);
        setSettings(newSettings);
    }, []);

    useEffect(() => {
        if (currentUser?.currentTerritorio?.statusId) {
            const newIsPTT = isPTTMode(currentUser.currentTerritorio.statusId);
            setIsPTTModeVar(newIsPTT);
            const newIsTerritorioEditable = isCurrentTerritorioEditable(
                currentUser.currentTerritorio.statusId
            );
            setIsCurrentTerritorioEditableVar(newIsTerritorioEditable);
            const newIsEnviarEnabled = isEnviarEnabled(currentUser.currentTerritorio.statusId);
            setIsEnviarEnabledVar(newIsEnviarEnabled);
            const newIsEnviarPTTEnabled = isEnviarPTTEnabled(
                currentUser.currentTerritorio.statusId
            );
            setIsEnviarPTTEnabledVar(newIsEnviarPTTEnabled);
        }
    }, [currentUser?.currentTerritorio?.statusId]);

    return (
        <RightDrawer.Navigator
            // @ts-ignore
            drawerStyle={Layout.drawer}
            initialRouteName="Home"
            screenOptions={{
                headerShown: false,
                drawerPosition: 'right',
            }}
            edgeWidth={0}
            // @ts-ignore
            drawerContent={(props) => {
                return (
                    <CustomDrawerContent
                        {...props}
                        currentUser={currentUser}
                        isPTTModeVar={isPTTModeVar}
                        isCurrentTerritorioEditableVar={isCurrentTerritorioEditableVar}
                        isEnviarEnabledVar={isEnviarEnabledVar}
                        isEnviarPTTEnabledVar={isEnviarPTTEnabledVar}
                        isOKTonomapa={isOKTonomapa}
                    />
                );
            }}>
            <RightDrawer.Screen name="Configuracoes" component={ConfiguracoesScreen} />
            <RightDrawer.Screen name="Sobre" component={SobreScreen} />
            <RightDrawer.Screen name="TICCAs" component={TICCAsScreen} />
            <RightDrawer.Screen name="FaleConosco" component={FaleConoscoScreen} />
            <RightDrawer.Screen name="TermosDeUso" component={TermosDeUsoScreen} />
            <RightDrawer.Screen name="DadosBasicos" component={DadosBasicosScreen} />
            <RightDrawer.Screen name="DadosPTT" component={DadosPTTScreen} />
            <RightDrawer.Screen name="Mensagens" component={MensagensScreen} />
            <RightDrawer.Screen name="AnexoList" component={AnexoListScreen} />
            <RightDrawer.Screen name="AnexoForm" component={AnexoFormScreen} />
            <RightDrawer.Screen name="Mapa" component={MapaScreen} />
            <RightDrawer.Screen
                name="Enviar"
                component={MapaScreen}
                initialParams={{ tela: TELAS.MAPA, firstRun: true }}
            />
            <RightDrawer.Screen
                name="Home"
                component={MapaScreen}
                initialParams={{ firstRun: true }}
            />
            <RightDrawer.Screen
                name="AppIntro"
                component={MapaScreen}
                initialParams={{ appIntro: true, appIntroDialog: false }}
            />
        </RightDrawer.Navigator>
    );
}
