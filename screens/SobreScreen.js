import React, { useEffect, useRef } from 'react';
import { Image, View } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { Button } from 'react-native-paper';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { Layout } from '../styles';
import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import TextoSobre from '../components/TextoSobre';
import colors from '../assets/colors';
import TELAS from '../bases/telas';

export default function SobreScreen({ navigation }) {
    const scrollRef = useRef(null);
    const isFocused = useIsFocused();

    useEffect(() => {
        if (isFocused) {
            scrollRef.current.scrollToPosition(0, 0, true);
        }
    }, [isFocused]);

    return (
        <View style={Layout.containerStretched}>
            <ToNoMapaAppbar navigation={navigation} title="Sobre o Tô no Mapa" />
            <KeyboardAwareScrollView
                ref={scrollRef}
                keyboardShouldPersistTaps="handled"
                enableOnAndroid={true}
                extraScrollHeight={50}>
                <View style={Layout.innerBody}>
                    <Image
                        source={require('../assets/images/logo/main/tonomapa_logo.png')}
                        style={Layout.logoImageCadastro}
                        resizeMode="contain"
                    />

                    <TextoSobre />

                    <View style={Layout.buttonsBottomWrapper}>
                        <View style={{ ...Layout.row }}>
                            <Button
                                mode="outlined"
                                dark={false}
                                color={colors.warning}
                                style={Layout.buttonCenter}
                                onPress={() => {
                                    navigation.navigate('TermosDeUso');
                                }}>
                                Termos de Uso
                            </Button>
                            <Button
                                mode="contained"
                                dark={true}
                                style={Layout.buttonCenter}
                                onPress={() => {
                                    navigation.navigate('Home', { tela: TELAS.MAPA });
                                }}>
                                Voltar
                            </Button>
                        </View>
                    </View>
                </View>
            </KeyboardAwareScrollView>
        </View>
    );
}
