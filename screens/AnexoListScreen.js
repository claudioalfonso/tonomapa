import React, { useState, useCallback, useRef, useEffect } from 'react';
import { useMutation, useQuery } from '@apollo/client';
import { Alert, BackHandler, Settings, View } from 'react-native';
import { FlashList } from '@shopify/flash-list';
import { FAB, Text, TouchableRipple } from 'react-native-paper';
import * as FileSystem from 'expo-file-system';
import * as IntentLauncher from 'expo-intent-launcher';
import { Asset } from 'expo-asset';
import { useFocusEffect } from '@react-navigation/native';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import { Layout, Anexos } from '../styles';
import colors from '../assets/colors';
import {
    preparaCreateOrUpdateAnexo,
    downloadAnexo,
    downloadThumbs,
    preparaDeleteAnexo,
    carregaSettings,
    persisteSettings,
} from '../db/api';
import { ehTmpId, exitScreen, log, logError } from '../utils/utils';
import metrics from '../utils/metrics';
import { isAnexoPTT } from '../bases/anexo';
import { isCurrentTerritorioEditable, isPTTMode } from '../bases/status';
import { CREATE_OR_UPDATE_ANEXO, DELETE_ANEXO, GET_ANEXOS, GET_USER_DATA } from '../db/queries';
import TELAS from '../bases/telas';
import { updateHandlerByName } from '../utils/graphqlUpdateHandlers';

import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import AnexoListItem from '../components/AnexoListItem';
import ToNoMapaLoading from '../components/ToNoMapaLoading';
import DefaultBottomBar from '../components/DefaultBottomBar';
import PTTBottomBar from '../components/PTTBottomBar';
import ToNoMapaSnackbar from '../components/ToNoMapaSnackbar';
import Balloon from '../components/third_party/Balloon';
import { ButtonCancel } from '../components/buttons';

let snackBarMessage = '';

// if (Platform.OS === 'android') {
//     UIManager.setLayoutAnimationEnabledExperimental &&
//         UIManager.setLayoutAnimationEnabledExperimental(true);
// }

export default function AnexoListScreen({ navigation, route }) {
    const { client, online, syncStatus } = useToNoMapa();

    const { data: currentUserData } = useQuery(GET_USER_DATA);
    const currentUser = currentUserData?.currentUser;
    const currentTerritorioId = currentUser?.currentTerritorio?.id;
    const variables = { territorioId: currentTerritorioId };
    const territorioStatusId = currentUser?.currentTerritorio?.statusId;

    const [carregando, setCarregando] = useState(false);

    const listRef = useRef(null);

    const [snackBarVisible, setSnackBarVisible] = useState(false);

    const [currentTerritorioEditable, setCurrentTerritorioEditable] = useState(false);

    const [pttMode, setPTTMode] = useState(false);

    const { data: anexosData } = useQuery(GET_ANEXOS, {
        fetchPolicy: 'cache-only',
        variables,
    });
    const anexos = anexosData?.anexos;
    const [CreateOrUpdateAnexo] = useMutation(CREATE_OR_UPDATE_ANEXO);
    const [deleteAnexo] = useMutation(DELETE_ANEXO);

    const [settings, setSettings] = useState(null);

    // const setAnimation = () => {
    //     LayoutAnimation.configureNext({
    //         duration: 250,
    //         delete: {
    //             type: LayoutAnimation.Types.easeOut,
    //             property: LayoutAnimation.Properties.scaleXY,
    //             springDamping: 0.7,
    //         },
    //     });
    //     LayoutAnimation.configureNext({
    //         duration: 500,
    //         create: {
    //             type: LayoutAnimation.Types.easeIn,
    //             property: LayoutAnimation.Properties.scaleXY,
    //             springDamping: 0.7,
    //         },
    //     });
    // };

    const apagarFn = (id) => {
        const mutateData = preparaDeleteAnexo({
            localId: id,
        });
        // setAnimation();
        deleteAnexo({
            ...mutateData,
            update: (cache, { data }) => {
                updateHandlerByName({
                    operationName: 'DeleteAnexo',
                    cache,
                    data,
                    localId: id,
                });
            },
        });
        snackBarMessage = 'Arquivo apagado com sucesso';
        setSnackBarVisible(true);
    };

    useFocusEffect(
        useCallback(() => {
            const onBackPress = () => {
                return exitScreen(navigation);
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);

            const newSettings = carregaSettings(client);
            setSettings(newSettings);

            return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [navigation])
    );

    useFocusEffect(
        useCallback(() => {
            setCurrentTerritorioEditable(isCurrentTerritorioEditable(territorioStatusId));
            setPTTMode(isPTTMode(territorioStatusId));
        }, [territorioStatusId])
    );

    useFocusEffect(
        useCallback(() => {
            if (online && anexos) {
                downloadThumbs(anexos, currentTerritorioId);
            }
        }, [online, anexos])
    );

    //Receiving file from AnexoForm and persisting it
    if (currentTerritorioId) {
        if (route.params && 'newAnexo' in route.params) {
            const newId = route.params.newAnexo.id;
            const mutateData = preparaCreateOrUpdateAnexo({
                newAnexo: route.params.newAnexo,
                currentTerritorioId,
            });
            delete route.params.newAnexo;

            snackBarMessage = ehTmpId(newId)
                ? 'Arquivo adicionado com sucesso'
                : 'Arquivo atualizado com sucesso';

            CreateOrUpdateAnexo({
                ...mutateData,
                update: (cache, { data }) => {
                    updateHandlerByName({
                        operationName: 'CreateOrUpdateAnexo',
                        cache,
                        data,
                        localId: newId,
                    });
                },
            });
            if (ehTmpId(newId)) {
                setTimeout(() => {
                    listRef.current.scrollToEnd({
                        animated: true,
                    });
                }, 1500);
            }
            // setAnimation();
        } else if (route.params && 'apagar' in route.params) {
            const targetId = route.params.apagar;
            apagarFn(targetId);
            delete route.params.apagar;
        }
    }

    const novoDocumento = () => {
        const routeParams = {
            currentTerritorioId,
            newAnexo: true,
            isPTTMode: pttMode,
        };
        navigation.navigate('AnexoForm', routeParams);
    };

    const onVisualizarItem = async (anexo) => {
        let localFileUri = anexo.localFileUri;
        if (!localFileUri) {
            if (!online) {
                Alert.alert(
                    'Sem internet',
                    'Tente novamente quando estiver conectada(o). Depois que você baixar o arquivo uma vez, ele ficará acessível mesmo sem internet.',
                    [
                        {
                            text: 'Ok',
                            onPress: () => {},
                        },
                    ],
                    { cancelable: true }
                );
                return;
            }
            setCarregando(true);
            const result = await downloadAnexo(anexo, anexos, currentTerritorioId);
            setCarregando(false);
            if (result.errors) {
                return;
            }
            snackBarMessage = 'Baixado com sucesso: ele será acessível sem internet!';
            setSnackBarVisible(true);
            localFileUri = result.uri;
        }
        FileSystem.getContentUriAsync(localFileUri)
            .then((cUri) => {
                IntentLauncher.startActivityAsync('android.intent.action.VIEW', {
                    data: cUri,
                    flags: 1,
                }).catch((err) => {
                    snackBarMessage =
                        'Seu celular não tem nenhum programa para abrir este tipo de arquivo.';
                    log(
                        `onVisualizarItem: Seu celular não tem nenhum programa para abrir este tipo de arquivo: ${cUri}`
                    );
                    logError(err);
                    setSnackBarVisible(true);
                });
            })
            .catch((e) => {
                console.log(`Não foi possível pegar o conteúdo remoto. ${e.message}`);
                log(`onVisualizarItem: Não foi possível pegar o conteúdo remoto ${localFileUri}`);
                logError(e);
            });
    };

    const onEditarItem = (anexo) => {
        navigation.navigate('AnexoForm', {
            currentTerritorioId,
            currentAnexo: anexo,
        });
    };

    const onApagarItem = (id) => {
        Alert.alert(
            'Apagar arquivo',
            'Não é possível desfazer esta ação. Deseja mesmo apagar este arquivo ou imagem?',
            [
                {
                    text: 'Não, manter',
                    onPress: () => {},
                },
                {
                    text: 'Sim, apagar',
                    onPress: () => {
                        apagarFn(id);
                    },
                },
            ],
            { cancelable: true }
        );
    };

    const renderItem = ({ item: anexo }) => {
        return (
            <AnexoListItem
                anexo={anexo}
                isPTTMode={pttMode}
                onVisualizar={onVisualizarItem}
                onEditar={currentTerritorioEditable ? onEditarItem : null}
                onApagar={currentTerritorioEditable ? onApagarItem : null}
                currentTerritorioEditable={currentTerritorioEditable}
            />
        );
    };

    if (!currentTerritorioId || !anexosData) {
        return <ToNoMapaLoading />;
    }

    const FooterComponent = () => <View style={{ height: 40 }} />;

    const handleDownloadAta = async () => {
        const [{ localUri }] = await Asset.loadAsync(
            require('../assets/docs/tonomapa_modelo_ata.pdf')
        );
        try {
            const cUri = await FileSystem.getContentUriAsync(localUri);
            try {
                await IntentLauncher.startActivityAsync('android.intent.action.VIEW', {
                    data: cUri,
                    flags: 1,
                });
            } catch (error) {
                snackBarMessage =
                    'Seu celular não tem nenhum programa para abrir este tipo de arquivo.';
                log(
                    `onVisualizarItem: Seu celular não tem nenhum programa para abrir este tipo de arquivo: ${cUri}`
                );
                logError(error);
                setSnackBarVisible(true);
            }
        } catch (error) {
            console.log(`Não foi possível pegar o conteúdo remoto. ${error.message}`);
            log(`onVisualizarItem: Não foi possível pegar o conteúdo remoto ${localUri}`);
            logError(error);
        }
    };

    const actionDownloadAta = {
        icon: 'download',
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
        onPress: handleDownloadAta,
    };

    const handleCloseBalloon = () => {
        persisteSettings({ showAtaBalloon: false }, client).then((newSettings) => {
            setSettings(newSettings);
        });
    };

    return (
        <>
            <View style={Layout.containerStretched}>
                <ToNoMapaAppbar
                    navigation={navigation}
                    title={!pttMode ? 'Arquivos e imagens' : 'Anexos PTT'}
                    customGoBack={() => {
                        exitScreen(navigation);
                    }}
                    rightActions={[{ ...actionDownloadAta }, { ...syncStatus }]}
                />
                <View style={Layout.innerBody}>
                    {anexos.length === 0 ? (
                        <View style={Anexos.introTextWrapper}>
                            <Text style={Anexos.introText}>
                                Fotos, boletins, relatórios, documentos e outros arquivos anexos com
                                mais informações sobre a comunidade.
                            </Text>
                        </View>
                    ) : (
                        <View style={{ height: '100%' }}>
                            <FlashList
                                ref={listRef}
                                data={anexos}
                                estimatedItemSize={128}
                                contentContainerStyle={Anexos.flatList}
                                ListFooterComponent={FooterComponent}
                                renderItem={renderItem}
                            />
                        </View>
                    )}
                </View>
                {currentTerritorioEditable && (
                    <FAB
                        style={Anexos.fab}
                        color={colors.Foundation.TextColor.White}
                        icon="plus"
                        onPress={novoDocumento}
                    />
                )}
            </View>
            {settings?.showAtaBalloon && (
                <>
                    <TouchableRipple style={Layout.appOverlay} onPress={handleCloseBalloon}>
                        <View></View>
                    </TouchableRipple>
                    <View style={Layout.balloonHintWrapper}>
                        <Balloon
                            borderColor={colors.Foundation.Basic.Gray}
                            backgroundColor={colors.Foundation.CorDeTexto.CorDeTexto}
                            borderWidth={metrics.tenWidth * 0.2}
                            borderRadius={metrics.tenWidth * 1.2}
                            width="100%"
                            triangleSize={metrics.tenWidth * 1.5}
                            triangleOffset={'83%'}
                            triangleDirection="top"
                            containerStyle={{
                                marginHorizontal: 0,
                                padding: metrics.tenWidth * 0.8,
                            }}>
                            <Text style={Layout.balloonHintTitle}>Baixar modelo de ata</Text>
                            <Text style={Layout.balloonHintText}>
                                A ata de reunião autorizando o mapeamento da comunidade é
                                obrigatória.
                            </Text>
                            <Text style={Layout.balloonHintText}>
                                No botão acima, você pode baixar o modelo de ata para imprimir,
                                completar e assinar!
                            </Text>
                            <ButtonCancel compact={true} mode="text" onPress={handleCloseBalloon}>
                                Fechar
                            </ButtonCancel>
                        </Balloon>
                    </View>
                </>
            )}
            <ToNoMapaSnackbar
                visible={snackBarMessage && snackBarVisible === true}
                setVisible={setSnackBarVisible}
                message={snackBarMessage}
            />
            {!pttMode ? (
                <DefaultBottomBar
                    currentTerritorio={currentUser.currentTerritorio}
                    navigation={navigation}
                    tela={TELAS.ARQUIVOS}
                />
            ) : (
                <PTTBottomBar
                    currentTerritorio={currentUser.currentTerritorio}
                    navigation={navigation}
                    tela={TELAS.ARQUIVOS}
                />
            )}
            {carregando && <ToNoMapaLoading isOverlay={true} />}
        </>
    );
}
