import React, { useCallback, useRef, useState } from 'react';
import { BackHandler, View, Alert } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import { useQuery } from '@apollo/client';
import { Bubble, GiftedChat } from 'react-native-gifted-chat';
import 'dayjs/locale/pt-br';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import { Layout } from '../styles';
import colors from '../assets/colors';
import { pegaMensagem } from '../bases/status';
import { GET_MENSAGENS, GET_USER_DATA } from '../db/queries';
import TELAS from '../bases/telas';
import { exitScreen } from '../utils/utils';
import metrics from '../utils/metrics';

import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import ToNoMapaLoading from '../components/ToNoMapaLoading';
import DefaultBottomBar from '../components/DefaultBottomBar';

const BubbleCustom = (props) => {
    return (
        <Bubble
            {...props}
            wrapperStyle={{
                left: { backgroundColor: colors.Foundation.Basic.White },
                right: { backgroundColor: colors.warning },
            }}
            textStyle={{
                left: { color: colors.Foundation.CorDeTexto.CorDeTexto2 },
                right: { color: colors.Foundation.TextColor.White },
            }}
        />
    );
};

export default function MensagensScreen({ navigation }) {
    const { syncStatus } = useToNoMapa();

    const [mensagensGiftedChat, setMensagensGiftedChat] = useState(null);
    const giftedChatRef = useRef(null);

    const { data: currentUserData } = useQuery(GET_USER_DATA, { fetchPolicy: 'cache-only' });
    const currentUser = currentUserData?.currentUser;

    const variables = {
        territorioId: currentUser?.currentTerritorio?.id,
    };
    const { data: mensagensData } = useQuery(GET_MENSAGENS, {
        fetchPolicy: 'cache-only',
        variables,
    });

    const mensagens = mensagensData?.mensagens;
    // const [CreateOrUpdateMensagem] = useMutation(CREATE_OR_UPDATE_MENSAGEM);
    // const [deleteMensagem] = useMutation(DELETE_MENSAGEM);

    useFocusEffect(
        useCallback(() => {
            if (!mensagens) {
                return;
            }
            const newMensagens = [];
            for (const mensagemRaw of mensagens) {
                const user = mensagemRaw.originIsDevice
                    ? { _id: currentUser.id }
                    : {
                          _id: -1,
                          name: 'Equipe Tô no Mapa',
                          avatar: require('../assets/images/logo/colorida/512/logo.png'),
                      };
                try {
                    const extra = JSON.parse(mensagemRaw.extra);
                    let text = extra ? extra.texto : mensagemRaw.texto;
                    if (!mensagemRaw.originIsDevice && extra.statusId) {
                        const statusMessage = pegaMensagem(extra.statusId);
                        text = text
                            ? `${statusMessage.nome}\\n\\nMensagem da equipe Tô no mapa:\\n\\n${text}`
                            : `${statusMessage.nome}: ${statusMessage.mensagem}`;
                    }
                    if (text) {
                        newMensagens.push({
                            _id: mensagemRaw.id ? mensagemRaw.id : new Date().getTime(),
                            text: text.replace(/\\n/g, '\n'),
                            createdAt: mensagemRaw.dataEnvio,
                            user,
                        });
                    }
                } catch (e) {
                    console.log({ SKIPPED: mensagemRaw, error: e.message });
                    continue;
                }
            }
            setMensagensGiftedChat(newMensagens);
        }, [mensagens])
    );

    useFocusEffect(
        useCallback(() => {
            const onBackPress = () => {
                return exitScreen(navigation);
            };
            BackHandler.addEventListener('hardwareBackPress', onBackPress);
            return () => BackHandler.removeEventListener('hardwareBackPress', onBackPress);
        }, [navigation])
    );

    if (!mensagensGiftedChat || !currentUser) {
        return <ToNoMapaLoading />;
    }

    return (
        <View style={Layout.containerStretched}>
            <ToNoMapaAppbar
                navigation={navigation}
                title="Mensagens"
                rightActions={[
                    {
                        icon: 'help-circle',
                        color: colors.Foundation.CorDeTexto.CorDeTexto2,
                        onPress: () => {
                            Alert.alert(
                                'Mensagens',
                                'Aqui estão as mensagens trocadas com a equipe Tô no mapa.\n\nVocê pode enviar uma mensagem no "Fale Conosco", no menu lateral.',
                                [
                                    {
                                        text: 'Entendi',
                                    },
                                ],
                                { cancelable: true }
                            );
                        },
                    },
                    { ...syncStatus },
                ]}
            />
            <GiftedChat
                ref={giftedChatRef}
                locale="pt-br"
                messages={mensagensGiftedChat}
                onSend={(messages) => {
                    return;
                }}
                user={{
                    _id: currentUser.id,
                }}
                renderInputToolbar={() => {
                    return <></>;
                }}
                bottomOffset={0}
                minInputToolbarHeight={0}
                scrollToBottom={false}
                dateFormat="LL"
                timeFormat="H[h]mm"
                inverted={false}
                alignTop={true}
                renderBubble={(props) => {
                    return BubbleCustom(props);
                }}
                listViewProps={{ style: { marginBottom: metrics.tenHeight * 10 } }}
            />
            <DefaultBottomBar
                currentTerritorio={currentUser.currentTerritorio}
                navigation={navigation}
                tela={TELAS.MENSAGENS}
            />
        </View>
    );
}
