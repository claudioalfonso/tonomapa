import React from 'react';
import { View } from 'react-native';
import { WebView } from 'react-native-webview';
import Constants from 'expo-constants';

import { Layout } from '../styles';

import ToNoMapaAppbar from '../components/ToNoMapaAppbar';
import ToNoMapaLoading from '../components/ToNoMapaLoading';
import { ButtonFullwidthPrimary } from '../components/buttons';
import metrics from '../utils/metrics';

const ActivityIndicatorElement = () => {
    return <ToNoMapaLoading isOverlay={true} />;
};

const runFirst = `
    const figures = document.getElementsByTagName('figure');
    const figureList = Array.prototype.slice.call(figures);
    figureList.forEach(figure => {
        figure.removeAttribute('class');
        const div = figure.children[0];
        div.removeAttribute('class');
    });
    const iframes = document.getElementsByTagName('iframe');
    const iframeList = Array.prototype.slice.call(iframes);
    iframeList.forEach(iframe => {
        iframe.removeAttribute('class');
        iframe.width="100%";
        iframe.height=200;
    });
    true;
`;

export default function TICCAsScreen({ navigation }) {
    return (
        <View style={Layout.containerStretched}>
            <ToNoMapaAppbar navigation={navigation} title="O que são TICCAs?" goBack={false} />
            <View style={{ ...Layout.containerStretched, paddingBottom: 20 }}>
                <WebView
                    source={{ uri: Constants.manifest.extra.TICCAsUri }}
                    startInLoadingState={true}
                    renderLoading={ActivityIndicatorElement}
                    allowsFullscreenVideoarrow={true}
                    allowsInlineMediaPlaybackarrow={true}
                    injectedJavaScript={runFirst}
                />
                <View style={{ paddingHorizontal: metrics.tenWidth * 3 }}>
                    <ButtonFullwidthPrimary
                        onPress={() => {
                            navigation.navigate('Home');
                        }}>
                        Voltar
                    </ButtonFullwidthPrimary>
                </View>
            </View>
        </View>
    );
}
