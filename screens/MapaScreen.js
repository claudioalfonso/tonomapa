import React, { useState, useReducer, useRef, useEffect, useCallback } from 'react';
import { AppState, BackHandler, View, Alert, Image, StatusBar, Platform } from 'react-native';
import { useFocusEffect } from '@react-navigation/native';
import * as Location from 'expo-location';
import { FAB } from 'react-native-paper';
import { MAP_TYPES } from 'react-native-maps';
import { useQuery, useMutation, useLazyQuery } from '@apollo/client';
import PropTypes from 'prop-types';
import Constants from 'expo-constants';
import moment from 'moment';

import colors from '../assets/colors';
import STATUS, {
    buildStatusMessage,
    alertStatusId,
    isPTTMode,
    validaDados,
    isCurrentTerritorioEditable,
    isValidado,
    isEnviarEnabled,
} from '../bases/status';
import TELAS from '../bases/telas';
import {
    carregaSettings,
    persisteSettings,
    preparaUpdateAppData,
    tonomapaCache,
    preparaDeleteUsoOuConflito,
    preparaCreateOrUpdateUsoOuConflito,
    preparaCreateOrUpdateMensagem,
    preparaDeleteAllUsosEConflitos,
    populateCachedData,
    getTerritorioReport,
} from '../db/api';
import { useToNoMapa } from '../context/ToNoMapaProvider';
import { Layout, LayoutMapa } from '../styles';
import metrics from '../utils/metrics';
import { openUrlInBrowser, ehTmpId, log, logError, geraTmpId } from '../utils/utils';
import { updateHandlerByName } from '../utils/graphqlUpdateHandlers';
import { getAllMapElementsCoords, gqlTerritorioToMapa } from '../maps/mapsUtils';
import {
    ICONE_TIPO,
    initialState,
    LOGO_IMAGE,
    reducer,
    edgePaddingAdjusted,
} from '../assets/constants';
import {
    GET_AREAS_DE_USO,
    CREATE_OR_UPDATE_AREA_DE_USO,
    DELETE_AREA_DE_USO,
    GET_CONFLITOS,
    CREATE_OR_UPDATE_CONFLITO,
    DELETE_CONFLITO,
    DELETE_ALL_USOS_E_CONFLITOS,
    GET_ANEXOS,
    GET_USER_DATA,
    SEND_USER_DATA,
    CREATE_OR_UPDATE_MENSAGEM,
    GET_MENSAGENS,
} from '../db/queries';
import SETTINGS from '../bases/settings';
import MENSAGEM from '../bases/mensagem';

import Map from '../components/Map';
import MapLegacy from '../components/MapLegacy';
import ToNoMapaIntro from '../components/ToNoMapaIntro';
import ToNoMapaLoading from '../components/ToNoMapaLoading';
import ToNoMapaSnackbar from '../components/ToNoMapaSnackbar';
import DefaultTopBar from '../components/DefaultTopBar';
import MapTopBar from '../components/MapTopBar';
import DefaultBottomBar from '../components/DefaultBottomBar';
import MapBottomBar from '../components/MapBottomBar';
import PTTBottomBar from '../components/PTTBottomBar';
import EnviarParaDashboardHandler from '../components/EnviarParaDashboardHandler';
import { ActivityAction, startActivityAsync } from 'expo-intent-launcher';

export default function MapaScreen({ navigation = null, route }) {
    /**
     * @type object
     */
    const [settings, setSettings] = useState(SETTINGS);

    const [tela, setTela] = useState(TELAS.MAPA);

    /**
     * @type object
     */
    const {
        client,
        signOut,
        notificationHandler: [notification, setNotification],
        online,
        enviarDadosDialogoHandler,
        windowCenter,
    } = useToNoMapa();

    const setShowEnviarDadosDialogo = enviarDadosDialogoHandler[1];

    const [isPTTModeVar, setIsPTTModeVar] = useState(false);
    const [isCurrentTerritorioEditableVar, setIsCurrentTerritorioEditableVar] = useState(true);

    const {
        data: currentUserData,
        error: currentUserError,
        loading: currentUserLoading,
        refetch: currentUserRefetch,
    } = useQuery(GET_USER_DATA, {
        notifyOnNetworkStatusChange: true,
        onCompleted: populateCachedData,
    });
    const currentUser = currentUserData?.currentUser;

    const [updateAppData] = useMutation(SEND_USER_DATA);
    const preparaAndPersisteCurrentUser = async (newCurrentUser, newSnackBarMessage) => {
        const mutateData = await preparaUpdateAppData(newCurrentUser);
        updateAppData(mutateData);

        tonomapaCache.writeQuery({
            query: GET_USER_DATA,
            data: { currentUser: newCurrentUser },
        });

        if (newSnackBarMessage) {
            snackBarMessage.current = newSnackBarMessage;
            setSnackBarVisible(true);
        }
    };

    const { data: areasDeUsoData } = useQuery(GET_AREAS_DE_USO, {
        fetchPolicy: 'cache-only',
        variables: { territorioId: currentUser?.currentTerritorio?.id },
    });
    const areasDeUso = areasDeUsoData?.areasDeUso;
    const [persisteAreaDeUso] = useMutation(CREATE_OR_UPDATE_AREA_DE_USO);
    const [deleteAreaDeUso] = useMutation(DELETE_AREA_DE_USO);

    const { data: conflitosData } = useQuery(GET_CONFLITOS, {
        fetchPolicy: 'cache-only',
        variables: { territorioId: currentUser?.currentTerritorio?.id },
    });
    const conflitos = conflitosData?.conflitos;
    const [persisteConflito] = useMutation(CREATE_OR_UPDATE_CONFLITO);
    const [deleteConflito] = useMutation(DELETE_CONFLITO);

    const { data: anexosData } = useQuery(GET_ANEXOS, {
        fetchPolicy: 'cache-only',
        variables: { territorioId: currentUser?.currentTerritorio?.id },
    });
    const anexos = anexosData?.anexos;

    const [deleteAllUsosEConflitos] = useMutation(DELETE_ALL_USOS_E_CONFLITOS);

    const { data: mensagensData } = useQuery(GET_MENSAGENS, {
        fetchPolicy: 'cache-only',
        variables: { territorioId: currentUser?.currentTerritorio?.id },
    });
    const mensagens = mensagensData?.mensagens;

    const [persisteMensagem] = useMutation(CREATE_OR_UPDATE_MENSAGEM);

    const [mensagensRefetch] = useLazyQuery(GET_MENSAGENS, {
        fetchPolicy: 'network-only',
        notifyOnNetworkStatusChange: true,
    });

    if (currentUserError) {
        console.log('Erro ao pegar os dados:', currentUserError);
        log('MapaScreen: erro no carregaTudoLocal');
        logError(currentUserError);
        Alert.alert(
            'Erro',
            `Não foi possível carregar os seus dados. Favor tentar entrar novamente, e se não funcionar, entre em contato com a equipe do Tô no Mapa. Mensagem de erro: ${currentUserError}`,
            [
                {
                    text: 'OK',
                    onPress: async () => {
                        signOut();
                    },
                },
            ],
            { cancelable: false }
        );
    }

    /**
     * @type object
     */
    const [state, setState] = useReducer(reducer, initialState);

    /**
     * @type object
     */
    const map = useRef(null);

    /**
     * @type object
     */
    const mapMarker = useRef([]);

    /**
     * @type object
     */
    const snackBarMessage = useRef('');

    const gpsInterval = useRef(null);

    /**
     * @type object
     */
    const [snackBarVisible, setSnackBarVisible] = useState(false);

    /**
     * Recarrega dados de usuário do servidor
     * @inner recarregaCurrentUser
     */
    const recarregaCurrentUser = () => {
        console.log('Refetched from server! Is it really necessary?');
        currentUserRefetch();
    };

    const addMensagens = (newMensagensRaw) => {
        if (!newMensagensRaw || newMensagensRaw.length === 0) {
            return;
        }
        const currentTerritorioId = currentUser.currentTerritorio.id;
        const currentStatusId = currentUser.currentTerritorio.statusId;
        let newStatusId;
        const newMensagens = [...mensagens];
        for (const newMensagemRaw of newMensagensRaw) {
            const newMensagem = {
                ...newMensagemRaw,
                dataRecebimento: new Date(),
            };
            newMensagens.push(newMensagem);
            const mutateData = preparaCreateOrUpdateMensagem({
                newMensagem,
                currentTerritorioId,
            });
            persisteMensagem({
                ...mutateData,
                update: (cache, { data }) => {
                    updateHandlerByName({
                        operationName: 'CreateOrUpdateMensagem',
                        cache,
                        data,
                        localId: newMensagem.id,
                    });
                },
            });

            try {
                const extra = JSON.parse(newMensagemRaw.extra);
                newStatusId = extra.statusId || newStatusId;
            } catch (e) {
                console.log('erro no parse da mensagem', newMensagemRaw);
            }
        }

        if (newMensagens.length > mensagens.length) {
            // write messages received to currentUser's cache, in case
            // the user is offline and closes the app.
            const newCurrentUser = {
                ...currentUser,
                currentTerritorio: {
                    ...currentUser.currentTerritorio,
                    mensagens: [...newMensagens],
                    statusId: newStatusId || currentStatusId,
                },
            };
            tonomapaCache.writeQuery({
                query: GET_USER_DATA,
                data: { currentUser: newCurrentUser },
                variables: {},
            });
        }

        if (newStatusId && newStatusId !== currentStatusId) {
            alertTerritorioStatusIdChanged(newStatusId);
            return;
        }

        const latestMensagem = newMensagensRaw.slice(-1)[0];
        Alert.alert(
            'Nova mensagem da equipe Tô no Mapa',
            `Mensagem:\n\n${latestMensagem.texto}`,
            [
                {
                    text: 'Ver todas as mensagens',
                    onPress: async () => {
                        navigation.navigate('Mensagens');
                    },
                },
                {
                    text: 'Ok',
                    onPress: async () => {},
                },
            ],
            { cancelable: false }
        );
    };

    const pingDashboardUpdates = async () => {
        const territorioId = currentUser?.currentTerritorio?.id;
        if (currentUser && territorioId) {
            const response = tonomapaCache.readQuery({
                query: GET_MENSAGENS,
                variables: { territorioId },
            });
            if (!response) {
                return;
            }
            const { mensagens } = response;
            const mensagensFromServer = mensagens.filter(
                (mensagem) => mensagem.originIsDevice !== true
            );
            const latestMensagemFromServer =
                mensagensFromServer.length > 0 ? mensagensFromServer.slice(-1)[0] : null;
            const minId = latestMensagemFromServer?.id || 1;
            const res = await mensagensRefetch({
                variables: {
                    territorioId,
                    minId,
                    senderIsDashboard: true,
                },
            });
            if (!res.data) {
                return;
            }
            addMensagens(res.data.mensagens);
        }
    };

    const setMapEditingHandler = () => {
        setTela(TELAS.MAPEAR_TERRITORIO);
    };

    /**
     * Atualiza as configurações de usuária e salva
     * @inner updateSettings
     * @param  {Object}       params Novo objeto de configurações "settings"
     */
    const updateSettings = (params) => {
        persisteSettings(params.settings, client).then((newSettings) => {
            setSettings(newSettings);
            if (!params.noSnackBarMessage) {
                snackBarMessage.current =
                    params.snackBarMessage || 'Configurações atualizadas com sucesso';
                setSnackBarVisible(true);
            }
        });
    };

    const mapFitToElements = ({ polygons, markers, region, editing = null }) => {
        if (!map.current) {
            return;
        }
        if (
            polygons.length ||
            markers.length ||
            (editing && editing.coordinates && editing.coordinates.length >= 3)
        ) {
            if (
                polygons.length === 0 &&
                markers.length === 1 &&
                (!editing || editing.coordinates.length < 3)
            ) {
                map.current.animateCamera({ center: markers[0].coordinate });
                return;
            }

            map.current.fitToCoordinates(getAllMapElementsCoords({ polygons, markers, editing }), {
                edgePadding: edgePaddingAdjusted,
            });
            return;
        }

        if (region) {
            map.current.animateToRegion(region, 2000);
        }
    };

    const mapFitToCurrentElements = () => {
        mapFitToElements({
            polygons: state.polygons,
            markers: state.markers,
            region: state.region,
            editing: settings.editing,
        });
    };

    const compartilharRelatorio = async () => {
        if (!isValidado(currentUser.currentTerritorio)) {
            Alert.alert(
                'Cadastro não validado',
                'Só é possível gerar e compartilhar o relatório quando o cadastro for validado pela equipe Tô no Mapa.\n\nPara isso preencha todas informações, anexe a ata da reunião comunitária e envie o cadastro para análise.',
                [
                    {
                        text: 'Entendi',
                        onPress: async () => {},
                    },
                ],
                { cancelable: false }
            );
            return;
        }

        if (!online) {
            Alert.alert(
                'Sem conexão de internet',
                'Só é possível gerar o relatório quando o seu celular estiver conectado na internet!\n\nPor favor, tente mais tarde, com o celular conectado.',
                [
                    {
                        text: 'Entendi',
                        onPress: () => {},
                    },
                ],
                { cancelable: false }
            );
            return;
        }

        setState({ carregando: true });
        const success = await getTerritorioReport(currentUser);
        setState({ carregando: false });

        if (!success) {
            Alert.alert(
                'Erro ao gerar o relatório!',
                'O servidor pode não estar respondendo adequadamente. Por favor, tente novamente mais tarde com boa conexão de internet. Se o erro persistir, favor entrar em contato com a equipe Tô no Mapa',
                [
                    {
                        text: 'Entendi',
                        onPress: () => {
                            navigation.jumpTo('Home');
                        },
                    },
                ],
                { cancelable: false }
            );
        }

        navigation.jumpTo('Home');
    };

    const persistePoligonos = useCallback(
        async ({ polygons, settings, newSnackBarMessage }) => {
            if (!polygons) {
                return;
            }

            //Traduz polygons para o formato "MultiPolygon"
            const multiPol = {
                type: 'MultiPolygon',
                coordinates: [],
            };

            for (const polygon of polygons) {
                const backPolygon = [[]];
                for (const i in polygon.coordinates) {
                    backPolygon[0].push([
                        polygon.coordinates[i].longitude,
                        polygon.coordinates[i].latitude,
                    ]);
                }
                backPolygon[0].push([
                    polygon.coordinates[0].longitude,
                    polygon.coordinates[0].latitude,
                ]);
                multiPol.coordinates.push(backPolygon);
            }
            // Update settings if necessary:
            if (settings) {
                updateSettings({ settings, noSnackBarMessage: true });
            }
            // Update currentUser (poligono)
            const newCurrentUser = {
                ...currentUser,
                currentTerritorio: {
                    ...currentUser.currentTerritorio,
                    poligono: {
                        ...multiPol,
                    },
                },
            };
            preparaAndPersisteCurrentUser(newCurrentUser, newSnackBarMessage);
            setState({ polygons });
        },
        [currentUser]
    );

    const persisteMoveUsoOuConflito = async () => {
        const adjustedWindowCenter = {
            x: windowCenter.x,
            y: windowCenter.y + metrics.tenHeight * 1.5,
        };
        const coords = await map.current.coordinateForPoint(adjustedWindowCenter);
        const usoOuConflitoEditado = {
            ...settings.editingUsoOuConflito.marker.object,
            posicao: {
                type: 'Point',
                coordinates: [coords.longitude, coords.latitude],
            },
        };
        navigation.jumpTo('Home', { usoOuConflitoEditado, tela: TELAS.MAPEAR_USOSECONFLITOS });
        updateSettings({
            settings: { editingUsoOuConflito: null },
            noSnackBarMessage: true,
        });
    };

    const deleteUsoOuConflito = ({ localId, type, showSnackBar }) => {
        const mutateData = preparaDeleteUsoOuConflito({ localId, type });
        const update = (cache, { data }) => {
            updateHandlerByName({
                operationName: `Delete${type}`,
                cache,
                data,
                localId,
            });
        };
        switch (type) {
            case 'Conflito':
                deleteConflito({
                    ...mutateData,
                    update,
                });
                break;
            case 'AreaDeUso':
                deleteAreaDeUso({
                    ...mutateData,
                    update,
                });
                break;
        }
        if (showSnackBar) {
            snackBarMessage.current = `${
                type === 'Conflito' ? 'Conflito' : 'Local de Uso'
            } apagado com sucesso`;
            setSnackBarVisible(true);
        }
    };

    const persisteUsoOuConflito = () => {
        const usoOuConflitoEditado = route.params.usoOuConflitoEditado;
        delete route.params.usoOuConflitoEditado;

        const localId = usoOuConflitoEditado.id;
        const type = usoOuConflitoEditado.__typename.replace('Type', '');

        if (route.params.apagarUsoOuConflito) {
            deleteUsoOuConflito({ localId, type, showSnackBar: true });
            return;
        }

        // Check if Conflito was changed to AreaDeUso or viceversa. If so, there will be TWO operations: delete and create, instead of update:
        const locais = type === 'AreaDeUso' ? conflitos : areasDeUso;
        const found = locais.find((local) => {
            return local.id === localId;
        });
        if (found) {
            setTimeout(() => {
                const typeToDelete = type === 'AreaDeUso' ? 'Conflito' : 'AreaDeUso';
                deleteUsoOuConflito({ localId, type: typeToDelete, showSnackBar: false });
            }, 500);
            usoOuConflitoEditado.id = geraTmpId();
        }

        const mutateData = preparaCreateOrUpdateUsoOuConflito({
            usoOuConflitoEditado,
            currentTerritorioId: currentUser.currentTerritorio.id,
            type,
        });
        const update = (cache, { data }) => {
            updateHandlerByName({
                operationName: `CreateOrUpdate${type}`,
                cache,
                data,
                localId,
            });
        };
        switch (type) {
            case 'Conflito':
                persisteConflito({
                    ...mutateData,
                    update,
                });
                snackBarMessage.current = ehTmpId(localId)
                    ? 'Conflito criado com sucesso'
                    : 'Conflito atualizado com sucesso';
                break;
            case 'AreaDeUso':
                persisteAreaDeUso({
                    ...mutateData,
                    update,
                });
                snackBarMessage.current = ehTmpId(localId)
                    ? 'Local de Uso criado com sucesso'
                    : 'Local de Uso atualizado com sucesso';
                break;
        }
        setSnackBarVisible(true);
    };

    const clearUsosEConflitos = () => {
        const mutateData = preparaDeleteAllUsosEConflitos(currentUser.currentTerritorio.id);
        deleteAllUsosEConflitos({
            ...mutateData,
            update: (cache, { data }) => {
                updateHandlerByName({
                    operationName: 'DeleteAllUsosEConflitos',
                    cache,
                    data,
                    localId: null,
                });
            },
        });
    };

    const validaDadosAsync = async (newEtapa = null) => {
        const { checkList: newSituacao, emPreenchimentoStatus } = await validaDados({
            currentUser,
            polygons: state.polygons,
            anexos,
            areasDeUso,
            conflitos,
        });
        const newStatusMessage = buildStatusMessage(
            currentUser.currentTerritorio.statusId,
            emPreenchimentoStatus
        );
        setState({
            enviarDadosDialogo: {
                ...initialState.enviarDadosDialogo,
                situacao: newSituacao,
                etapa: newEtapa || state.enviarDadosDialogo.etapa,
            },
            statusMessage: newStatusMessage,
        });
    };

    const alertTerritorioStatusIdChanged = (newStatusId) => {
        // These internal status changes don't have to generate an alert
        if ([STATUS.ENVIO_PTT_NAFILA, STATUS.ENVIO_ARQUIVOS_PTT_NAFILA].includes(newStatusId)) {
            return;
        }

        tonomapaCache.writeQuery({
            query: GET_USER_DATA,
            data: {
                currentUser: {
                    ...currentUser,
                    currentTerritorio: {
                        ...currentUser.currentTerritorio,
                        statusId: newStatusId,
                    },
                },
            },
        });

        const { title, body, urlReferencia, hasFaleConoscoBtn } = alertStatusId({
            statusId: newStatusId,
        });

        const botoes = [];

        const btnEntendi = {
            text: 'Entendi',
            onPress: () => {
                if (newStatusId === STATUS.OK_TONOMAPA) {
                    Alert.alert(
                        `A comunidade ${currentUser.currentTerritorio.nome} foi validada. E agora?`,
                        'Deseja saber mais sobre TICCAs?',
                        [
                            {
                                text: 'Não',
                                onPress: () => {},
                            },
                            {
                                text: 'Sim, eu quero',
                                onPress: () => {
                                    navigation.jumpTo('TICCAs');
                                },
                            },
                        ],
                        { cancelable: false }
                    );
                    return;
                }
            },
        };

        if (urlReferencia) {
            botoes.push({
                text: 'Ir ao site',
                onPress: () => {
                    openUrlInBrowser(urlReferencia);
                    navigation.jumpTo('Home');
                },
            });
        }

        if (hasFaleConoscoBtn) {
            botoes.push({
                text: 'Fale conosco',
                onPress: () => {
                    navigation.jumpTo('FaleConosco');
                },
            });
        }

        botoes.push(btnEntendi);

        Alert.alert(title, body, botoes, { cancelable: false });
    };

    const handleTerritorioStatusAlert = () => {
        const statusId = currentUser.currentTerritorio.statusId;
        if (
            [
                STATUS.EM_PREENCHIMENTO,
                STATUS.REVISADO_COM_PENDENCIAS,
                STATUS.EM_PREENCHIMENTO_PTT,
            ].includes(statusId)
        ) {
            setShowEnviarDadosDialogo(true);
            return;
        }

        const { title, body, urlReferencia, hasFaleConoscoBtn } = alertStatusId({
            statusMessage: state.statusMessage,
        });

        const botoes = [];

        if (isValidado(statusId)) {
            botoes.push({
                text: 'Compartilhar relatório',
                onPress: () => {
                    navigation.jumpTo('Home', { compartilharRelatorio: true });
                },
            });
        }

        if (urlReferencia) {
            botoes.push({
                text: 'Ir ao site',
                onPress: () => {
                    openUrlInBrowser(urlReferencia);
                    navigation.jumpTo('Home');
                },
            });
        }

        if (hasFaleConoscoBtn) {
            botoes.push({
                text: 'Fale conosco',
                onPress: () => {
                    navigation.jumpTo('FaleConosco');
                },
            });
        }

        botoes.push({
            text: 'Entendi',
            onPress: () => {},
        });

        Alert.alert(title, body, botoes, { cancelable: true });
    };

    const alertGpsIsDisabled = () => {
        const title = 'Localização desativada!';
        const body =
            'Por favor, ative a localização do seu celular para poder mapear usando o modo GPS!';
        const botoes = [];
        if (Platform.OS === 'android') {
            botoes.push({
                text: 'Ativar localização',
                onPress: () => {
                    startActivityAsync(ActivityAction.LOCATION_SOURCE_SETTINGS);
                },
            });
        }
        botoes.push({
            text: 'Entendi',
            onPress: () => {},
        });

        Alert.alert(title, body, botoes, { cancelable: true });
    };

    const checkGpsServicesAsync = async () => {
        const isGpsEnabled = await Location.hasServicesEnabledAsync();
        if (!isGpsEnabled) {
            clearInterval(gpsInterval.current);
            gpsInterval.current = null;
            updateSettings({ settings: { useGPS: false }, snackBarMessage: 'GPS desativado' });
            alertGpsIsDisabled();
            return;
        }
    };

    useFocusEffect(
        useCallback(() => {
            if (tela === TELAS.MAPA) {
                if (gpsInterval.current) {
                    clearInterval(gpsInterval.current);
                    gpsInterval.current = null;
                }
                return;
            }

            if (settings?.useGPS) {
                gpsInterval.current = setInterval(() => {
                    checkGpsServicesAsync();
                }, 5000);
                return;
            }

            if (gpsInterval.current) {
                clearInterval(gpsInterval.current);
                gpsInterval.current = null;
            }
        }, [settings?.useGPS, tela])
    );

    useFocusEffect(
        useCallback(() => {
            const newSettings = carregaSettings(client);
            setSettings(newSettings);

            if (settings) {
                const newTela = route.params?.tela || TELAS.MAPA;
                if (newTela !== tela) {
                    setTela(newTela);
                }
            }

            if (!route.params) {
                return;
            }
            if (route.params && 'appIntro' in route.params) {
                delete route.params.appIntro;
                updateSettings({ settings: { appIntro: true }, noSnackBarMessage: true });
            }
            if ('usoOuConflitoEditado' in route.params) {
                persisteUsoOuConflito();
                return;
            }
            if ('snackBarMessage' in route.params && !route.params.newCurrentUser) {
                snackBarMessage.current = route.params.snackBarMessage;
                delete route.params.snackBarMessage;
                setSnackBarVisible(true);
            }
            if (map && 'compartilharRelatorio' in route.params) {
                delete route.params.compartilharRelatorio;
                compartilharRelatorio();
                return;
            }
            if (route.params && 'firstRun' in route.params && !currentUserLoading) {
                delete route.params.firstRun;
                if (settings && settings.editing !== null) {
                    route.params.tela = TELAS.MAPEAR_TERRITORIO;
                }
                pingDashboardUpdates();
            }
            if (route.params.newMensagem) {
                const newMensagem = route.params.newMensagem;
                delete route.params.newMensagem;

                const mutateData = preparaCreateOrUpdateMensagem({
                    newMensagem,
                    currentTerritorioId: currentUser.currentTerritorio.id,
                });
                persisteMensagem({
                    ...mutateData,
                    update: (cache, { data }) => {
                        updateHandlerByName({
                            operationName: 'CreateOrUpdateMensagem',
                            cache,
                            data,
                            localId: newMensagem.id,
                        });
                    },
                });
                if (!route.params.newCurrentUser && route.params.snackBarMessage) {
                    snackBarMessage.current = route.params.snackBarMessage;
                    setSnackBarVisible(true);
                    delete route.params.snackBarMessage;
                }
            }
            if (route.params.newCurrentUser) {
                preparaAndPersisteCurrentUser(
                    route.params.newCurrentUser,
                    route.params.snackBarMessage
                );
                delete route.params.newCurrentUser;
                delete route.params.snackBarMessage;
            }
            if (route.params.entraModoPTT) {
                delete route.params.entraModoPTT;
                preparaAndPersisteCurrentUser(
                    {
                        ...currentUser,
                        currentTerritorio: {
                            ...currentUser.currentTerritorio,
                            statusId: STATUS.EM_PREENCHIMENTO_PTT,
                        },
                    },
                    'Modo cadastro PTT'
                );
            }
        }, [route.params])
    );

    useFocusEffect(
        useCallback(() => {
            if (!currentUser || currentUserLoading) {
                if (!currentUserLoading) {
                    recarregaCurrentUser();
                }
                return;
            }
            if (!currentUser.currentTerritorio.id) {
                return;
            }
            if (areasDeUso && conflitos) {
                const {
                    region,
                    polygons,
                    markers,
                    markersTipoUsoConflito,
                    mapMarker: newMapMarker,
                } = gqlTerritorioToMapa({
                    currentUser,
                    conflitos,
                    areasDeUso,
                    unidadeArea: settings.unidadeArea,
                });

                if (
                    'newTerritorioId' in route.params &&
                    (currentUser.currentTerritorio.id === route.params.newTerritorioId ||
                        route.params.newTerritorioId === null)
                ) {
                    delete route.params.newTerritorioId;
                    mapFitToElements({ polygons, markers, region });
                }

                mapMarker.current = newMapMarker;
                setState({
                    region,
                    polygons,
                    markers,
                    markersTipoUsoConflito,
                });
            }
        }, [areasDeUso, conflitos, currentUser?.currentTerritorio?.poligono])
    );

    useEffect(() => {
        if (currentUserData && areasDeUsoData && conflitosData && anexosData) {
            validaDadosAsync();
        }
    }, [currentUser, anexos, areasDeUso, conflitos, state.polygons]);

    useEffect(() => {
        if (currentUserData && areasDeUsoData && conflitosData && anexosData) {
            const newCurrentUser = {
                ...currentUser,
                currentTerritorio: {
                    ...currentUser.currentTerritorio,
                    areasDeUso: [...areasDeUso],
                    conflitos: [...conflitos],
                    anexos: [...anexos],
                },
            };
            tonomapaCache.writeQuery({
                query: GET_USER_DATA,
                data: { currentUser: newCurrentUser },
                variables: {},
            });
        }
    }, [conflitos, anexos, areasDeUso]);

    // useFocusEffect(useCallback(() => {
    //     mapFitToCurrentElements();
    // }, [state.region, state.polygons, state.markers]));

    useEffect(() => {
        if (currentUser) {
            if (notification) {
                if (notification.isMultiple) {
                    console.log('received multiple notifications!', notification);
                    setNotification(null);
                    pingDashboardUpdates();
                    return;
                }
                const notificationData = notification.request.content.data;
                if (notificationData.territorioId === parseInt(currentUser.currentTerritorio.id)) {
                    const newMensagens = [
                        {
                            ...MENSAGEM,
                            id: notificationData.id,
                            texto: notificationData.texto,
                            extra: JSON.stringify(notificationData),
                            dataEnvio: notificationData.dataEnvio
                                ? new Date(moment(notificationData.dataEnvio).valueOf())
                                : '',
                            originIsDevice: notificationData.originIsDevice,
                        },
                    ];
                    addMensagens(newMensagens);
                    setNotification(null);
                    return;
                }

                const territorioNotificado = currentUser.territorios.find(
                    (territorio) => parseInt(notification.territorioId) === territorio.id
                );
                if (!territorioNotificado) {
                    console.log('Não encontrei o território notificado! o que houve???');
                    setNotification(null);
                    return;
                }

                Alert.alert(
                    'Nova mensagem da equipe Tô no Mapa',
                    `A equipe Tô no Mapa enviou uma mensagem para o território '${territorioNotificado.nome}': \n\n${notification.texto} \n\nVocê pode mudar para ${territorioNotificado.nome} em "Configurações".`,
                    [
                        {
                            text: 'Ir para as Configurações',
                            onPress: async () => {
                                navigation.jumpTo('Configuracoes');
                            },
                        },
                        {
                            text: 'Ok',
                            onPress: async () => {},
                        },
                    ],
                    { cancelable: true }
                );
                setNotification(null);
            }
        }
    }, [currentUser, notification]);

    useEffect(() => {
        const onChangeAppState = (appState) => {
            if (appState.match(/inactive|background/)) {
                return;
            }
            pingDashboardUpdates();
        };
        const appStateChange = AppState.addEventListener('change', onChangeAppState);

        return () => {
            appStateChange.remove();
        };
    }, []);

    useFocusEffect(
        useCallback(() => {
            const onBackPress = () => {
                navigation.jumpTo('Home');
                return true;
            };
            const backHandlerBackPress = BackHandler.addEventListener(
                'hardwareBackPress',
                onBackPress
            );

            return () => {
                backHandlerBackPress.remove();
            };
        }, [])
    );

    useFocusEffect(
        useCallback(() => {
            if (currentUser?.currentTerritorio?.statusId) {
                const newIsPTT = isPTTMode(currentUser.currentTerritorio.statusId);
                setIsPTTModeVar(newIsPTT);
                const newIsTerritorioEditable = isCurrentTerritorioEditable(
                    currentUser.currentTerritorio.statusId
                );
                setIsCurrentTerritorioEditableVar(newIsTerritorioEditable);
            }
        }, [currentUser?.currentTerritorio?.statusId])
    );

    let mapTypeStyles = {
        label: 'Mapa',
        iconColor: colors.Foundation.TextColor.White,
        polygonFillColor: colors.polygonBackgroundHybrid,
        polygonEditingStrokeColor: colors.background,
        backgroundMarkerView: 'white',
        logoImageToShow: LOGO_IMAGE.branca,
        markerIconToShow: ICONE_TIPO.locationMarkClaro,
        iconButtonStyle: LayoutMapa.mapearStatusButtonClaro,
        iconButtonLeftStyle: LayoutMapa.mapearStatusButtonLeftClaro,
    };
    if (Constants.manifest.extra.xyzRTiles.using || settings.mapType !== MAP_TYPES.HYBRID) {
        mapTypeStyles = {
            label: 'Satélite',
            iconColor: colors.Foundation.CorDeTexto.CorDeTexto2,
            polygonFillColor: colors.polygonBackgroundStandard,
            polygonEditingStrokeColor: colors.Foundation.CorDeTexto.CorDeTexto2,
            backgroundMarkerView: 'black',
            logoImageToShow: LOGO_IMAGE.colorida,
            markerIconToShow: ICONE_TIPO.locationMark,
            iconButtonStyle: LayoutMapa.mapearStatusButtonClaro,
            iconButtonLeftStyle: LayoutMapa.mapearStatusButtonLeftClaro,
        };
    }

    //////////////////////
    // RENDER!
    /////////////////////
    if (!settings || !state.region || !currentUser || currentUserLoading) {
        return <ToNoMapaLoading />;
    }

    return (
        <>
            <View style={Layout.containerStretched}>
                <StatusBar
                    translucent={true}
                    backgroundColor={colors.Foundation.Green.G300}
                    barStyle="dark-content"
                />
                <Map
                    ref={map}
                    tela={tela}
                    setTela={setTela}
                    persistePoligonos={persistePoligonos}
                    markers={state.markers}
                    polygons={state.polygons}
                    region={state.region}
                    markersTipoUsoConflito={state.markersTipoUsoConflito}
                    mapTypeStyles={mapTypeStyles}
                    mapFitToElements={mapFitToCurrentElements}
                    settings={settings}
                    updateSettings={updateSettings}
                    isCurrentTerritorioEditableVar={isCurrentTerritorioEditableVar}
                    isPTTModeVar={isPTTModeVar}
                />

                {tela === TELAS.MAPA ? (
                    <DefaultTopBar
                        statusMessage={state.statusMessage}
                        mapTypeStyles={mapTypeStyles}
                        handleTerritorioStatusAlert={handleTerritorioStatusAlert}
                    />
                ) : (
                    <>
                        <MapTopBar
                            navigation={navigation}
                            tela={tela}
                            currentTerritorio={currentUser.currentTerritorio}
                            settings={settings}
                            updateSettings={updateSettings}
                            markers={state.markers}
                            polygons={state.polygons}
                            mapFitToElements={mapFitToCurrentElements}
                        />
                        <MapBottomBar
                            tela={tela}
                            setTela={setTela}
                            currentTerritorio={currentUser.currentTerritorio}
                            settings={settings}
                            persisteMoveUsoOuConflito={persisteMoveUsoOuConflito}
                            updateSettings={updateSettings}
                            persistePoligonos={persistePoligonos}
                            markers={state.markers}
                            polygons={state.polygons}
                            snackBarMessage={snackBarMessage}
                            setSnackBarVisible={setSnackBarVisible}
                            clearUsosEConflitos={clearUsosEConflitos}
                        />
                    </>
                )}

                <EnviarParaDashboardHandler
                    currentUser={currentUser}
                    navigation={navigation}
                    settings={settings}
                    setState={setState}
                    enviarDadosDialogo={state.enviarDadosDialogo}
                    snackBarMessage={snackBarMessage}
                    validaDadosAsync={validaDadosAsync}
                    isEnviarEnabledVar={isEnviarEnabled}
                    isPTTModeVar={isPTTModeVar}
                />
                {tela === TELAS.MAPA && (
                    <>
                        <Image
                            source={mapTypeStyles.logoImageToShow}
                            style={
                                !isPTTModeVar
                                    ? LayoutMapa.mapearLogoImageBottom
                                    : LayoutMapa.mapearLogoImageBottomPTT
                            }
                            resizeMode="contain"
                        />
                        {!isPTTModeVar && isCurrentTerritorioEditableVar && (
                            <FAB
                                style={Layout.fab}
                                color={colors.Foundation.CorDeTexto.CorDeTexto2}
                                icon="pencil"
                                label="mapear"
                                onPress={setMapEditingHandler}
                            />
                        )}
                    </>
                )}
                <ToNoMapaSnackbar
                    visible={snackBarVisible === true}
                    setVisible={setSnackBarVisible}
                    style={{ bottom: metrics.tenWidth * 8, zIndex: 100 }}
                    message={snackBarMessage.current}
                />
            </View>
            {tela === TELAS.MAPA && (
                <>
                    {isPTTModeVar ? (
                        <PTTBottomBar
                            navigation={navigation}
                            tela={tela}
                            currentTerritorio={currentUser.currentTerritorio}
                        />
                    ) : (
                        <DefaultBottomBar
                            navigation={navigation}
                            tela={tela}
                            currentTerritorio={currentUser.currentTerritorio}
                        />
                    )}
                </>
            )}
            {state.carregando && <ToNoMapaLoading isOverlay={true} />}
            {currentUser.currentTerritorio.nome && settings.appIntro && (
                <ToNoMapaIntro
                    introDialog={route.params?.appIntroDialog}
                    onDone={() => {
                        updateSettings({
                            settings: { appIntro: false },
                            noSnackBarMessage: true,
                        });
                    }}
                />
            )}
        </>
    );
}

MapaScreen.propTypes = {
    navigation: PropTypes.object,
    route: PropTypes.object,
};
