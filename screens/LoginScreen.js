import React, { useEffect, useReducer, useRef, useState } from 'react';
import { Alert, Image, StyleSheet, View, Text } from 'react-native';
import { Dialog, Portal, useTheme } from 'react-native-paper';
import * as yup from 'yup';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import { Layout } from '../styles';
import metrics from '../utils/metrics';

import TextoSobre from '../components/TextoSobre';
import { TextInputDefault, TextInputTelefone } from '../components/inputs';
import {
    ButtonContainedPrimary,
    ButtonFullwidthPrimary,
    ButtonHelperText,
    ButtonTextPrimary,
    ButtonVoltar,
} from '../components/buttons';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

const styles = StyleSheet.create({
    bottomButton: {
        marginBottom: 16,
    },
    logoImage: {
        marginTop: 70,
        marginBottom: 20,
        height: 198,
        width: 237,
        alignSelf: 'center',
    },
    topView: {
        flexGrow: 1,
    },
    bottomView: {},
});

export default function LoginScreen({ navigation, route }) {
    const { signIn, online } = useToNoMapa();

    const theme = useTheme();

    const reducer = (state, newState) => {
        return { ...state, ...newState };
    };
    /**
     * @type object
     */
    const [state, setState] = useReducer(reducer, {
        username: '',
        password: '',
        telefone: '',
    });

    const [errors, setErrors] = useState([]);

    const [preRequisitosOpen, setPreRequisitosOpen] = useState(false);

    if (route.params && 'username' in route.params) {
        const newState = {
            username: route.params.username,
            telefone: route.params.username,
            password: route.params.password,
        };
        delete route.params.username;
        delete route.params.password;
        setState(newState);
    }

    const scrollRef = useRef(null);

    const alertOffline = () => {
        const title = 'Sem internet!';
        const body =
            'Não dá pra fazer login ou cadastrar uma conta sem conexão de internet!\n\nDepois que você entrar, o App funciona totalmente sem internet. Só para entrar pela primeira vez precisa internet.\n\nPor favor, tente novamente mais tarde, com conexão.';
        Alert.alert(
            title,
            body,
            [
                {
                    text: 'Entendi',
                    onPress: () => {},
                },
            ],
            { cancelable: true }
        );
    };

    const validaDadosAsync = async () => {
        const newErrors = [];
        const schema = yup
            .string()
            .required('telefone|O número de telefone é obrigatório!')
            .matches(/\d{11}/, 'telefone|Telefone inválido')
            .nullable();
        await schema.validate(state.username, { abortEarly: false }).catch((err) => {
            newErrors.push(err.errors[0].split('|'));
            // const title = err.errors[0];
            // const body = 'Por favor, coloque o número do seu telefone, com DDD';
            // Alert.alert(
            //     title,
            //     body,
            //     [
            //         {
            //             text: 'Entendi',
            //             onPress: () => {},
            //         },
            //     ],
            //     { cancelable: true }
            // );
        });
        setErrors(newErrors);
        if (newErrors.length > 0) {
            return false;
        }
        return true;
    };

    const onSubmitForm = async () => {
        if (!online) {
            alertOffline();
            return;
        }
        const result = await validaDadosAsync();
        if (result) {
            if (!state.password) {
                const title = 'Quer se cadastrar?';
                const body =
                    'Se você já usou o App antes, precisa colocar o seu código de segurança.\n\nSe esqueceu seu código, clique em "Esqueci meu Código"\n\nSe é a sua primeira vez no Tô no Mapa, cadastre-se!';
                Alert.alert(
                    title,
                    body,
                    [
                        {
                            text: 'Cadastrar nova conta',
                            onPress: () => {
                                navigation.navigate('Cadastro', { username: state.username });
                            },
                        },
                        {
                            text: 'Cancelar',
                            onPress: () => {},
                        },
                    ],
                    { cancelable: true }
                );
                return;
            }
            signIn({ username: state.username, password: state.password, navigation });
        }
    };

    const handleQueroMeCadastrar = () => {
        if (!online) {
            alertOffline();
            return;
        }
        setPreRequisitosOpen(true);
        // navigation.navigate('PreRequisitos');
    };

    const esqueciMeuCodigo = () => {
        if (!online) {
            alertOffline();
            return;
        }
        navigation.navigate('PedirCodigoSeguranca', {
            usuaria: state.telefone,
        });
    };

    const getErrorMsg = (key) => {
        const error = errors.find((e) => {
            return e[0] === key;
        });
        if (!error) {
            return null;
        }
        return error[1];
    };

    useEffect(() => {
        if (errors.length > 0) {
            validaDadosAsync();
        }
    }, [state.telefone]);

    return (
        <>
            <View style={Layout.containerStretched}>
                <KeyboardAwareScrollView keyboardShouldPersistTaps="handled" enableOnAndroid={true}>
                    <Image
                        source={require('../assets/capa.png')}
                        style={{ height: metrics.tenHeight * 35, width: '100%' }}
                    />
                    <View
                        style={{
                            position: 'absolute',
                            top: metrics.tenHeight * 3,
                            width: '100%',
                            flex: 1,
                            alignItems: 'center',
                        }}>
                        <Image
                            source={require('../assets/icons/login/icon-login.png')}
                            style={styles.logoImage}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={{ ...Layout.loginBody, marginBottom: metrics.tenHeight * 2 }}>
                        <TextInputTelefone
                            label="Seu telefone"
                            required={true}
                            value={state.telefone}
                            onChangeText={(usuaria) => {
                                setState({
                                    telefone: usuaria,
                                    username: usuaria.replace(/[^0-9.]/g, ''),
                                });
                            }}
                            error={getErrorMsg('telefone')}
                        />

                        <TextInputDefault
                            label="Código de segurança"
                            value={state.password}
                            keyboardType="number-pad"
                            onChangeText={(password) => {
                                setState({
                                    password: password,
                                });
                            }}
                        />
                        <ButtonHelperText onPress={esqueciMeuCodigo}>
                            Esqueci o meu código
                        </ButtonHelperText>

                        <ButtonFullwidthPrimary
                            style={{ marginTop: metrics.tenHeight * 3 }}
                            onPress={onSubmitForm}>
                            Entrar
                        </ButtonFullwidthPrimary>

                        <ButtonTextPrimary onPress={handleQueroMeCadastrar}>
                            Ainda não tenho cadastro
                        </ButtonTextPrimary>
                    </View>
                </KeyboardAwareScrollView>
            </View>
            <Portal>
                <Dialog
                    visible={preRequisitosOpen}
                    onDismiss={() => {
                        setPreRequisitosOpen(false);
                    }}>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingHorizontal: metrics.tenHeight * 2,
                            paddingVertical: 0,
                        }}>
                        <Image
                            source={require('../assets/images/logo/colorida/512/logo.png')}
                            style={{ marginRight: metrics.tenWidth * 0.8, height: 60, width: 60 }}
                        />
                        <Text style={{ fontWeight: '500', fontSize: metrics.tenHeight * 1.8 }}>
                            O que é o Tô no Mapa?
                        </Text>
                    </View>
                    <Dialog.ScrollArea style={{ maxHeight: '70%' }}>
                        <TextoSobre />
                    </Dialog.ScrollArea>
                    <Dialog.Actions>
                        <View
                            style={{
                                flex: 1,
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                            }}>
                            <ButtonVoltar
                                onPress={() => {
                                    setPreRequisitosOpen(false);
                                }}>
                                Voltar
                            </ButtonVoltar>
                            <ButtonContainedPrimary
                                onPress={() => {
                                    setPreRequisitosOpen(false);
                                    navigation.navigate('Cadastro');
                                }}>
                                Me cadastrar
                            </ButtonContainedPrimary>
                        </View>
                    </Dialog.Actions>
                </Dialog>
            </Portal>
        </>
    );
}
