import React from 'react';
import { View } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';

import colors from '../assets/colors';
import { Layout } from '../styles';

export default function ToNoMapaLoading({ isOverlay = false }) {
    return (
        <View style={isOverlay ? Layout.appOverlay : Layout.containerStretched}>
            <ActivityIndicator
                animating={true}
                color={colors.Foundation.Green.G300}
                size={'large'}
                style={Layout.logoImageSplash}
            />
        </View>
    );
}
