import React from 'react';
import { View } from 'react-native';
import { RadioButton, Text } from 'react-native-paper';

import { Layout } from '../styles';
import { TerritorioSelect } from './inputs';

export default function TerritorioList({
    currentTerritorio,
    territorios,
    novoTerritorio,
    setNovoTerritorio,
    setDialogListaTerritoriosVisible,
}) {
    if (!currentTerritorio.id) {
        return (
            <View>
                <Text style={Layout.textParagraph}>
                    Você ainda não enviou dados para o Tô no Mapa. Quando tiver enviado, poderá
                    adicionar novos territórios a serem mapeados aqui.
                </Text>
            </View>
        );
    }
    return (
        <>
            {territorios.length > 10 ? (
                <TerritorioSelect
                    territorios={territorios}
                    value={`${novoTerritorio ? novoTerritorio.id : currentTerritorio.id}`}
                    onValueChange={(newId) => {
                        if (!novoTerritorio || newId !== novoTerritorio.id) {
                            setNovoTerritorio(
                                territorios.find((item) => {
                                    return item.id === newId;
                                })
                            );
                        }
                        setDialogListaTerritoriosVisible(false);
                    }}
                />
            ) : (
                <RadioButton.Group
                    value={`${novoTerritorio ? novoTerritorio.id : currentTerritorio.id}`}
                    onValueChange={(newId) => {
                        if (!novoTerritorio || newId !== novoTerritorio.id) {
                            setNovoTerritorio(
                                territorios.find((item) => {
                                    return item.id === newId;
                                })
                            );
                        }
                        setDialogListaTerritoriosVisible(false);
                    }}>
                    {territorios.map((item) => (
                        <View style={Layout.row} key={`${item.id}`}>
                            <Text style={Layout.text}>{item.nome}</Text>
                            <RadioButton value={`${item.id}`} />
                        </View>
                    ))}
                </RadioButton.Group>
            )}
        </>
    );
}
