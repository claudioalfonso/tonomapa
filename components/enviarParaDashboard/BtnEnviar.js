import React from 'react';
import { Alert } from 'react-native';
import { Button } from 'react-native-paper';

export default function BtnEnviar({ isPTTModeVar, onPress, online }) {
    if (!online) {
        return (
            <Button
                onPress={() => {
                    Alert.alert(
                        'Sem internet!',
                        'Não é possível enviar os dados para análise sem conexão à internet.\n\nPor favor, tente novamente mais tarde, com conexão.',
                        [
                            {
                                text: 'Entendi',
                                onPress: () => {},
                            },
                        ],
                        { cancelable: true }
                    );
                }}>
                Sem internet: Não dá pra enviar
            </Button>
        );
    }
    return (
        <>
            {!isPTTModeVar ? (
                <Button
                    onPress={() => {
                        onPress(true);
                    }}>
                    Enviar para análise!
                </Button>
            ) : (
                <Button
                    onPress={() => {
                        onPress(true);
                    }}>
                    Enviar
                </Button>
            )}
        </>
    );
}
