import React from 'react';
import { Text, View } from 'react-native';

import { Layout } from '../../styles';

export default function Sucesso({ isPTTModeVar }) {
    if (isPTTModeVar) {
        return (
            <View style={Layout.col}>
                <Text style={Layout.textParagraph}>
                    Você já fez o pré-cadastro da comunidade na Plataforma de Territórios
                    Tradicionais do Conselho Nacional de Povos e Comunidades Tradicionais do
                    Ministério Público Federal.
                </Text>
                <Text style={Layout.textParagraph}>
                    Para seguir o cadastramento, acesse https://territoriostradicionais.mpf.mp.br .
                </Text>
            </View>
        );
    }
    return (
        <View style={Layout.col}>
            <Text style={Layout.textParagraph}>
                Os dados da comunidade foram enviados para revisão pela equipe Tô no Mapa.
            </Text>
            <Text style={Layout.textParagraph}>
                Não desinstale o app, pois é nele que vamos responder se as informações foram
                aceitas ou se são necessárias alterações.
            </Text>
            <Text style={Layout.textParagraph}>Em breve daremos a resposta.</Text>
        </View>
    );
}
