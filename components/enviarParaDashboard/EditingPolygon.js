import React from 'react';
import { Text, View } from 'react-native';

import { Layout } from '../../styles';

export default function EditingPolygon() {
    return (
        <View style={Layout.colLeft}>
            <Text style={Layout.textParagraph}>Ops: você tem uma área sendo editada.</Text>
            <Text style={Layout.textParagraph}>
                Por favor, vá em "mapear território" para salvar esta área ou então cancelar a
                edição.
            </Text>
        </View>
    );
}
