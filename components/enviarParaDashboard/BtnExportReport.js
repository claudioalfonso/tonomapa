import React from 'react';
import { Button } from 'react-native-paper';

export default function BtnExportReport({ navigation, onPress }) {
    return (
        <Button
            onPress={() => {
                onPress();
                navigation.navigate('Home', { compartilharRelatorio: true });
            }}>
            Exportar relatório
        </Button>
    );
}
