import React from 'react';
import { Button } from 'react-native-paper';

import { initialState } from '../../assets/constants';
import TELAS from '../../bases/telas';

export default function BtnOk({ navigation, onPress, editing, label = null }) {
    return (
        <Button
            onPress={() => {
                onPress();
                navigation.navigate('Home', {
                    tela: editing ? TELAS.MAPEAR_TERRITORIO : TELAS.MAPA,
                });
            }}>
            {label || 'Ok'}
        </Button>
    );
}
