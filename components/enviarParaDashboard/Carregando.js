import React from 'react';
import { View } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';

import colors from '../../assets/colors';
import { Layout } from '../../styles';

export default function Carregando() {
    return (
        <View style={Layout.rowCenter}>
            <ActivityIndicator animating={true} color={colors.Foundation.Green.G300} />
        </View>
    );
}
