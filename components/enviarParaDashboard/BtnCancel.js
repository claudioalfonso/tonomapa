import React from 'react';
import { Button } from 'react-native-paper';

import { initialState } from '../../assets/constants';
import TELAS from '../../bases/telas';

export default function BtnCancel({ onPress }) {
    return <Button onPress={onPress}>Fechar</Button>;
}
