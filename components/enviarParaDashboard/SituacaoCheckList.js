import React, { useEffect, useState } from 'react';
import { Text, View } from 'react-native';
import { Avatar } from 'react-native-paper';

import metrics from '../../utils/metrics';
import { Layout } from '../../styles';
import { GET_MENSAGENS } from '../../db/queries';
import { isPTTMode } from '../../bases/status';
import { tonomapaCache } from '../../db/api';
import colors from '../../assets/colors';

export default function SituacaoCheckList({ situacao, currentTerritorio, online, isPTTModeVar }) {
    const territorioId = currentTerritorio.id;
    const statusId = currentTerritorio.statusId;
    const [pendencias, setPendencias] = useState(null);
    useEffect(() => {
        const { mensagens: mensagensAll } = tonomapaCache.readQuery({
            query: GET_MENSAGENS,
            variables: { territorioId },
        });
        const mensagens = mensagensAll.filter((item) => {
            if (!item.extra) {
                return false;
            }
            const extra = JSON.parse(item.extra);
            return extra && extra.statusId === statusId;
        });
        if (mensagens.length === 0) {
            setPendencias(null);
            return;
        }
        const ultimaMensagem = mensagens[mensagens.length - 1];
        const pendencia = ultimaMensagem.texto || null;
        setPendencias(pendencia);
    }, []);
    if (!situacao) {
        return <></>;
    }
    return (
        <>
            {pendencias && (
                <View style={Layout.textAlert}>
                    <Text>{pendencias}</Text>
                </View>
            )}
            {situacao.map((resultado, index) => (
                <View style={Layout.row} key={`${index}`}>
                    <Avatar.Icon
                        size={metrics.tenWidth * 3.6}
                        icon={resultado.icon}
                        color={resultado.iconStyle.color}
                        style={resultado.iconStyle}
                    />
                    <Text>{resultado.text}</Text>
                </View>
            ))}
            <View style={Layout.row} key="isOnline">
                <Avatar.Icon
                    size={metrics.tenWidth * 3.6}
                    icon={online ? 'check' : 'minus'}
                    color={
                        online
                            ? colors.Foundation.CorDeTexto.CorDeTexto2
                            : colors.Foundation.CorDeTexto.CorDeTexto
                    }
                    style={{
                        backgroundColor: online
                            ? colors.Foundation.Green.G300
                            : colors.Foundation.Feedback.Error,
                    }}
                />
                <Text>{online ? 'Conectado na internet' : 'Não conectado na internet'}</Text>
            </View>
            {!online && (
                <Text style={Layout.row}>
                    Não dá para enviar para análise, pois o celular não está conectado na internet.
                    Por favor, tente mais tarde, com internet.
                </Text>
            )}
            {isPTTModeVar && situacao.some((item) => item.pttError) && (
                <Text style={Layout.row}>
                    Não é possível enviar para a PTT. Por favor, complemente as informações que
                    faltam (círculos vermelhos acima).
                </Text>
            )}
        </>
    );
}
