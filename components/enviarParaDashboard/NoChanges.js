import React from 'react';
import { Text, View } from 'react-native';

import { Layout } from '../../styles';

export default function EditingPolygon() {
    return (
        <View style={Layout.colLeft}>
            <Text style={Layout.textParagraph}>Dados sincronizados com o servidor:</Text>
            <Text style={Layout.textParagraph}>
                Você não alterou nenhuma informação desde a última vez que enviou os dados ao Tô no
                Mapa.
            </Text>
        </View>
    );
}
