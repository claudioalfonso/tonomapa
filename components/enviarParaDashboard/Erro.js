import React from 'react';
import { Text, View } from 'react-native';

import { Layout } from '../../styles';

export default function Erro({ apiErrors }) {
    return (
        <View style={Layout.col}>
            <Text style={{ ...Layout.textParagraph, ...Layout.bold }}>Dados não enviados.</Text>
            {apiErrors.networkError != '' && apiErrors.graphQLErrors.length === 0 && (
                <>
                    <Text style={Layout.textParagraph}>
                        Falha na conexão com a internet durante o envio.
                    </Text>
                    <Text
                        style={{
                            ...Layout.textParagraph,
                            ...Layout.italic,
                        }}>
                        {apiErrors.networkError}
                    </Text>
                    <Text style={Layout.textParagraph}>
                        Por favor, se a internet está boa, entre em contato com nossa equipe de
                        apoio e compartilhe o texto do erro acima.
                    </Text>
                </>
            )}
            {apiErrors.graphQLErrors.length > 0 && (
                <>
                    <Text style={Layout.textParagraph}>
                        Infelizmente encontramos um erro ao tentar enviar os dados. Se possível,
                        tente mais uma vez, e de não der certo novamente, pedimos que envie um print
                        desta mensagem para a equipe Tô no Mapa:
                    </Text>
                    <Text
                        style={{
                            ...Layout.textParagraph,
                            ...Layout.italic,
                        }}>
                        {apiErrors.graphQLErrors.join(', ')}
                    </Text>
                </>
            )}
        </View>
    );
}
