import React, { useEffect } from 'react';
import { View, Text } from 'react-native';
import { Appbar, IconButton } from 'react-native-paper';

import { Layout, LayoutMapa } from '../styles';
import { isCurrentTerritorioEditable } from '../bases/status';
import TELAS from '../bases/telas';
import metrics from '../utils/metrics';
import colors from '../assets/colors';

import BottomBarItem from './BottomBarItem';

export default function PTTBottomBar({ currentTerritorio, navigation, tela }) {
    const isCurrentTerritorioEditableVar = isCurrentTerritorioEditable(currentTerritorio);

    const setMapEditingHandler = () => {
        navigation.jumpTo('Home');
    };

    const dadosHandler = () => {
        if (isCurrentTerritorioEditableVar) {
            navigation.navigate('DadosPTT');
            return;
        }

        // if (handleTerritorioStatusAlert) {
        //     handleTerritorioStatusAlert();
        // }
    };

    const arquivosHandler = () => {
        navigation.navigate('AnexoList');
    };

    const menuHandler = () => {
        navigation.openDrawer();
    };

    return (
        <>
            <View
                style={{
                    position: 'absolute',
                    bottom: metrics.tenHeight * 6.4,
                    left: 0,
                    right: 0,
                    backgroundColor: colors.Foundation.Green.G400,
                    alignItems: 'center',
                    paddingVertical: metrics.tenHeight * 0.4,
                }}>
                <Text
                    style={{
                        ...Layout.topBarTitle,
                        color: colors.Foundation.CorDeTexto.CorDeTexto,
                    }}>
                    Modo PTT
                </Text>
            </View>

            <Appbar style={LayoutMapa.appbarBottom}>
                <View style={LayoutMapa.appbarBottomActions}>
                    <BottomBarItem
                        label="Mapa"
                        icon="map"
                        iconActive="map"
                        disabled={false}
                        onPress={setMapEditingHandler}
                        active={tela === TELAS.MAPA}
                    />

                    <BottomBarItem
                        label="Dados"
                        icon="file-document-outline"
                        iconActive="file-document"
                        disabled={!isCurrentTerritorioEditableVar}
                        onPress={dadosHandler}
                        active={tela === TELAS.DADOS_PTT}
                    />

                    <BottomBarItem
                        label="Arquivos"
                        icon="file-image-marker-outline"
                        iconActive="file-image-marker"
                        onPress={arquivosHandler}
                        active={tela === TELAS.ARQUIVOS}
                    />

                    <BottomBarItem label="Menu" icon="menu" onPress={menuHandler} />
                </View>
            </Appbar>
        </>
    );
}
