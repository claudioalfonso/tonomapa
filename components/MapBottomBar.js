import React from 'react';
import { Surface } from 'react-native-paper';
import { Text, Alert, View } from 'react-native';

import TELAS from '../bases/telas';
import { Layout, LayoutMapa } from '../styles';
import metrics from '../utils/metrics';
import { pluralize } from '../utils/utils';
import { qtdePoligonos } from '../maps/mapsUtils';
import { ButtonCancel, ButtonContainedTertiary } from './buttons';

export default function MapBottomBar({
    tela,
    setTela,
    currentTerritorio,
    settings,
    updateSettings,
    persistePoligonos,
    persisteMoveUsoOuConflito,
    markers,
    polygons,
    snackBarMessage,
    setSnackBarVisible,
    clearUsosEConflitos,
}) {
    const qtdePoligonosVar = qtdePoligonos(currentTerritorio);

    const apagarPoligonoEmEdicao = () => {
        const msgSuccess = 'Edição cancelada com sucesso!';
        if (settings.editing) {
            Alert.alert(
                'Cancelar a demarcação desta área',
                'Você deseja cancelar o cadastro de nova área que está em edição?',
                [
                    {
                        text: 'Sim',
                        onPress: () => {
                            if (settings.editing.originalPolygon) {
                                persistePoligonos({
                                    polygons: [...polygons, settings.editing.originalPolygon],
                                    settings: { editing: null },
                                    newSnackBarMessage: msgSuccess,
                                });
                                return;
                            }

                            snackBarMessage.current = msgSuccess;
                            setSnackBarVisible(true);
                            updateSettings({
                                settings: { editing: null },
                                noSnackBarMessage: true,
                            });
                        },
                    },
                    {
                        text: 'Não, continuar mapeando',
                        onPress: () => {},
                    },
                ],
                { cancelable: true }
            );
        }
    };

    const apagarTodosPoligonosOuMarkers = () => {
        let title, message, qtde;
        if (tela === TELAS.MAPEAR_TERRITORIO) {
            qtde = qtdePoligonosVar;
            title = pluralize(qtde, 'Apagar a área', 'Apagar todas as áreas');
            message = pluralize(
                qtde,
                'Apagar a área que você mapeou da comunidade.\n\nEsta ação não tem volta!\n\nQuer apagar a área?',
                'Apagar todas as ' +
                    qtde +
                    ' áreas que você mapeou da comunidade.\n\nEsta ação não tem volta!\n\nQuer apagar todas as áreas?'
            );
        } else {
            qtde = markers.length;
            title = pluralize(
                qtde,
                'Apagar o local de uso ou conflito',
                'Apagar todos os locais de uso e conflitos'
            );
            message = pluralize(
                qtde,
                'Apagar o local de uso ou conflito que você registrou\n\nEsta ação não tem volta!\n\nQuer apagar o local?',
                'Apagar todos os ' +
                    qtde +
                    ' locais de uso e conflitos que você registrou.\n\nEsta ação não tem volta!\n\nQuer apagar todos os locais?'
            );
        }
        Alert.alert(
            title,
            message,
            [
                {
                    text: 'Não',
                    onPress: () => {},
                },
                {
                    text: 'Apagar!',
                    onPress: () => {
                        if (tela === TELAS.MAPEAR_TERRITORIO) {
                            persistePoligonos({
                                polygons: [],
                                newSnackBarMessage: pluralize(
                                    qtde,
                                    'Área apagada com sucesso!',
                                    qtde + ' áreas apagadas com sucesso!'
                                ),
                            });
                            return;
                        }

                        clearUsosEConflitos();
                    },
                },
            ],
            { cancelable: true }
        );
    };

    const handleRedBtn = (e) => {
        if (!settings.editing && !settings.editingUsoOuConflito) {
            apagarTodosPoligonosOuMarkers();
            return;
        }

        if (settings.editing) {
            apagarPoligonoEmEdicao();
            return;
        }

        if (settings.editingUsoOuConflito) {
            updateSettings({ settings: { editingUsoOuConflito: null }, noSnackBarMessage: true });
        }
    };

    const handleOkBtn = () => {
        switch (tela) {
            case TELAS.MAPEAR_TERRITORIO:
                if (settings.editing) {
                    persistePoligonos({
                        polygons: [...polygons, settings.editing],
                        settings: { editing: null },
                        newSnackBarMessage: 'Área da comunidade salva com sucesso!',
                    });
                    break;
                }
                setTela(TELAS.MAPA);
                break;
            case TELAS.MAPEAR_USOSECONFLITOS:
                if (settings.editingUsoOuConflito) {
                    persisteMoveUsoOuConflito();
                    break;
                }
                setTela(TELAS.MAPA);
                break;
        }
    };

    return (
        <Surface
            style={{
                width: '100%',
                borderTopRightRadius: metrics.tenHeight * 3,
                borderTopLeftRadius: metrics.tenHeight * 3,
                paddingTop: metrics.tenHeight * 1,
                position: 'absolute',
                bottom: 0,
                zIndex: 10,
            }}>
            <View style={Layout.col}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={LayoutMapa.mapearButtonsContainerRow}>
                        <ButtonCancel
                            onPress={handleRedBtn}
                            disabled={
                                (tela === TELAS.MAPEAR_TERRITORIO &&
                                    !settings.editing &&
                                    !qtdePoligonosVar) ||
                                (tela === TELAS.MAPEAR_USOSECONFLITOS && !markers.length)
                            }
                            style={{ flex: 1, marginRight: metrics.tenWidth * 0.8 }}
                            labelStyle={{ padding: metrics.tenHeight * 0.2 }}
                            icon={
                                settings.editing || settings.editingUsoOuConflito
                                    ? 'cancel'
                                    : 'broom'
                            }>
                            {settings.editing || settings.editingUsoOuConflito ? (
                                <Text>Cancelar</Text>
                            ) : (
                                <Text>Limpar</Text>
                            )}
                        </ButtonCancel>

                        <ButtonContainedTertiary
                            onPress={handleOkBtn}
                            disabled={
                                tela === TELAS.MAPEAR_TERRITORIO &&
                                settings.editing &&
                                settings.editing.coordinates.length < 3
                            }
                            style={{ flex: 1, marginLeft: metrics.tenWidth * 0.8 }}
                            labelStyle={{ padding: metrics.tenHeight * 0.2 }}
                            icon={'check-circle-outline'}>
                            {settings.editing || settings.editingUsoOuConflito
                                ? 'Salvar'
                                : 'Feito!'}
                        </ButtonContainedTertiary>
                    </View>
                </View>
            </View>
        </Surface>
    );
}
