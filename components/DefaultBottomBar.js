import React from 'react';
import { View } from 'react-native';
import { Appbar } from 'react-native-paper';

import { LayoutMapa } from '../styles';
import TELAS from '../bases/telas';
import { isCurrentTerritorioEditable, isPTTMode } from '../bases/status';

import BottomBarItem from './BottomBarItem';

export default function DefaultBottomBar({ currentTerritorio, navigation, tela }) {
    const isPTTModeVar = isPTTMode(currentTerritorio);
    const isCurrentTerritorioEditableVar = isCurrentTerritorioEditable(currentTerritorio);

    const dadosHandler = () => {
        if (isCurrentTerritorioEditableVar) {
            navigation.jumpTo(!isPTTModeVar ? 'DadosBasicos' : 'DadosPTT');
            return;
        }
        // if (handleTerritorioStatusAlert) {
        //     handleTerritorioStatusAlert();
        // }
    };

    const arquivosHandler = () => {
        navigation.jumpTo('AnexoList');
    };

    const inicioHandler = () => {
        navigation.jumpTo('Home');
    };

    const locaisHandler = () => {
        if (isCurrentTerritorioEditableVar) {
            navigation.jumpTo('Home', { tela: TELAS.MAPEAR_USOSECONFLITOS });
            return;
        }
    };

    const menuHandler = () => {
        navigation.openDrawer();
    };

    return (
        <Appbar style={LayoutMapa.appbarBottom}>
            <View style={LayoutMapa.appbarBottomActions}>
                <BottomBarItem
                    label="Mapa"
                    icon="map"
                    iconActive="map"
                    disabled={false}
                    onPress={inicioHandler}
                    active={tela === TELAS.MAPA}
                />

                <BottomBarItem
                    label="Locais"
                    icon="map-marker-outline"
                    iconActive="map-marker"
                    disabled={!isCurrentTerritorioEditableVar}
                    onPress={locaisHandler}
                    active={tela === TELAS.MAPEAR_USOSECONFLITOS}
                />

                <BottomBarItem
                    label="Dados"
                    icon="file-document-outline"
                    iconActive="file-document"
                    disabled={!isCurrentTerritorioEditableVar}
                    onPress={dadosHandler}
                    active={[TELAS.DADOS_BASICOS, TELAS.DADOS_PTT].includes(tela)}
                />

                <BottomBarItem
                    label="Arquivos"
                    icon="file-image-marker-outline"
                    iconActive="file-image-marker"
                    onPress={arquivosHandler}
                    active={tela === TELAS.ARQUIVOS}
                />

                <BottomBarItem label="Menu" icon="menu" onPress={menuHandler} />
            </View>
        </Appbar>
    );
}
