import React from 'react';
import { Button } from 'react-native-paper';

import colors from '../../assets/colors';
import { Layout } from '../../styles';

export default function ButtonTextPrimary({ onPress = null, children = null, ...props }) {
    const childrenElements = children || <></>;
    return (
        <Button
            mode="text"
            labelStyle={Layout.buttonLabel}
            uppercase={false}
            onPress={onPress}
            {...props}>
            {childrenElements}
        </Button>
    );
}
