import React from 'react';
import { Button } from 'react-native-paper';

import { Layout } from '../../styles';
import colors from '../../assets/colors';

export default function ButtonFullwidthSecondary({
    onPress = null,
    children,
    style = null,
    ...props
}) {
    return (
        <Button
            mode="contained"
            uppercase={false}
            style={{ width: '100%', ...style }}
            labelStyle={Layout.buttonLabel}
            color={colors.Foundation.Green.G400}
            onPress={onPress}
            {...props}>
            {children}
        </Button>
    );
}
