import React from 'react';
import { Button } from 'react-native-paper';

import colors from '../../assets/colors';
import { Layout } from '../../styles';

export default function ButtonTextSecondary({ onPress = null, children, ...props }) {
    return (
        <Button
            mode="text"
            color={colors.Foundation.Green.G300}
            labelStyle={Layout.buttonLabel}
            uppercase={false}
            onPress={onPress}
            {...props}>
            {children}
        </Button>
    );
}
