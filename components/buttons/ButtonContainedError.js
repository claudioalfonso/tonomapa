import React from 'react';
import { Button } from 'react-native-paper';

import colors from '../../assets/colors';
import { Layout } from '../../styles';

export default function ButtonContainedError({
    onPress = null,
    labelStyle = {},
    children = null,
    ...props
}) {
    const childrenElements = children || <></>;
    return (
        <Button
            mode="contained"
            color={colors.Foundation.Feedback.Error}
            labelStyle={{ ...Layout.buttonLabel, labelStyle }}
            uppercase={false}
            onPress={onPress}
            {...props}>
            {childrenElements}
        </Button>
    );
}
