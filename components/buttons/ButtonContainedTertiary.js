import React from 'react';
import { Button } from 'react-native-paper';

import colors from '../../assets/colors';
import { Layout } from '../../styles';

export default function ButtonContainedTertiary({
    onPress = null,
    labelStyle = {},
    children,
    ...props
}) {
    return (
        <Button
            mode="contained"
            color={colors.Foundation.Green.G300}
            labelStyle={{ ...Layout.buttonLabel, ...labelStyle }}
            uppercase={false}
            onPress={onPress}
            {...props}>
            {children}
        </Button>
    );
}
