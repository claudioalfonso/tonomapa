import React from 'react';
import { Button } from 'react-native-paper';

import colors from '../../assets/colors';
import { Layout } from '../../styles';

export default function ButtonOutlinedSecondary({
    onPress = null,
    style = {},
    children,
    ...props
}) {
    return (
        <Button
            mode="outlined"
            color={colors.Foundation.Green.G400}
            style={{ borderColor: colors.Foundation.Green.G400, ...style }}
            labelStyle={Layout.buttonLabel}
            uppercase={false}
            onPress={onPress}
            {...props}>
            {children}
        </Button>
    );
}
