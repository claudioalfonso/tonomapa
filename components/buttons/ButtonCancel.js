import React from 'react';
import { Button } from 'react-native-paper';

import colors from '../../assets/colors';
import { Layout } from '../../styles';

export default function ButtonCancel({
    onPress = null,
    style = {},
    labelStyle = {},
    children = null,
    ...props
}) {
    const childElements = children || <></>;
    return (
        <Button
            mode="outlined"
            labelStyle={{ ...Layout.buttonLabel, ...labelStyle }}
            color={colors.Foundation.Feedback.Error}
            style={{ borderColor: colors.Foundation.Feedback.Error, ...style }}
            uppercase={false}
            onPress={onPress}
            {...props}>
            {childElements}
        </Button>
    );
}
