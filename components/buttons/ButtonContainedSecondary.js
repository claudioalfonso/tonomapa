import React from 'react';
import { Button } from 'react-native-paper';

import colors from '../../assets/colors';
import { Layout } from '../../styles';

export default function ButtonContainedSecondary({ onPress = null, children, ...props }) {
    return (
        <Button
            mode="contained"
            color={colors.Foundation.Green.G400}
            labelStyle={Layout.buttonLabel}
            uppercase={false}
            onPress={onPress}
            {...props}>
            {children}
        </Button>
    );
}
