import React from 'react';
import { Button } from 'react-native-paper';

import { Layout } from '../../styles';

export default function ButtonVoltar({ onPress = null, children, ...props }) {
    return (
        <Button
            mode="text"
            labelStyle={{ ...Layout.buttonLabel, marginLeft: 0 }}
            uppercase={false}
            onPress={onPress}
            {...props}>
            {children}
        </Button>
    );
}
