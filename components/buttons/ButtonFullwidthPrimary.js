import React from 'react';
import { Button } from 'react-native-paper';

import { Layout } from '../../styles';

export default function ButtonFullwidthPrimary({
    onPress = null,
    children,
    style = null,
    ...props
}) {
    return (
        <Button
            mode="contained"
            uppercase={false}
            style={{ width: '100%', ...style }}
            labelStyle={Layout.buttonLabel}
            onPress={onPress}
            {...props}>
            {children}
        </Button>
    );
}
