import React from 'react';
import { Button } from 'react-native-paper';

import { Layout } from '../../styles';

export default function ButtonContainedPrimary({
    onPress = null,
    labelStyle = {},
    children = null,
    ...props
}) {
    const childrenElements = children || <></>;
    return (
        <Button
            mode="contained"
            labelStyle={{ ...Layout.buttonLabel, labelStyle }}
            uppercase={false}
            onPress={onPress}
            {...props}>
            {childrenElements}
        </Button>
    );
}
