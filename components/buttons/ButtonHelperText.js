import React from 'react';
import { Button } from 'react-native-paper';

import { Layout } from '../../styles';
import colors from '../../assets/colors';

export default function ButtonHelperText({ onPress = null, children }) {
    return (
        <Button
            mode="text"
            compact
            uppercase={false}
            style={{ marginTop: 0, alignSelf: 'flex-start' }}
            labelStyle={{
                textDecorationStyle: 'solid',
                textDecorationLine: 'underline',
            }}
            color={colors.Foundation.Green.G300}
            onPress={onPress}>
            {children}
        </Button>
    );
}
