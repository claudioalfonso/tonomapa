import React from 'react';
import { HelperText } from 'react-native-paper';
import { StyleSheet, Image } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

// @ts-ignore
import tipos from '../../bases/tipos';
import { pickerStyles } from '../../styles/pickers';
import { Inputs, Layout } from '../../styles';
import { geraIconeTipoUsoOuConflito } from '../../bases/icones';

import InputLabel from './InputLabel';
import TextInputDefault from './TextInputDefault';

const pickerSelectStyles = StyleSheet.create(pickerStyles);

const TipoConflitoSelect = ({ onValueChange, selectedValue, error }) => {
    const getTiposConflito = () => {
        return tipos.data.tiposConflito.map((tipoConflito) => ({
            ...tipoConflito,
            label: tipoConflito.nome,
            value: tipoConflito.id,
        }));
    };

    const iconRequire = selectedValue
        ? geraIconeTipoUsoOuConflito('conflito', selectedValue)
        : null;

    return (
        <TextInputDefault
            label="Tipo de conflito"
            required={true}
            value={selectedValue}
            error={error}
            render={() => (
                <>
                    {iconRequire && (
                        <Image source={iconRequire} style={Layout.iconForConflitoSelect} />
                    )}
                    <RNPickerSelect
                        onValueChange={onValueChange}
                        placeholder={{ label: '', value: null }}
                        useNativeAndroidPickerStyle={false}
                        style={pickerSelectStyles}
                        items={getTiposConflito()}
                        value={selectedValue}
                    />
                </>
            )}
        />
    );
};

export default TipoConflitoSelect;
