import React from 'react';
import { View, Text } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';

// @ts-ignore
import tipos from '../../bases/tipos';
import metrics from '../../utils/metrics';
import { Inputs } from '../../styles';
import colors from '../../assets/colors';
import { color } from 'react-native-reanimated';

const TipoComunidadeSelect = ({ selectedItems = [], onValueChange }) => {
    const getTiposComunidade = () => {
        return tipos.data.tiposComunidade.map((tipoComunidade) => ({
            ...tipoComunidade,
            name: tipoComunidade.nome,
            id: tipoComunidade.id,
        }));
    };

    const NoResultsComponent = () => {
        return (
            <View
                style={{
                    // @ts-ignore
                    textAlign: 'center',
                    flex: 1,
                    margin: 10,
                }}>
                <Text>Não há segmento de comunidade com esta busca</Text>
            </View>
        );
    };

    return (
        <View
            style={{
                borderWidth: 1,
                borderColor: colors.Foundation.CorDeTexto.CorDeTexto2,
                borderRadius: 6,
                marginTop: metrics.tenHeight * 2,
            }}>
            <SectionedMultiSelect
                items={getTiposComunidade()}
                IconRenderer={MaterialIcons}
                uniqueKey="id"
                selectText="Segmento(s) da comunidade"
                confirmText="Concluído"
                selectedText="tipos"
                searchPlaceholderText="Buscar"
                noResultsComponent={NoResultsComponent}
                styles={{
                    selectToggleText: {
                        fontSize: metrics.tenWidth * 1.6,
                        color: colors.Foundation.CorDeTexto.CorDeTexto2,
                    },
                    selectToggle: {
                        marginVertical: metrics.tenHeight * 1.6,
                        marginHorizontal: metrics.tenWidth * 1.4,
                    },
                    chipsWrapper: {
                        marginBottom: metrics.tenHeight * 0.8,
                        marginHorizontal: metrics.tenWidth * 1.4,
                    },
                    chipText: {
                        color: colors.Foundation.CorDeTexto.CorDeTexto2,
                    },
                }}
                colors={{ primary: colors.Foundation.Brown.B300 }}
                single={false}
                alwaysShowSelectText={true}
                readOnlyHeadings={false}
                onSelectedItemsChange={onValueChange}
                selectedItems={selectedItems}
            />
        </View>
    );
};

export default TipoComunidadeSelect;
