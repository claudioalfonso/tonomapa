import React from 'react';
import { HelperText, TextInput } from 'react-native-paper';

import colors from '../../assets/colors';
import { Inputs } from '../../styles';

import InputLabel from './InputLabel';

export default function TextInputDefault({
    label,
    error = null,
    info = null,
    required = false,
    ...props
}) {
    return (
        <>
            <TextInput
                label={<InputLabel label={label} required={required} />}
                mode="outlined"
                style={Inputs.textInputLogin}
                activeOutlineColor={colors.Foundation.Green.G300}
                error={!!error}
                {...props}
            />
            {!!error && <HelperText type="error">{error}</HelperText>}
            {!!info && !error && <HelperText type="info">{info}</HelperText>}
        </>
    );
}
