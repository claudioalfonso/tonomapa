import React from 'react';
import { View } from 'react-native';
import { useLazyQuery } from '@apollo/client';
import { Button, Dialog, Text } from 'react-native-paper';

import { GET_USER_DATA, GET_USER_DATA_WITH_TERRITORIO_ID } from '../../db/queries';
import { Layout } from '../../styles';
import { populateCachedData, tonomapaCache } from '../../db/api';
import { useToNoMapa } from '../../context/ToNoMapaProvider';
import { log, logError } from '../../utils/utils';

export default function DialogTerritorioSwitcher({
    novoTerritorio,
    setNovoTerritorio,
    handleTerritorioSwitch,
    online,
    isSyncing,
    // SNACKBAR_MESSAGES,
}) {
    if (!novoTerritorio) {
        return <></>;
    }

    return (
        <Dialog
            visible={true}
            onDismiss={() => {
                setNovoTerritorio(null);
            }}>
            <Dialog.Title>
                {novoTerritorio.id !== 'criarTerritorio'
                    ? `Mudar para ${novoTerritorio.nome}`
                    : 'Registrar nova comunidade'}
            </Dialog.Title>
            <Dialog.Content>
                {novoTerritorio.id !== 'criarTerritorio' ? (
                    <>
                        {!online ? (
                            <View>
                                <Text style={Layout.textParagraph}>
                                    <Text style={Layout.bold}>Sem internet!</Text>
                                </Text>
                                <Text style={Layout.textParagraph}>
                                    Para mudar de comunidade, o seu celular precisa estar conectado
                                    na internet.
                                </Text>
                                <Text style={Layout.textParagraph}>
                                    Por favor, tente mais tarde, com internet.
                                </Text>
                            </View>
                        ) : isSyncing ? (
                            <View>
                                <Text style={Layout.textParagraph}>
                                    <Text style={Layout.bold}>Sincronizando os dados...</Text>
                                </Text>
                                <Text style={Layout.textParagraph}>
                                    Para mudar de comunidade, aguarde que a sincronização de dados
                                    termine.
                                </Text>
                                <Text style={Layout.textParagraph}>
                                    Por favor, tente daqui a pouco novamente.
                                </Text>
                            </View>
                        ) : (
                            <View>
                                <Text style={Layout.textParagraph}>Tem certeza?</Text>
                            </View>
                        )}
                    </>
                ) : (
                    <>
                        {!online ? (
                            <View>
                                <Text style={Layout.textParagraph}>
                                    <Text style={Layout.bold}>Sem internet!</Text>
                                </Text>
                                <Text style={Layout.textParagraph}>
                                    Para registrar uma nova comunidade, o seu celular precisa estar
                                    conectado na internet.
                                </Text>
                                <Text style={Layout.textParagraph}>
                                    Por favor, tente mais tarde, com internet.
                                </Text>
                            </View>
                        ) : isSyncing ? (
                            <View>
                                <Text style={Layout.textParagraph}>
                                    <Text style={Layout.bold}>Sincronizando os dados...</Text>
                                </Text>
                                <Text style={Layout.textParagraph}>
                                    Para registrar uma nova comunidade, aguarde que a sincronização
                                    de dados termine.
                                </Text>
                                <Text style={Layout.textParagraph}>
                                    Por favor, tente daqui a pouco novamente.
                                </Text>
                            </View>
                        ) : (
                            <View>
                                <Text style={Layout.textParagraph}>
                                    Tem certeza de que quer registrar uma nova comunidade no Tô no
                                    Mapa?
                                </Text>
                            </View>
                        )}
                    </>
                )}
            </Dialog.Content>
            <Dialog.Actions style={{ flexDirection: 'column', alignItems: 'flex-end' }}>
                <Button
                    onPress={() => {
                        setNovoTerritorio(null);
                    }}>
                    {online && !isSyncing ? 'Cancelar' : 'Entendi'}
                </Button>
                {online && !isSyncing && (
                    <Button onPress={handleTerritorioSwitch}>
                        {novoTerritorio.id !== 'criarTerritorio'
                            ? 'Sim, mudar comunidade'
                            : 'Sim, registrar nova comunidade'}
                    </Button>
                )}
            </Dialog.Actions>
        </Dialog>
    );
}
