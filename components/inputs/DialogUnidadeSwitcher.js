import React from 'react';
import { View } from 'react-native';
import { Button, Dialog, RadioButton, Text } from 'react-native-paper';

import { Layout } from '../../styles';
import { UNIDADES_AREA } from '../../maps/mapsUtils';

export default function DialogUnidadeSwitcher({
    settings,
    updateSettings,
    dialogListaUnidadesVisible,
    setDialogListaUnidadesVisible,
}) {
    return (
        <Dialog
            visible={dialogListaUnidadesVisible}
            onDismiss={() => {
                setDialogListaUnidadesVisible(false);
            }}>
            <Dialog.Title>Unidade de área</Dialog.Title>
            <Dialog.Content>
                <RadioButton.Group
                    value={settings.unidadeArea.sigla}
                    onValueChange={(value) => {
                        for (let unidadeArea of Object.values(UNIDADES_AREA)) {
                            if (unidadeArea.sigla === value) {
                                settings.unidadeArea = unidadeArea;
                                updateSettings({
                                    settings: { unidadeArea: unidadeArea },
                                });
                                break;
                            }
                        }
                        setDialogListaUnidadesVisible(false);
                    }}>
                    {Object.values(UNIDADES_AREA).map((unidadeArea) => (
                        <View style={Layout.row} key={unidadeArea.sigla}>
                            <Text style={Layout.text}>
                                {unidadeArea.nome} ({unidadeArea.sigla})
                            </Text>
                            <RadioButton value={unidadeArea.sigla} />
                        </View>
                    ))}
                </RadioButton.Group>
            </Dialog.Content>
            <Dialog.Actions style={{ flexDirection: 'column', alignItems: 'flex-end' }}>
                <Button
                    onPress={() => {
                        setDialogListaUnidadesVisible(false);
                    }}>
                    Cancelar
                </Button>
            </Dialog.Actions>
        </Dialog>
    );
}
