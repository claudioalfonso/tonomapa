import React from 'react';
import { Text } from 'react-native';
import { Inputs } from '../../styles';

export default function InputLabel({ label, required = false }) {
    return (
        <Text style={Inputs.textInputLabel}>
            {label}
            {required && <Text style={{ color: 'red' }}> *</Text>}
        </Text>
    );
}
