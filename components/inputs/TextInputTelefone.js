import React from 'react';
import { HelperText, TextInput } from 'react-native-paper';
import { TextInputMask } from 'react-native-masked-text';

import colors from '../../assets/colors';
import { Inputs } from '../../styles';

import InputLabel from './InputLabel';

export default function TextInputTelefone({ label, error = null, required = false, ...props }) {
    return (
        <>
            <TextInput
                label={<InputLabel label={label} required={required} />}
                mode="outlined"
                style={Inputs.textInputLogin}
                activeOutlineColor={colors.Foundation.Green.G300}
                error={!!error}
                render={(renderProps) => (
                    // @ts-ignore
                    <TextInputMask
                        {...renderProps}
                        type={'cel-phone'}
                        keyboardType="phone-pad"
                        options={{
                            maskType: 'BRL',
                            withDDD: true,
                            dddMask: '(99) ',
                        }}
                    />
                )}
                {...props}
            />
            {!!error && <HelperText type="error">{error}</HelperText>}
        </>
    );
}
