import React from 'react';
import { HelperText, TextInput } from 'react-native-paper';
import { TextInputMask } from 'react-native-masked-text';

import colors from '../../assets/colors';
import { Inputs } from '../../styles';

import InputLabel from './InputLabel';

export default function TextInputCEP({
    label,
    error = null,
    required = false,
    omitDot = false,
    ...props
}) {
    const mask = omitDot ? '99999-999' : '99.999-999';
    return (
        <>
            <TextInput
                label={<InputLabel label={label} required={required} />}
                mode="outlined"
                style={Inputs.textInputLogin}
                activeOutlineColor={colors.Foundation.Green.G300}
                error={!!error}
                render={(props) => (
                    // @ts-ignore
                    <TextInputMask
                        {...props}
                        type={'custom'}
                        keyboardType="number-pad"
                        options={{ mask }}
                    />
                )}
                {...props}
            />
            {!!error && <HelperText type="error">{error}</HelperText>}
        </>
    );
}
