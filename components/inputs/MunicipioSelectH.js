import React, { useRef } from 'react';
import { HelperText, TextInput } from 'react-native-paper';
import { StyleSheet } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

import { Inputs } from '../../styles';
import { pickerStyles } from '../../styles/pickers';
import municipios from '../../bases/municipios';
import estados from '../../bases/estados';
import colors from '../../assets/colors';

import InputLabel from './InputLabel';

const pickerSelectStyles = StyleSheet.create(pickerStyles);

const MunicipioSelectH = ({
    labelEstado = 'Estado',
    required = false,
    estadoId,
    disabled = false,
    onValueChange,
    labelMunicipio = 'Município',
    municipioId,
    error = null,
}) => {
    const mudouEstado = useRef(false);

    return (
        <>
            <TextInput
                label={<InputLabel label={labelEstado} required={required} />}
                value={estadoId}
                style={Inputs.textInputLogin}
                mode="outlined"
                activeOutlineColor={colors.Foundation.Green.G300}
                render={() => (
                    <RNPickerSelect
                        key="estados"
                        disabled={disabled}
                        onValueChange={(estadoId) => {
                            mudouEstado.current = true;
                            onValueChange({
                                estadoId: estadoId,
                                municipioId: null,
                            });
                        }}
                        placeholder={{ label: '', value: null }}
                        useNativeAndroidPickerStyle={false}
                        style={pickerSelectStyles}
                        items={estados}
                        value={estadoId}
                    />
                )}
            />
            <TextInput
                label={<InputLabel label={labelMunicipio} required={required} />}
                value={municipioId}
                style={Inputs.textInputLogin}
                error={!!error}
                mode="outlined"
                activeOutlineColor={colors.Foundation.Green.G300}
                render={() => {
                    return (
                        <RNPickerSelect
                            key="municipios"
                            disabled={disabled || estadoId === null}
                            placeholder={{ label: '', value: null }}
                            useNativeAndroidPickerStyle={false}
                            style={pickerSelectStyles}
                            items={estadoId ? municipios[estadoId] : []}
                            value={municipioId}
                            onValueChange={(municipioId) => {
                                if (mudouEstado.current && municipioId === null) {
                                    return;
                                }

                                mudouEstado.current = false;
                                onValueChange({
                                    municipioId: municipioId,
                                });
                            }}
                        />
                    );
                }}
            />
            {!!error && <HelperText type="error">{error}</HelperText>}
        </>
    );
};

export default MunicipioSelectH;
