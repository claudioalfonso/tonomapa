import React from 'react';
import DropDown from 'react-native-paper-dropdown';

const TerritorioSelect = ({ value, territorios: territoriosRaw, onValueChange }) => {
    const [showDropDown, setShowDropDown] = React.useState(false);
    const territorios = territoriosRaw.map((territorio) => {
        return {
            value: territorio.id,
            label: territorio.nome,
        };
    });

    return (
        <DropDown
            label={'Comunidade'}
            mode={'outlined'}
            value={value}
            setValue={onValueChange}
            list={territorios}
            visible={showDropDown}
            showDropDown={() => setShowDropDown(true)}
            onDismiss={() => setShowDropDown(false)}
        />
    );
};
export default TerritorioSelect;
