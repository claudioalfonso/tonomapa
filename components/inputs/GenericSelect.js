import React from 'react';
import { HelperText, TextInput } from 'react-native-paper';
import { StyleSheet } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

import { pickerStyles } from '../../styles/pickers';
import { Inputs } from '../../styles';

import InputLabel from './InputLabel';

const pickerSelectStyles = StyleSheet.create(pickerStyles);

const GenericSelect = ({
    label,
    onValueChange,
    selectedValue,
    items,
    error = null,
    info = null,
}) => {
    return (
        <>
            <TextInput
                label={<InputLabel label={label} />}
                value={selectedValue}
                style={Inputs.textInput}
                mode="outlined"
                render={() => (
                    <RNPickerSelect
                        onValueChange={(value) => onValueChange(value)}
                        placeholder={{ label: '', value: null }}
                        useNativeAndroidPickerStyle={false}
                        style={pickerSelectStyles}
                        items={items}
                        value={selectedValue}
                    />
                )}
                error={!!error}
            />
            {!!error && <HelperText type="error">{error}</HelperText>}
            {!!info && !error && <HelperText type="info">{info}</HelperText>}
        </>
    );
};

export default GenericSelect;
