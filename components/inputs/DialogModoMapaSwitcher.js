import React from 'react';
import { View } from 'react-native';
import { MAP_TYPES } from 'react-native-maps';
import { Button, Dialog, RadioButton, Text } from 'react-native-paper';

import { Layout } from '../../styles';

export default function DialogModoMapaSwitcher({
    settings,
    updateSettings,
    dialogModoMapaVisible,
    setDialogModoMapaVisible,
}) {
    return (
        <Dialog
            visible={dialogModoMapaVisible}
            onDismiss={() => {
                setDialogModoMapaVisible(false);
            }}>
            <Dialog.Title>Modo de visualização</Dialog.Title>
            <Dialog.Content>
                <RadioButton.Group
                    value={settings.mapType}
                    onValueChange={(value) => {
                        updateSettings({ settings: { mapType: value } });
                        setDialogModoMapaVisible(false);
                    }}>
                    <View style={Layout.row} key={MAP_TYPES.HYBRID}>
                        <Text style={Layout.text}>Satélite</Text>
                        <RadioButton value={MAP_TYPES.HYBRID} />
                    </View>
                    <View style={Layout.row} key={MAP_TYPES.STANDARD}>
                        <Text style={Layout.text}>Mapa</Text>
                        <RadioButton value={MAP_TYPES.STANDARD} />
                    </View>
                </RadioButton.Group>
            </Dialog.Content>
            <Dialog.Actions style={{ flexDirection: 'column', alignItems: 'flex-end' }}>
                <Button
                    onPress={() => {
                        setDialogModoMapaVisible(false);
                    }}>
                    Cancelar
                </Button>
            </Dialog.Actions>
        </Dialog>
    );
}
