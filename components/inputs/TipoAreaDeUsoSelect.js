import React from 'react';
import { HelperText, TextInput } from 'react-native-paper';
import { StyleSheet, Image } from 'react-native';
import RNPickerSelect from 'react-native-picker-select';

import { pickerStyles } from '../../styles/pickers';
import { Inputs, Layout } from '../../styles';
// @ts-ignore
import tipos from '../../bases/tipos';
import { geraIconeTipoUsoOuConflito } from '../../bases/icones';

import InputLabel from './InputLabel';
import TextInputDefault from './TextInputDefault';

const pickerSelectStyles = StyleSheet.create(pickerStyles);

const TipoAreaDeUsoSelect = ({ onValueChange, selectedValue, error }) => {
    const getTiposAreaDeUso = () => {
        return tipos.data.tiposAreaDeUso.map((tipoAreaDeUso) => ({
            ...tipoAreaDeUso,
            label: tipoAreaDeUso.nome,
            value: tipoAreaDeUso.id,
        }));
    };

    const iconRequire = selectedValue
        ? geraIconeTipoUsoOuConflito('areaDeUso', selectedValue)
        : null;

    return (
        <TextInputDefault
            label="Tipo de local de uso"
            required={true}
            value={selectedValue}
            error={error}
            render={() => (
                <>
                    {iconRequire && <Image source={iconRequire} style={Layout.iconForUsoSelect} />}
                    <RNPickerSelect
                        onValueChange={onValueChange}
                        placeholder={{ label: '', value: null }}
                        useNativeAndroidPickerStyle={false}
                        style={pickerSelectStyles}
                        items={getTiposAreaDeUso()}
                        value={selectedValue}
                    />
                </>
            )}
        />
    );
};

export default TipoAreaDeUsoSelect;
