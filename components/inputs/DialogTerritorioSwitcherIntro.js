import React from 'react';
import { Button, Dialog } from 'react-native-paper';

import { isEnviarEnabled } from '../../bases/status';

import TerritorioList from '../TerritorioList';

export default function DialogTerritorioSwitcherIntro({
    currentUser,
    navigation,
    novoTerritorio,
    setNovoTerritorio,
    dialogListaTerritoriosVisible,
    setDialogListaTerritoriosVisible,
}) {
    const territorios = currentUser?.territorios;
    const isEnviarEnabledVar = isEnviarEnabled(currentUser?.currentTerritorio);
    return (
        <Dialog
            visible={dialogListaTerritoriosVisible}
            onDismiss={() => {
                setDialogListaTerritoriosVisible(false);
            }}>
            <Dialog.Title>Comunidades</Dialog.Title>
            <Dialog.Content>
                <TerritorioList
                    currentTerritorio={currentUser.currentTerritorio}
                    territorios={territorios}
                    setNovoTerritorio={setNovoTerritorio}
                    novoTerritorio={novoTerritorio}
                    setDialogListaTerritoriosVisible={setDialogListaTerritoriosVisible}
                />
            </Dialog.Content>
            <Dialog.Actions style={{ flexDirection: 'column', alignItems: 'flex-end' }}>
                <Button
                    onPress={() => {
                        setDialogListaTerritoriosVisible(false);
                    }}>
                    Cancelar
                </Button>
                <Button
                    onPress={() => {
                        setDialogListaTerritoriosVisible(false);
                        if (isEnviarEnabledVar) {
                            setNovoTerritorio({ id: 'criarTerritorio' });
                        } else {
                            navigation.navigate('CadastroTerritorio');
                        }
                    }}>
                    Adicionar comunidade
                </Button>
            </Dialog.Actions>
        </Dialog>
    );
}
