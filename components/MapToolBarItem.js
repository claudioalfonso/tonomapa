import React from 'react';
import { Text } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import { MaterialCommunityIcons } from '@expo/vector-icons';

import { LayoutMapa } from '../styles';
import metrics from '../utils/metrics';
import colors from '../assets/colors';

export default function MapToolBarItem({
    onPress,
    label = null,
    icon = null,
    iconActive = null,
    active = false,
    position = 'right',
}) {
    if (!icon && !label) {
        return <></>;
    }
    let styleWrapper =
        position === 'right' ? 'mapToolbarItemRightWrapper' : 'mapToolbarItemTopWrapper';
    return (
        <TouchableRipple
            onPress={onPress}
            style={active ? LayoutMapa[`${styleWrapper}Active`] : LayoutMapa[styleWrapper]}>
            {icon ? (
                <MaterialCommunityIcons name={active ? iconActive || icon : icon} size={26} />
            ) : (
                <Text
                    style={{
                        padding: metrics.tenHeight * 1.2,
                        fontSize:
                            position === 'right'
                                ? metrics.tenHeight * 1.2
                                : metrics.tenHeight * 1.6,
                        fontWeight: '500',
                        color: active
                            ? colors.Foundation.CorDeTexto.CorDeTexto
                            : colors.Foundation.CorDeTexto.CorDeTexto2,
                    }}>
                    {label}
                </Text>
            )}
        </TouchableRipple>
    );
}
