import React from 'react';
import { Alert, Text } from 'react-native';
import { Dialog, Portal } from 'react-native-paper';
import { useMutation } from '@apollo/client';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import { Layout } from '../styles';
import {
    ENVIA_TERRITORIO_PARA_ANALISE,
    ENVIA_TERRITORIO_PARA_PTT,
    GET_USER_DATA,
} from '../db/queries';
import {
    tonomapaCache,
    preparaEnviaTerritorioParaAnalise,
    preparaEnviaTerritorioParaPtt,
} from '../db/api';

import SituacaoCheckList from './enviarParaDashboard/SituacaoCheckList';
import EditingPolygon from './enviarParaDashboard/EditingPolygon';
import NoChanges from './enviarParaDashboard/NoChanges';
import Carregando from './enviarParaDashboard/Carregando';
import Erro from './enviarParaDashboard/Erro';
import Sucesso from './enviarParaDashboard/Sucesso';
import BtnCancel from './enviarParaDashboard/BtnCancel';
import BtnEnviar from './enviarParaDashboard/BtnEnviar';
import BtnExportReport from './enviarParaDashboard/BtnExportReport';
import BtnOk from './enviarParaDashboard/BtnOk';

export default function EnviarParaDashboardHandler({
    currentUser,
    navigation,
    settings,
    setState,
    enviarDadosDialogo,
    snackBarMessage,
    validaDadosAsync,
    isPTTModeVar,
    isEnviarEnabledVar,
}) {
    const currentTerritorio = currentUser.currentTerritorio;
    const {
        enviarDadosDialogoHandler: [showEnviarDadosDialogo, setShowEnviarDadosDialogo],
        online,
    } = useToNoMapa();
    const [enviaTerritorioParaAnalise] = useMutation(ENVIA_TERRITORIO_PARA_ANALISE);
    const [enviaTerritorioParaPtt] = useMutation(ENVIA_TERRITORIO_PARA_PTT);
    /**
     * @type object
     */
    const enviarDadosDialogoErro = {
        ...enviarDadosDialogo,
        botaoEnviar: null,
        botaoCancelar: null,
        botaoOk: true,
        etapa: 'erro',
    };

    const clearEnviarDadosDialogo = () => {
        const newEtapa = enviarDadosDialogo.etapa === 'sucesso' ? 'situacao' : null;
        validaDadosAsync(newEtapa);
        setShowEnviarDadosDialogo(false);
    };

    const onEnviar = async () => {
        setState({
            enviarDadosDialogo: {
                ...enviarDadosDialogo,
                etapa: 'carregando',
            },
        });
        if (!isPTTModeVar) {
            const mutateData = preparaEnviaTerritorioParaAnalise({
                territorioId: currentTerritorio.id,
            });
            const {
                data: { enviaTerritorioParaAnalise: enviaTerritorioParaAnaliseData },
            } = await enviaTerritorioParaAnalise(mutateData);

            if (enviaTerritorioParaAnaliseData.success) {
                tonomapaCache.writeQuery({
                    query: GET_USER_DATA,
                    data: {
                        currentUser: {
                            ...currentUser,
                            currentTerritorio: {
                                ...enviaTerritorioParaAnaliseData.data,
                            },
                        },
                    },
                });
                snackBarMessage.current = 'Dados da comunidade enviados para análise com sucesso!';
                setState({
                    enviarDadosDialogo: {
                        ...enviarDadosDialogo,
                        etapa: 'sucesso',
                    },
                });
                return;
            }

            console.log('Erro!', enviaTerritorioParaAnaliseData.errors);
            setState({
                enviarDadosDialogo: {
                    ...enviarDadosDialogoErro,
                },
            });
            return;
        }

        const mutateData = preparaEnviaTerritorioParaPtt({
            territorioId: currentTerritorio.id,
        });
        const {
            data: { enviaTerritorioParaPtt: enviaTerritorioParaPttData },
        } = await enviaTerritorioParaPtt(mutateData);

        if (enviaTerritorioParaPttData.success) {
            tonomapaCache.writeQuery({
                query: GET_USER_DATA,
                data: {
                    currentUser: {
                        ...currentUser,
                        currentTerritorio: {
                            ...enviaTerritorioParaPttData.data,
                        },
                    },
                },
            });
            snackBarMessage.current =
                'Dados da comunidade enviados para a plataforma PTT com sucesso!';
            setState({
                enviarDadosDialogo: {
                    ...enviarDadosDialogo,
                    etapa: 'sucesso',
                },
            });
            return;
        }

        console.log('Erro!', enviaTerritorioParaPttData.errors);
        setState({
            enviarDadosDialogo: {
                ...enviarDadosDialogoErro,
            },
        });
    };

    const onEnviarTemCerteza = () => {
        // setShowEnviarDadosDialogo(false);
        const titleAlert = !isPTTModeVar
            ? 'Tem certeza que quer enviar os dados para análise?'
            : 'Tem certeza que quer enviar os dados para a PTT?';

        const bodyAlert = !isPTTModeVar
            ? 'A equipe Tô no Mapa vai analisar os dados enviados.\n\nVocê não poderá fazer alterações nesta comunidade enquanto não vier o retorno da análise.\n\nTem certeza que quer enviar para análise?'
            : 'Você não poderá desfazer esta ação. Tem certeza?';

        Alert.alert(
            titleAlert,
            bodyAlert,
            [
                {
                    text: 'Enviar para análise',
                    onPress: onEnviar,
                },
                {
                    text: 'Cancelar',
                },
            ],
            { cancelable: true }
        );
    };

    const dialogTitle = !isPTTModeVar ? 'Situação do cadastro' : 'Enviar para a PTT';
    const enviadoAnaliseTitle = !isPTTModeVar
        ? 'Enviado para análise!'
        : 'Dados já encaminhados para a PTT';

    return (
        <Portal>
            <Dialog visible={showEnviarDadosDialogo} onDismiss={clearEnviarDadosDialogo}>
                <Dialog.Title>
                    {enviarDadosDialogo.etapa === 'sucesso' ? enviadoAnaliseTitle : dialogTitle}
                </Dialog.Title>
                <Dialog.Content>
                    <Text style={Layout.enviarTitle}>{currentTerritorio.nome}</Text>

                    {enviarDadosDialogo.etapa === 'situacao' && (
                        <>
                            {!settings.editing && isEnviarEnabledVar && (
                                <SituacaoCheckList
                                    situacao={enviarDadosDialogo.situacao}
                                    currentTerritorio={currentUser.currentTerritorio}
                                    online={online}
                                    isPTTModeVar={isPTTModeVar}
                                />
                            )}
                            {settings.editing && <EditingPolygon />}
                            {!isEnviarEnabledVar && <NoChanges />}
                        </>
                    )}

                    {enviarDadosDialogo.etapa === 'sucesso' && (
                        <Sucesso isPTTModeVar={isPTTModeVar} />
                    )}

                    {enviarDadosDialogo.etapa === 'erro' && (
                        <Erro apiErrors={enviarDadosDialogo.apiErrors} />
                    )}

                    {enviarDadosDialogo.etapa === 'carregando' && <Carregando />}
                </Dialog.Content>
                {enviarDadosDialogo.etapa !== 'carregando' && (
                    <>
                        {online ? (
                            <Dialog.Actions
                                style={
                                    enviarDadosDialogo.etapa === 'situacao' && !isPTTModeVar
                                        ? Layout.dialogButtonsMultiLine
                                        : Layout.dialogButtons
                                }>
                                {!settings.editing && isEnviarEnabledVar && (
                                    <>
                                        {enviarDadosDialogo.botaoCancelar && (
                                            <BtnCancel onPress={clearEnviarDadosDialogo} />
                                        )}
                                        {enviarDadosDialogo.etapa !== 'sucesso' &&
                                            enviarDadosDialogo.botaoEnviar &&
                                            enviarDadosDialogo.situacao &&
                                            !enviarDadosDialogo.situacao.some(
                                                (item) => item.pttError
                                            ) && (
                                                <BtnEnviar
                                                    onPress={onEnviarTemCerteza}
                                                    isPTTModeVar={isPTTModeVar}
                                                    online={online}
                                                />
                                            )}
                                    </>
                                )}

                                {/*enviarDadosDialogo.etapa === 'sucesso' && !isPTTModeVar && (
                                    <BtnExportReport
                                        onPress={clearEnviarDadosDialogo}
                                        navigation={navigation}
                                    />
                                )*/}

                                {(settings.editing ||
                                    !isEnviarEnabledVar ||
                                    enviarDadosDialogo.botaoOk) && (
                                    <BtnOk
                                        onPress={clearEnviarDadosDialogo}
                                        navigation={navigation}
                                        editing={settings.editing}
                                    />
                                )}
                            </Dialog.Actions>
                        ) : (
                            <Dialog.Actions style={Layout.dialogButtons}>
                                <BtnOk
                                    onPress={clearEnviarDadosDialogo}
                                    navigation={navigation}
                                    editing={settings.editing}
                                    label="Fechar"
                                />
                            </Dialog.Actions>
                        )}
                    </>
                )}
            </Dialog>
        </Portal>
    );
}
