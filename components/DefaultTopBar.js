import React from 'react';
import { View } from 'react-native';
import { FAB, IconButton } from 'react-native-paper';
import { MAP_TYPES } from 'react-native-maps';
import { SafeAreaView } from 'react-native-safe-area-context';
import Constants from 'expo-constants';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import TELAS from '../bases/telas';
import metrics from '../utils/metrics';

export default function DefaultTopBar({
    statusMessage,
    mapTypeStyles,
    handleTerritorioStatusAlert,
}) {
    const { syncStatus } = useToNoMapa();
    // Alternative Icons for the 3 syncStatuses (from MaterialCommunityIcons):
    // Offline: 'wifi-off' - 'sync-off' - 'shield-off' - 'shield-off-outline'
    // Syncing: 'wifi-sync' - 'sync' - 'sync-circle' - 'shield-sync' - 'shield-sync-outline'
    // Synced: 'check-all' - 'checkbox-marked-circle' - 'checkbox-marked-circle-outline' - 'shield-check' - 'shield-check-outline' - 'wifi-check'

    return (
        <View
            style={{
                paddingHorizontal: metrics.tenWidth * 0.5,
                position: 'absolute',
                top: metrics.tenHeight * 1,
                left: 0,
                width: '100%',
            }}>
            <SafeAreaView
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingHorizontal: metrics.tenWidth * 1.6,
                }}>
                {statusMessage && (
                    <FAB
                        // size={metrics.tenWidth * 2.5}
                        color={statusMessage.color}
                        icon={statusMessage.icon}
                        uppercase={false}
                        style={{ ...mapTypeStyles.iconButtonStyle, margin: 0, padding: 0 }}
                        onPress={handleTerritorioStatusAlert}
                        label={statusMessage.nome}
                    />
                )}
                {/*!Constants.manifest.extra.xyzRTiles.using && (
                    <Button
                        mode="contained"
                        color={colors.background}
                        onPress={() => {
                            switch (settings.mapType) {
                                case MAP_TYPES.STANDARD:
                                    settings.mapType = MAP_TYPES.HYBRID;
                                    break;
                                case MAP_TYPES.HYBRID:
                                    settings.mapType = MAP_TYPES.STANDARD;
                                    break;
                            }
                            updateSettings({
                                settings: { mapType: settings.mapType },
                                noSnackBarMessage: true,
                            });
                        }}>
                        {mapTypeStyles.label}
                    </Button>
                    )*/}

                <FAB
                    // size={metrics.tenWidth * 2.5}
                    color={syncStatus.color}
                    icon={syncStatus.icon}
                    uppercase={false}
                    style={{ ...mapTypeStyles.iconButtonLeftStyle, margin: 0, padding: 0 }}
                    onPress={syncStatus.onPress}
                    // label={syncStatus.title}
                />
            </SafeAreaView>
        </View>
    );
}
