import React from 'react';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { Layout, LayoutMapa } from '../styles';
import colors from '../assets/colors';
import { DrawerItem } from '@react-navigation/drawer';

export default function CustomDrawerItem({
    target = null,
    label,
    icon,
    iconActive = null,
    iconStyle = null,
    navigation,
    index = null,
    routes = [],
    onPress = null,
    labelStyle = null,
}) {
    const isFocused = () => {
        if (isNaN(index) || target === null) {
            return false;
        }
        return index === routes.findIndex((e) => e.name === target);
    };
    const Icon = () => {
        let style = isFocused() ? Layout.drawerIconActive : Layout.drawerIcon;
        if (iconStyle && !isFocused()) {
            style = iconStyle;
        }
        return (
            <MaterialCommunityIcons name={isFocused() ? iconActive || icon : icon} style={style} />
        );
    };
    return (
        <DrawerItem
            label={label}
            labelStyle={labelStyle || Layout.drawerLabel}
            icon={Icon}
            focused={isFocused()}
            activeTintColor={colors.Foundation.Green.G300}
            activeBackgroundColor={colors.Foundation.Green.G55}
            onPress={() => {
                if (!onPress) {
                    navigation.jumpTo(target);
                    return;
                }
                onPress();
            }}
        />
    );
}
