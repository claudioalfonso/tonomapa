import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, Dialog, Portal } from 'react-native-paper';
import { View, Text } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';

import { SLIDES } from '../bases/intro_slides';
import { Layout, SlideStyles } from '../styles';

import SlideWidget from './SlideWidget';
import Slide from './Slide';

const renderSlideNext = () => {
    return <SlideWidget type="Next" />;
};
const renderSlidePrev = () => {
    return <SlideWidget type="Prev" />;
};
const renderSlideDone = () => {
    return <SlideWidget type="Done" />;
};

export default function ToNoMapaIntro({ onDone, introDialog: initialIntroDialog = true }) {
    const [introDialog, setIntroDialog] = React.useState(initialIntroDialog);

    useEffect(() => {
        setIntroDialog(initialIntroDialog);
    }, []);

    return (
        <>
            <Portal>
                <Dialog
                    visible={introDialog}
                    onDismiss={() => {
                        setIntroDialog(false);
                    }}>
                    <Dialog.Title>Boas vindas ao Tô no Mapa!</Dialog.Title>
                    <Dialog.Content>
                        <View style={Layout.col}>
                            <Text style={Layout.textParagraph}>
                                Você deseja conhecer como funciona o aplicativo?
                            </Text>
                        </View>
                    </Dialog.Content>
                    <Dialog.Actions style={{ justifyContent: 'space-between' }}>
                        <Button
                            onPress={() => {
                                onDone();
                            }}>
                            Não
                        </Button>
                        <Button
                            onPress={() => {
                                setIntroDialog(false);
                            }}>
                            Sim!
                        </Button>
                    </Dialog.Actions>
                </Dialog>
            </Portal>

            <View style={SlideStyles.wrapper}>
                <AppIntroSlider
                    renderItem={Slide}
                    data={SLIDES}
                    renderNextButton={renderSlideNext}
                    renderPrevButton={renderSlidePrev}
                    renderDoneButton={renderSlideDone}
                    showSkipButton={true}
                    skipLabel="PULAR"
                    dotStyle={{ backgroundColor: '#666' }}
                    onDone={onDone}
                />
            </View>
        </>
    );
}

ToNoMapaIntro.propTypes = {
    onDone: PropTypes.func,
};
