import React from 'react';
import { Text } from 'react-native';
import { Snackbar } from 'react-native-paper';

import colors from '../assets/colors';

export default function ToNoMapaSnackbar({ message, visible, setVisible, style = {} }) {
    return (
        <Snackbar
            visible={message && visible === true}
            style={{
                position: 'absolute',
                bottom: 0,
                backgroundColor: colors.Foundation.Green.G75,
                ...style,
            }}
            duration={2000}
            onDismiss={() => {
                setVisible(false);
            }}>
            <Text style={{ color: colors.Foundation.Feedback.Success }}>{message}</Text>
        </Snackbar>
    );
}
