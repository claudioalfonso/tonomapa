import React from 'react';
import { View, Alert } from 'react-native';
import { MAP_TYPES } from 'react-native-maps';
import * as Location from 'expo-location';

import { LayoutMapa } from '../styles';
import metrics from '../utils/metrics';
import { log, logError } from '../utils/utils';

import MapToolBarItem from './MapToolBarItem';

export default function MapToolBar({ onUpdate, settings, setCarregando, mapFitToElements }) {
    const alertAppSemPermissoesGPS = (isFalha = false) => {
        const title = isFalha
            ? 'Não foi possível encontrar a sua posição'
            : 'Aplicativo sem permissões';
        const body = isFalha
            ? 'Pode ser por falta de cobertura GPS aqui, ou alguma configuração do seu aparelho.'
            : 'O Tô no Mapa não pode acessar a localização do seu celular! Para poder usar o GPS, vá para as configurações do celular, e conceda permissão de localização para a equipe do Tô no Mapa.';
        Alert.alert(
            title,
            body,
            [
                {
                    text: 'Ok',
                    onPress: async () => {},
                },
            ],
            { cancelable: true }
        );
    };
    const handleLocationPermissions = async () => {
        try {
            setCarregando(true);
            let ret = await Location.getForegroundPermissionsAsync();
            if (ret.status !== 'granted') {
                ret = await Location.requestForegroundPermissionsAsync();
            }
            setCarregando(false);
            if (ret.status !== 'granted') {
                console.log('Não foram dadas as permissões para GPS!');
                alertAppSemPermissoesGPS();
                return;
            }
            if (!settings.useGPS) {
                onUpdate({ useGPS: true });
            }
        } catch (e) {
            setCarregando(false);
            console.log('Erro no retorno do locationAtivarAsync no locationAtivar!', e);
            log('Erro no retorno do locationAtivarAsync no locationAtivar');
            logError(e);
            alertAppSemPermissoesGPS();
        }
    };

    return (
        <View
            style={{
                position: 'absolute',
                top: metrics.tenHeight * 16,
                right: 0,
                height: '100%',
                flex: 1,
                justifyContent: 'flex-start',
            }}>
            <View style={LayoutMapa.mapToolbarGroup}>
                <MapToolBarItem
                    onPress={() => onUpdate({ mapType: MAP_TYPES.HYBRID })}
                    // icon="satellite"
                    active={settings.mapType === MAP_TYPES.HYBRID}
                    label="Satélite"
                    position="right"
                />
                <MapToolBarItem
                    onPress={() => onUpdate({ mapType: MAP_TYPES.STANDARD })}
                    // icon="map-outline"
                    // iconActive="map"
                    active={settings.mapType === MAP_TYPES.STANDARD}
                    label="Mapa"
                    position="right"
                />
                <MapToolBarItem
                    onPress={mapFitToElements}
                    // icon="arrow-expand"
                    active={false}
                    label="Ver tudo"
                    position="right"
                />
                <MapToolBarItem
                    onPress={handleLocationPermissions}
                    // icon="crosshairs-gps"
                    active={settings.useGPS}
                    label={settings.useGPS ? 'Centralizado' : 'Recentralizar'}
                    position="right"
                />
            </View>
        </View>
    );
}
