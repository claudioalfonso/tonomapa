import React from 'react';
import { View, Text } from 'react-native';
import { Avatar } from 'react-native-paper';

import colors from '../assets/colors';
import metrics from '../utils/metrics';

export default function SlideWidget({ type }) {
    let icon, label;
    switch (type) {
        case 'Next':
            icon = 'arrow-right-thick';
            break;
        case 'Prev':
            icon = 'arrow-left-thick';
            break;
        case 'Done':
            icon = 'check';
            break;
        case 'Skip':
            icon = '';
            label = 'Pular';
            break;
    }
    return (
        <View
            style={{
                height: metrics.tenWidth * 2.6,
                flexDirection: 'row',
                alignContent: 'center',
                alignItems: 'center',
            }}>
            {icon === '' && <Text>{label}</Text>}
            {icon && (
                <Avatar.Icon
                    size={metrics.tenWidth * 2.6}
                    color={colors.Foundation.CorDeTexto.CorDeTexto2}
                    style={{ backgroundColor: colors.Foundation.Basic.White }}
                    icon={icon}
                />
            )}
        </View>
    );
}
