import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-paper';

import { isImage } from '../db/arquivos';
import { isAnexoPTT } from '../bases/anexo';
import { pluralize } from '../utils/utils';
import { Layout } from '../styles';

export default function AnexoListHeader({ anexos, pttMode }) {
    let images = 0;
    let docs = 0;
    for (let anexo of anexos) {
        if (pttMode === isAnexoPTT(anexo.tipoAnexo)) {
            if (isImage(anexo.mimetype)) {
                images++;
            } else {
                docs++;
            }
        }
    }
    let stats = '';
    if (docs) {
        stats += docs.toString() + pluralize(docs, ' arquivo', ' arquivos');
        if (images) {
            stats += ' e ';
        }
    }
    if (images) {
        stats += images.toString() + pluralize(images, ' imagem', ' imagens');
    }
    return (
        <View style={{ ...Layout.statusBar, marginBottom: 8 }}>
            <Text style={Layout.statusBarText}>
                {stats != '' ? stats : 'Não há nenhum arquivo'}
            </Text>
        </View>
    );
}
