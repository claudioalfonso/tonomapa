import React, { useRef, forwardRef, useState, useEffect, useCallback } from 'react';
import { Image, View, Text, Alert } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import MapView, { PROVIDER_GOOGLE, MAP_TYPES, Polygon, Marker, Callout } from 'react-native-maps';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Constants from 'expo-constants';
import { useQuery } from '@apollo/client';
import { debounce } from 'lodash';

import { Layout, LayoutMapa } from '../styles';
import TELAS from '../bases/telas';
import { calculaAreaPoligono, formataArea, UNIDADES_AREA } from '../maps/mapsUtils';
import metrics from '../utils/metrics';
import colors from '../assets/colors';
import { edgePaddingAdjusted } from '../assets/constants';
import { GET_USER_DATA } from '../db/queries';
import { useNavigation } from '@react-navigation/native';

import Balloon from '../components/third_party/Balloon';

import { ButtonCancel, ButtonContainedSecondary } from './buttons';
import ToNoMapaLoading from './ToNoMapaLoading';

const MapLegacy = forwardRef(function MapLegacy(
    /**
     * @type object
     */
    {
        tela,
        setTela,
        persistePoligonos,
        markers,
        polygons,
        region,
        mapMarker,
        markersTipoUsoConflito,
        mapTypeStyles,
        settings,
        updateSettings,
        isCurrentTerritorioEditableVar,
        isPTTModeVar,
    },
    ref
) {
    const navigation = useNavigation();

    // @ts-ignore
    const map = ref?.current;

    const [carregando, setCarregando] = useState(false);

    const locationMarker = useRef();

    const [locationMarkerCoordinate, setLocationMarkerCoordinate] = useState({
        latitude: region.latitude,
        longitude: region.longitude,
    });

    /**
     * @type object
     */
    const mapEditingPolygon = useRef();

    const { data: currentUserData } = useQuery(GET_USER_DATA);

    const [currentTerritorio, setCurrentTerritorio] = useState(null);

    const localUpdateSettings = async (newSettings) => {
        updateSettings({ settings: newSettings, noSnackBarMessage: true });
    };

    /**
     * @type {Array}
     */
    const mapEditingPolygonEdge = [];

    /**
     * @type object
     */
    const calloutVisible = useRef(false);

    const frustratedMoveCount = useRef(0);

    const zoomRef = useRef(0);

    const [hintBalloon, setHintBalloon] = useState(false);

    const useDebounce = (callback, delay) => {
        const debouncedFn = useCallback(
            debounce((...args) => callback(...args), delay),
            [delay]
        );
        return debouncedFn;
    };
    const onPanDragEnd = useDebounce(async (mapRef) => {
        const { zoom } = await mapRef.getCamera();
        if (zoom !== zoomRef.current) {
            frustratedMoveCount.current = 0;
            zoomRef.current = zoom;
            return;
        }

        frustratedMoveCount.current++;
        if (
            (frustratedMoveCount.current === 3 && settings.showGPSHelpBalloon) ||
            (frustratedMoveCount.current === 3 && !settings.showGPSHelpBalloon)
        ) {
            localUpdateSettings({ showGPSHelpBalloon: false });
            setHintBalloon(true);
            frustratedMoveCount.current = 0;
            return;
        }
    }, 500);
    const handlePanDrag = () => {
        if (tela !== TELAS.MAPA && settings.useGPS) {
            // localUpdateSettings({ useGPS: false });
            onPanDragEnd(map);
        }
    };

    const handleMapViewPress = (e) => {
        if (calloutVisible.current) {
            calloutVisible.current = false;
            return;
        }

        if (tela === TELAS.MAPEAR_TERRITORIO) {
            adicionarVerticePoligono(e);
            return;
        }

        adicionarPonto(e);
    };

    const handleLocationMarkerPress = () => {
        // @ts-ignore
        locationMarker.current.hideCallout();
        if (tela === TELAS.MAPEAR_TERRITORIO) {
            adicionarVerticePoligonoGPS();
            return;
        }

        adicionarPontoGPS();
    };

    const handleEditingPolygonPress = () => {
        if (tela !== TELAS.MAPEAR_TERRITORIO) {
            setTela(TELAS.MAPEAR_TERRITORIO);
        }
    };

    const handlePolygonPress = (e, index, area) => {
        if (!isCurrentTerritorioEditableVar || isPTTModeVar) {
            return;
        }

        calloutVisible.current = true;
        const alertApagarArea = () => {
            Alert.alert(
                'Apagar área',
                `Tem certeza que quer apagar esta área de ${formataArea(
                    area,
                    settings.unidadeArea
                )}?\n\nEsta operação não tem volta!\n\nQuer apagar?`,
                [
                    {
                        text: 'Não',
                    },
                    {
                        text: 'Sim',
                        onPress: () => {
                            polygons.splice(index, 1);
                            persistePoligonos({
                                polygons: polygons,
                                newSnackBarMessage: 'Área da comunidade apagada com sucesso!',
                            });
                        },
                    },
                ],
                { cancelable: true }
            );
        };
        if (settings.editaPoligonos) {
            const buttons = [
                {
                    text: 'Cancelar',
                },
                {
                    text: 'Apagar',
                    onPress: alertApagarArea,
                },
                {
                    text: 'Editar',
                    onPress: () => {
                        const polygon = polygons.splice(index, 1);
                        persistePoligonos({ polygons });
                        localUpdateSettings({
                            editing: {
                                id: polygon[0].id,
                                coordinates: polygon[0].coordinates,
                                holes: polygon[0].holes,
                                area: polygon[0].area,
                                originalPolygon: polygon[0],
                            },
                        });
                        if (tela !== TELAS.MAPEAR_TERRITORIO) {
                            setTela(TELAS.MAPEAR_TERRITORIO);
                        }
                    },
                },
            ];
            if (tela === TELAS.MAPEAR_TERRITORIO) {
                buttons.push({
                    text: 'Adicionar vértice de nova área',
                    onPress: async () => {
                        adicionarVerticePoligono();
                    },
                });
            }
            Alert.alert(
                'Editar ou apagar área',
                `Você pode voltar a editar esta área (${formataArea(
                    area,
                    settings.unidadeArea
                )}) ou apagá-la.\n\nque deseja fazer?`,
                buttons,
                { cancelable: true }
            );
            return;
        }

        alertApagarArea();
    };

    const handlePolygonEdgePress = () => {
        calloutVisible.current = true;
    };

    const handlePolygonEdgeCalloutPress = (index) => {
        Alert.alert(
            'Apagar vértice',
            'Tem certeza que quer apagar este ponto da área da comunidade?',
            [
                {
                    text: 'Não',
                },
                {
                    text: 'Sim, apagar',
                    onPress: () => {
                        mapEditingPolygonEdge[index].hideCallout();
                        calloutVisible.current = false;
                        const coords = [...settings.editing.coordinates];
                        coords.splice(index, 1);
                        const newEditing = {
                            ...settings.editing,
                            coordinates: coords,
                        };
                        localUpdateSettings({ editing: newEditing });
                    },
                },
            ],
            { cancelable: true }
        );
    };

    const handlePolygonEdgeDragEnd = (e, index) => {
        const coords = [...settings.editing.coordinates];
        coords[index] = e.nativeEvent.coordinate;
        const newEditing = {
            ...settings.editing,
            coordinates: coords,
        };
        localUpdateSettings({ editing: newEditing });
    };

    const handleMarkerPress = () => {
        calloutVisible.current = true;
        if (tela !== TELAS.MAPEAR_USOSECONFLITOS) {
            return;
        }
    };

    const handleMarkerDragEnd = (e, index) => {
        const coords = [...settings.editing.coordinates];
        coords[index] = e.nativeEvent.coordinate;
        const newEditing = {
            ...settings.editing,
            coordinates: coords,
        };
        localUpdateSettings({ editing: newEditing });
    };

    const handleCloseBalloon = () => {
        setHintBalloon(false);
    };

    const handleDisableGPS = () => {
        updateSettings({
            settings: { useGPS: false },
            noSnackBarMessage: false,
            snackBarMessage: 'GPS desativado',
        });
        handleCloseBalloon();
    };

    const adicionarPonto = (e) => {
        // @ts-ignore
        navigation.navigate('EditarUsoConflito', {
            usoOuConflito: {
                posicao: {
                    type: 'Point',
                    coordinates: [
                        e.nativeEvent.coordinate.longitude,
                        e.nativeEvent.coordinate.latitude,
                    ],
                },
            },
        });
    };

    /* Marca ponto de uso ou conflito */
    const adicionarPontoGPS = () => {
        const lastCoord = markers[markers.length - 1]
            ? markers[markers.length - 1].coordinate
            : null;
        if (
            lastCoord &&
            lastCoord.latitude === locationMarkerCoordinate.latitude &&
            lastCoord.longitude === locationMarkerCoordinate.longitude
        ) {
            Alert.alert(
                'Ponto já adicionado',
                'Você já adicionou este ponto.',
                [
                    {
                        text: 'Ok',
                    },
                ],
                { cancelable: true }
            );
            return;
        }

        // @ts-ignore
        navigation.navigate('EditarUsoConflito', {
            usoOuConflito: {
                posicao: {
                    type: 'Point',
                    coordinates: [
                        locationMarkerCoordinate.longitude,
                        locationMarkerCoordinate.latitude,
                    ],
                },
            },
        });
    };

    const editarPonto = (usoOuConflito, index = null) => {
        if (index !== null) {
            mapMarker[index].hideCallout();
            calloutVisible.current = false;
        }
        // @ts-ignore
        navigation.navigate('EditarUsoConflito', {
            usoOuConflito,
        });
    };

    // Adiciona vértice ao polígono:
    const adicionarVerticePoligono = (e) => {
        if (!settings.editing) {
            const coords = e.nativeEvent.coordinate;
            const newCoords = [coords];
            const newEditing = {
                id: polygons.length + 1,
                coordinates: newCoords,
                holes: [],
                area: 0,
            };
            localUpdateSettings({ editing: newEditing });
            return;
        }

        const newCoords = [...settings.editing.coordinates, e.nativeEvent.coordinate];
        const newArea = calculaAreaPoligono(newCoords, settings.unidadeArea);
        const newEditing = {
            ...settings.editing,
            coordinates: newCoords,
            area: newArea,
        };
        localUpdateSettings({ editing: newEditing });
    };

    const adicionarVerticePoligonoGPS = () => {
        if (!settings.editing) {
            const newEditing = {
                id: polygons.length + 1,
                coordinates: [locationMarkerCoordinate],
                holes: [],
                area: 0,
            };
            localUpdateSettings({ editing: newEditing });
            return;
        }

        const lastCoord = settings.editing.coordinates[settings.editing.coordinates.length - 1];
        if (
            lastCoord.latitude === locationMarkerCoordinate.latitude &&
            lastCoord.longitude === locationMarkerCoordinate.longitude
        ) {
            Alert.alert(
                'Ponto já adicionado',
                'Você já adicionou este ponto.',
                [
                    {
                        text: 'Ok',
                    },
                ],
                { cancelable: true }
            );
            return;
        }

        const newCoords = [...settings.editing.coordinates, locationMarkerCoordinate];
        const newArea = calculaAreaPoligono(newCoords, settings.unidadeArea);
        const newEditing = {
            ...settings.editing,
            coordinates: newCoords,
            area: newArea,
        };
        localUpdateSettings({ editing: newEditing });
    };

    const mapFitToElements = useCallback(() => {
        if (!map) {
            return;
        }

        /*DEBUG*/ //console.log('mapFitToElements', state.polygons.length, state.markers.length);
        if (
            polygons.length ||
            markers.length ||
            (settings.editing &&
                settings.editing.coordinates &&
                settings.editing.coordinates.length >= 3)
        ) {
            if (
                polygons.length === 0 &&
                markers.length === 1 &&
                (!settings.editing || settings.editing.coordinates.length < 3)
            ) {
                map.animateCamera({ center: markers[0].coordinate });
                return;
            }

            let coords = [];
            for (let i = 0; i < polygons.length; i++) {
                coords = coords.concat(polygons[i].coordinates);
            }
            for (let i = 0; i < markers.length; i++) {
                coords.push(markers[i].coordinate);
            }
            if (settings.editing && settings.editing.coordinates.length >= 3) {
                coords = coords.concat(settings.editing.coordinates);
            }
            map.fitToCoordinates(coords, {
                edgePadding: edgePaddingAdjusted,
            });
            return;
        }

        if (region) {
            map.animateToRegion(region, 2000);
        }
    }, [polygons, markers, settings.editing]);

    useEffect(() => {
        if (currentUserData) {
            setCurrentTerritorio(currentUserData.currentUser.currentTerritorio);
        }
    }, [currentUserData]);

    return (
        <>
            <MapView
                ref={ref}
                provider={Constants.manifest.extra.xyzRTiles.using ? null : PROVIDER_GOOGLE}
                style={LayoutMapa.map}
                mapType={
                    Constants.manifest.extra.xyzRTiles.using ? MAP_TYPES.NONE : settings.mapType
                }
                initialRegion={region}
                zoomControlEnabled={false}
                showsUserLocation={true}
                userLocationAnnotationTitle="Meu local"
                showsMyLocationButton={false}
                showsBuildings={false}
                showsIndoors={false}
                showsPointsOfInterest={false}
                showsTraffic={false}
                scrollEnabled={tela === TELAS.MAPA || !settings.useGPS}
                pitchEnabled={tela === TELAS.MAPA || !settings.useGPS}
                rotateEnabled={tela === TELAS.MAPA || !settings.useGPS}
                onUserLocationChange={(e) => {
                    if (tela !== TELAS.MAPA && settings.useGPS) {
                        const newCoords = {
                            latitude: e.nativeEvent.coordinate.latitude,
                            longitude: e.nativeEvent.coordinate.longitude,
                        };
                        map.animateCamera({ center: newCoords });
                        setLocationMarkerCoordinate(newCoords);
                    }
                }}
                onPanDrag={handlePanDrag}
                onPress={handleMapViewPress}>
                {settings.useGPS && tela !== TELAS.MAPA && (
                    <Marker
                        key="locationMarker"
                        ref={locationMarker}
                        coordinate={locationMarkerCoordinate}
                        onPress={() => {
                            calloutVisible.current = true;
                        }}
                        centerOffset={{ x: 0.3, y: 1 }}
                        anchor={{ x: 0.3, y: 1 }}>
                        <Image
                            source={mapTypeStyles.markerIconToShow}
                            style={Layout.icon}
                            resizeMode="contain"
                        />
                        <Callout onPress={handleLocationMarkerPress}>
                            <View style={[Layout.col, { width: metrics.tenWidth * 16 }]}>
                                <Text style={Layout.calloutActionCenter}>
                                    Clique para adicionar
                                </Text>
                                <Text style={Layout.calloutActionCenter}>sua posição</Text>
                            </View>
                        </Callout>
                    </Marker>
                )}

                {polygons.map((polygon, index) => (
                    <Polygon
                        key={`polygon_${index}`}
                        coordinates={polygon.coordinates}
                        strokeColor={colors.warning}
                        fillColor={mapTypeStyles.polygonFillColor}
                        strokeWidth={3}
                        tappable={tela !== TELAS.MAPEAR_USOSECONFLITOS}
                        zIndex={2}
                        onPress={(e) => {
                            handlePolygonPress(e, index, polygon.area);
                        }}
                    />
                ))}
                {markers.map((marker, index) => (
                    <Marker
                        key={`marker_${index}`}
                        coordinate={marker.coordinate}
                        draggable={isCurrentTerritorioEditableVar && !isPTTModeVar}
                        onPress={handleMarkerPress}
                        onDragEnd={(e) => {
                            handleMarkerDragEnd(e, index);
                        }}>
                        <Image source={marker.icone} style={Layout.icon} resizeMode="contain" />
                        <Callout
                            onPress={() => {
                                if (isCurrentTerritorioEditableVar && !isPTTModeVar) {
                                    editarPonto(marker.object, index);
                                }
                            }}>
                            <View style={[Layout.col, { width: metrics.tenWidth * 16 }]}>
                                {marker.title != null && marker.title.trim() != '' && (
                                    <Text style={Layout.calloutTitle}>{marker.title}</Text>
                                )}
                                <Text style={Layout.calloutDescription}>
                                    <>
                                        {marker.object.__typename === 'ConflitoType' ? (
                                            <Text>Conflito: {markersTipoUsoConflito[index]}</Text>
                                        ) : (
                                            <Text>Uso: {markersTipoUsoConflito[index]}</Text>
                                        )}
                                    </>
                                </Text>
                                {'area' in marker.object && parseFloat(marker.object.area) > 0 && (
                                    <Text style={Layout.calloutDescription}>
                                        Área:{' '}
                                        {formataArea(
                                            marker.object.area,
                                            UNIDADES_AREA.METRO_QUADRADO
                                        )}
                                    </Text>
                                )}
                                {marker.object.descricao != null &&
                                    marker.object.descricao.trim() != '' && (
                                        <Text style={Layout.calloutDescriptionSub}>
                                            {marker.object.descricao}
                                        </Text>
                                    )}

                                {isCurrentTerritorioEditableVar && !isPTTModeVar && (
                                    <Text style={Layout.calloutAction}>Editar ou apagar</Text>
                                )}
                            </View>
                        </Callout>
                    </Marker>
                ))}
                {settings.editing && (
                    <>
                        <Polygon
                            key="mapEditingPolygon"
                            ref={mapEditingPolygon}
                            coordinates={settings.editing.coordinates}
                            holes={settings.editing.holes}
                            strokeColor={mapTypeStyles.polygonEditingStrokeColor}
                            fillColor="rgba(255,0,0,0.5)"
                            strokeWidth={3}
                            tappable={tela === TELAS.MAPA}
                            onPress={handleEditingPolygonPress}
                        />
                        {!settings.editaPoligonos &&
                            tela === TELAS.MAPEAR_TERRITORIO &&
                            settings.editing.coordinates.map((coord, index) => (
                                <Marker
                                    key={`polygonEdge_${index}`}
                                    coordinate={coord}
                                    centerOffset={{ x: 0.5, y: 0.5 }}
                                    anchor={{ x: 0.5, y: 0.5 }}>
                                    {(index === 0 ||
                                        index === settings.editing.coordinates.length - 1) && (
                                        <MaterialCommunityIcons
                                            name="circle"
                                            size={metrics.tenWidth * 2}
                                            style={{
                                                paddingTop: 0,
                                                paddingLeft: 0,
                                                color:
                                                    index === 0
                                                        ? colors.Foundation.Feedback.Success
                                                        : colors.Foundation.Feedback.Error,
                                            }}
                                        />
                                    )}
                                </Marker>
                            ))}
                        {settings.editaPoligonos &&
                            tela === TELAS.MAPEAR_TERRITORIO &&
                            settings.editing.coordinates.map((coord, index) => (
                                <Marker
                                    key={index}
                                    ref={(ref) => (mapEditingPolygonEdge[index] = ref)}
                                    coordinate={coord}
                                    onPress={handlePolygonEdgePress}
                                    centerOffset={{ x: 0.5, y: 0.5 }}
                                    anchor={{ x: 0.5, y: 0.5 }}
                                    draggable
                                    onDragEnd={(e) => {
                                        handlePolygonEdgeDragEnd(e, index);
                                    }}>
                                    <View
                                        style={[
                                            Layout.vertice,
                                            {
                                                backgroundColor: mapTypeStyles.backgroundMarkerView,
                                            },
                                        ]}>
                                        {(index === 0 ||
                                            index === settings.editing.coordinates.length - 1) && (
                                            <MaterialCommunityIcons
                                                name="circle-double"
                                                size={metrics.tenWidth * 2}
                                                style={{
                                                    paddingTop: 0,
                                                    paddingLeft: 0,
                                                    color:
                                                        index === 0
                                                            ? colors.Foundation.Feedback.Success
                                                            : colors.Foundation.Feedback.Error,
                                                }}
                                            />
                                        )}
                                    </View>
                                    <Callout
                                        onPress={() => {
                                            handlePolygonEdgeCalloutPress(index);
                                        }}>
                                        <View
                                            style={{
                                                ...Layout.col,
                                                minWidth: metrics.tenWidth * 10,
                                            }}>
                                            <Text style={{ ...Layout.calloutTitle, flex: 1 }}>
                                                {'Vértice nº ' + (index + 1)}
                                            </Text>
                                            {index === settings.editing.coordinates.length - 1 && (
                                                <Text style={Layout.calloutDescription}>
                                                    Último vértice
                                                </Text>
                                            )}
                                            <Text style={Layout.calloutAction}>Apagar</Text>
                                        </View>
                                    </Callout>
                                </Marker>
                            ))}
                    </>
                )}
            </MapView>
            {tela !== TELAS.MAPA && hintBalloon && (
                <>
                    <TouchableRipple
                        style={Layout.appOverlayZIndex100}
                        onPress={handleCloseBalloon}>
                        <View></View>
                    </TouchableRipple>
                    <View style={Layout.balloonHintWrapper}>
                        <Balloon
                            borderColor={colors.Foundation.Basic.Gray}
                            backgroundColor={colors.Foundation.CorDeTexto.CorDeTexto}
                            borderWidth={metrics.tenWidth * 0.2}
                            borderRadius={metrics.tenWidth * 1.2}
                            width="100%"
                            triangleSize={metrics.tenWidth * 1.5}
                            triangleOffset={'94%'}
                            triangleDirection="top"
                            containerStyle={{
                                marginHorizontal: 0,
                                padding: metrics.tenWidth * 0.8,
                            }}>
                            <Text style={Layout.balloonHintTitle}>Modo GPS está ativado!</Text>
                            <Text style={Layout.balloonHintText}>
                                Quando o GPS está ativado, o mapa muda quando você se move.
                            </Text>
                            <Text style={Layout.balloonHintText}>
                                Se você quer mover o mapa com a mão, desative o modo GPS no botão
                                acima!
                            </Text>
                            <View style={Layout.buttonsBottomDialogWrapper}>
                                <ButtonCancel
                                    compact={true}
                                    mode="text"
                                    onPress={handleCloseBalloon}>
                                    Entendi
                                </ButtonCancel>
                                <ButtonContainedSecondary
                                    compact={true}
                                    onPress={handleDisableGPS}
                                    icon="crosshairs-gps">
                                    Desligar GPS
                                </ButtonContainedSecondary>
                            </View>
                        </Balloon>
                    </View>
                </>
            )}
            {carregando && <ToNoMapaLoading isOverlay={true} />}
        </>
    );
});

export default React.memo(MapLegacy);
