import React from 'react';
import { View, Text } from 'react-native';
import { Appbar, TouchableRipple } from 'react-native-paper';
import metrics from '../utils/metrics';
import { LayoutMapa } from '../styles';
import colors from '../assets/colors';

export default function BottomBarItem({
    onPress,
    icon,
    iconActive = null,
    label = null,
    disabled = false,
    active = false,
}) {
    return (
        <TouchableRipple
            onPress={onPress}
            style={
                active
                    ? LayoutMapa.appbarBottomItemWrapperActive
                    : LayoutMapa.appbarBottomItemWrapper
            }>
            <View style={LayoutMapa.appbarBottomItem}>
                <Appbar.Action
                    icon={active ? iconActive || icon : icon}
                    color={
                        active
                            ? colors.Foundation.Green.G300
                            : colors.Foundation.CorDeTexto.CorDeTexto2
                    }
                    style={LayoutMapa.appbarBottomAction}
                    disabled={disabled}
                    size={metrics.tenWidth * 3}
                />
                {label && (
                    <Text
                        style={
                            active
                                ? LayoutMapa.appbarBottomLabelActive
                                : LayoutMapa.appbarBottomLabel
                        }>
                        {label}
                    </Text>
                )}
            </View>
        </TouchableRipple>
    );
}
