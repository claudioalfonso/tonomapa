import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import * as RootNavigation from '../utils/RootNavigation';
import LoginScreen from '../screens/LoginScreen';
import PreRequisitosScreen from '../screens/PreRequisitosScreen';
import CadastroScreen from '../screens/CadastroScreen';
import MainScreen from '../screens/MainScreen';
import EditarUsoConflitoScreen from '../screens/EditarUsoConflitoScreen';
import WaitingScreen from '../screens/WaitingScreen';
import PedirCodigoSegurancaScreen from '../screens/PedirCodigoSegurancaScreen';
import { useToNoMapa } from '../context/ToNoMapaProvider';

export default function ToNoMapaNavigation() {
    const RootStack = createStackNavigator();
    const { isLoading, userToken } = useToNoMapa();

    return (
        <NavigationContainer ref={RootNavigation.navigationRef} onReady={() => {}}>
            <RootStack.Navigator
                screenOptions={{
                    headerShown: false,
                }}>
                {isLoading && (
                    <RootStack.Screen
                        name="Waiting"
                        component={WaitingScreen}
                        options={{ headerShown: false }}
                    />
                )}
                {!isLoading && userToken === null && (
                    <>
                        <RootStack.Screen name="Login" component={LoginScreen} />
                        <RootStack.Screen name="PreRequisitos" component={PreRequisitosScreen} />
                        <RootStack.Screen name="Cadastro" component={CadastroScreen} />
                        <RootStack.Screen
                            name="PedirCodigoSeguranca"
                            component={PedirCodigoSegurancaScreen}
                        />
                    </>
                )}
                {!isLoading && userToken && (
                    <>
                        <RootStack.Screen name="Main" component={MainScreen} />
                        <RootStack.Screen
                            name="EditarUsoConflito"
                            component={EditarUsoConflitoScreen}
                        />
                        <RootStack.Screen name="CadastroTerritorio" component={CadastroScreen} />
                    </>
                )}
            </RootStack.Navigator>
        </NavigationContainer>
    );
}
