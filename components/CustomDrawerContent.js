import React from 'react';
import { Alert, View } from 'react-native';
import { Drawer } from 'react-native-paper';
import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';

import { Layout } from '../styles';
import { openUrlInBrowser } from '../utils/utils';
import STATUS, { alertStatusId, isValidado } from '../bases/status';
import { useToNoMapa } from '../context/ToNoMapaProvider';
import TELAS from '../bases/telas';

import CustomDrawerItem from './CustomDrawerItem';

export default function CustomDrawerContent(props) {
    const {
        currentUser,
        isPTTModeVar,
        isCurrentTerritorioEditableVar,
        isEnviarEnabledVar,
        isEnviarPTTEnabledVar,
        navigation,
        state: { index, routes },
    } = props;

    const { enviarDadosDialogoHandler } = useToNoMapa();
    const setShowEnviarDadosDialogo = enviarDadosDialogoHandler[1];

    const alertTerritorioStatusIdChanged = () => {
        const status = currentUser?.currentTerritorio?.statusId;

        const { title, body, urlReferencia, hasFaleConoscoBtn } = alertStatusId({
            statusId: status,
        });

        const botoes = [];

        if (urlReferencia) {
            botoes.push({
                text: 'Ir ao site',
                onPress: () => {
                    openUrlInBrowser(urlReferencia);
                },
            });
        }

        if (hasFaleConoscoBtn) {
            botoes.push({
                text: 'Fale conosco',
                onPress: () => {
                    navigation.jumpTo('FaleConosco');
                },
            });
        }

        botoes.push({
            text: 'Entendi',
        });

        Alert.alert(title, body, botoes, { cancelable: true });
    };

    const alertDadosJaEncaminhadosPTT = () => {
        Alert.alert(
            'Dados já encaminhados para a PTT',
            'Você já fez o pré-cadastro da comunidade na Plataforma de Territórios Tradicionais do Conselho Nacional de Povos e Comunidades Tradicionais do Ministério Público Federal. Para seguir o cadastramento, acesse https://territoriostradicionais.mpf.mp.br',
            [
                {
                    text: 'ok',
                    onPress: async () => {},
                },
            ],
            { cancelable: true }
        );
    };

    const handleDados = () => {
        if (isPTTModeVar) {
            if (isCurrentTerritorioEditableVar) {
                navigation.jumpTo('DadosPTT');
                return;
            }
            alertDadosJaEncaminhadosPTT();
            return;
        }

        if (isCurrentTerritorioEditableVar) {
            navigation.jumpTo('DadosBasicos');
            return;
        }
        alertTerritorioStatusIdChanged();
    };

    const handleUsosEConflitos = () => {
        if (isPTTModeVar) {
            alertDadosJaEncaminhadosPTT();
            return;
        }

        if (isCurrentTerritorioEditableVar) {
            navigation.jumpTo('Home', { tela: TELAS.MAPEAR_USOSECONFLITOS });
            return;
        }
        alertTerritorioStatusIdChanged();
    };

    const handleCompartilharRelatorio = () => {
        navigation.jumpTo('Home', { compartilharRelatorio: true });
    };

    const handleMensagens = () => {
        if (currentUser) {
            if (currentUser.currentTerritorio?.id) {
                navigation.navigate('Mensagens');
                return;
            }

            Alert.alert(
                'Território ainda não cadastrado no Tô no Mapa',
                'Você só poderá receber e enviar mensagens à equipe Tô no Mapa após ter enviado os dados pela primeira vez para o Tô no Mapa.',
                [
                    {
                        text: 'Enviar Dados',
                        onPress: async () => {
                            setShowEnviarDadosDialogo(true);
                            navigation.navigate('Home');
                        },
                    },
                    {
                        text: 'Ok',
                        onPress: async () => {},
                    },
                ],
                { cancelable: true }
            );
        }
    };

    const handleEnviarParaRevisao = () => {
        if (isEnviarPTTEnabledVar) {
            if (isEnviarEnabledVar) {
                setShowEnviarDadosDialogo(true);
                navigation.navigate('Home');
                return;
            }

            Alert.alert(
                'Cadastrar comunidade na PTT',
                'Graças a uma parceria estabelecida com o Tô no Mapa, você pode cadastrar a comunidade também na Plataforma de Territórios Tradicionais (PTT) desenvolvida pelo Conselho Nacional dos Povos e Comunidades Tradicionais (CNPCT) e Ministério Público Federal (MPF).\n\nIsso pode ser feito aqui mesmo no aplicativo!\n\nBasta concordar com esse novo cadastro e fornecer dados adicionais e arquivos específicos.\n\nQuer começar?',
                [
                    {
                        text: 'Não',
                        onPress: async () => {},
                    },
                    {
                        text: 'Sim, iniciar cadastro',
                        onPress: async () => {
                            navigation.jumpTo('Home', {
                                entraModoPTT: true,
                            });
                        },
                    },
                ],
                { cancelable: true }
            );
            navigation.navigate('Home');
            return;
        }

        setShowEnviarDadosDialogo(true);
        navigation.navigate('Home');
    };

    return (
        <DrawerContentScrollView
            {...props}
            contentContainerStyle={{
                flexGrow: 1,
                justifyContent: 'space-between',
            }}>
            <View>
                <Drawer.Section title={currentUser?.currentTerritorio?.nome || 'Comunidade'}>
                    <CustomDrawerItem
                        target="Home"
                        label="Mapa"
                        icon="map"
                        navigation={navigation}
                        index={index}
                        routes={routes}
                    />

                    {!isPTTModeVar && (
                        <CustomDrawerItem
                            label="Locais"
                            icon="map-marker-outline"
                            iconStyle={
                                isCurrentTerritorioEditableVar
                                    ? Layout.drawerIcon
                                    : Layout.drawerIconDisabled
                            }
                            navigation={navigation}
                            index={index}
                            routes={routes}
                            labelStyle={
                                isCurrentTerritorioEditableVar
                                    ? Layout.drawerLabel
                                    : Layout.drawerLabelDisabled
                            }
                            onPress={handleUsosEConflitos}
                        />
                    )}

                    <CustomDrawerItem
                        target={!isPTTModeVar ? 'DadosBasicos' : 'DadosPTT'}
                        label={!isPTTModeVar ? 'Dados da comunidade' : 'Dados PTT'}
                        icon="file-document-outline"
                        iconActive="file-document"
                        iconStyle={
                            isCurrentTerritorioEditableVar
                                ? Layout.drawerIcon
                                : Layout.drawerIconDisabled
                        }
                        navigation={navigation}
                        index={index}
                        routes={routes}
                        labelStyle={
                            isCurrentTerritorioEditableVar
                                ? Layout.drawerLabel
                                : Layout.drawerLabelDisabled
                        }
                        onPress={handleDados}
                    />

                    <CustomDrawerItem
                        target="AnexoList"
                        label={!isPTTModeVar ? 'Arquivos e imagens' : 'Anexos PTT'}
                        icon="file-image-marker"
                        navigation={navigation}
                        index={index}
                        routes={routes}
                    />

                    <CustomDrawerItem
                        label="Compartilhar relatório"
                        icon="share"
                        navigation={navigation}
                        index={index}
                        routes={routes}
                        onPress={handleCompartilharRelatorio}
                    />

                    {!isPTTModeVar && (
                        <CustomDrawerItem
                            target="Mensagens"
                            label="Mensagens"
                            icon="message"
                            navigation={navigation}
                            index={index}
                            routes={routes}
                            onPress={handleMensagens}
                        />
                    )}

                    <CustomDrawerItem
                        target="EnviarParaRevisao"
                        label={
                            isEnviarPTTEnabledVar ? 'Enviar para a PTT!' : 'Enviar para análise!'
                        }
                        labelStyle={
                            isCurrentTerritorioEditableVar ||
                            currentUser?.currentTerritorio?.statusId === STATUS.OK_TONOMAPA
                                ? Layout.drawerLabel
                                : Layout.drawerLabelDisabled
                        }
                        icon="upload"
                        navigation={navigation}
                        index={index}
                        routes={routes}
                        onPress={handleEnviarParaRevisao}
                    />
                </Drawer.Section>

                <Drawer.Section title="App Tô no Mapa">
                    <CustomDrawerItem
                        target="Sobre"
                        label="Sobre o Tô no mapa"
                        icon="information"
                        navigation={navigation}
                        index={index}
                        routes={routes}
                    />

                    <CustomDrawerItem
                        target="TICCAs"
                        label="TICCAs: Gestão"
                        icon="home-group"
                        navigation={navigation}
                        index={index}
                        routes={routes}
                    />

                    <CustomDrawerItem
                        target="AppIntro"
                        label="Ver tutorial"
                        icon="ship-wheel"
                        navigation={navigation}
                    />
                </Drawer.Section>
            </View>

            <Drawer.Section>
                <CustomDrawerItem
                    target="FaleConosco"
                    label="Fale conosco"
                    icon="help-circle"
                    navigation={navigation}
                />

                <CustomDrawerItem
                    target="Configuracoes"
                    label="Configurações"
                    icon="cog"
                    navigation={navigation}
                    index={index}
                    routes={routes}
                />
            </Drawer.Section>
        </DrawerContentScrollView>
    );
}
