import React from 'react';
import { Appbar } from 'react-native-paper';

import { Layout } from '../styles';
import metrics from '../utils/metrics';

export default function ToNoMapaAppbar({
    navigation,
    title,
    rightActions = [],
    goBack = false,
    customGoBack = null,
}) {
    return (
        <Appbar.Header style={Layout.topBar}>
            <Appbar.BackAction
                size={metrics.tenWidth * 2.5}
                onPress={() =>
                    customGoBack
                        ? customGoBack()
                        : goBack
                        ? navigation.goBack()
                        : navigation.navigate('Home')
                }
            />
            <Appbar.Content title={title} titleStyle={Layout.topBarTitle} />
            {rightActions.map((rightAction, index) => (
                <Appbar.Action
                    key={`${index}`}
                    icon={rightAction.icon}
                    color={rightAction.color}
                    onPress={rightAction.onPress}
                    style={{ marginRight: 0, marginLeft: 0 }}
                />
            ))}
        </Appbar.Header>
    );
}
