import React from 'react';
import { View, Text, Image, Dimensions } from 'react-native';

import Balloon from '../components/third_party/Balloon';

import metrics from '../utils/metrics';
import { SlideStyles } from '../styles';

// const { width: windowWidth, height: windowHeight } = Dimensions.get('window');

// ratio is 720x1197 -> 0,6332
// ratio is 720x1458 -> 0,493488691

// TODO: Make higher resolution images
// if (windowWidth >= 500 && windowHeight >= 760) {
//     imageDimensions.width = metrics.tenHeight * 40;
//     imageDimensions.height = metrics.tenHeight * 50.5;
// }

export default function Slide({ item }) {
    const imageDimensions = {
        width: metrics.tenHeight * 29.61,
        height: metrics.tenHeight * 60,
    };
    return (
        <View style={{ ...SlideStyles.content, backgroundColor: item.backgroundColor }}>
            <View style={{ position: 'relative', ...imageDimensions }}>
                <Image source={item.image} style={imageDimensions} />
                <View style={item.balloonStyles}>
                    <Balloon
                        borderColor="#999"
                        backgroundColor={item.balloonBackgroundColor}
                        width={item.balloonWidth}
                        borderWidth={metrics.tenHeight * 0.2}
                        borderRadius={metrics.tenHeight * 1.2}
                        triangleSize={metrics.tenHeight * 1.5}
                        triangleOffset={item.triangleOffset}
                        triangleDirection={item.triangleDirection}
                        containerStyle={{ padding: metrics.tenHeight * 0.8 }}>
                        <Text style={{ ...SlideStyles.text, color: item.balloonTextColor }}>
                            {item.text}
                        </Text>
                    </Balloon>
                </View>
            </View>
        </View>
    );
}
