import React, { useState } from 'react';
import { Alert, Text, View } from 'react-native';
import * as Location from 'expo-location';

import { areaPoligonos, calculaAreaPoligono, formataArea, qtdePoligonos } from '../maps/mapsUtils';
import TELAS from '../bases/telas';
import { Layout } from '../styles';
import { getUsoOuConflitoLabel, log, logError, pluralize } from '../utils/utils';
import colors from '../assets/colors';

import ToNoMapaAppbar from './ToNoMapaAppbar';
import ToNoMapaLoading from './ToNoMapaLoading';

export default function MapTopBar({
    navigation,
    tela,
    currentTerritorio,
    settings,
    updateSettings,
    markers,
    polygons,
    mapFitToElements,
}) {
    const [carregando, setCarregando] = useState(false);
    let appBarTitle = null;
    let statusMapeamentoTxt = '';
    switch (tela) {
        case TELAS.MAPEAR_TERRITORIO:
            appBarTitle = 'Mapear';
            if (
                (!settings.editing ||
                    !settings.editing.coordinates.length ||
                    settings.editing.coordinates.length === 0) &&
                qtdePoligonos(currentTerritorio) === 0
            ) {
                statusMapeamentoTxt =
                    'Mova o mapa para escolher onde deve colocar cada ponta da área da comunidade';
            } else {
                if (qtdePoligonos(currentTerritorio) > 0) {
                    const areaTxt =
                        ' (' + formataArea(areaPoligonos(polygons), settings.unidadeArea) + '). ';
                    statusMapeamentoTxt =
                        qtdePoligonos(currentTerritorio) +
                        pluralize(
                            qtdePoligonos(currentTerritorio),
                            ' área salva',
                            ' áreas salvas'
                        ) +
                        areaTxt;
                }
                if (settings.editing && settings.editing.coordinates.length) {
                    //statusMapeamentoTxt += "Marcando nova área: " + settings.editing.coordinates.length + pluralize(settings.editing.coordinates.length, " vértice."," vértices.");
                    statusMapeamentoTxt += 'Nova área em edição';
                    if (settings.editing.coordinates.length >= 3) {
                        const area = calculaAreaPoligono(
                            settings.editing.coordinates,
                            settings.unidadeArea,
                            true
                        );
                        statusMapeamentoTxt += ' (' + area + ')';
                    }
                }
            }
            break;
        case TELAS.MAPEAR_USOSECONFLITOS:
            appBarTitle = 'Locais';

            if (settings.editingUsoOuConflito) {
                const tipo = getUsoOuConflitoLabel(settings.editingUsoOuConflito.marker.object);
                const nome = settings.editingUsoOuConflito.marker.object.nome;
                statusMapeamentoTxt = `${tipo}: ${nome}`;
                break;
            }

            if (!markers.length) {
                statusMapeamentoTxt =
                    'Mova o mapa para escolher onde colocar o local, e então clique no botão "Novo Local de Uso ou Conflito"';
                break;
            }

            const conflitosLength = markers.filter(
                (marker) => marker.object.__typename === 'ConflitoType'
            ).length;
            const usosLength = markers.filter(
                (marker) => marker.object.__typename === 'AreaDeUsoType'
            ).length;
            statusMapeamentoTxt = `${usosLength} ${pluralize(
                usosLength,
                'local de uso',
                'locais de uso'
            )} e ${conflitosLength} ${pluralize(conflitosLength, 'conflito', 'conflitos')}`;
            break;
    }

    const alertAppSemPermissoesGPS = (isFalha = false) => {
        const title = isFalha
            ? 'Não foi possível encontrar a sua posição'
            : 'Aplicativo sem permissões';
        const body = isFalha
            ? 'Pode ser por falta de cobertura GPS aqui, ou alguma configuração do seu aparelho.'
            : 'O Tô no Mapa não pode acessar a localização do seu celular! Para poder usar o GPS, vá para as configurações do celular, e conceda permissão de localização para a equipe do Tô no Mapa.';
        Alert.alert(
            title,
            body,
            [
                {
                    text: 'Ok',
                    onPress: async () => {},
                },
            ],
            { cancelable: true }
        );
    };

    const handleLocationPermissions = async () => {
        if (settings.useGPS) {
            updateSettings({
                settings: { useGPS: false },
                noSnackBarMessage: false,
                snackBarMessage: 'GPS desativado',
            });
            return;
        }
        try {
            setCarregando(true);
            let ret = await Location.getForegroundPermissionsAsync();
            if (ret.status !== 'granted') {
                ret = await Location.requestForegroundPermissionsAsync();
            }
            setCarregando(false);
            if (ret.status !== 'granted') {
                alertAppSemPermissoesGPS();
                return;
            }
            if (!settings.useGPS) {
                updateSettings({
                    settings: { useGPS: true },
                    noSnackBarMessage: false,
                    snackBarMessage: 'GPS ativado',
                });
            }
        } catch (e) {
            setCarregando(false);
            console.log('Erro no retorno do locationAtivarAsync no locationAtivar!', e);
            log('Erro no retorno do locationAtivarAsync no locationAtivar');
            logError(e);
            alertAppSemPermissoesGPS();
        }
    };

    return (
        <>
            <View style={{ position: 'absolute', top: 0, width: '100%' }}>
                <ToNoMapaAppbar
                    navigation={navigation}
                    title={appBarTitle}
                    rightActions={[
                        {
                            icon: 'arrow-expand',
                            color: settings.useGPS
                                ? colors.divider
                                : colors.Foundation.CorDeTexto.CorDeTexto2,
                            onPress: settings.useGPS ? null : mapFitToElements,
                        },
                        {
                            icon: 'crosshairs-gps',
                            color: settings.useGPS
                                ? colors.Foundation.CorDeTexto.CorDeTexto
                                : colors.Foundation.CorDeTexto.CorDeTexto2,
                            onPress: handleLocationPermissions,
                        },
                    ]}
                />
                {statusMapeamentoTxt != '' && (
                    <View style={Layout.statusBar}>
                        <Text style={Layout.statusBarText}>{statusMapeamentoTxt}</Text>
                    </View>
                )}
            </View>
            {carregando && <ToNoMapaLoading isOverlay={true} />}
        </>
    );
}
