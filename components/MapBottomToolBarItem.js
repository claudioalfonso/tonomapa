import React from 'react';
import { Appbar, Button } from 'react-native-paper';

import colors from '../assets/colors';
import metrics from '../utils/metrics';

export default function MapBottomToolBarItem({
    onPress,
    disabled = false,
    icon = null,
    label = null,
}) {
    if (label) {
        return (
            <Button
                icon={icon}
                color={colors.Foundation.CorDeTexto.CorDeTexto}
                style={{
                    backgroundColor: colors.Foundation.Brown.B300,
                    padding: metrics.tenHeight * 1,
                }}
                onPress={onPress}>
                {label}
            </Button>
        );
    }
    return (
        <Appbar.Action
            icon={icon}
            color={colors.Foundation.CorDeTexto.CorDeTexto}
            disabled={disabled}
            onPress={onPress}
        />
    );
}
