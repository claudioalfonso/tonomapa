import React, { useRef, forwardRef, useState, useCallback, useReducer } from 'react';
import { Image, View, Text, Alert } from 'react-native';
import { Appbar, TouchableRipple } from 'react-native-paper';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import Constants from 'expo-constants';
import MapView, { PROVIDER_GOOGLE, MAP_TYPES, Polygon, Marker, Callout } from 'react-native-maps';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useQuery } from '@apollo/client';
import { debounce } from 'lodash';

import { useToNoMapa } from '../context/ToNoMapaProvider';
import { Layout, LayoutMapa } from '../styles';
import TELAS from '../bases/telas';
import { calculaAreaPoligono, formataArea, UNIDADES_AREA } from '../maps/mapsUtils';
import metrics from '../utils/metrics';
import colors from '../assets/colors';
import { getUsoOuConflitoLabel } from '../utils/utils';
import { GET_USER_DATA } from '../db/queries';

import Balloon from '../components/third_party/Balloon';

import MapBottomToolBarItem from './MapBottomToolBarItem';
import ToNoMapaLoading from './ToNoMapaLoading';
import { ButtonCancel, ButtonContainedSecondary } from './buttons';

const initialMapEditingPolygon = {
    coordinates: null,
    activeEdge: null,
    updateActiveEdgeCoords: null,
};

const Map = forwardRef(function Map(
    /**
     * @type object
     */
    {
        tela,
        setTela,
        persistePoligonos,
        markers,
        polygons,
        region,
        markersTipoUsoConflito,
        mapTypeStyles,
        settings,
        updateSettings,
        isCurrentTerritorioEditableVar,
        isPTTModeVar,
    },
    ref
) {
    const { windowCenter } = useToNoMapa();

    const navigation = useNavigation();

    // @ts-ignore
    const map = ref?.current;

    const [carregando, setCarregando] = useState(false);

    const intervalId = useRef(null);

    const activeEdgeCoords = useRef(null);

    const reducer = (state, action) => {
        if (action.updateActiveEdgeCoords) {
            const newCoords = [...state.coordinates];
            newCoords[state.activeEdge] = action.updateActiveEdgeCoords;
            activeEdgeCoords.current = action.updateActiveEdgeCoords;
            return { coordinates: newCoords, activeEdge: state.activeEdge };
        }
        return { ...state, ...action };
    };

    /**
     * @type object
     */
    const [mapEditingPolygon, setMapEditingPolygon] = useReducer(reducer, initialMapEditingPolygon);

    const { data: currentUserData } = useQuery(GET_USER_DATA);
    const [currentTerritorio, setCurrentTerritorio] = useState(null);

    const localUpdateSettings = async (newSettings) => {
        updateSettings({ settings: newSettings, noSnackBarMessage: true });
    };

    /**
     * @type {Array}
     */
    const mapEditingPolygonEdgesRefs = [];

    // const [editingMarker, setEditingMarker] = useState(null);

    /**
     * @type object
     */
    const calloutVisible = useRef(false);

    const frustratedMoveCount = useRef(0);

    const zoomRef = useRef(0);

    const [hintBalloon, setHintBalloon] = useState(false);

    const useDebounce = (callback, delay) => {
        const debouncedFn = useCallback(
            debounce((...args) => callback(...args), delay),
            [delay]
        );
        return debouncedFn;
    };
    const onPanDragEnd = useDebounce(async (mapRef) => {
        const { zoom } = await mapRef.getCamera();
        if (zoom !== zoomRef.current) {
            frustratedMoveCount.current = 0;
            zoomRef.current = zoom;
            return;
        }

        frustratedMoveCount.current++;
        if (
            (frustratedMoveCount.current === 3 && settings.showGPSHelpBalloon) ||
            (frustratedMoveCount.current === 3 && !settings.showGPSHelpBalloon)
        ) {
            localUpdateSettings({ showGPSHelpBalloon: false });
            setHintBalloon(true);
            frustratedMoveCount.current = 0;
            return;
        }
    }, 500);
    const handlePanDrag = () => {
        if (tela !== TELAS.MAPA && settings.useGPS) {
            // localUpdateSettings({ useGPS: false });
            onPanDragEnd(map);
        }
    };

    const handleMapViewPress = () => {
        if (settings.editing) {
            setMapEditingPolygon({ activeEdge: null });
            return;
        }
    };

    const handleMarkerPress = (marker) => {
        if (tela !== TELAS.MAPEAR_USOSECONFLITOS) {
            return;
        }
        map.setCamera({ center: marker.coordinate });
        localUpdateSettings({ editingUsoOuConflito: { marker } });
    };

    const handleLocationMarkerPress = () => {
        if (tela === TELAS.MAPEAR_TERRITORIO) {
            adicionarVerticePoligono();
            return;
        }
        adicionarPonto();
    };

    const handlePolygonPress = (polygon, index) => {
        if (!isCurrentTerritorioEditableVar || isPTTModeVar) {
            return;
        }

        calloutVisible.current = true;
        const alertApagarArea = () => {
            Alert.alert(
                'Apagar área',
                'Tem certeza que quer apagar esta área de ' +
                    formataArea(polygon.area, settings.unidadeArea) +
                    '?\n\nEsta operação não tem volta!\n\nQuer apagar?',
                [
                    {
                        text: 'Não',
                    },
                    {
                        text: 'Sim',
                        onPress: () => {
                            polygons.splice(index, 1);
                            persistePoligonos({
                                polygons: polygons,
                                newSnackBarMessage: 'Área da comunidade apagada com sucesso!',
                            });
                        },
                    },
                ],
                { cancelable: true }
            );
        };
        if (settings.editaPoligonos) {
            const buttons = [
                {
                    text: 'Cancelar',
                },
                {
                    text: 'Apagar',
                    onPress: async () => {
                        alertApagarArea();
                    },
                },
                {
                    text: 'Editar',
                    onPress: async () => {
                        const polygon = polygons.splice(index, 1);
                        persistePoligonos({ polygons });
                        localUpdateSettings({
                            editing: {
                                id: polygon[0].id,
                                coordinates: polygon[0].coordinates,
                                activeEdge: polygon[0].coordinates.length - 1,
                                holes: polygon[0].holes,
                                area: polygon[0].area,
                                originalPolygon: polygon[0],
                            },
                        });
                        if (tela !== TELAS.MAPEAR_TERRITORIO) {
                            setTela(TELAS.MAPEAR_TERRITORIO);
                        }
                    },
                },
            ];
            // if (tela === TELAS.MAPEAR_TERRITORIO) {
            //     buttons.push({
            //         text: 'Adicionar vértice de nova área',
            //         onPress: async () => {
            //             adicionarVerticePoligono();
            //         },
            //     });
            // }
            Alert.alert(
                'Editar ou apagar área',
                'Você pode voltar a editar esta área (' +
                    formataArea(polygon.area, settings.unidadeArea) +
                    ') ou apagá-la.\n\nO que deseja fazer?',
                buttons,
                { cancelable: true }
            );
            return;
        }

        alertApagarArea();
    };

    const handleRegionChangeComplete = (newRegion, isGesture) => {
        if (
            !isGesture ||
            tela !== TELAS.MAPEAR_TERRITORIO ||
            mapEditingPolygon.coordinates === null ||
            mapEditingPolygon.activeEdge === null
        ) {
            return;
        }

        const newCoords = [...settings.editing.coordinates];
        newCoords[mapEditingPolygon.activeEdge] = {
            latitude: newRegion.latitude,
            longitude: newRegion.longitude,
        };
        localUpdateSettings({
            editing: {
                ...settings.editing,
                activeEdge: mapEditingPolygon.activeEdge,
                coordinates: newCoords,
            },
        });
    };

    const handleCloseBalloon = () => {
        setHintBalloon(false);
    };

    const handleDisableGPS = () => {
        updateSettings({
            settings: { useGPS: false },
            noSnackBarMessage: false,
            snackBarMessage: 'GPS desativado',
        });
        handleCloseBalloon();
    };

    /* Marca ponto de uso ou conflito */
    const adicionarPonto = async () => {
        const adjustedWindowCenter = {
            x: windowCenter.x,
            y: windowCenter.y + metrics.tenHeight * 1.1,
        };
        const coords = await map.coordinateForPoint(adjustedWindowCenter);
        const usoOuConflito = {
            posicao: {
                type: 'Point',
                coordinates: [coords.longitude, coords.latitude],
            },
        };
        // @ts-ignore
        navigation.navigate('EditarUsoConflito', { usoOuConflito });
    };

    const editarPonto = async (marker = null) => {
        if (marker) {
            // @ts-ignore
            navigation.navigate('EditarUsoConflito', { usoOuConflito: marker });
            return;
        }

        const adjustedWindowCenter = {
            x: windowCenter.x,
            y: windowCenter.y + metrics.tenHeight * 1.1,
        };
        const coords = await map.coordinateForPoint(adjustedWindowCenter);
        const usoOuConflito = {
            ...settings.editingUsoOuConflito.marker.object,
            posicao: {
                type: 'Point',
                coordinates: [coords.longitude, coords.latitude],
            },
        };
        localUpdateSettings({ editingUsoOuConflito: null });
        // @ts-ignore
        navigation.navigate('EditarUsoConflito', { usoOuConflito });
    };

    // Adiciona vértice ao polígono:
    const adicionarVerticePoligono = async () => {
        let newEditing;
        let newCoords = [];
        if (!settings.editing) {
            const adjustedWindowCenter = {
                x: windowCenter.x,
                y: windowCenter.y + metrics.tenHeight * 1.1,
            };
            const coords = await map.coordinateForPoint(adjustedWindowCenter);
            newCoords = [coords, coords];
            newEditing = {
                id: polygons.length + 1,
                coordinates: newCoords,
                activeEdge: 1,
                holes: [],
                area: 0,
            };
        } else {
            for (let i = 0; i < mapEditingPolygon.coordinates.length; i++) {
                newCoords.push(mapEditingPolygon.coordinates[i]);
                if (i === mapEditingPolygon.activeEdge) {
                    const newCoord = {
                        latitude: mapEditingPolygon.coordinates[i].latitude,
                        longitude: mapEditingPolygon.coordinates[i].longitude,
                    };
                    newCoords.push(newCoord);
                }
            }
            const newArea = calculaAreaPoligono(newCoords, settings.unidadeArea);
            newEditing = {
                ...settings.editing,
                coordinates: newCoords,
                activeEdge: mapEditingPolygon.activeEdge + 1,
                area: newArea,
            };
        }
        localUpdateSettings({ editing: newEditing });
    };

    const removeVerticePoligono = () => {
        if (settings.editing.coordinates.length === 1) {
            localUpdateSettings({ editing: null });
            return;
        }
        const newCoords = [...settings.editing.coordinates];
        newCoords.splice(mapEditingPolygon.activeEdge, 1);
        localUpdateSettings({
            editing: {
                ...settings.editing,
                coordinates: newCoords,
                activeEdge: mapEditingPolygon.activeEdge - 1,
            },
        });
    };

    const updateAsync = async () => {
        if (!map) {
            return;
        }
        const { zoom, center } = await map.getCamera();
        let delta = 0.0001;
        if (zoom > 14) {
            delta = 0.00003;
        } else if (zoom > 10) {
            delta = 0.00005;
        } else if (zoom < 6) {
            delta = 0.0002;
        }
        if (
            activeEdgeCoords.current &&
            delta > 0 &&
            Math.abs(center.latitude - activeEdgeCoords.current.latitude) < delta &&
            Math.abs(center.longitude - activeEdgeCoords.current.longitude) < delta
        ) {
            return;
        }

        setMapEditingPolygon({ updateActiveEdgeCoords: center });
    };

    useFocusEffect(
        useCallback(() => {
            if (currentUserData) {
                setCurrentTerritorio(currentUserData.currentUser.currentTerritorio);
            }
        }, [currentUserData])
    );

    useFocusEffect(
        useCallback(() => {
            if (mapEditingPolygon.activeEdge !== null) {
                map.setCamera({
                    center: mapEditingPolygon.coordinates[mapEditingPolygon.activeEdge],
                });
            }
        }, [mapEditingPolygon.activeEdge])
    );

    useFocusEffect(
        useCallback(() => {
            if (!settings.editing || tela !== TELAS.MAPEAR_TERRITORIO) {
                // if settings.editing, then tela changed, so we must only set activeEdge to null, since we are out of TELAS.MAPEAR_TERRITORIO.
                const newMapEditingPolygon = settings.editing
                    ? { coordinates: settings.editing.coordinates, activeEdge: null }
                    : initialMapEditingPolygon;

                setMapEditingPolygon(newMapEditingPolygon);

                if (intervalId.current) {
                    clearInterval(intervalId.current);
                    intervalId.current = null;
                }
                return;
            }

            setMapEditingPolygon({
                coordinates: settings.editing.coordinates,
                activeEdge: settings.editing.activeEdge || 0,
            });

            if (!intervalId.current) {
                intervalId.current = setInterval(() => {
                    updateAsync();
                }, 50);
            }
        }, [settings.editing, tela])
    );

    return (
        <>
            <MapView
                ref={ref}
                provider={Constants.manifest.extra.xyzRTiles.using ? null : PROVIDER_GOOGLE}
                style={LayoutMapa.map}
                mapType={
                    Constants.manifest.extra.xyzRTiles.using ? MAP_TYPES.NONE : settings.mapType
                }
                initialRegion={region}
                zoomControlEnabled={false}
                showsUserLocation={true}
                userLocationAnnotationTitle="Meu local"
                showsMyLocationButton={false}
                showsBuildings={false}
                showsIndoors={false}
                showsPointsOfInterest={false}
                showsTraffic={false}
                moveOnMarkerPress={false}
                scrollEnabled={tela === TELAS.MAPA || !settings.useGPS}
                pitchEnabled={tela === TELAS.MAPA || !settings.useGPS}
                rotateEnabled={tela === TELAS.MAPA || !settings.useGPS}
                onUserLocationChange={(e) => {
                    if (tela !== TELAS.MAPA && settings.useGPS) {
                        const newCoords = {
                            latitude: e.nativeEvent.coordinate.latitude,
                            longitude: e.nativeEvent.coordinate.longitude,
                        };
                        map.animateCamera({ center: newCoords });
                    }
                }}
                onPanDrag={handlePanDrag}
                onPress={handleMapViewPress}
                onRegionChangeComplete={handleRegionChangeComplete}>
                {polygons.map((polygon, index) => (
                    <Polygon
                        key={`polygon_${index}`}
                        coordinates={polygon.coordinates}
                        strokeColor={colors.warning}
                        fillColor={mapTypeStyles.polygonFillColor}
                        strokeWidth={3}
                        tappable={tela !== TELAS.MAPEAR_USOSECONFLITOS}
                        zIndex={2}
                        onPress={() => handlePolygonPress(polygon, index)}
                    />
                ))}
                {markers.map((marker, index) => (
                    <React.Fragment key={`marker_${marker.id}`}>
                        {(!settings.editingUsoOuConflito ||
                            settings.editingUsoOuConflito.marker.id !== marker.id) && (
                            <Marker
                                coordinate={marker.coordinate}
                                onPress={() => handleMarkerPress(marker)}>
                                <Image
                                    source={marker.icone}
                                    style={Layout.icon}
                                    resizeMode="contain"
                                />
                                <Callout
                                    onPress={() => {
                                        if (isCurrentTerritorioEditableVar && !isPTTModeVar) {
                                            editarPonto(marker.object);
                                        }
                                    }}>
                                    {tela !== TELAS.MAPEAR_USOSECONFLITOS && (
                                        <View
                                            style={[Layout.col, { width: metrics.tenWidth * 16 }]}>
                                            {marker.title != null && marker.title.trim() != '' && (
                                                <Text style={Layout.calloutTitle}>
                                                    {marker.title}
                                                </Text>
                                            )}
                                            <Text style={Layout.calloutDescription}>
                                                <>
                                                    {marker.object.__typename === 'ConflitoType' ? (
                                                        <Text>
                                                            Conflito:{' '}
                                                            {markersTipoUsoConflito[index]}
                                                        </Text>
                                                    ) : (
                                                        <Text>
                                                            Uso: {markersTipoUsoConflito[index]}
                                                        </Text>
                                                    )}
                                                </>
                                            </Text>
                                            {'area' in marker.object &&
                                                parseFloat(marker.object.area) > 0 && (
                                                    <Text style={Layout.calloutDescription}>
                                                        Área:{' '}
                                                        {formataArea(
                                                            marker.object.area,
                                                            UNIDADES_AREA.METRO_QUADRADO
                                                        )}
                                                    </Text>
                                                )}
                                            {marker.object.descricao != null &&
                                                marker.object.descricao.trim() != '' && (
                                                    <Text style={Layout.calloutDescriptionSub}>
                                                        {marker.object.descricao}
                                                    </Text>
                                                )}

                                            {isCurrentTerritorioEditableVar && !isPTTModeVar && (
                                                <Text style={Layout.calloutAction}>
                                                    Editar ou apagar
                                                </Text>
                                            )}
                                        </View>
                                    )}
                                </Callout>
                            </Marker>
                        )}
                    </React.Fragment>
                ))}
                {settings.editing && mapEditingPolygon.coordinates && (
                    <>
                        <Polygon
                            key="mapEditingPolygon"
                            coordinates={mapEditingPolygon.coordinates}
                            holes={settings.editing.holes}
                            strokeColor={mapTypeStyles.polygonEditingStrokeColor}
                            fillColor="rgba(255,0,0,0.5)"
                            strokeWidth={3}
                            tappable={tela === TELAS.MAPA}
                            onPress={() => {
                                if (tela !== TELAS.MAPEAR_TERRITORIO) {
                                    setTela(TELAS.MAPEAR_TERRITORIO);
                                }
                            }}
                        />
                        {!settings.editaPoligonos &&
                            tela === TELAS.MAPEAR_TERRITORIO &&
                            settings.editing.coordinates.map((coord, index) => (
                                <Marker
                                    key={index}
                                    coordinate={coord}
                                    centerOffset={{ x: 0.5, y: 0.5 }}
                                    anchor={{ x: 0.5, y: 0.5 }}>
                                    {index === mapEditingPolygon.activeEdge && (
                                        <MaterialCommunityIcons
                                            name="circle"
                                            size={metrics.tenWidth * 2}
                                            style={{
                                                paddingTop: 0,
                                                paddingLeft: 0,
                                                color: colors.Foundation.Green.G300,
                                            }}
                                        />
                                    )}
                                </Marker>
                            ))}
                        {settings.editaPoligonos &&
                            tela === TELAS.MAPEAR_TERRITORIO &&
                            mapEditingPolygon.coordinates.map((coord, index) => (
                                <Marker
                                    key={index}
                                    ref={(ref) => (mapEditingPolygonEdgesRefs[index] = ref)}
                                    coordinate={coord}
                                    onPress={() => {
                                        setMapEditingPolygon({ activeEdge: index });
                                    }}
                                    centerOffset={{ x: 0.5, y: 0.5 }}
                                    anchor={{ x: 0.5, y: 0.5 }}>
                                    <View
                                        style={[
                                            Layout.vertice,
                                            {
                                                backgroundColor: mapTypeStyles.backgroundMarkerView,
                                            },
                                        ]}>
                                        {index === mapEditingPolygon.activeEdge && (
                                            <MaterialCommunityIcons
                                                name="circle-double"
                                                size={metrics.tenWidth * 2}
                                                style={{
                                                    paddingTop: 0,
                                                    paddingLeft: 0,
                                                    color: colors.Foundation.Green.G300,
                                                }}
                                            />
                                        )}
                                    </View>
                                </Marker>
                            ))}
                    </>
                )}
            </MapView>
            {tela === TELAS.MAPEAR_TERRITORIO && (
                <>
                    <View
                        style={{
                            position: 'absolute',
                            bottom: metrics.tenHeight * 8,
                            left: 0,
                            right: 0,
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            zIndex: 20,
                        }}>
                        {settings.editing && mapEditingPolygon.coordinates ? (
                            <Appbar
                                style={{
                                    backgroundColor: colors.Foundation.Brown.B300,
                                    borderRadius: metrics.tenHeight * 0.6,
                                }}>
                                <MapBottomToolBarItem
                                    onPress={() => {
                                        const currentActiveEdge = mapEditingPolygon.activeEdge
                                            ? mapEditingPolygon.activeEdge
                                            : 0;
                                        const newActiveEdge =
                                            currentActiveEdge >= 1
                                                ? currentActiveEdge - 1
                                                : mapEditingPolygon.coordinates.length - 1;
                                        setMapEditingPolygon({ activeEdge: newActiveEdge });
                                    }}
                                    icon="chevron-left"
                                />
                                <MapBottomToolBarItem
                                    disabled={mapEditingPolygon.activeEdge === null}
                                    onPress={() => {
                                        adicionarVerticePoligono();
                                    }}
                                    icon="plus"
                                />
                                <MapBottomToolBarItem
                                    disabled={mapEditingPolygon.activeEdge === null}
                                    onPress={() => {
                                        removeVerticePoligono();
                                    }}
                                    icon="minus"
                                />
                                <MapBottomToolBarItem
                                    onPress={() => {
                                        const currentActiveEdge = mapEditingPolygon.activeEdge
                                            ? mapEditingPolygon.activeEdge
                                            : 0;
                                        const newActiveEdge =
                                            currentActiveEdge <
                                            mapEditingPolygon.coordinates.length - 1
                                                ? currentActiveEdge + 1
                                                : 0;
                                        setMapEditingPolygon({ activeEdge: newActiveEdge });
                                    }}
                                    icon="chevron-right"
                                />
                            </Appbar>
                        ) : (
                            <MapBottomToolBarItem
                                onPress={() => {
                                    adicionarVerticePoligono();
                                }}
                                icon="plus"
                                label="Nova Área"
                            />
                        )}
                    </View>
                    {!settings.editing && (
                        <>
                            {settings.useGPS ? (
                                <TouchableRipple
                                    style={{
                                        position: 'absolute',
                                        left: windowCenter.x - metrics.tenWidth * 1.5,
                                        top: windowCenter.y - metrics.tenHeight * 2,
                                    }}
                                    onPress={handleLocationMarkerPress}>
                                    <Image
                                        source={mapTypeStyles.markerIconToShow}
                                        style={Layout.icon}
                                        resizeMode="contain"
                                    />
                                </TouchableRipple>
                            ) : (
                                <View
                                    style={{
                                        position: 'absolute',
                                        left: windowCenter.x - metrics.tenWidth * 1,
                                        top: windowCenter.y - metrics.tenHeight * 1,
                                    }}>
                                    <MaterialCommunityIcons
                                        color={mapTypeStyles.iconColor}
                                        name="circle-double"
                                        size={metrics.tenHeight * 2}
                                    />
                                </View>
                            )}
                        </>
                    )}
                </>
            )}
            {tela === TELAS.MAPEAR_USOSECONFLITOS && (
                <>
                    <View
                        style={{
                            position: 'absolute',
                            bottom: metrics.tenHeight * 8,
                            left: 0,
                            right: 0,
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'center',
                            zIndex: 20,
                        }}>
                        <MapBottomToolBarItem
                            onPress={() => {
                                if (settings.editingUsoOuConflito) {
                                    editarPonto();
                                    return;
                                }
                                adicionarPonto();
                            }}
                            icon={settings.editingUsoOuConflito?.marker ? 'pencil' : 'plus'}
                            label={
                                settings.editingUsoOuConflito?.marker
                                    ? `Editar ou apagar ${getUsoOuConflitoLabel(
                                          settings.editingUsoOuConflito.marker.object
                                      )}`
                                    : 'Novo Local'
                            }
                        />
                    </View>
                    {settings.useGPS ? (
                        <View
                            style={{
                                position: 'absolute',
                                left: windowCenter.x - metrics.tenWidth * 2,
                                top: windowCenter.y - metrics.tenHeight * 2,
                            }}>
                            <Image
                                source={mapTypeStyles.markerIconToShow}
                                style={Layout.icon}
                                resizeMode="contain"
                            />
                        </View>
                    ) : (
                        <>
                            {settings.editingUsoOuConflito ? (
                                <View
                                    style={{
                                        position: 'absolute',
                                        left: windowCenter.x - metrics.tenWidth * 2,
                                        top: windowCenter.y - metrics.tenHeight * 2.8,
                                    }}>
                                    <Image
                                        source={settings.editingUsoOuConflito.marker.icone}
                                        style={Layout.icon}
                                        resizeMode="contain"
                                    />
                                </View>
                            ) : (
                                <View
                                    style={{
                                        position: 'absolute',
                                        left: windowCenter.x - metrics.tenWidth * 2,
                                        top: windowCenter.y - metrics.tenHeight * 2,
                                    }}>
                                    <MaterialCommunityIcons
                                        color={mapTypeStyles.iconColor}
                                        name="adjust"
                                        size={metrics.tenHeight * 4}
                                    />
                                </View>
                            )}
                        </>
                    )}
                </>
            )}
            {tela !== TELAS.MAPA && hintBalloon && (
                <>
                    <TouchableRipple
                        style={Layout.appOverlayZIndex100}
                        onPress={handleCloseBalloon}>
                        <View></View>
                    </TouchableRipple>
                    <View style={Layout.balloonHintWrapper}>
                        <Balloon
                            borderColor={colors.Foundation.Basic.Gray}
                            backgroundColor={colors.Foundation.CorDeTexto.CorDeTexto}
                            borderWidth={metrics.tenWidth * 0.2}
                            borderRadius={metrics.tenWidth * 1.2}
                            width="100%"
                            triangleSize={metrics.tenWidth * 1.5}
                            triangleOffset={'94%'}
                            triangleDirection="top"
                            containerStyle={{
                                marginHorizontal: 0,
                                padding: metrics.tenWidth * 0.8,
                            }}>
                            <Text style={Layout.balloonHintTitle}>Modo GPS está ativado!</Text>
                            <Text style={Layout.balloonHintText}>
                                Quando o GPS está ativado, o mapa muda quando você se move.
                            </Text>
                            <Text style={Layout.balloonHintText}>
                                Se você quer mover o mapa com a mão, desative o modo GPS no botão
                                acima!
                            </Text>
                            <View style={Layout.buttonsBottomDialogWrapper}>
                                <ButtonCancel
                                    compact={true}
                                    mode="text"
                                    onPress={handleCloseBalloon}>
                                    Entendi
                                </ButtonCancel>
                                <ButtonContainedSecondary
                                    compact={true}
                                    onPress={handleDisableGPS}
                                    icon="crosshairs-gps">
                                    Desligar GPS
                                </ButtonContainedSecondary>
                            </View>
                        </Balloon>
                    </View>
                </>
            )}
            {carregando && <ToNoMapaLoading isOverlay={true} />}
        </>
    );
});

export default React.memo(Map);
