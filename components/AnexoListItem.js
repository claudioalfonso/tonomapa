import React from 'react';
import { View, Image } from 'react-native';
import { Card, Chip, IconButton, Text, TouchableRipple } from 'react-native-paper';
import { FontAwesome } from '@expo/vector-icons';

import { formataTamanhoArquivo, iconeDoMimeType, isImage, isVideo } from '../db/arquivos';
import { Anexos } from '../styles';
import colors from '../assets/colors';
import { TIPOS_ANEXO, isAnexoPTT } from '../bases/anexo';
import metrics from '../utils/metrics';

export default function AnexoListItem({
    anexo,
    isPTTMode,
    onVisualizar,
    onEditar,
    onApagar,
    currentTerritorioEditable,
}) {
    if (isPTTMode !== isAnexoPTT(anexo.tipoAnexo)) {
        return <></>;
    }

    let localThumbUri = null;
    let icone = null;
    const ehVideo = isVideo(anexo.mimetype);
    if ((anexo.localFileUri || anexo.localThumbUri) && (isImage(anexo.mimetype) || ehVideo)) {
        localThumbUri = anexo.localThumbUri || anexo.localFileUri;
    } else {
        icone = iconeDoMimeType(anexo.mimetype, 'awesome');
    }

    return (
        <Card style={{ marginTop: metrics.tenHeight * 2, padding: metrics.tenHeight * 1.6 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <TouchableRipple
                    style={{ marginRight: metrics.tenHeight * 1.6 }}
                    onPress={() => {
                        onVisualizar(anexo);
                    }}>
                    <>
                        {localThumbUri ? (
                            <>
                                <Image
                                    style={{
                                        width: metrics.tenWidth * 10,
                                        height: metrics.tenHeight * 10,
                                    }}
                                    source={{ uri: localThumbUri }}
                                />
                                {ehVideo && (
                                    <FontAwesome
                                        name="play-circle-o"
                                        color={colors.Foundation.TextColor.White}
                                        size={40}
                                        style={Anexos.playIcon}
                                    />
                                )}
                            </>
                        ) : (
                            <View style={Anexos.itemIconWrapper}>
                                <FontAwesome
                                    // @ts-ignore
                                    name={icone}
                                    color={colors.Foundation.CorDeTexto.CorDeTexto2}
                                    size={72}
                                />
                            </View>
                        )}
                    </>
                </TouchableRipple>
                <View
                    style={{
                        flex: 1,
                        flexGrow: 1,
                    }}>
                    <Text style={Anexos.itemTitle}>{anexo.nome}</Text>
                    {!!anexo.fileSize && (
                        <Text>Tamanho: {formataTamanhoArquivo(anexo.fileSize)}</Text>
                    )}
                    {!!anexo.descricao && anexo.descricao != '' && (
                        <Text style={Anexos.itemDescricao}>{anexo.descricao}</Text>
                    )}
                    {anexo.tipoAnexo === 'ata' && <Text style={Anexos.itemTitle}>Ata</Text>}
                    {anexo.tipoAnexo != '' && anexo.tipoAnexo !== 'ata' && (
                        <Text style={Anexos.itemTitle}>{TIPOS_ANEXO[anexo.tipoAnexo]}</Text>
                    )}
                    <Chip style={{ marginTop: metrics.tenHeight * 1.6 }}>
                        {anexo.publico ? 'Público' : 'Não público'}
                    </Chip>
                </View>
                {currentTerritorioEditable && (
                    <View style={{ flex: 1, flexGrow: 0.2, flexDirection: 'column' }}>
                        {onApagar !== null && (
                            <IconButton
                                icon="trash-can-outline"
                                color={colors.Foundation.Brown.B300}
                                size={metrics.tenHeight * 2.4}
                                onPress={(evt) => onApagar(anexo.id)}></IconButton>
                        )}
                        {onEditar !== null && (
                            <IconButton
                                icon="pencil"
                                color={colors.Foundation.Brown.B300}
                                size={metrics.tenHeight * 2.4}
                                onPress={(evt) => onEditar(anexo)}></IconButton>
                        )}
                    </View>
                )}
            </View>
        </Card>
    );
}
