import { RetryLink } from '@apollo/client/link/retry';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createUploadLink } from 'apollo-upload-client';
import QueueLink from 'apollo-link-queue';
import SerializingLink from 'apollo-link-serialize';
import Constants from 'expo-constants';
import uuid from 'react-native-uuid';
import { onError } from '@apollo/client/link/error';
import { ApolloLink } from '@apollo/client';
import { setContext } from '@apollo/client/link/context';
import NetInfo from '@react-native-community/netinfo';

import { ehDadoLocal } from './utils';
import { addTrackedMutation, resetTokenOnError } from '../db/api';

const DASHBOARD_ENDPOINT = Constants.manifest.extra.dashboardEndpoint;
const GRAPHQL_ENDPOINT = `${DASHBOARD_ENDPOINT}/graphql`;

const resetApiErrors = () => {
    apiErrors = {
        graphQLErrors: [],
        networkError: '',
    };
};
let apiErrors;
resetApiErrors();

const removeLocalCreate = async (localId) => {
    const keys = await AsyncStorage.getAllKeys();
    for (const key of keys) {
        if (key.includes('trackedMutations-')) {
            const trackedMutationJSON = await AsyncStorage.getItem(key);
            const { operationName, context } = JSON.parse(trackedMutationJSON);
            if (operationName.includes('Create') && context.localId === localId) {
                await AsyncStorage.removeItem(key);
                return true;
            }
        }
    }
    return false;
};

/**
 * Retorna header e credenciais com o token
 * @type {Object}
 */
const linkContext = setContext(async (headers) => {
    const token = await AsyncStorage.getItem('token');
    const { isConnected } = await NetInfo.fetch();
    const asyncStorageKeys = await AsyncStorage.getAllKeys();
    const trackedMutationsKeys = asyncStorageKeys.filter((key) =>
        key.includes('trackedMutations-')
    );

    const ret = {
        headers: {
            ...headers,
            authorization: `JWT ${token}`,
        },
        credentials: 'same-origin',
        isConnected,
        trackedMutationsKeys,
    };
    return ret;
});

/**
 * 01. logLink
 * Shows operationName and variables of all graphql requests (if debugGraphQL is true in settings)
 */
export const logLink = new ApolloLink((operation, forward) => {
    if (!forward) {
        return null;
    }
    if (Constants.manifest.extra.debugGraphQL) {
        console.log({ operationName: operation.operationName, variables: operation.variables });
    }
    return forward(operation).map((data) => {
        return data;
    });
});

/**
 * 02. middlewareLink
 * Adds authorization parameters to request headers
 */
export const middlewareLink = linkContext.concat(resetTokenOnError);

/**
 * 03. errorLink
 * Treats errors in graphql requests (now it only logs to console and sentry...)
 * TODO: show to user that something went wrong...
 */
export const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
        graphQLErrors.map((error) => {
            // const msg = `[Erro no GraphQL]: Message: ${error.message}, Location: ${error.locations}, Path: ${error.path}`;
            const msg = `[Erro no GraphQL]: ${error.message}`;
            apiErrors.graphQLErrors.push(msg);
            console.log(error);
        });

    if (networkError) {
        apiErrors.networkError = networkError;
        console.log(`[Falha de conexão]: ${networkError}`);
    }
});

/**
 * 04. trackMutationsLink
 * Adds a list of mutations to asyncStorage, so that closing App doesn't clear pending (queued) offline mutations!
 */
export const trackMutationsLink = new ApolloLink((operation, forward) => {
    if (!forward) {
        return null;
    }
    const { isConnected, serializationKey, shouldTrack, localId, trackedMutationsKeys } =
        operation.getContext();

    const operationName = operation.operationName;
    const query = operation.query;
    const variables = operation.variables;

    const ords = [0, ...trackedMutationsKeys.map((key) => key.split('-')[1])];
    const maxOrd = Math.max(...ords);
    const uuidV4 = uuid.v4();
    const operationId = localId
        ? `trackedMutations-${maxOrd + 1}-${operationName}-${localId}-${uuidV4}`
        : `trackedMutations-${maxOrd + 1}-${operationName}-${uuidV4}`;

    const isLocalDelete =
        operationName !== 'DeleteAllUsosEConflitos' &&
        operationName.includes('Delete') &&
        ehDadoLocal(variables);
    if (isLocalDelete) {
        // If it's deleting an item still not sent to server, remove that creation operation
        // from trackMutations list:
        removeLocalCreate(variables.id);
        // Terminate the link chain, since the creation operation was removed from queue
        return;
    }
    if (shouldTrack) {
        addTrackedMutation({
            operationId,
            operationName,
            query,
            variables,
            context: { serializationKey, shouldTrack: false, localId },
        });
        // If some other operation of same nature was done before, remove it from queue, since this new operation will be enough:
        if (localId) {
            const sameNatureOperations = trackedMutationsKeys.filter((key) => {
                const [type, ord, keyOperationName, keyLocalId, ...other] = key.split('-');
                return keyOperationName === operationName && keyLocalId === localId;
            });
            for (const key of sameNatureOperations) {
                AsyncStorage.removeItem(key);
            }
        }
        if (!isConnected) {
            // Offline: terminate the link chain, since the operation was added to track.
            return null;
        }
    }
    return forward(operation).map((data) => {
        if (shouldTrack && operationId) {
            AsyncStorage.removeItem(operationId);
        }
        return data;
    });
});

/**
 * 05. queueLink
 * Enqueues mutations when offline (works only in same app usage. If app
 * closes, trackMutationsLink saves the day)
 */
export const queueLink = new QueueLink();

/**
 * 06. serializingLink
 * Maintains the order of the mutations for each content type
 * (areasDeUso, conflitos, mensagens, anexos...)
 */
export const serializingLink = new SerializingLink();

/**
 * 07. retryLink
 * Tries 5 times to send a graphql request to server
 */
export const retryLink = new RetryLink({
    delay: {
        initial: 5000,
        max: Infinity,
    },
    attempts: {
        max: 5,
        retryIf: (error, _operation) => {
            return (
                !!error &&
                typeof error.statusCode === 'undefined' &&
                apiErrors.graphQLErrors.length === 0
            );
        },
    },
});

/**
 * 08. httpLink
 * Finally, the terminating link, which sends data to server
 * Note: When offline, trackMutationsLink will terminate after
 * persisting mutation in some cases.
 */
export const httpLink = createUploadLink({ uri: GRAPHQL_ENDPOINT });
