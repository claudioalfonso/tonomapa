import * as React from 'react';
import { log, logError } from './utils';

export const navigationRef = React.createRef();
// export const navigationRefIsReady = React.createRef();

export function navigate(name, params) {
    if (navigationRef.current) {
        try {
            navigationRef.current.navigate(name, params);
        } catch (e) {
            console.log('O navegador não estava pronto para lidar com esta request!', {
                name,
                params,
                e,
            });
            log(
                `navigate: O navegador não estava pronto para lidar com esta request: ${name} params=${JSON.stringify(
                    params
                )}`
            );
            logError(e);
        }
    } else {
        console.log('O navegador não estava pronto para lidar com esta request!', name);
        // TODO: add this action to a queue I can call later
    }
}
