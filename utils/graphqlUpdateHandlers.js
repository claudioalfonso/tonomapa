import {
    GET_ANEXOS,
    GET_AREAS_DE_USO,
    GET_CONFLITOS,
    GET_MENSAGENS,
    GET_USER_DATA,
} from '../db/queries';

const handleGenericUpdate = async ({ cache, data, query, key, updateKey, localId }) => {
    const {
        [updateKey]: { data: object },
    } = data;
    const { currentUser } = cache.readQuery({ query: GET_USER_DATA });
    const variables = { territorioId: currentUser.currentTerritorio.id };

    const cachedData = cache.readQuery({ query, variables });

    if (cachedData === null) {
        return;
    }
    const { [key]: allObjects } = cachedData;
    const id = localId ? localId.toString() : object.id.toString();
    let newObjects;
    const foundObj = allObjects.find((item) => {
        return item.id.toString() === id.toString();
    });
    if (!foundObj) {
        // CREATE:
        newObjects = [...allObjects, object];
    } else {
        // UPDATE:
        newObjects = allObjects.map((item) => {
            return item.id.toString() === id.toString() ? { ...item, ...object } : item;
        });
    }
    const newData = { [key]: newObjects };
    cache.writeQuery({ query, data: newData, variables });
};

const handleGenericDelete = async ({ cache, query, key, localId }) => {
    const { currentUser } = cache.readQuery({ query: GET_USER_DATA });
    const variables = { territorioId: currentUser.currentTerritorio.id };

    const cachedData = cache.readQuery({ query, variables });
    if (cachedData === null) {
        return;
    }
    const { [key]: allObjects } = cachedData;
    const newObjects = allObjects.filter((item) => {
        return item.id !== localId;
    });
    if (newObjects.length !== allObjects.length) {
        const newData = { [key]: newObjects };
        cache.writeQuery({
            query,
            data: newData,
            variables,
        });
    }
};

const handleDeleteAllUsosEConflitos = async (cache) => {
    const { currentUser } = cache.readQuery({ query: GET_USER_DATA });
    const variables = { territorioId: currentUser.currentTerritorio.id };
    cache.writeQuery({
        query: GET_AREAS_DE_USO,
        data: { areasDeUso: [] },
        variables,
    });
    cache.writeQuery({
        query: GET_CONFLITOS,
        data: { conflitos: [] },
        variables,
    });
};

export const updateHandlerByName = ({ operationName, cache, data, localId }) => {
    if (data === undefined) {
        return;
    }

    if (operationName === 'updateAppData') {
        return;
    }

    if (operationName === 'DeleteAllUsosEConflitos') {
        handleDeleteAllUsosEConflitos(cache);
        return;
    }

    let key;
    let query;
    let updateKey;
    switch (operationName) {
        case 'CreateOrUpdateAnexo':
        case 'DeleteAnexo':
            updateKey = 'updateAnexo';
            key = 'anexos';
            query = GET_ANEXOS;
            break;
        case 'CreateOrUpdateAreaDeUso':
        case 'DeleteAreaDeUso':
            updateKey = 'updateAreaDeUso';
            key = 'areasDeUso';
            query = GET_AREAS_DE_USO;
            break;
        case 'CreateOrUpdateConflito':
        case 'DeleteConflito':
            updateKey = 'updateConflito';
            key = 'conflitos';
            query = GET_CONFLITOS;
            break;
        case 'CreateOrUpdateMensagem':
        case 'DeleteMensagem':
            updateKey = 'updateMensagem';
            key = 'mensagens';
            query = GET_MENSAGENS;
            break;
    }

    if (operationName.includes('Delete')) {
        return handleGenericDelete({
            cache,
            query,
            key,
            localId,
        });
    }

    if (operationName.includes('CreateOrUpdate')) {
        return handleGenericUpdate({
            cache,
            data,
            query,
            key,
            updateKey,
            localId,
        });
    }
};
