import * as yup from 'yup';

export const PTT_TERRITORIO_VALIDADOR = yup.object().shape({
    email: yup
        .string()
        .email('email|E-mail tem que ser válido')
        .required('email|Seu email é obrigatório!')
        .nullable(),
    currentTerritorio: yup.object().shape({
        cep: yup.string().nullable(),
        descricaoAcesso: yup
            .string()
            .required('descricaoAcesso|A descrição de acesso é obrigatória!')
            .nullable(),
        zonaLocalizacao: yup
            .string()
            .required('zonaLocalizacao|Este campo é obrigatório!')
            .nullable(),
        outraZonaLocalizacao: yup
            .string()
            .when('zonaLocalizacao', {
                is: 'OUTRO',
                then: yup
                    .string()
                    .required(
                        'outraZonaLocalizacao|Ao escolher a opção "outro", você deve indicar qual!'
                    )
                    .nullable(),
            })
            .nullable(),
        autoIdentificacao: yup.string().nullable(),
        segmentoId: yup
            .number()
            .required('segmentoId|O segmento da comunidade é obrigatório!')
            .nullable(),
        historiaDescricao: yup.string().nullable(),
        ecNomeContato: yup.string().nullable(),
        ecLogradouro: yup.string().nullable(),
        ecNumero: yup.string().nullable(),
        ecComplemento: yup.string().nullable(),
        ecBairro: yup.string().nullable(),
        ecCep: yup.string().nullable(),
        ecCaixaPostal: yup.string().nullable(),
        ecMunicipioId: yup
            .number()
            .required('ecMunicipioId|O Município do contato é obrigatório!')
            .nullable(),
        ecEmail: yup
            .string()
            .email('ecEmail|O formato do e-mail está inválido! Favor corrigir.')
            .required('ecEmail|Este campo é obrigatório!')
            .nullable(),
        ecTelefone: yup
            .string()
            .matches(/\(\d{2}\) \d{5}-\d{4}/, 'ecTelefone|Telefone inválido! Favor corrigir.')
            .required('ecTelefone|O telefone do contato é obrigatório!')
            .nullable(),
    }),
});

export const ANEXO_VALIDADOR = yup.object().shape({
    nome: yup.string().required('nome|O nome do arquivo/imagem é obrigatório').nullable(),
    pttTipo: null,
    descricao: yup
        .string()
        .when('pttTipo', {
            is: true,
            then: yup.string().required('descricao|A descrição é obrigatória!').nullable(),
        })
        .nullable(),
    tipoAnexo: yup
        .string()
        .when('pttTipo', {
            is: true,
            then: yup
                .string()
                .oneOf(
                    ['FONTE_INFORMACAO', 'ICONE', 'ANEXO'],
                    'tipoAnexo|O tipo de anexo tem que ser uma das opções!'
                )
                .required('tipoAnexo|O tipo de anexo é obrigatório!')
                .nullable(),
        })
        .nullable(),
    mimetype: yup
        .string()
        .when('tipoAnexo', {
            is: 'FONTE_INFORMACAO',
            then: yup
                .string()
                .oneOf(
                    [
                        'application/pdf',
                        'application/msword',
                        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
                    ],
                    'tipoAnexo|A fonte de informação só pode ser enviada nos formatos .pdf, .doc ou .docx!'
                )
                .nullable(),
        })
        .when('tipoAnexo', {
            is: 'ICONE',
            then: yup
                .string()
                .oneOf(
                    [null, 'image/png', 'image/jpeg'],
                    'tipoAnexo|O Ícone tem que ser uma imagem em formato .png ou .jpg!'
                )
                .nullable(),
        })
        .when('tipoAnexo', {
            is: 'ANEXO',
            then: yup
                .string()
                .oneOf(
                    [null, 'image/png', 'image/jpeg', 'video/mp4', 'video/x-msvideo'],
                    'tipoAnexo|O tipo "Anexo" deve ser uma imagem ou vídeo em um dos seguintes formatos: .png, .jpeg, .avi ou .mp4'
                )
                .nullable(),
        })
        .nullable(),
});

export const DADOSPTT_SCREEN_VALIDADOR = yup.object().shape({
    email: yup.string().email('email|Email inválido').nullable(),
    currentTerritorio: yup.object().shape({
        cep: yup.string().nullable(),
        descricaoAcesso: yup.string().nullable(),
        zonaLocalizacao: yup.string().nullable(),
        outraZonaLocalizacao: yup
            .string()
            .when('zonaLocalizacao', {
                is: 'OUTRO',
                then: yup.string().nullable(),
            })
            .nullable(),
        autoIdentificacao: yup.string().nullable(),
        segmentoId: yup.object().nullable(),
        historiaDescricao: yup.string().nullable(),
        ecNomeContato: yup.string().nullable(),
        ecLogradouro: yup.string().nullable(),
        ecNumero: yup.string().nullable(),
        ecComplemento: yup.string().nullable(),
        ecBairro: yup.string().nullable(),
        ecCep: yup.string().nullable(),
        ecCaixaPostal: yup.string().nullable(),
        ecMunicipioId: yup
            .number()
            .required('ecMunicipioId|O Estado e Município do contato é obrigatório')
            .nullable(),
        ecEmail: yup.string().email('ecEmail|E-mail inválido!').nullable(),
        ecTelefone: yup
            .string()
            .matches(/\(\d{2}\) \d{5}-\d{4}/, 'ecTelefone|Telefone inválido')
            .nullable(),
    }),
});

export const PEDIR_CODIGO_SEGURANCA_VALIDADOR = yup.object().shape({
    usuaria: yup
        .string()
        .matches(/\(\d{2}\) \d{5}-\d{4}/, 'telefone|Telefone inválido! Favor corrigir.')
        .required('telefone|O telefone é obrigatório!')
        .nullable(),
    cpf: yup.string().required('cpf|O CPF é obrigatório!').nullable(),
    fotoDocumento: yup
        .object()
        .required('fotoDocumento|O envio de um documento com sua foto é obrigatório')
        .nullable(),
    // nome: yup.string().required('O seu nome é obrigatório!').nullable(),
});
