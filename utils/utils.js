import { Linking } from 'react-native';
import municipios from '../bases/municipios';
import * as Localization from 'expo-localization';
import * as Sentry from 'sentry-expo';
import Constants from 'expo-constants';

export const pluralize = (val, word, plural = word + 's') => {
    const _pluralize = (num, word, plural = word + 's') =>
        [1, -1].includes(Number(num)) ? word : plural;
    if (typeof val === 'object') return (num, word) => _pluralize(num, word, val[word]);
    return _pluralize(val, word, plural);
};

export const pr = (param) => {
    if (param === null || param === void 0) {
        return 'NULL';
    } else {
        return param.toString();
    }
};

export const formataNumeroBR = (num, decimais = 2) => {
    if (!num) {
        return '';
    }
    return num
        .toFixed(decimais)
        .replace('.', ',')
        .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
};

export const deepEqual = (a, b) => {
    if (a === b) return true;

    if (a === null || typeof a != 'object' || b === null || typeof b != 'object') return false;

    let keysA = Object.keys(a),
        keysB = Object.keys(b);

    if (keysA.length != keysB.length) return false;

    for (let key of keysA) {
        if (!keysB.includes(key) || !deepEqual(a[key], b[key])) return false;
    }

    return true;
};

const tmpId = '__tmpId_';

export const ehTmpId = (id) => {
    return !id || (typeof id === 'string' && id.includes(tmpId));
};

export const ehDadoLocal = (obj) => {
    return ehTmpId(obj.id);
};

export const geraTmpId = (append = null) => {
    if (append === null) {
        append = Math.random().toString(16).substring(7);
    }
    return tmpId + append;
};

export const resgataEstadoMunicipioDoId = (municipioIdRaw) => {
    const municipioId = municipioIdRaw || 5300108;
    const estadoId = parseInt(municipioId.toString().substring(0, 2));
    const mun = municipios[estadoId].find(
        (element) => element.codigo_ibge === parseInt(municipioId)
    );
    return {
        estadoId: estadoId,
        municipio: mun,
    };
};

export function parseLocaleNumber(stringNumber) {
    const digitGroupingSeparator =
        (1000).toLocaleString(Localization.locale)[1] === ',' ? ',' : '.';
    const decimalSeparator = digitGroupingSeparator === '.' ? ',' : '.';
    const newStringNumber = stringNumber
        .replace(new RegExp(`\\${digitGroupingSeparator}`, 'g'), '')
        .replace(new RegExp(`\\${decimalSeparator}`), '.');
    return Number(newStringNumber);
}

export function toFixedLocale(value, numDigits = 2) {
    if (value === null || isNaN(value)) {
        return null;
    }
    const digitGroupingSeparator =
        (1000).toLocaleString(Localization.locale)[1] === ',' ? ',' : '.';
    const decimalSeparator = digitGroupingSeparator === '.' ? ',' : '.';

    const standardFixedString = value.toFixed(numDigits);

    if (decimalSeparator === ',') {
        return standardFixedString.replace('.', ',');
    }
    return standardFixedString; // Locale matches JavaScript default
}

export function toCleanLocale(valueString) {
    const digitGroupingSeparator =
        (1000).toLocaleString(Localization.locale)[1] === ',' ? ',' : '.';
    const decimalSeparator = digitGroupingSeparator === '.' ? ',' : '.';
    if (decimalSeparator === ',') {
        return valueString.replace(/[^\d,]+/g, '');
    }
    return valueString.replace(/[^\d\.]+/g, '');
}

export const openUrlInBrowser = (url) => {
    Linking.canOpenURL(url).then((supported) => {
        if (supported) {
            Linking.openURL(url);
        } else {
            console.log("Don't know how to open URI: " + url);
        }
    });
};

export const omitDeep = (obj, key) => {
    const keys = Object.keys(obj);
    const newObj = {};
    keys.forEach((i) => {
        if (i !== key) {
            const val = obj[i];
            if (Array.isArray(val)) {
                newObj[i] = omitDeepArrayWalk(val, key);
            } else if (typeof val === 'object' && val !== null) {
                newObj[i] = omitDeep(val, key);
            } else {
                newObj[i] = val;
            }
        }
    });
    return newObj;
};

const omitDeepArrayWalk = (arr, key) => {
    return arr.map((val) => {
        if (Array.isArray(val)) {
            return omitDeepArrayWalk(val, key);
        } else if (typeof val === 'object') {
            return omitDeep(val, key);
        }
        return val;
    });
};

export const exitScreen = (navigation) => {
    navigation.navigate('Home');
    return true;
};

export const log = (msg, type = 'info') => {
    if (type === 'info') {
        console.log(msg);
    } else {
        console.error(msg);
    }

    if (!Constants.manifest.extra.sentryDns) {
        return;
    }
    if (type === 'info') {
        Sentry.Native.captureMessage(msg);
        return;
    }
    Sentry.Native.captureException(msg);
};

export const logError = (error) => {
    return log(error, 'error');
};

export const getUsoOuConflitoLabel = (usoOuConflito) => {
    return usoOuConflito.__typename === 'Conflito' ? 'Conflito' : 'Uso';
};

export const delay = (ms) => new Promise((res) => setTimeout(res, ms));
