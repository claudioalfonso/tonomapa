﻿const estados = [
    { codigo_uf: 12, uf: 'AC', label: 'Acre', value: 12 },
    { codigo_uf: 27, uf: 'AL', label: 'Alagoas', value: 27 },
    { codigo_uf: 16, uf: 'AP', label: 'Amapá', value: 16 },
    { codigo_uf: 13, uf: 'AM', label: 'Amazonas', value: 13 },
    { codigo_uf: 29, uf: 'BA', label: 'Bahia', value: 29 },
    { codigo_uf: 23, uf: 'CE', label: 'Ceará', value: 23 },
    { codigo_uf: 53, uf: 'DF', label: 'Distrito Federal', value: 53 },
    { codigo_uf: 32, uf: 'ES', label: 'Espírito Santo', value: 32 },
    { codigo_uf: 52, uf: 'GO', label: 'Goiás', value: 52 },
    { codigo_uf: 21, uf: 'MA', label: 'Maranhão', value: 21 },
    { codigo_uf: 51, uf: 'MT', label: 'Mato Grosso', value: 51 },
    { codigo_uf: 50, uf: 'MS', label: 'Mato Grosso do Sul', value: 50 },
    { codigo_uf: 31, uf: 'MG', label: 'Minas Gerais', value: 31 },
    { codigo_uf: 15, uf: 'PA', label: 'Pará', value: 15 },
    { codigo_uf: 25, uf: 'PB', label: 'Paraíba', value: 25 },
    { codigo_uf: 41, uf: 'PR', label: 'Paraná', value: 41 },
    { codigo_uf: 26, uf: 'PE', label: 'Pernambuco', value: 26 },
    { codigo_uf: 22, uf: 'PI', label: 'Piauí', value: 22 },
    { codigo_uf: 33, uf: 'RJ', label: 'Rio de Janeiro', value: 33 },
    { codigo_uf: 24, uf: 'RN', label: 'Rio Grande do Norte', value: 24 },
    { codigo_uf: 43, uf: 'RS', label: 'Rio Grande do Sul', value: 43 },
    { codigo_uf: 11, uf: 'RO', label: 'Rondônia', value: 11 },
    { codigo_uf: 14, uf: 'RR', label: 'Roraima', value: 14 },
    { codigo_uf: 42, uf: 'SC', label: 'Santa Catarina', value: 42 },
    { codigo_uf: 35, uf: 'SP', label: 'São Paulo', value: 35 },
    { codigo_uf: 28, uf: 'SE', label: 'Sergipe', value: 28 },
    { codigo_uf: 17, uf: 'TO', label: 'Tocantins', value: 17 },
];

export const pegaEstado = (estadoId) => {
    let estadoRes;
    estadoId = parseInt(estadoId);
    for (let estado of estados) {
        if (estado.codigo_uf === estadoId) {
            estadoRes = estado;
            break;
        }
    }
    return estadoRes;
};

export default estados;
