import { MAP_TYPES } from 'react-native-maps';
import { UNIDADES_AREA } from '../maps/mapsUtils';

const SETTINGS = {
    appIntro: true,
    useGPS: false,
    useMapLegacy: false,
    mapType: MAP_TYPES.HYBRID,
    editaPoligonos: true,
    editing: null,
    unidadeArea: UNIDADES_AREA.HECTARE,
    showAtaBalloon: true,
    showGPSHelpBalloon: true,
};

export default SETTINGS;
