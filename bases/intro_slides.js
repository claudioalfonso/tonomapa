import colors from '../assets/colors';
import metrics from '../utils/metrics';

const DEFAULT_SLIDE = {
    title: '',
    backgroundColor: '#000',
    image: require('../assets/intro/tour.jpg'),
    triangleDirection: 'bottom',
    triangleOffset: '10%',
    balloonWidth: metrics.tenHeight * 27,
    balloonTextColor: colors.Foundation.CorDeTexto.CorDeTexto2,
    balloonBackgroundColor: colors.Foundation.Basic.White,
};

export const SLIDES = [
    {
        key: '1',
        ...DEFAULT_SLIDE,
        text: 'Com o Tô no Mapa, sua comunidade pode se mapear e identificar usos e conflitos',
        triangleDirection: 'bottom',
        balloonStyles: { position: 'absolute', top: metrics.tenHeight * 5, left: 0 },
        triangleOffset: '50%',
    },
    {
        key: '2',
        ...DEFAULT_SLIDE,
        text: 'Aqui você pode colocar informações da comunidade, como o nome, segmento da comunidade, número de famílias, data de fundação, etc.',
        balloonStyles: { position: 'absolute', bottom: metrics.tenHeight * 6, left: 0 },
        triangleOffset: '45%',
    },
    {
        key: '3',
        ...DEFAULT_SLIDE,
        text: 'Este botão é para você mapear a comunidade! Veja como fazer...',
        balloonStyles: { position: 'absolute', bottom: metrics.tenHeight * 12.5, left: 0 },
        triangleOffset: '75%',
    },
    {
        key: '4',
        ...DEFAULT_SLIDE,
        text: 'Mapear a comunidade é fácil. Basta mover a tela, e a ponta ativa se mexe!. Você pode adicionar pontas (+), remover pontas (-) e mover qual é a ponta ativa ("<" e ">").',
        balloonStyles: { position: 'absolute', top: metrics.tenHeight * 10, right: 0 },
        triangleOffset: '48%',
        triangleDirection: 'bottom',
        image: require('../assets/intro/tourMapearTerritorioEditando.jpg'),
    },
    {
        key: '5',
        ...DEFAULT_SLIDE,
        text: 'Ou você pode usar a localização do celular (GPS)...',
        balloonStyles: {
            position: 'absolute',
            top: metrics.tenHeight * 3,
            right: -metrics.tenHeight * 2.5,
        },
        triangleOffset: '81%',
        triangleDirection: 'top',
        image: require('../assets/intro/tourMapearTerritorioGPS.jpg'),
        balloonWidth: metrics.tenHeight * 24,
    },
    /*{
        key: "6",
        ...DEFAULT_SLIDE,
        text: 'Basta se mover e clicar na sua posição e adicionar o ponto!',
        balloonStyles: {position:'absolute', top: 90, right: -40},
        triangleOffset: '50%',
        triangleDirection: 'left',
        image: require('../assets/intro/tourMapearTerritorio.jpg'),
        balloonWidth: 130
    },*/
    {
        key: '7',
        ...DEFAULT_SLIDE,
        text: 'Aqui é onde você registra locais de uso e de conflito',
        balloonStyles: { position: 'absolute', bottom: metrics.tenHeight * 6, left: 0 },
        triangleOffset: '25%',
    },
    {
        key: '8',
        ...DEFAULT_SLIDE,
        text: 'Você também pode mandar fotos e documentos sobre a comunidade!',
        balloonStyles: { position: 'absolute', bottom: metrics.tenHeight * 6, left: 0 },
        triangleOffset: '65%',
    },
    {
        key: '9',
        ...DEFAULT_SLIDE,
        text: 'No menu lateral você vê estas e outras opções',
        balloonStyles: {
            position: 'absolute',
            bottom: -metrics.tenHeight * 1.2,
            left: -metrics.tenHeight * 4,
        },
        triangleDirection: 'right',
        triangleOffset: '75%',
        image: require('../assets/intro/tourMenuLateral.jpg'),
        balloonWidth: metrics.tenHeight * 10,
    },
    {
        key: '10',
        ...DEFAULT_SLIDE,
        text: 'Quando terminar, não se esqueça de enviar os dados clicando em "Enviar para análise!"',
        balloonStyles: { position: 'absolute', top: metrics.tenHeight * 33, left: 0 },
        triangleDirection: 'top',
        triangleOffset: '28%',
        image: require('../assets/intro/tourMenuLateral.jpg'),
    },
    {
        key: '11',
        ...DEFAULT_SLIDE,
        text: 'Acompanhe a situação das informações cadastradas da comunidade e enviadas ao Tô no Mapa. Basta clicar aqui para saber se o mapeamento da sua comunidade foi validado!',
        triangleDirection: 'top',
        triangleOffset: '20%',
        balloonStyles: {
            position: 'absolute',
            top: metrics.tenHeight * 6,
            left: -metrics.tenHeight,
        },
    },
    {
        key: '12',
        ...DEFAULT_SLIDE,
        text: 'Você pode ver este tutorial novamente quando precisar!',
        balloonStyles: { position: 'absolute', top: metrics.tenHeight * 38, left: 0 },
        triangleDirection: 'bottom',
        triangleOffset: '28%',
        image: require('../assets/intro/tourMenuLateral.jpg'),
    },
];
