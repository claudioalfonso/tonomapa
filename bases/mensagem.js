const MENSAGEM = {
    __typename: 'MensagemType',
    id: null,
    dataEnvio: null,
    dataRecebimento: null,
    originIsDevice: true,
    extra: null,
    texto: null,
};

export default MENSAGEM;
