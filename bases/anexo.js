const ANEXO = {
    __typename: 'AnexoType',
    id: null,
    nome: null,
    descricao: null,
    tipoAnexo: null,
    mimetype: null,
    arquivo: null,
    thumb: null,
    criacao: null,
    publico: false,
};

export const TIPOS_ANEXO = {
    ANEXO: 'Foto ou vídeo',
    FONTE_INFORMACAO: 'Documento',
    // ICONE: 'Logomarca da associação',
};

export const TIPOS_ANEXO_INFO = {
    ANEXO: 'Imagens da comunidade. São aceitos arquivos nos seguintes formatos: ".jpeg", ".png", ".avi" ou ".mp4"',
    FONTE_INFORMACAO:
        'É obrigatório enviar pelo menos um arquivo com informações. O formato tem que ser ".pdf", ".doc" ou ".docx"',
    // ICONE: 'Só pode ser do tipo ".jpeg" ou ".png"',
};

export const isAnexoPTT = (tipoAnexo) => {
    if (!tipoAnexo) {
        return false;
    }
    return TIPOS_ANEXO.hasOwnProperty(tipoAnexo);
};

export default ANEXO;
