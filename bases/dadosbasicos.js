const DADOS_BASICOS_TERRITORIO = {
    nome: { type: 'Ignore' },
    poligono: { type: 'Ignore' },
    publico: { type: 'Boolean' },
    enviadoPtt: { type: 'Boolean' },
    anoFundacao: { type: 'Number' },
    qtdeFamilias: { type: 'Number' },
    statusId: { type: 'Number' },
    tiposComunidade: { type: 'Array', __typename: 'tipoComunidade' },
    municipioReferenciaId: { type: 'String' },
    cep: { type: 'String' },
    descricaoAcesso: { type: 'String' },
    descricaoAcessoPrivacidade: { type: 'Boolean' },
    zonaLocalizacao: { type: 'String' },
    outraZonaLocalizacao: { type: 'String' },
    autoIdentificacao: { type: 'String' },
    segmentoId: { type: 'String' },
    historiaDescricao: { type: 'String' },
    ecNomeContato: { type: 'String' },
    ecLogradouro: { type: 'String' },
    ecNumero: { type: 'String' },
    ecComplemento: { type: 'String' },
    ecBairro: { type: 'String' },
    ecCep: { type: 'String' },
    ecCaixaPostal: { type: 'String' },
    ecMunicipioId: { type: 'String' },
    ecEmail: { type: 'String' },
    ecTelefone: { type: 'String' },
};

export const DADOS_BASICOS_TERRITORIO_TONOMAPA = [
    'nome',
    'anoFundacao',
    'qtdeFamilias',
    'tiposComunidade',
    'municipioReferenciaId',
];

export const adjustTerritorioFieldTypes = (currentTerritorio) => {
    for (let campo of Object.keys(DADOS_BASICOS_TERRITORIO)) {
        let valor;
        switch (DADOS_BASICOS_TERRITORIO[campo].type) {
            case 'Number':
                valor = currentTerritorio[campo] ? parseInt(currentTerritorio[campo]) : null;
                break;
            case 'String':
                valor = currentTerritorio[campo] ? currentTerritorio[campo].toString() : null;
                break;
            case 'Array':
                valor = [];
                for (let id of currentTerritorio[campo]) {
                    if (typeof id === 'object') {
                        valor.push(id);
                        continue;
                    }
                    const item = {
                        id,
                        __typename: DADOS_BASICOS_TERRITORIO[campo].__typename,
                    };
                    valor.push(item);
                }
                break;
            case 'Boolean':
            case 'Ignore':
            default:
                valor = null;
                break;
        }
        if (valor) {
            currentTerritorio[campo] = valor;
        }
    }
    return currentTerritorio;
};

export const LABELS_USO = {
    title: 'Marcar local de uso',
    editTitle: 'Editar local de uso',
    snackBarMessage: 'Local de uso adicionado com sucesso!',
    snackBarMessageEdited: 'Local de uso alterado com sucesso!',
    snackBarMessageDelete: 'Local de uso apagado com sucesso!',
    nome: 'Nome do local de uso',
    descricao: 'Informações adicionais',
    area: 'Área do local (em metros quadrados)',
    tipo: 'Tipo de uso deste local',
    erroTipo: 'Este campo é obrigatório!',
};

export const LABELS_CONFLITO = {
    title: 'Marcar conflito',
    editTitle: 'Editar conflito',
    snackBarMessage: 'Conflito adicionado com sucesso!',
    snackBarMessageEdited: 'Conflito alterado com sucesso!',
    snackBarMessageDelete: 'Conflito apagado com sucesso!',
    nome: 'Nome do conflito',
    descricao: 'Observações',
    area: '',
    tipo: 'Tipo de conflito',
    erroTipo: 'Este campo é obrigatório!',
};

export default DADOS_BASICOS_TERRITORIO;
