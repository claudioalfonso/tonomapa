import Constants from 'expo-constants';

// @ts-ignore
import tipos from './tipos';
import colors from '../assets/colors';
import { DADOS_BASICOS_TERRITORIO_TONOMAPA } from './dadosbasicos';
import { pluralize } from '../utils/utils';
import { PTT_TERRITORIO_VALIDADOR } from '../utils/schemas';
import { isAnexoPTT } from './anexo';
import { formataArea } from '../maps/mapsUtils';

const PONTUACAO = {
    NAO_TEM: 0,
    // Dados básicos:
    DADOS_BASICOS_NAO_PREENCHIDOS: 0,
    DADOS_BASICOS_INCOMPLETOS: 1,
    DADOS_BASICOS_COMPLETOS: 5,
    // Polígonos
    POLIGONO: 10,
    // Usos e conflitos:
    USO_OU_CONFLITO: 1,
    // Anexos:
    ANEXO: 1,
    // Ata:
    ATA: 10,
};

export const STATUS = {
    EM_PREENCHIMENTO: 2,
    EM_PREENCHIMENTO_INTERNO: {
        // Nada preenchido (circle-slice-2): <=4
        // Pouco preenchido (circle-slice-4): >=5 <=8
        // Razoável (circle-slice-6): >=10 <25
        // Completo (circle): >=25
        NADA: 4,
        POUCO: 8,
        RAZOAVEL: 24,
        COMPLETO: 25,
    },
    ENVIADO_PENDENTE: 3,
    OK_TONOMAPA: 4,
    REVISADO_COM_PENDENCIAS: 8,
    REJEITADO_TONOMAPA: 9,
    OK_PTT: 5,
    EM_PREENCHIMENTO_PTT: 10,
    ENVIO_PTT_NAFILA: 11,
    ENVIO_ARQUIVOS_PTT_NAFILA: 12,
};

const tiposStatus = tipos.data.territorioStatus;

export const pegaMensagem = (statusId) => {
    let mensagem = {};
    for (let i = 0; i < tiposStatus.length; i++) {
        if (tiposStatus[i].id === statusId.toString()) {
            mensagem = tiposStatus[i];
            break;
        }
    }
    return mensagem;
};

const buildStatusMessageEmPreenchimento = (statusMessageRaw, emPreenchimentoStatus) => {
    let icon = 'circle-slice-4';
    let mensagemExtra = '';
    switch (emPreenchimentoStatus) {
        case STATUS.EM_PREENCHIMENTO_INTERNO.NADA:
            icon = 'circle-slice-2';
            mensagemExtra = '\n\nAinda não há informações sobre a comunidade';
            break;
        case STATUS.EM_PREENCHIMENTO_INTERNO.POUCO:
            icon = 'circle-slice-4';
            mensagemExtra =
                '\n\nJá temos alguns dados da comunidade, mas falta pelo menos a área, a ata e completar as informações básicas';
            break;
        case STATUS.EM_PREENCHIMENTO_INTERNO.RAZOAVEL:
            icon = 'circle-slice-6';
            mensagemExtra = '\n\nTá quase tudo completo!';
            break;
        case STATUS.EM_PREENCHIMENTO_INTERNO.COMPLETO:
            icon = 'checkbox-marked-circle';
            mensagemExtra =
                '\n\nJá está completo: você já registrou pelo menos os dados básicos, a área da comunidade e incluiu a ata!';
            break;
    }
    return {
        ...statusMessageRaw,
        mensagem: statusMessageRaw.mensagem + mensagemExtra,
        icon,
        color: colors.Foundation.Brown.B300,
    };
};

export const buildStatusMessage = (statusId, emPreenchimentoStatus = null) => {
    if (!statusId) {
        return null;
    }
    let statusMessage = pegaMensagem(statusId);
    switch (statusId) {
        case STATUS.REVISADO_COM_PENDENCIAS:
        case STATUS.EM_PREENCHIMENTO:
        case STATUS.EM_PREENCHIMENTO_PTT:
            statusMessage = buildStatusMessageEmPreenchimento(statusMessage, emPreenchimentoStatus);
            break;
        case STATUS.ENVIADO_PENDENTE:
        case STATUS.ENVIO_PTT_NAFILA:
        case STATUS.ENVIO_ARQUIVOS_PTT_NAFILA:
            statusMessage = {
                ...statusMessage,
                icon: 'circle',
                color: colors.Foundation.Brown.B300,
            };
            break;
        case STATUS.OK_TONOMAPA:
        case STATUS.OK_PTT:
            statusMessage = {
                ...statusMessage,
                icon: 'checkbox-marked-circle',
                color: colors.Foundation.Green.G300,
            };
            break;
        case STATUS.REJEITADO_TONOMAPA:
            statusMessage = {
                ...statusMessage,
                icon: 'close-circle',
                color: colors.Foundation.Feedback.Error,
            };
            break;
        case STATUS.OK_PTT:
            statusMessage = {
                ...statusMessage,
                icon: 'checkbox-marked-circle',
                color: colors.Foundation.Green.G300,
            };
            break;
    }
    return statusMessage;
};

export const alertStatusId = ({ statusId = null, statusMessage = null }) => {
    if (!statusId && !statusMessage) {
        return {};
    }
    const newStatusMessage = statusId ? buildStatusMessage(statusId) : statusMessage;
    const title = statusId ? `Atenção: ${newStatusMessage.nome}` : newStatusMessage.nome;
    const body = newStatusMessage.mensagem ? newStatusMessage.mensagem.replace(/\\n/g, '\n') : '';
    return {
        title,
        body,
        urlReferencia: newStatusMessage.urlReferencia,
        hasFaleConoscoBtn: newStatusMessage.hasFaleConoscoBtn,
    };
};

export const isEnviarEnabled = (statusOrCurrentTerritorio) => {
    if (!statusOrCurrentTerritorio) {
        return true;
    }

    const statusId =
        typeof statusOrCurrentTerritorio === 'object'
            ? statusOrCurrentTerritorio.statusId
            : parseInt(statusOrCurrentTerritorio);

    return ![
        STATUS.REJEITADO_TONOMAPA,
        STATUS.OK_TONOMAPA,
        STATUS.ENVIADO_PENDENTE,
        STATUS.OK_PTT,
        STATUS.ENVIO_PTT_NAFILA,
        STATUS.ENVIO_ARQUIVOS_PTT_NAFILA,
    ].includes(statusId);
};

export const isCurrentTerritorioEditable = (statusOrCurrentTerritorio) => {
    if (!statusOrCurrentTerritorio) {
        return true;
    }

    const statusId =
        typeof statusOrCurrentTerritorio === 'object'
            ? statusOrCurrentTerritorio.statusId
            : parseInt(statusOrCurrentTerritorio);

    return [
        STATUS.EM_PREENCHIMENTO,
        STATUS.REVISADO_COM_PENDENCIAS,
        STATUS.EM_PREENCHIMENTO_PTT,
    ].includes(statusId);
};

export const isPTTMode = (statusOrCurrentTerritorio) => {
    if (!statusOrCurrentTerritorio) {
        return false;
    }

    const statusId =
        typeof statusOrCurrentTerritorio === 'object'
            ? statusOrCurrentTerritorio.statusId
            : parseInt(statusOrCurrentTerritorio);

    return [
        STATUS.EM_PREENCHIMENTO_PTT,
        STATUS.ENVIO_PTT_NAFILA,
        STATUS.ENVIO_ARQUIVOS_PTT_NAFILA,
    ].includes(statusId);
};

export const isEnviarPTTEnabled = (statusOrCurrentTerritorio) => {
    if (
        !statusOrCurrentTerritorio ||
        statusOrCurrentTerritorio.enviadoPtt === true ||
        Constants.manifest.extra.isPTTDisabled
    ) {
        return false;
    }

    const statusId =
        typeof statusOrCurrentTerritorio === 'object'
            ? statusOrCurrentTerritorio.statusId
            : parseInt(statusOrCurrentTerritorio);

    return (isPTTMode(statusId) && isEnviarEnabled(statusId)) || statusId === STATUS.OK_TONOMAPA;
};

export const isValidado = (statusOrCurrentTerritorio) => {
    if (!statusOrCurrentTerritorio) {
        return false;
    }

    const statusId =
        typeof statusOrCurrentTerritorio === 'object'
            ? statusOrCurrentTerritorio.statusId
            : parseInt(statusOrCurrentTerritorio);

    return [STATUS.OK_TONOMAPA, STATUS.OK_PTT].includes(statusId) || isPTTMode(statusId);
};

export const getSyncStatus = (online, isSyncing, onPress) => {
    const syncStatus = {
        onPress,
        icon: 'wifi-off',
        color: colors.Foundation.Basic.Disabled,
        title: 'Sem internet',
        body: 'Você está desconectada(o). Mas tudo bem! O Tô no Mapa funciona perfeitamente, mesmo sem internet! Assim que você conseguir sinal, abra o app e os dados da comunidade serão sincronizados automaticamente.',
    };

    if (!online) {
        return syncStatus;
    }

    if (isSyncing) {
        return {
            ...syncStatus,
            icon: 'sync',
            color: colors.Foundation.Feedback.Info,
            title: 'Em sincronização',
            body: 'Suas alterações estão sendo sincronizadas com o tô no mapa...',
        };
    }

    return {
        ...syncStatus,
        icon: 'check-all',
        color: colors.Foundation.Feedback.Success,
        title: 'Sincronizado!',
        body: 'As informações da comunidade estão sincronizadas com o Tô no Mapa!',
    };
};

/**
 * Fornece informações a respeito da situação em que está o preenchimento do território
 * @method
 * @param  {Object} params {currentUser, markers, poligonos}
 * @return Promise<object>        {Array situacaoArray, Integer statusId}
 */
export const validaDados = async ({ currentUser, polygons, anexos, areasDeUso, conflitos }) => {
    const checkList = [];
    let pontuacao = 0;
    let camposPreenchidos = 0;
    let statusFormularioBasico = PONTUACAO.DADOS_BASICOS_INCOMPLETOS;
    const statusNovo = {
        poligonos: null,
        usoOuConflito: null,
    };
    const iconStyleSuccess = {
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
        backgroundColor: colors.Foundation.Green.G300,
    };
    const iconStyleWarning = {
        color: colors.Foundation.CorDeTexto.CorDeTexto,
        backgroundColor: colors.Foundation.Brown.B300,
    };
    const iconStyleError = {
        color: colors.Foundation.CorDeTexto.CorDeTexto,
        backgroundColor: colors.Foundation.Feedback.Error,
    };

    const territorio = currentUser.currentTerritorio;

    if (territorio !== null) {
        //ver se poligono está criado
        const temPoligonos = polygons.length > 0;
        if (temPoligonos) {
            const area = polygons.reduce((area, current) => area + current.area, 0);
            checkList.push({
                icon: 'check',
                text: 'Área definida (' + formataArea(area) + ')',
                iconStyle: iconStyleSuccess,
            });
        } else {
            checkList.push({
                pttError: true,
                icon: 'minus',
                text: 'Nenhuma área definida',
                iconStyle: iconStyleError,
            });
        }
        statusNovo.poligonos = temPoligonos;
        pontuacao += temPoligonos ? PONTUACAO.POLIGONO : PONTUACAO.NAO_TEM;

        // Dados do PTT estão corretos?
        if (!isPTTMode(currentUser?.currentTerritorio?.statusId)) {
            // Dados básicos estão completos?
            for (const campo of DADOS_BASICOS_TERRITORIO_TONOMAPA) {
                switch (campo) {
                    case 'tiposComunidade':
                        if (territorio.tiposComunidade.length > 0) {
                            camposPreenchidos++;
                        }
                        break;
                    default:
                        if (territorio[campo]) {
                            camposPreenchidos++;
                        }
                        break;
                }
            }
            if (camposPreenchidos === 0) {
                statusFormularioBasico = PONTUACAO.DADOS_BASICOS_NAO_PREENCHIDOS;
            } else if (camposPreenchidos === DADOS_BASICOS_TERRITORIO_TONOMAPA.length) {
                statusFormularioBasico = PONTUACAO.DADOS_BASICOS_COMPLETOS;
            }

            /*DEBUG*/ //console.log('statusFormularioBasico', statusFormularioBasico);
            switch (statusFormularioBasico) {
                case PONTUACAO.DADOS_BASICOS_NAO_PREENCHIDOS:
                    checkList.push({
                        icon: 'minus',
                        text: 'Dados básicos não preenchidos',
                        iconStyle: iconStyleError,
                    });
                    break;
                case PONTUACAO.DADOS_BASICOS_INCOMPLETOS:
                    checkList.push({
                        icon: 'check',
                        text: 'Dados básicos incompletos',
                        iconStyle: iconStyleWarning,
                    });
                    break;
                case PONTUACAO.DADOS_BASICOS_COMPLETOS:
                    checkList.push({
                        icon: 'check',
                        text: 'Dados básicos completos',
                        iconStyle: iconStyleSuccess,
                    });
                    break;
            }
            pontuacao += statusFormularioBasico;

            // Registrou locais de uso ou conflitos?
            const qtdeUsosEConflitos = areasDeUso.length + conflitos.length;
            checkList.push(
                qtdeUsosEConflitos > 0
                    ? {
                          icon: 'check',
                          text:
                              qtdeUsosEConflitos.toString() +
                              pluralize(
                                  qtdeUsosEConflitos,
                                  ' local de uso ou conflito',
                                  ' locais de uso e conflitos'
                              ),
                          iconStyle: iconStyleSuccess,
                      }
                    : {
                          icon: 'minus',
                          text: 'Nenhum local de uso ou de conflito',
                          iconStyle: iconStyleError,
                      }
            );
            statusNovo.usoOuConflito = qtdeUsosEConflitos > 0;
            pontuacao += statusNovo.usoOuConflito ? PONTUACAO.USO_OU_CONFLITO : PONTUACAO.NAO_TEM;

            // Anexou imagens/documentos?
            const qtdeAnexos = anexos.filter((anexo) => !isAnexoPTT(anexo.tipoAnexo)).length;
            checkList.push(
                qtdeAnexos > 0
                    ? {
                          icon: 'check',
                          text: `${qtdeAnexos} ${pluralize(
                              qtdeAnexos,
                              ' arquivo ou imagem',
                              ' arquivos e imagens'
                          )}`,
                          iconStyle: iconStyleSuccess,
                      }
                    : {
                          icon: 'minus',
                          text: 'Nenhuma imagem ou documento',
                          iconStyle: iconStyleWarning,
                      }
            );
            statusNovo.anexos = qtdeAnexos > 0;
            pontuacao += statusNovo.anexos ? PONTUACAO.ANEXO : PONTUACAO.NAO_TEM;

            // Anexou ata?
            statusNovo.temAta = false;
            if (qtdeAnexos > 0) {
                const qtdeAtaDocs = anexos.filter((anexo) => {
                    return anexo.tipoAnexo === 'ata';
                }).length;
                checkList.push(
                    qtdeAtaDocs > 0
                        ? {
                              icon: 'check',
                              text: `Ata anexada (${qtdeAtaDocs} ${pluralize(
                                  qtdeAtaDocs,
                                  ' arquivo',
                                  ' arquivos'
                              )} )`,
                              iconStyle: iconStyleSuccess,
                          }
                        : {
                              icon: 'minus',
                              text: 'Não há ata anexada',
                              iconStyle: iconStyleError,
                          }
                );
                statusNovo.temAta = qtdeAtaDocs > 0;
            }
            pontuacao += statusNovo.temAta ? PONTUACAO.ATA : PONTUACAO.NAO_TEM;
        } else {
            const isValid = await PTT_TERRITORIO_VALIDADOR.isValid(currentUser);

            checkList.push(
                isValid
                    ? {
                          icon: 'check',
                          text: 'Dados obrigatórios preenchidos',
                          iconStyle: iconStyleSuccess,
                      }
                    : {
                          pttError: true,
                          icon: 'minus',
                          text: 'Dados PTT incompletos',
                          iconStyle: iconStyleError,
                      }
            );

            checkList.push(
                anexos.some((anexo) => isAnexoPTT(anexo.tipoAnexo))
                    ? {
                          icon: 'check',
                          text: 'Anexo(s) PTT inserido(s)',
                          iconStyle: iconStyleSuccess,
                      }
                    : {
                          pttError: true,
                          icon: 'minus',
                          text: 'Anexo(s) PTT não inserido(s)',
                          iconStyle: iconStyleError,
                      }
            );
        }

        let emPreenchimentoStatus = STATUS.EM_PREENCHIMENTO_INTERNO.COMPLETO;
        if (pontuacao <= STATUS.EM_PREENCHIMENTO_INTERNO.NADA) {
            emPreenchimentoStatus = STATUS.EM_PREENCHIMENTO_INTERNO.NADA;
        } else if (pontuacao <= STATUS.EM_PREENCHIMENTO_INTERNO.POUCO) {
            emPreenchimentoStatus = STATUS.EM_PREENCHIMENTO_INTERNO.POUCO;
        } else if (pontuacao <= STATUS.EM_PREENCHIMENTO_INTERNO.RAZOAVEL) {
            emPreenchimentoStatus = STATUS.EM_PREENCHIMENTO_INTERNO.RAZOAVEL;
        }
        return { checkList, emPreenchimentoStatus };
    }
};

export default STATUS;
