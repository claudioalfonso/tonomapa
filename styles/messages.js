import Alignments from './alignments';
import colors from '../assets/colors';
import { StyleSheet } from 'react-native';

const Messages = StyleSheet.create({
    alert: {
        ...Alignments.defaultMargin,
        alignSelf: 'stretch',
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4,
        padding: 8,
    },

    alertPrimary: {
        ...alert,
        backgroundColor: colors.Foundation.Green.G300,
    },

    alertDanger: {
        ...alert,
        backgroundColor: colors.Foundation.Feedback.Error,
    },
});

export default Messages;
