import colors from '../assets/colors';
import metrics from '../utils/metrics';
import { StyleSheet } from 'react-native';

const basic = StyleSheet.create({
    body: {
        paddingHorizontal: metrics.tenWidth * 0.8,
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    text: {
        fontSize: metrics.tenWidth * 1.6,
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
        flexShrink: 1,
    },
    bold: {
        fontWeight: 'bold',
    },
    italic: {
        fontStyle: 'italic',
    },
    appOverlay: {
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        left: 0,
        top: 0,
        width: '100%',
        height: '100%',
        backgroundColor: colors.appOverlay,
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: metrics.tenWidth * 0.8,
    },
    col: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    buttonLabel: {
        fontSize: metrics.tenHeight * 1.6,
        padding: metrics.tenHeight * 0.8,
    },
    drawerLabel: {
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
        fontSize: metrics.tenWidth * 1.6,
    },
    drawerIcon: {
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
        fontSize: metrics.tenWidth * 3.0,
    },
    statusBarText: {
        paddingHorizontal: metrics.tenWidth * 1.2,
        paddingBottom: metrics.tenHeight * 0.8,
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
        fontSize: metrics.tenWidth * 2,
    },
    iconForConflitoSelect: {
        position: 'absolute',
        bottom: metrics.tenHeight * 1.4,
        right: metrics.tenWidth * 1,
    },
});

const Layout = StyleSheet.create({
    italic: basic.italic,

    bold: basic.bold,

    container: basic.container,

    containerCentered: {
        ...basic.container,
        alignItems: 'center',
    },

    containerStretched: {
        ...basic.container,
        alignItems: 'stretch',
    },

    icon: {
        height: metrics.tenHeight * 5,
        width: metrics.tenWidth * 5,
    },

    iconUso: {
        height: metrics.tenHeight * 5,
    },

    vertice: {
        height: metrics.tenWidth * 2,
        alignContent: 'center',
        alignItems: 'center',
        width: metrics.tenWidth * 2,
        borderRadius: metrics.tenWidth * 2,
        backgroundColor: 'white',
    },

    body: basic.body,

    loginBody: {
        flex: 1,
        paddingTop: metrics.tenHeight * 4,
        paddingHorizontal: metrics.tenWidth * 5,
    },

    bodyWithBottomBar: {
        ...basic.body,
        marginBottom: metrics.tenHeight * 6,
    },

    bodyConfiguracoes: {
        ...basic.body,
        paddingHorizontal: metrics.tenHeight * 2,
        paddingTop: metrics.tenHeight * 2,
    },

    bodyFlexEnd: {
        ...basic.body,
        flex: 1,
        justifyContent: 'flex-end',
    },

    innerBody: {
        paddingHorizontal: metrics.tenWidth * 3,
        paddingBottom: metrics.tenHeight * 14,
    },

    topBar: {
        width: '100%',
        backgroundColor: colors.Foundation.Green.G300,
    },

    appOverlay: basic.appOverlay,

    appOverlayZIndex100: {
        ...basic.appOverlay,
        zIndex: 100,
    },

    appOverlayOpaque: {
        ...basic.appOverlay,
        backgroundColor: colors.Foundation.Basic.White,
    },

    topBarTitle: {
        fontSize: metrics.tenWidth * 2,
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
    },

    row: basic.row,

    rowCenter: {
        ...basic.row,
        justifyContent: 'center',
    },

    rowConfiguracoes: {
        ...basic.row,
        paddingBottom: metrics.tenWidth * 2,
    },

    secaoConfiguracoes: {
        borderTopWidth: 1,
        borderColor: colors.divider,
        paddingTop: metrics.tenWidth * 1.6,
        paddingBottom: metrics.tenWidth * 1.2,
    },

    secaoConfiguracoesText: {
        fontSize: metrics.tenWidth * 2.0,
        color: colors.Foundation.Brown.B300,
    },

    rowConfiguracoesInner: {
        ...basic.row,
        alignItems: 'baseline',
    },

    rowConfiguracoesComDesc: {
        paddingBottom: metrics.tenWidth * 2,
    },

    rowAnexoForm: {
        ...basic.row,
        marginTop: metrics.tenWidth * 1,
        marginBottom: metrics.tenWidth * 1,
    },

    rowWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
    },

    col: basic.col,

    colLeft: {
        ...basic.col,
        alignItems: 'flex-start',
    },

    buttonRight: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginTop: metrics.tenWidth * 0.8,
    },

    buttonLeft: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginTop: metrics.tenWidth * 0.8,
    },

    buttonLabel: basic.buttonLabel,

    buttonPrimaryLabel: {
        ...basic.buttonLabel,
        color: colors.Foundation.TextColor.White,
    },

    buttonCenter: {
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
        marginTop: metrics.tenWidth * 3,
    },

    buttonsBottomWrapperScroll: {
        marginTop: metrics.tenWidth * 6,
        marginBottom: metrics.tenWidth * 2,
    },

    buttonsBottomWrapper: {
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: metrics.tenWidth * 2,
    },

    buttonsBottomDialogWrapper: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: metrics.tenWidth * 2,
    },

    buttonsAbsoluteBottomWrapper: {
        position: 'absolute',
        padding: metrics.tenWidth * 2,
        backgroundColor: '#fff',
        bottom: 0,
        left: 0,
        right: 0,
    },

    subRow: {
        ...basic.row,
        paddingTop: 0,
        paddingLeft: metrics.tenWidth * 0.8,
    },

    alignCenter: {
        justifyContent: 'center',
    },

    drawer: {
        width: metrics.tenWidth * 32,
    },

    drawerLabel: basic.drawerLabel,

    drawerHeader: {
        ...basic.row,
        justifyContent: 'flex-start',
        paddingRight: metrics.tenWidth * 1.2,
        paddingLeft: metrics.tenWidth * 1.8,
        paddingTop: metrics.tenWidth * 0.4,
        paddingBottom: metrics.tenWidth * 0.8,
        borderBottomWidth: 1,
        borderColor: colors.divider,
    },

    drawerHeaderLabel: {
        ...basic.drawerLabel,
        ...basic.bold,
        paddingLeft: metrics.tenWidth * 3,
        flex: 1,
        flexWrap: 'wrap',
    },

    drawerLabelDisabled: {
        ...basic.drawerLabel,
        color: colors.Foundation.Basic.Disabled,
    },

    drawerIcon: basic.drawerIcon,

    drawerIconActive: {
        ...basic.drawerIcon,
        color: colors.Foundation.Green.G300,
    },

    drawerIconDisabled: {
        ...basic.drawerIcon,
        color: colors.Foundation.Basic.Disabled,
    },

    drawerItem: {
        backgroundColor: 'transparen',
    },

    drawerItemSobre: {
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
        fontSize: metrics.tenWidth * 1.6,
    },

    drawerEnviar: {
        backgroundColor: colors.warning,
    },

    drawerEnviarLabel: {
        color: colors.Foundation.TextColor.White,
        fontSize: metrics.tenWidth * 1.6,
    },

    drawerEnviarIcon: {
        color: colors.Foundation.TextColor.White,
        fontSize: metrics.tenWidth * 3.0,
    },

    drawerLogoImage: {
        height: metrics.tenWidth * 3,
        width: metrics.tenWidth * 3,
    },

    switchRight: {
        flex: 0.2,
    },

    text: basic.text,

    textPeq: {
        ...basic.text,
        fontSize: metrics.tenWidth * 1.3,
    },

    textLeftFromSwitch: {
        ...basic.text,
        flex: 0.8,
    },

    textParagraph: {
        ...basic.text,
        alignSelf: 'flex-start',
        marginBottom: metrics.tenWidth * 0.8,
    },

    textParagraphTitle: {
        ...basic.text,
        fontSize: metrics.tenHeight * 1.6,
        fontWeight: '500',
        alignSelf: 'flex-start',
        marginTop: metrics.tenHeight * 3,
    },

    textAlert: {
        ...basic.text,
        alignSelf: 'flex-start',
        borderWidth: 2,
        width: '100%',
        padding: 0.5 * metrics.tenHeight,
        marginBottom: 0.5 * metrics.tenHeight,
        borderColor: colors.Foundation.Green.G300,
        backgroundColor: colors.polygonBackgroundStandard,
    },

    balloonHintWrapper: {
        position: 'absolute',
        top: metrics.tenHeight * 6.5,
        width: '100%',
        paddingHorizontal: metrics.tenHeight * 1.2,
        zIndex: 110,
    },

    balloonHintText: {
        ...basic.text,
        alignSelf: 'flex-start',
        marginBottom: metrics.tenWidth * 0.8,
        fontSize: metrics.tenHeight * 1.6,
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
    },

    balloonHintTitle: {
        ...basic.text,
        alignSelf: 'flex-start',
        marginBottom: metrics.tenWidth * 0.8,
        fontSize: metrics.tenHeight * 2,
        fontWeight: '700',
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
    },

    descConfiguracoes: {
        ...basic.text,
        ...basic.italic,
    },

    calloutDescription: {
        ...basic.text,
        fontSize: metrics.tenWidth * 1.4,
    },

    calloutDescriptionSub: {
        ...basic.text,
        ...basic.italic,
        alignSelf: 'flex-start',
        fontSize: metrics.tenWidth * 1.2,
    },

    calloutTitle: {
        fontSize: metrics.tenWidth * 1.6,
        ...basic.bold,
    },

    enviarTitle: {
        ...basic.text,
        textTransform: 'uppercase',
        fontWeight: 'bold',
        paddingBottom: metrics.tenWidth * 1.2,
    },

    calloutAction: {
        marginTop: metrics.tenWidth * 0.8,
        marginBottom: metrics.tenWidth * 0.8,
        paddingBottom: 0,
        fontSize: metrics.tenWidth * 1.2,
        textTransform: 'uppercase',
    },

    calloutActionCenter: {
        alignSelf: 'center',
    },

    logoImageCadastro: {
        height: metrics.tenWidth * 12,
        width: metrics.tenWidth * 10,
        alignSelf: 'center',
    },

    statusBar: {
        width: '100%',
        alignItems: 'center',
        backgroundColor: colors.Foundation.Green.G300,
    },

    statusBarText: basic.statusBarText,

    statusBarTextTitle: {
        alignSelf: 'flex-start',
        ...basic.statusBarText,
        ...basic.bold,
    },

    statusBarTextDesc: {
        ...basic.statusBarText,
        ...basic.italic,
        fontSize: metrics.tenWidth * 1.5,
    },

    dialogButtonsMultiLine: {
        flexDirection: 'column',
        alignItems: 'flex-end',
    },

    dialogButtons: {},

    mapContainer: {
        ...StyleSheet.absoluteFillObject,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    mapContainerShot: {
        width: 794,
        height: 1123,
        justifyContent: 'flex-end',
        alignItems: 'center',
    },

    logoImageSplash: {
        marginTop: 0,
        height: '100%',
        width: '100%',
    },

    iconForConflitoSelect: basic.iconForConflitoSelect,

    iconForUsoSelect: {
        ...basic.iconForConflitoSelect,
        bottom: metrics.tenHeight * 1,
    },

    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: metrics.tenHeight * 8,
        backgroundColor: colors.Foundation.Green.G300,
    },
});

export default Layout;
