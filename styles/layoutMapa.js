import colors from '../assets/colors';
import { StyleSheet } from 'react-native';
import metrics from '../utils/metrics';

const basic = StyleSheet.create({
    appbarBottom: {
        position: 'absolute',
        height: metrics.tenWidth * 7,
        left: metrics.tenWidth * -0.4,
        right: 0,
        bottom: 0,
        padding: 0,
        backgroundColor: colors.Foundation.Basic.White,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    appbarBottomItemWrapper: {
        flexGrow: 1,
        borderTopWidth: metrics.tenHeight * 0.4,
        borderTopColor: colors.Foundation.Basic.White,
    },
    mapToolbarItemWrapper: {
        backgroundColor: colors.background,
        borderLeftWidth: metrics.tenHeight * 0.4,
        borderLeftColor: colors.background,
        padding: metrics.tenHeight * 0.8,
    },
    mapToolbarItemRightWrapper: {
        borderLeftWidth: metrics.tenHeight * 0.4,
        paddingVertical: metrics.tenHeight * 0.1,
        paddingHorizontal: 0,
        width: metrics.tenWidth * 11.5,
        backgroundColor: '#FFFBFE',
        borderLeftColor: '#FFFBFE',
    },
    mapToolbarItemTopWrapper: {
        borderBottomWidth: metrics.tenHeight * 0.4,
        padding: metrics.tenHeight * 0.8,
        width: metrics.tenWidth * 18,
        backgroundColor: '#FFFBFE',
        borderBottomColor: '#FFFBFE',
        alignItems: 'center',
    },
    mapearButtons: {
        borderRadius: metrics.tenWidth * 0.4,
        padding: 0,
    },
});

const LayoutMapa = StyleSheet.create({
    map: {
        ...StyleSheet.absoluteFillObject,
        flex: 1,
    },

    appbarBottom: basic.appbarBottom,

    appbarBottomActions: {
        backgroundColor: colors.background,
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-around',
        alignItems: 'stretch',
        width: '100%',
        margin: 0,
    },

    appbarBottomAction: {
        margin: 0,
        height: metrics.tenHeight * 3,
    },

    appbarBottomContent: {
        margin: 0,
        height: metrics.tenHeight * 1,
        padding: 0,
    },

    appbarBottomLabel: {
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
    },

    appbarBottomLabelActive: {
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
    },

    appbarBottomTerritoryLabel: {
        fontSize: metrics.tenHeight * 2,
        color: colors.Foundation.TextColor.White,
        margin: 0,
        padding: 0,
    },

    appbarBottomItem: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingTop: metrics.tenHeight * 0.5,
        paddingLeft: metrics.tenWidth * 0.6,
        paddingRight: metrics.tenWidth * 0.6,
    },

    appbarBottomItemWrapper: basic.appbarBottomItemWrapper,

    appbarBottomItemWrapperActive: {
        ...basic.appbarBottomItemWrapper,
        backgroundColor: colors.Foundation.Green.G55,
        borderTopColor: colors.Foundation.Green.G300,
    },

    mapToolbarItemWrapper: basic.mapToolbarItemWrapper,

    mapToolbarItemWrapperActive: {
        ...basic.mapToolbarItemWrapper,
        backgroundColor: colors.Foundation.Green.G55,
        borderLeftColor: colors.Foundation.Green.G300,
    },

    mapToolbarItemRightWrapper: basic.mapToolbarItemRightWrapper,

    mapToolbarItemRightWrapperActive: {
        ...basic.mapToolbarItemRightWrapper,
        backgroundColor: '#718B26',
        borderLeftColor: '#3A3A3A',
    },

    mapToolbarItemTopWrapper: basic.mapToolbarItemTopWrapper,

    mapToolbarItemTopWrapperActive: {
        ...basic.mapToolbarItemTopWrapper,
        backgroundColor: '#718B26',
        borderBottomColor: '#3A3A3A',
    },

    mapToolbarGroup: {
        marginBottom: metrics.tenHeight * 2,
    },

    mapBottomToolbarItemWrapper: {
        backgroundColor: colors.Foundation.Green.G400,
    },

    appbarBottomPTT: {
        ...basic.appbarBottom,
        backgroundColor: colors.Foundation.Green.G300,
    },

    appbarBottomCircleEnviar: {
        position: 'absolute',
        bottom: metrics.tenWidth * 1.7,
        right: metrics.tenWidth * 1.3,
    },

    appbarBottomCircleEnviarPTT: {
        position: 'absolute',
        bottom: metrics.tenWidth * 1.7,
        right: metrics.tenWidth * 0.5,
    },

    mapearButtonsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 0,
        paddingTop: 0,
        paddingBottom: metrics.tenWidth * 0.8,
        paddingRight: metrics.tenWidth * 0.8,
        paddingLeft: metrics.tenWidth * 0.8,
    },

    mapearButtonsContainerRow: {
        flexDirection: 'row',
        flexGrow: 1,
        justifyContent: 'space-between',
        paddingVertical: metrics.tenHeight * 0.8,
        paddingHorizontal: metrics.tenWidth * 2.4,
        marginBottom: 0,
        marginRight: metrics.tenWidth * 0.6,
    },

    mapearButtons: basic.mapearButtons,

    mapearButtonsLeft: {
        ...basic.mapearButtons,
        marginRight: metrics.tenWidth * 0.4,
    },

    mapearButtonsRight: {
        ...basic.mapearButtons,
        marginLeft: metrics.tenWidth * 0.4,
    },

    mapearButtonsMiddle: {
        ...basic.mapearButtons,
        marginLeft: metrics.tenWidth * 0.4,
        marginRight: metrics.tenWidth * 0.4,
    },

    mapearButtonsLabel: {
        marginLeft: metrics.tenWidth * 3,
        alignSelf: 'center',
        fontSize: metrics.tenWidth * 1.6,
        color: colors.Foundation.TextColor.White,
        textTransform: 'none',
    },

    avatarButtonCancel: {
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
        backgroundColor: colors.background,
    },

    avatarButtonExit: {
        color: colors.Foundation.CorDeTexto.CorDeTexto,
        backgroundColor: colors.Foundation.Brown.B300,
    },

    avatarButtonClean: {
        color: colors.Foundation.CorDeTexto.CorDeTexto,
        backgroundColor: colors.Foundation.Brown.B300,
    },

    avatarButtonDone: {
        color: colors.Foundation.Green.G300,
    },

    avatarButtonAtivo: {
        color: colors.Foundation.Green.G300,
        backgroundColor: colors.background,
    },

    avatarButtonModoInativo: {
        color: colors.Foundation.TextColor.White,
        backgroundColor: colors.Foundation.CorDeTexto.CorDeTexto2,
    },

    avatarButtonModoAtivo: {
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
        backgroundColor: colors.Foundation.Basic.White,
    },

    mapearLogoImage: {
        alignSelf: 'center',
        backgroundColor: '#fff',
        borderRadius: 50,
        padding: metrics.tenWidth * 1,
        marginRight: metrics.tenWidth * 1,
        width: metrics.tenWidth * 5,
        height: metrics.tenWidth * 5,
    },

    mapearLogoImageBottom: {
        position: 'absolute',
        bottom: metrics.tenHeight * 9,
        left: metrics.tenWidth * 1,
    },

    mapearLogoImageBottomPTT: {
        position: 'absolute',
        bottom: metrics.tenHeight * 11,
        left: metrics.tenWidth * 1,
    },

    syncStatusIcon: {
        position: 'absolute',
        top: 20,
        right: 20,
        backgroundColor: '#fff',
        borderRadius: 50,
        padding: 10,
        width: metrics.tenWidth * 8,
        height: metrics.tenWidth * 8,
    },

    mapearStatusButton: {},

    mapearStatusButtonClaro: {
        backgroundColor: colors.Foundation.Basic.White,
    },

    mapearStatusButtonLeft: {},

    mapearStatusButtonLeftClaro: {
        backgroundColor: colors.Foundation.Basic.White,
    },
});

export default LayoutMapa;
