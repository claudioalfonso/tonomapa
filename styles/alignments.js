import { StyleSheet } from 'react-native';

const Alignments = StyleSheet.create({
    defaultMargin: {
        marginLeft: 8,
        marginRight: 8,
    },

    smSpaceTop: {
        marginTop: 4,
    },

    mdSpaceTop: {
        marginTop: 8,
    },

    lgSpaceTop: {
        marginTop: 16,
    },

    smSpaceBottom: {
        marginBottom: 4,
    },

    mdSpaceBottom: {
        marginBottom: 8,
    },

    lgSpaceBottom: {
        marginBottom: 16,
    },
});

export default Alignments;
