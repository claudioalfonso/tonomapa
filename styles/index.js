import Inputs from './inputs';
import Alignments from './alignments';
import Messages from './messages';
import Layout from './layout';
import LayoutMapa from './layoutMapa';
import Anexos from './anexos';
import SlideStyles from './slides';

export { Inputs, Alignments, Messages, Layout, LayoutMapa, Anexos, SlideStyles };
