import metrics from '../utils/metrics';
export const pickerStyles = {
    inputIOS: {
        fontSize: metrics.tenWidth * 1.6,
        paddingHorizontal: metrics.tenWidth * 1,
        paddingVertical: metrics.tenHeight * 0.8,
        color: 'black',
        paddingRight: metrics.tenWidth * 3, // to ensure the text is never behind the icon
        marginTop: metrics.tenHeight * 0.4,
        marginLeft: metrics.tenWidth * 0.8,
    },
    inputAndroid: {
        fontSize: metrics.tenWidth * 1.6,
        paddingHorizontal: metrics.tenWidth * 1,
        paddingVertical: metrics.tenHeight * 0.8,
        color: 'black',
        paddingRight: metrics.tenWidth * 3, // to ensure the text is never behind the icon
        marginTop: metrics.tenHeight * 0.4,
        marginLeft: metrics.tenWidth * 0.8,
    },
};
