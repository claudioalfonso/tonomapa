import colors from '../assets/colors';
import metrics from '../utils/metrics';
import { StyleSheet } from 'react-native';

const basic = StyleSheet.create({
    textInputCompact: {
        backgroundColor: colors.background,
        width: '100%',
    },
    textInput: {
        backgroundColor: colors.background,
        width: '100%',
        marginBottom: 0,
        marginTop: metrics.tenHeight * 3,
    },
});

const Inputs = StyleSheet.create({
    textInputCompact: basic.textInputCompact,

    textInput: basic.textInput,

    textInputLabel: {
        fontSize: metrics.tenWidth * 1.6,
    },

    textAreaLabel: {
        fontSize: metrics.tenWidth * 1.6,
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
    },

    textInputLogin: {
        ...basic.textInputCompact,
        marginTop: metrics.tenHeight * 1,
        fontSize: metrics.tenWidth * 1.8,
    },
    // textInputLogin: {
    //     ...basic.textInputCompact,
    //     marginTop: metrics.tenHeight * 1,
    //     fontSize: metrics.tenWidth * 1.8,
    //     backgroundColor: colors.Foundation.Green.G300,
    // },

    textInputLoginActive: {
        ...basic.textInput,
        backgroundColor: colors.Foundation.Green.G300,
    },

    caption: {
        fontSize: metrics.tenWidth * 1.8,
        color: colors.Foundation.CorDeTexto.CorDeTexto2,
        marginTop: metrics.tenHeight * 3,
    },

    selectLabel: {
        marginBottom: 10,
    },

    checkBoxes: {
        flexDirection: 'row',
        alignItems: 'center',
    },
});

export default Inputs;
