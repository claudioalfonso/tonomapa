import colors from '../assets/colors';
import metrics from '../utils/metrics';
import { StyleSheet } from 'react-native';

const SlideStyles = StyleSheet.create({
    wrapper: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    content: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'blue',
    },
    image: {},
    text: {
        color: colors.Foundation.TextColor.White,
        fontSize: metrics.tenWidth * 1.8,
        textAlign: 'left',
    },
    title: {
        fontSize: metrics.tenWidth * 2.2,
        color: 'white',
        textAlign: 'center',
        marginBottom: metrics.tenWidth * 2,
    },
});

export default SlideStyles;
