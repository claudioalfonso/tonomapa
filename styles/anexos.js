import { StyleSheet } from 'react-native';

import colors from '../assets/colors';
import Layout from './layout';
import metrics from '../utils/metrics';

const Anexos = StyleSheet.create({
    flatList: {
        padding: 0,
    },

    itemSeparatorComponent: {
        borderColor: colors.warning,
        borderBottomWidth: 1,
        marginBottom: 20,
    },

    item: {
        paddingTop: 0,
        paddingBottom: 20,
        paddingLeft: 16,
        paddingRight: 16,
        marginHorizontal: 8,
    },

    itemTitle: {
        ...Layout.bold,
        ...Layout.textParagraph,
        marginBottom: 0,
    },

    itemDescricao: {
        ...Layout.italic,
    },

    itemTamanho: {
        fontSize: metrics.tenWidth * 1.4,
        marginBottom: 0,
    },

    itemNaoEnviado: {
        fontSize: metrics.tenWidth * 1.2,
        color: '#080',
    },

    itemImageWrapper: {
        height: 130,
        overflow: 'hidden',
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'stretch',
        marginBottom: metrics.tenHeight * 0.4,
        marginTop: metrics.tenHeight * 2,
    },

    itemWithoutImageWrapper: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },

    itemWithoutImageDetails: {
        flex: 1,
    },

    itemIconWrapper: {
        marginRight: 20,
    },

    formIconWrapper: {
        flex: 1,
        alignItems: 'center',
        marginTop: metrics.tenHeight * 2,
        paddingHorizontal: metrics.tenHeight * 2,
        paddingVertical: metrics.tenHeight * 3,
        backgroundColor: colors.Foundation.Basic.Disabled,
    },

    itemImage: {
        flex: 1,
    },

    itemActions: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginTop: 16,
    },

    introTextWrapper: {
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 8,
        paddingRight: 8,
    },

    introText: {
        ...Layout.textParagraph,
        ...Layout.italic,
        marginBottom: 0,
    },

    fab: {
        position: 'absolute',
        margin: metrics.tenHeight * 1.6,
        right: 0,
        bottom: metrics.tenHeight * 8.5,
        backgroundColor: colors.Foundation.Brown.B400,
    },

    playIcon: {
        position: 'absolute',
        right: 5,
        top: 0,
    },
});

export default Anexos;
