## Development

This project is using the *Expo Managed Workflow*.

You can follow the instructions on [https://expo.io/learn](expo installation).

1. Install node.js (you can use nvm to manage node versions)
2. install expo globally:
  ```console
  $ npm install expo-cli --global
  ```
3. Download project
  ```console
  $ git clone https://gitlab.com/eita/tonomapa/tonomapa.git
  $ cd tonomapa 
  ```
4. Create local configuration file - you can replace the values for your installation or [https://docs.expo.io/versions/v39.0.0/config/app/](make other changes).
  ```console
  $ cp app.config_example.js app.config.js
  $ vim app.config.js
  ```
5. Run
 ```console
 $ expo start
 ```

### Important reference (push notification and others):

https://docs.expo.io/guides/

