import React from 'react';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';

import colors from './assets/colors';

import ToNoMapaProvider from './context/ToNoMapaProvider';
import ToNoMapaNavigation from './components/ToNoMapaNavigation';

const theme = {
    ...DefaultTheme,
    roundness: 6,
    colors: {
        ...DefaultTheme.colors,
        primary: colors.Foundation.Brown.B300,
        accent: colors.Foundation.Green.G300,
        text: colors.Foundation.CorDeTexto.CorDeTexto2,
        ...colors,
    },
};
export default function App() {
    return (
        <PaperProvider theme={theme}>
            <ToNoMapaProvider>
                <ToNoMapaNavigation />
            </ToNoMapaProvider>
        </PaperProvider>
    );
}
