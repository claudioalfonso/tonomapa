export default ({ config }) => {
    const version = {
        versionName: '3.05',
        build: 305,
    };
    config.extra = {
        eas: {
            projectId: '279ef150-89c7-4e61-bbba-19b778b4b249',
        },
        dataDaVersao: '24/06/2023',
        testing: false,
        debugGraphQL: false,
        isPTTDisabled: false,
        dashboardEndpoint: process.env.dashboardEndpoint,
        // dashboardEndpoint: process.env.dashboardHomologEndpoint,
        maxFileSize: 12 * 1024 * 1024,
        SENTRY_DSN: process.env.SENTRY_DSN,
        termosDeUsoUri: 'http://tonomapa.eita.org.br/termosdeuso',
        TICCAsUri: 'http://tonomapa.eita.org.br/ticcas',
        xyzRTiles: {
            using: false,
        },
        pollingRate: 5 * 60 * 1000,
        buildNumber: version.build,
    };
    config.version = version.versionName;
    config.ios.buildNumber = version.versionName;
    config.ios.googleServicesFile = process.env.iosGoogleServicesFile;
    config.ios.config.googleMapsApiKey = process.env.iosGoogleMapsApiKey;

    config.android.versionCode = version.build;
    config.android.googleServicesFile = process.env.googleServicesFile;
    config.android.config.googleMaps.apiKey = process.env.androidGoogleMapsApiKey;

    config.hooks.postPublish = [
        {
            file: 'sentry-expo/upload-sourcemaps',
            config: {
                organization: 'cooperativa-eita',
                project: 'tonomapa',
                authToken: process.env.SENTRY_AUTH_TOKEN,
            },
        },
    ];

    return {
        ...config,
    };
};
