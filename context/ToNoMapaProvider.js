import React, {
    createContext,
    useContext,
    useEffect,
    useMemo,
    useReducer,
    useRef,
    useState,
} from 'react';
import NetInfo from '@react-native-community/netinfo';
import { Alert, Dimensions, Platform } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Notifications from 'expo-notifications';
import * as Device from 'expo-device';
import Constants from 'expo-constants';
import { ApolloClient, ApolloLink, ApolloProvider } from '@apollo/client';
import { AsyncStorageWrapper, persistCache } from 'apollo3-cache-persist';
import * as Sentry from 'sentry-expo';

import {
    sair,
    serverLogin,
    setCachedToken,
    LOGIN_ERROR,
    tonomapaCache,
    executeTrackedMutations,
    authStateReducer,
    populateCachedData,
} from '../db/api';
import { GET_USER_DATA } from '../db/queries';
import { log, logError } from '../utils/utils';
import {
    errorLink,
    httpLink,
    logLink,
    middlewareLink,
    queueLink,
    retryLink,
    serializingLink,
    trackMutationsLink,
} from '../utils/apolloClientLinks';
import { createAnexosDir, limpaTodosAnexos } from '../db/arquivos';
import { getSyncStatus } from '../bases/status';

import ToNoMapaLoading from '../components/ToNoMapaLoading';

Notifications.setNotificationHandler({
    handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: false,
        shouldSetBadge: false,
    }),
});

/**
 * Contexto para login/logout/signin/signout
 * @type {React.Context}
 */
const ToNoMapaContext = createContext({});

export default function ToNoMapaProvider({ children }) {
    const [client, setClient] = useState(null);
    const [notification, setNotification] = useState(null);
    const [online, setOnline] = useState(false);
    const [isSyncing, setIsSyncing] = useState(false);
    const [showEnviarDadosDialogo, setShowEnviarDadosDialogo] = useState(false);
    const alertSyncStatus = () => {
        Alert.alert(
            syncStatus.title,
            syncStatus.body,
            [
                {
                    text: 'Entendi',
                    onPress: () => {},
                },
            ],
            { cancelable: true }
        );
        return;
    };

    const syncStatus = getSyncStatus(online, isSyncing, alertSyncStatus);

    const initialAuthState = { isLoading: true, isSignout: false, userToken: null };

    /**
     * @type object
     */
    const [state, dispatch] = useReducer(authStateReducer, initialAuthState);

    const notificationSubscription = useRef(null);

    const pushToken = useRef(null);

    const windowWidth = Dimensions.get('window').width;
    const windowHeight = Dimensions.get('window').height;
    const windowCenter = { x: windowWidth / 2, y: windowHeight / 2 };

    const handleNotification = (newNotification) => {
        // Vibration.vibrate();
        /*DEBUG*/ //console.log('handleNotification');
        /*DEBUG*/ //console.log(notification.request.content.data);
        if (Platform.OS === 'android') {
            Notifications.dismissAllNotificationsAsync();
        } else {
            Notifications.setBadgeCountAsync(0);
        }
        setNotification(newNotification);
    };

    const initNotification = async () => {
        try {
            const { status: existingStatus } = await Notifications.getPermissionsAsync();
            let finalStatus = existingStatus;
            if (existingStatus !== 'granted') {
                const { status } = await Notifications.requestPermissionsAsync();
                finalStatus = status;
            }
            if (finalStatus !== 'granted') {
                log('Não há permissões para receber push notifications');
                return;
            }

            if (Device.isDevice) {
                const newPushToken = (await Notifications.getExpoPushTokenAsync()).data;
                await AsyncStorage.setItem('pushToken', newPushToken);
                /*DEBUG*/ //log('Device token: + newPushToken);

                if (Platform.OS === 'android') {
                    Notifications.setNotificationChannelAsync('default', {
                        name: 'default',
                        importance: Notifications.AndroidImportance.MAX,
                    });
                }
                return newPushToken;
            }
            log('Erro ao tentar pegar o pushToken. Tem que ser um dispositivo físico!');
            return null;
        } catch (e) {
            logError(e);
            const newPushToken = await AsyncStorage.getItem('pushToken');
            log('error in initNotification. Got from AsyncStorage: ' + newPushToken);
            return newPushToken;
        }
    };

    useEffect(() => {
        const initAsync = async () => {
            await persistCache({
                cache: tonomapaCache,
                storage: new AsyncStorageWrapper(AsyncStorage),
                // trigger: 'background',
            });
            const newClient = new ApolloClient({
                link: ApolloLink.from([
                    logLink,
                    middlewareLink,
                    errorLink,
                    serializingLink,
                    trackMutationsLink,
                    queueLink,
                    retryLink,
                    httpLink,
                ]),
                cache: tonomapaCache,
                connectToDevTools: true,
            });

            // When app is closed and launches again, and there are queued mutations,
            // all optimisticResponses are lost. There is a need to repopulate lists
            // from more reliable currentUser cache instance:
            const data = tonomapaCache.readQuery({ query: GET_USER_DATA });
            if (data) {
                populateCachedData(data, true);
            }

            await createAnexosDir();

            try {
                const userToken = await AsyncStorage.getItem('token');
                setClient(newClient);
                dispatch({ type: 'RESTORE_TOKEN', token: userToken });
            } catch (e) {
                // console.log('Restoring token failed', e);
                log('Restoring token failed');
                logError(e);
            }
        };

        initAsync();

        if (Constants.manifest.extra.sentryDns) {
            Sentry.init({
                dsn: Constants.manifest.extra.sentryDns,
                enableInExpoDevelopment: true,
                debug: false,
                release: Constants.manifest.version,
            });
        }
        const unsubscribeNetInfo = NetInfo.addEventListener((netInfoState) => {
            setOnline(netInfoState.isConnected);
        });

        return () => {
            unsubscribeNetInfo();
        };
    }, []);

    useEffect(() => {
        if (online) {
            queueLink.open();
            if (client) {
                setIsSyncing(true);
                executeTrackedMutations(client).then(() => {
                    setIsSyncing(false);
                });
            }
            if (!pushToken.current) {
                initNotification().then((newPushToken) => {
                    if (newPushToken) {
                        notificationSubscription.current = {
                            foregrounded:
                                Notifications.addNotificationReceivedListener(handleNotification),
                            backgrounded: Notifications.addNotificationResponseReceivedListener(
                                (response) => {
                                    // console.log('backgrounded fired', response);
                                }
                            ),
                        };
                        pushToken.current = newPushToken;
                    }
                });
            }
        } else {
            queueLink.close();
        }
        return () => {
            if (notificationSubscription.current) {
                Notifications.removeNotificationSubscription(
                    notificationSubscription.current.foregrounded
                );
                Notifications.removeNotificationSubscription(
                    notificationSubscription.current.backgrounded
                );
            }
        };
    }, [online, client]);

    const toNoMapaContext = useMemo(
        () => ({
            signIn: async (data) => {
                dispatch({ type: 'IS_SIGNING_IN' });
                /*DEBUG*/ //console.log("signIn", data);

                const onLoginSuccess = async (userToken) => {
                    /*DEBUG*/ //console.log('onLoginSuccess', userToken);
                    await AsyncStorage.setItem('token', userToken);
                    // await AsyncStorage.setItem(
                    //     'version',
                    //     Constants.manifest.extra.buildNumber.toString()
                    // );
                    await limpaTodosAnexos();

                    try {
                        dispatch({ type: 'SIGN_IN', token: userToken });
                    } catch (error) {
                        // console.log('LOGIN: error in getting user info', error);
                        log('LOGIN: error in getting user info');
                        logError(error);
                    }
                };

                const onLoginError = (errorCode) => {
                    let actions, title, message;
                    log('onLoginError. ErrorCode: ' + errorCode);
                    switch (errorCode) {
                        case LOGIN_ERROR.NO_USER_AND_NO_PASSWORD:
                            title = 'Telefone em branco';
                            message =
                                'Você precisa fornecer o seu telefone para entrar ou se cadastrar';
                            actions = [
                                {
                                    text: 'OK',
                                    onPress: () => {
                                        data.navigation.navigate('Login', {
                                            username: data.username,
                                            password: data.password,
                                        });
                                    },
                                },
                            ];
                            break;
                        case LOGIN_ERROR.NO_PASSWORD:
                            title = 'Cadastre-se no Tô no mapa';
                            message =
                                'Se você nunca usou o app Tô no mapa, cadastre-se! Deseja se cadastrar?';
                            actions = [
                                {
                                    text: 'Não',
                                    onPress: () => {
                                        data.navigation.navigate('Login', {
                                            username: data.username,
                                            password: data.password,
                                        });
                                    },
                                },
                                {
                                    text: 'Sim, quero me cadastrar',
                                    onPress: () => {
                                        data.navigation.navigate('Cadastro', {
                                            username: data.username,
                                        });
                                    },
                                },
                            ];
                            break;
                        case LOGIN_ERROR.USER_NOT_FOUND:
                            title = 'Telefone ou código não encontrado';
                            message =
                                'Não foi possível entrar com este telefone e código de segurança. Se você não tem o código de segurança, cadastre-se! Se tem o código, por favor tente novamente';
                            actions = [
                                {
                                    text: 'Quero tentar de novo',
                                    onPress: () => {
                                        data.navigation.navigate('Login', {
                                            username: data.username,
                                            password: data.password,
                                        });
                                    },
                                },
                                {
                                    text: 'Sim, quero me cadastrar',
                                    onPress: () => {
                                        data.navigation.navigate('Cadastro', {
                                            username: data.username,
                                        });
                                    },
                                },
                            ];
                            break;
                        case LOGIN_ERROR.MISFORMATTED_USER:
                            title = 'Número de telefone mal formatado';
                            message = 'Você digitou o número corretamente? Por favor verifique.';
                            actions = [
                                {
                                    text: 'OK',
                                    onPress: () => {
                                        data.navigation.navigate('Login', {
                                            username: data.username,
                                            password: data.password,
                                        });
                                    },
                                },
                            ];
                            break;
                        case LOGIN_ERROR.NETWORK:
                            title = 'Erro de rede';
                            message =
                                'Não foi possível conectar. Verifique sua conexão com a Internet. Caso ela esteja boa, pode estar acontecendo algum problema em nossos servidores; neste caso, por favor tente novamente mais tarde.';
                            actions = [
                                {
                                    text: 'OK',
                                    onPress: () => {
                                        data.navigation.navigate('Login', {
                                            username: data.username,
                                            password: data.password,
                                        });
                                    },
                                },
                            ];
                            break;
                    }
                    dispatch({ type: 'SIGN_IN_ERROR' });
                    Alert.alert(title, message, actions, { cancelable: false });
                };
                // Login on server. Network NEEDED!
                serverLogin(data.username, data.password, onLoginSuccess, onLoginError);
            },
            signOut: () => {
                sair(client);
                dispatch({ type: 'SIGN_OUT' });
            },
            signUp: async (userToken) => {
                /*DEBUG*/ //console.log("signUp");
                await AsyncStorage.setItem('token', userToken);
                setCachedToken(userToken);
                dispatch({ type: 'SIGN_IN', token: userToken });
            },
            client,
            notificationHandler: [notification, setNotification],
            enviarDadosDialogoHandler: [showEnviarDadosDialogo, setShowEnviarDadosDialogo],
            isLoading: state.isLoading,
            userToken: state.userToken,
            online,
            isSyncing,
            syncStatus,
            windowCenter,
        }),
        [online, syncStatus, client, notification, state]
    );

    if (!client) {
        return <ToNoMapaLoading />;
    }

    return (
        <ToNoMapaContext.Provider value={toNoMapaContext}>
            <ApolloProvider client={client}>{children}</ApolloProvider>
        </ToNoMapaContext.Provider>
    );
}

export function useToNoMapa() {
    return useContext(ToNoMapaContext);
}
